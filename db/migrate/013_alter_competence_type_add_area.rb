# -*- encoding : utf-8 -*-
migration 13, :alter_competence_type_add_area do

  up do
    #agregado con condicion de que no exista 
    if table_exists?(:competence_types)
      if !table_column_exists?( :competence_types, :area_id )
        modify_table :competence_types do
          add_column :area_id, Integer
        end
      end
    end

    #agregado con condicion de que no exista

     if table_exists?(:job_descriptions)
       if !table_column_exists?( :job_descriptions, :datos_empresa_anterior )
         modify_table :job_descriptions do
           add_column :datos_empresa_anterior, 'TEXT'
         end
       end
     end

  end

  down do
    if table_exists?(:competence_types)
      modify_table :competence_types do
        drop_column :area_id
      end
    end
    if table_exists?(:job_descriptions)
      modify_table :job_descriptions do
        drop_column :datos_empresa_anterior
      end
    end
  end

end
