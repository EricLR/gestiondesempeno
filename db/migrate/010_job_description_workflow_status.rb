# -*- encoding : utf-8 -*-
migration 10, :job_description_workflow_status do

  up do
    modify_table :job_descriptions do
      add_column :analista_id, 'INT(10)'
      add_column :status, 'INT(10) DEFAULT 0'
    end
  end

  down do
    modify_table :job_descriptions do
      drop_column :analista_id
      drop_column :status
    end
  end

end
