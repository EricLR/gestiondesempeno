migration 22, :tracing_competences_kpi_aprobe do


    up do
      if table_exists?(:tracing_kpis)
        modify_table :tracing_kpis do
          add_column :aprobe, Integer
          add_column :apelacion, 'Text'
        end
      end
      if table_exists?(:tracing_competences)
        modify_table :tracing_competences do
          add_column :aprobe, Integer
          add_column :apelacion, 'Text'
        end
      end
      if table_exists?(:evaluations)
          drop_table :evaluations
      end
      if !table_exists?(:evaluations)
        create_table :evaluations do
          column :id, 		          Integer, 	serial: true
          column :kpi_result,               Float
          column :kpi_ponderation,          Float
          column :kpi_final_result,         'TEXT'
          column :competence_result,        Float
          column :competence_ponderation,   Float
          column :competence_final_result,  'TEXT'
          column :kpi_competence_result,    Float
          column :boss_ponderation,         Float
          column :boss_result,              Float
          column :final_result,             'TEXT'
          column :evaluator_closed,         'TINYINT(1) DEFAULT 0'
          column :evaluated_closed,         'TINYINT(1) DEFAULT 0'
          column :evaluator_id,             Integer
          column :evaluated_id,             Integer
          column :period_id,             Integer
          column :kpi_aprobe,               Integer
          column :competence_aprobe,        Integer
          column :created_at,       DateTime
          column :updated_at,       DateTime
        end
      end
    end


    down do
    if table_exists?(:tracing_kpis)
      modify_table :tracing_kpis do
        drop_column :aprobe
        drop_column :apelacion
      end
    end
    if table_exists?(:tracing_competences)
      modify_table :tracing_competences do
        drop_column :aprobe
        drop_column :apelacion
      end
    end
    end

end
