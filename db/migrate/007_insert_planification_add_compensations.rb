# -*- encoding : utf-8 -*-
migration 7, :insert_planification_add_compensations do

  up do
  	modify_table :insert_planifications do
  		drop_foreign_key :user
  		drop_foreign_key :key_performance_indicator_id
  		drop_column :key_performance_indicator_id
  		
  		add_column :description, 	  'TEXT'
  		add_column :jobs, 		 	  'TEXT'
  		add_column :mod_dates,	 	  'TEXT'
  		add_column :competences_ids,  'TEXT'
  		add_column :kpi_ids, 		  'TEXT'
  	end
  end

  down do
  	modify_table :insert_planifications do
  		
  		add_column :key_performance_indicator_id, Integer, allow_nil: true
  		
  		drop_column :description
  		drop_column :jobs
  		drop_column :mod_dates
  		drop_column :competences_ids
  		drop_column :kpi_ids
  		
  	end
  end

end
