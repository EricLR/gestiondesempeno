# -*- encoding : utf-8 -*-
migration 5, :main_result_add_number do

  up do
  	modify_table :main_results do
  		add_column :number, Integer
  	end
  end

  down do
  	modify_table :main_results do
  		drop_column :number
  	end
  end

end
