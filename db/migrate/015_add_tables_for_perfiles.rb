# -*- encoding : utf-8 -*-
migration 15, :add_tables_for_perfiles do

  up do
    unless table_exists?(:areas)
      create_table :areas do
        column :id, Integer, serial: true
        column :name, String
      end
    end
    unless table_exists?(:empresa_tipos)
      create_table :empresa_tipos do
        column :id, Integer, serial: true
        column :name, String
      end
    end
    unless table_exists?(:sector_experiences)
      create_table :sector_experiences do
        column :id, Integer, serial: true
        column :name, String
      end
    end
    unless table_exists?(:sectors)
      create_table :sectors do
        column :id, Integer, serial: true
        column :name, String
      end
    end
    unless table_exists?(:tamano_empresas)
      create_table :tamano_empresas do
        column :id, Integer, serial: true
        column :name, String
      end
    end
  end

  down do
    drop_table :areas
    drop_table :empresa_tipos
    drop_table :sector_experiences
    drop_table :sectors
    drop_table :tamano_empresas
  end

end
