# -*- encoding : utf-8 -*-
migration 8, :kpi_and_conduct_add_detail do

  up do
  	modify_table :planification_competences do
  		add_column :detail, 'TEXT'
  	end
  	modify_table :key_performance_indicators do
  		add_column :detail, 'TEXT'
  	end
  	
  end

  down do
  	modify_table :planification_competences do
  		drop_column :detail, 'TEXT'
  	end
  	modify_table :key_performance_indicators do
  		drop_column :detail, 'TEXT'
  	end
  end

end
