migration 20, :detail_planifications do


    up do
      if table_exists?(:planifications)
        modify_table :planifications do
          add_column :detail, 'Text'
        end
      end
    end


    down do
      if !table_exists?(:planifications)
        modify_table :planifications do
          drop_column :detail
        end
      end
    end

end
