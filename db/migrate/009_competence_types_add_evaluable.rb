# -*- encoding : utf-8 -*-
migration 9, :competence_types_add_evaluable do

  up do
  	modify_table :competence_types do
  		add_column :evaluable, 'TINYINT(1) DEFAULT 1'
  	end
  end

  down do
  	modify_table :competence_types do
  		drop_column :evaluable
  	end
  end

end
