# -*- encoding : utf-8 -*-
migration 3, :datos_job_descriptions, repository: :rvelasco do

  up do
  	unless table_column_exists? :jobs, :job_description_id
  		modify_table :jobs do
  			add_column :job_description_id, Integer, allow_nil: true
  		end
  	end
  	
  	
  	
  end

  down do
  	if table_column_exists? :jobs, :job_description_id
  		modify_table :jobs do
  			drop_column :job_description_id
  		end
  	end

  end

end
