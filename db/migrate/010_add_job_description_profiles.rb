# -*- encoding : utf-8 -*-
migration 10, :add_job_description_profiles do

  up do
    modify_table :job_descriptions do
      add_column :anios_experiencia_laboral,   'TEXT'
      add_column :anios_experiencia_cargo,     'TEXT'
      add_column :estudio_basico,              'TEXT'
      add_column :anios_estudio,               'TEXT'
      add_column :conocimiento_organizacional, 'TEXT'
      add_column :habilidades_funcionales,     'TEXT'
      add_column :definicion,                  'TEXT'
      add_column :conductas,                   'TEXT'
      add_column :certificaciones,             'TEXT'
      add_column :seguridad,                   'TEXT'
      add_column :calidad,                     'TEXT'
      add_column :medio_ambiente,              'TEXT'
      add_column :caracteristicas_ambientales, 'TEXT'
      add_column :tipo_movilidad,              'TEXT'
      add_column :frecuencia,                  'TEXT'
      add_column :idiomas,                     'TEXT'
      add_column :nivel,                       'TEXT'
  	end
  end

  down do
    modify_table :job_descriptions do
      drop_column :anios_experiencia_laboral,   'TEXT'
      drop_column :anios_experiencia_cargo,     'TEXT'
      drop_column :estudio_basico,              'TEXT'
      drop_column :anios_estudio,               'TEXT'
      drop_column :conocimiento_organizacional, 'TEXT'
      drop_column :habilidades_funcionales,     'TEXT'
      drop_column :definicion,                  'TEXT'
      drop_column :conductas,                   'TEXT'
      drop_column :certificaciones,             'TEXT'
      drop_column :seguridad,                   'TEXT'
      drop_column :calidad,                     'TEXT'
      drop_column :medio_ambiente,              'TEXT'
      drop_column :caracteristicas_ambientales, 'TEXT'
      drop_column :tipo_movilidad,              'TEXT'
      drop_column :frecuencia,                  'TEXT'
      drop_column :idiomas,                     'TEXT'
      drop_column :nivel,                       'TEXT'
    end
  end

end
