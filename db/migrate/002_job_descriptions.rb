# -*- encoding : utf-8 -*-
migration 2, :job_descriptions do

  up do
  	unless table_exists? :job_descriptions
  		create_table :job_descriptions do
			column :id,               Integer, serial: true
			column :name,             String, :length => 255
			column :mission,          'TEXT'
			column :dicisions,        'TEXT'
			column :recomendations,   'TEXT'
			column :internal_impact,            'TEXT' # Inciden internamente para el logro de los resultados esperados del cargo
			column :external_impact,            'TEXT' # Inciden externamente para el logo de los resultados esperados del cargo
			column :main_challenge,             'TEXT' # El principal desafío del cargo es
			column :internal_contacts,          'TEXT' # El cargo requiere contactos internos con (quien y para qué)
			column :external_contacts,          'TEXT' # El cargo requiere contactos externos con (quién y para qué)
			column :aditional_responsability,   'TEXT' # El cargo adicionalmiente es responsabe de
			column :others,                     'TEXT' # Otros del cargo
			column :knowledge,                  'TEXT' # Conocimientos de
			column :experience,                 'TEXT' # Experiencia en
			column :habilidades,                'TEXT'
  		end
  	end
  	
  	unless table_column_exists? :main_results, :job_description_id
  		modify_table :main_results do
  			add_column :job_description_id, Integer, allow_nil: true
  			change_column :job_id, 'INT(10) NULL'
  		end
  	end
  	
  	unless table_column_exists? :dimensions, :job_description_id
  		modify_table :dimensions do
  			add_column :job_description_id, Integer, allow_nil: true
  			change_column :job_id, 'INT(10) NULL'
  		end
  	end
  	
  	
  end

  down do
  	if table_exists? :job_descriptions
  		drop_table :job_descriptions
  	end
  	
  	if table_column_exists? :main_results, :job_description_id
  		modify_table :main_results do
  			drop_column :job_description_id
  			change_column :job_id, 'INT(10) NOT NULL'
  		end
  	end
  	
  	if table_column_exists? :dimensions, :job_description_id
  		modify_table :dimensions do
  			drop_column :job_description_id
  			change_column :job_id, 'INT(10) NOT NULL'
  		end
  	end
  	
  	
  	
  end

end
