# -*- encoding : utf-8 -*-
migration 12, :mantenedor_perfil do

  up do

    unless table_exists?(:experiencia_laborals)
          create_table :experiencia_laborals do
            column :id, Integer, serial: true
            column :name, String
          end
      end
    unless table_exists?(:experiencia_en_cargos)
            create_table :experiencia_en_cargos do
              column :id, Integer, serial: true
              column :name, String
            end
        end
    
    unless table_exists?(:tipo_movilidads)
          create_table :tipo_movilidads do
            column :id, Integer, serial: true
            column :name, String
          end
      end
    unless table_exists?(:frecuencia)
          create_table :frecuencia do
            column :id, Integer, serial: true
            column :name, String
          end
      end
    unless table_exists?(:idiomas)
          create_table :idiomas do
            column :id, Integer, serial: true
            column :name, String
          end
      end

    unless table_exists?(:nivel_hablados)
          create_table :nivel_hablados do
            column :id, Integer, serial: true
            column :name, String
          end
      end
    unless table_exists?(:nivel_escritos)
          create_table :nivel_escritos do
            column :id, Integer, serial: true
            column :name, String
          end
      end

    unless table_exists?(:nivel_lecturas)
          create_table :nivel_lecturas do
            column :id, Integer, serial: true
            column :name, String
          end
      end
    unless table_exists?(:estudios)
          create_table :estudios do
            column :id, Integer, serial: true
            column :name, String
          end
     end
    unless table_exists?(:certificacions)
          create_table :certificacions do
            column :id, Integer, serial: true
            column :name, String
          end
      end
    unless table_exists?(:seguridads)
          create_table :seguridads do
            column :id, Integer, serial: true
            column :name, String
          end
      end
    unless table_exists?(:calidads)
          create_table :calidads do
            column :id, Integer, serial: true
            column :name, String
          end
      end
    unless table_exists?(:medio_ambientes)
          create_table :medio_ambientes do
            column :id, Integer, serial: true
            column :name, String
          end
      end
    unless table_exists?(:conocimiento_organizacionals)
          create_table :conocimiento_organizacionals do
            column :id, Integer, serial: true
            column :name, String
          end
      end

    unless table_exists?(:habilidades_funcionales)
          create_table :habilidades_funcionales do
            column :id, Integer, serial: true
            column :name, String
          end
      end
    unless table_exists?(:caracteristicas_ambientales_cargos)
          create_table :caracteristicas_ambientales_cargos do
            column :id, Integer, serial: true
            column :name, String
          end
      end



  end

  down do
    drop_table :experiencia_laborals
    drop_table :experiencia_en_cargos
    drop_table :competencia
    drop_table :conducta
    drop_table :tipo_movilidads
    drop_table :frecuencia
    drop_table :idiomas
    drop_table :nivel_hablados
    drop_table :nivel_escritos
    drop_table :nivel_lecturas
    drop_table :estudios
    drop_table :certificacions
    drop_table :seguridads
    drop_table :calidads
    drop_table :medio_ambientes
    drop_table :conocimiento_organizacionals
    drop_table :habilidades_funcionales
    drop_table :caracteristicas_ambientales_cargos
  end

end
