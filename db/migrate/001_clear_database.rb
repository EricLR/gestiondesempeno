# -*- encoding : utf-8 -*-
migration 1, :clear_database, repository: :default do
=begin
  up do
  	disable_foreign_key_checks
  	say "Hago un chequeo y eliminacion de tablas relacionadas a JOBS"
  	if table_exists? :accounts
  		say "Elimino accounts"
  		drop_table :accounts
  	end

  	if table_exists?( :job_competences ) && !table_column_exists?( :job_competences, :job_description_id )#### VER SI ES NECESARIO ELIMINAR, DA ERROR EN PRODUCCION SI NO EXISTE
  		say "Elimino job_competences"
  		# drop_table :job_competences
      modify_table :job_competences do
        # drop_foreign_key :job_id
        add_column :job_description_id, Integer
      end
  	end
  	if table_exists? :functional_bosses
  		say "Elimino functional_bosses"
  		drop_table :functional_bosses
  	end



  	if table_exists?( :positions ) && table_column_exists?(:positions, :job_id)
  		say "Elimino positions"
  		modify_table :positions do
        if foreign_key_exists? :job_id
          drop_foreign_key :job_id
        end
  			drop_column :job_id
  		end
  		drop_table :positions
  	end
  	say "Checkeo si JOBS existe en Base de datos incorrecta"
  	if table_exists? :jobs
  		drop_table(:jobs)
  		say "Existe y elimino JOBS"
  	end

  	if table_exists? :profiles
  		say "Elimino profiles"
  		drop_table :profiles
  	end
  	if table_exists? :companies
  		say "Elimino companies"
  		drop_table :companies
  	end
  	if table_exists? :roles
  		say "Elimino roles"
  		drop_table :roles
  	end
  	if table_exists? :users
  		say "Elimino users"
  		drop_table :users
  	end
  	if table_exists? :user_configs
  		say "Elimino user_configs"
  		drop_table :user_configs
  	end

  	enable_foreign_key_checks

  end
=end
  down do
  end
end
