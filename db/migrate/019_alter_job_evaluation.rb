# -*- encoding : utf-8 -*-
migration 19, :alter_job_evaluation do
  up do
    if table_exists?(:job_evaluations)
      modify_table :job_evaluations do
        add_column :errores, 'Text'
        add_column :comentario, 'Text'
      end
    end
  end

  down do
    if table_exists?(:job_evaluations)
      modify_table :job_evaluations do
        drop_column :errores
        drop_column :comentario
      end
    end
  end
end
