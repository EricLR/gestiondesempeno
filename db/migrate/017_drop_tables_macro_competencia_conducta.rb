migration 17, :drop_tables_macro_competencia_conducta do


    up do
      drop_table :macro_competencia
      drop_table :competencia
      drop_table :conducta

    end


    down do

        unless table_exists?(:macro_competencia)
          create_table :macro_competencia do
            column :id, Integer, serial: true
            column :name, String
            column :area_id, Integer
          end
        end

        unless table_exists?(:competencia)

            create_table :competencia do
              column :id, Integer, serial: true
              column :name, String
              column :macro_competencia_id, Integer
            end
        end


        unless table_exists?(:conducta)

          create_table :conducta do
            column :id, Integer, serial: true
            column :name, String
            column :competencia_id, Integer
          end
        end


    end

end
