# -*- encoding : utf-8 -*-
migration 11, :create_comment do

  up do
    create_table :comments do
      column :id, 		          Integer, 	serial: true
  		column :detalle, 		     'TEXT'
      column :extra, 		       'TEXT'
      column :author_id,       'INT(10)'
      column :commentable_id,  'INT(10)'
      column :commentable_type, String
      column :created_at,       DateTime
      column :updated_at,       DateTime
    end
  end

  down do
    drop_table :comments
  end

end
