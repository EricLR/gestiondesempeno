# -*- encoding : utf-8 -*-
migration 18, :alter_job_evaluation_data do
  up do
    if table_exists?(:job_evaluation_data)
      modify_table :job_evaluation_data do
        add_column :management_type_id, Integer
      end
    end
  end

  down do
    if table_exists?(:job_evaluation_data)
      modify_table :job_evaluation_data do
        drop_column :management_type_id
      end
    end
  end
end
