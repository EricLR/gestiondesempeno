migration 21, :tracing_kpi_comment do


    up do
      if table_exists?(:tracing_kpis)
        modify_table :tracing_kpis do
          add_column :comment, 'Text'
        end
      end
    end


    down do
      if table_exists?(:tracing_kpis)
        modify_table :tracing_kpis do
          drop_column :comment
        end
      end

    end

end
