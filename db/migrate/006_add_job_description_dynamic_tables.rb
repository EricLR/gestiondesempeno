# -*- encoding : utf-8 -*-
migration 6, :add_job_description_dynamic_tables do

  up do
  	create_table :jd_tables do
  		column :id, 		Integer, 	serial: true
  		column :name, 		String, 	length: 255
  		column :position, 	Integer, 	default: 1
  	end unless table_exists?(:jd_tables)

    unless table_exists?(:jd_rows)

  	  create_table :jd_rows do
  		  column :id, 		Integer, 	serial: true
    		column :name, 		String, 	length: 255
    		column :position, 	Integer, 	default: 1
  	  	column :jd_table_id,'BIGINT(20) unsigned NOT NULL'
    	end
    	modify_table :jd_rows do
  	  	add_foreign_key :jd_table_id, :jd_tables
    	end
    end

    unless table_exists?(:jd_columns)
      create_table :jd_columns do
        column :id, 		Integer, 	serial: true
        column :name, 		String, 	length: 255
        column :position, 	Integer, 	default: 1
        column :jd_table_id,'BIGINT(20) unsigned NOT NULL'
      end

      modify_table :jd_columns do
        add_foreign_key :jd_table_id, :jd_tables
      end
    end

  	unless table_exists?(:jd_table_data)
      create_table :jd_table_data do
        column :id, 			            Integer, 	serial: true
        column :value, 			          'TEXT'
        column :jd_table_id,	'BIGINT(20) unsigned NOT NULL'
        column :jd_row_id,		'BIGINT(20) unsigned NOT NULL'
        column :jd_column_id,	'BIGINT(20) unsigned NOT NULL'
        column :job_description_id, 'BIGINT(20) unsigned NOT NULL'
      end

      modify_table :jd_table_data do
        add_foreign_key :jd_table_id, 	:jd_tables
        add_foreign_key :jd_row_id, 	:jd_rows
        add_foreign_key :jd_column_id, 	:jd_columns
        add_foreign_key :job_description_id,  :job_descriptions
      end
    end
  end

  down do
  	disable_foreign_key_checks
    drop_table :jd_table_data
    drop_table :jd_rows
    drop_table :jd_columns
    drop_table :jd_tables
    enable_foreign_key_checks
  end

end
