# -*- encoding : utf-8 -*-
migration 4, :role_add_permissions, repository: :rvelasco do

  up do
  	modify_table :roles do
  		add_column :permissions, 'VARCHAR(1000)', default: ''
  	end
  end

  down do
  	modify_table :roles do
  		drop_column :permissions
  	end
  end

end
