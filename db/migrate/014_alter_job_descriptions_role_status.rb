# -*- encoding : utf-8 -*-
migration 14, :alter_job_descriptions_role_status do

  up do
    if table_exists?(:job_descriptions)
      modify_table :job_descriptions do
        add_column :role_status, Integer
      end
    end
  end

  down do
    if table_exists?(:job_descriptions)
      modify_table :job_descriptions do
        drop_column :role_status
      end
    end
  end

end
