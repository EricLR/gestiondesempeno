# -*- encoding : utf-8 -*-
i = JobEvaluationDataDescription.create :name => 'LIMITADA:  Instrucciones básicas y rutinas de trabajo simples para llevar a cabo tareas manuales. El trabajo de este tipo es extremadamente simple, tiene un ciclo natural corto y suele involucrar un esfuerzo manual.  Los ocupantes de estos puestos deben responder a algunas órdenes simples.  En general, la competencia en el puesto se logra en pocos días.'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'L-'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'L'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'L+'

i = JobEvaluationDataDescription.create :name => 'BÁSICA: Instrucción primaria con alguna práctica de trabajo y/ o instrucción laboral para el desempeño de tareas. Estos puestos requieren habilidad para entender instrucciones orales o escritas y para desempeñar tareas simples.  Sus ocupantes suelen necesitar destrezas básicas de aritmética, lectura y escritura.  El trabajo es simple y repetitivo por naturaleza, y se llega a ser competente en el puesto en aproximadamente un mes.  Los trabajos que no requieren una habilidad de lectura o escritura se evaluarán debajo de “A”. operativas o de oficina muy básicas que pueden involucrar el uso de equipos o materiales comunes y muy simples para un único propósito.'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'A-'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'A'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'A+'

i = JobEvaluationDataDescription.create :name => 'TÉCNICA ELEMENTAL: Conocimiento o familiaridad con un trabajo rutinario estandarizado y / o el uso de máquinas o equipos simples. Estos puestos desempeñan asignaciones de trabajos simples, a veces altamente repetitivas en su naturaleza.  Dentro del universo de trabajos de oficina, en estos puestos se necesita el conocimiento de equipos (copiadoras, equipos de entrada con teclado), así como la habilidad para hacer ajustes menores.  En un lugar productivo (fábrica), estos puestos pueden operar la maquinaria de producción o equipos de manejo de materiales.  La competencia en el trabajo se suele producir entre los 3 y 6 meses.'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'B-'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'B'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'B+'

i = JobEvaluationDataDescription.create :name => 'TÉCNICA: Conocimiento de procedimientos y sistemas de trabajo que podrá incluir destreza o manipulación de un equipamiento especializado. Estos puestos requieren un conocimiento general de las rutinas y procedimientos del trabajo general de oficina.  Puede requerir el uso de destrezas tales como taquigrafía, contabilidad u operación de equipos especializados de complejidad moderada.  La competencia en el puesto se suele producir de los 6 meses o dos años.'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'C-'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'C'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'C+'

i = JobEvaluationDataDescription.create :name => 'TÉCNICA AVANZADA: Algunas habilidades especializadas (generalmente no teóricas), adquiridas dentro o fuera del puesto, que dan amplitud o profundidad adicional a una función específica. Estos puestos requieren destrezas adquiridas a través de la acumulación de experiencia de trabajo o de entrenamiento vocacional prolongado, relacionado con el trabajo especializado, similar al ejercicio o práctica como ayudante, asistente o practicante o una serie de cursos relacionados con el trabajo que se dan en un programa de título asociado'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'D-'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'D'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'D+'

i = JobEvaluationDataDescription.create :name => 'ESPECIALIZACIÓN: Suficiencia en una disciplina especializada que requiere la comprensión de teorías y principios científicos, o de prácticas y antecedentes complejos o de ambos. Estos puestos generalmente requieren un conocimiento de base y la aplicación de una disciplina teórica o científica, incluyendo los principios subyacentes en contraposición con las prácticas (por ejemplo:  principios de contabilidad en contraposición con las prácticas de contabilidad), requieren también trabajar con niveles equivalentes de dificultad / abstracción en disciplinas especializadas / técnicas.'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'E-'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'E'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'E+'

i = JobEvaluationDataDescription.create :name => 'ESPECIALIZACIÓN MADURA: Competencia obtenida a través de una maduración profesional lograda por una vasta experiencia que combina una amplia comprensión de prácticas y precedentes involucrados, o de teorías y principios científicos o de ambos. Estos puestos requieren profesionales profundamente “experimentados” cuyas destrezas profesionales de base y conocimientos adquiridos en su trabajo de nivel “E” hayan sido suplementados a través de una experiencia sustancial y considerable de trabajo en un campo de especialización.'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'F-'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'F'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'F+'

i = JobEvaluationDataDescription.create :name => 'MAESTRÍA ESPECIALIZADA: Profundo conocimiento de teorías y principios científicos o amplia experiencia multidisciplinaria en la gestión de negocios o ambos (dominio completo). Los puestos en este nivel requieren un conocimiento técnico/profesional profundo en un campo especializado, (química, ingeniería, contabilidad, mercadeo, etc.) o un amplio conocimiento generalizado acerca de la Organización (corporación) que generalmente se asocia con los puestos de la Gerencia Superior.  Los especialistas “profundos” suelen utilizar el dominio de su función o disciplina para “escribir los libros de la Compañía” sobre el tema, como expertos en la materia, o autoridades de la compañía en el campo especializado.'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'G-'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'G'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'G+'

i = JobEvaluationDataDescription.create :name => 'MAESTRÍA PROFESIONAL: Profundidad excepcional de conocimientos en una disciplina científica a través de un desarrollo especial. Reconocimiento externo en un campo complejo, generalmente científico (dominio excepcional). Estos puestos tienen una especialización más profunda que los puestos “G” y su experiencia y conocimiento con autoridad se reconoce fuera de los límites de la Organización.  Participan con sus compañeros de afuera en eventos nacionales y se los cita debido a su liderazgo reconocido en una actividad científica o profesional compleja.  Son el pináculo nacional del conocimiento.'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'H-'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'H'
JobEvaluationData::create :kind => JobEvaluationData::KH_TECNICO, :job_evaluation_data_description => i, :name => 'H+'

i = JobEvaluationDataDescription.create :name => 'TAREA: Ejecución de tarea/s o funciones muy específicas en cuanto a su objeto y contenido, sin la necesidad de conocimiento o con conocimiento limitado de otras actividades relacionadas. Estos puestos generalmente se concentran en el cumplimiento de actividades individuales, más que en las que integran el flujo de trabajo de la unidad.  Los puestos realizan tareas sin conocer dónde se origina el trabajo, cuál es el destino del mismo, o cómo será usado en futuras operaciones.  Si bien el trabajo realizado es necesario, generalmente es posible alterar la secuencia del mismo con un mínimo o ningún impacto en el resto de la unidad;  o bien el trabajo es estructurado de manera tal que las tareas son presentadas al ocupante en el orden que deben ser realizadas, con muy baja posibilidad de alterar la secuencia.'
JobEvaluationData::create :kind => JobEvaluationData::KH_GERENCIA, :job_evaluation_data_description => i, :name => 'T-'
JobEvaluationData::create :kind => JobEvaluationData::KH_GERENCIA, :job_evaluation_data_description => i, :name => 'T'
JobEvaluationData::create :kind => JobEvaluationData::KH_GERENCIA, :job_evaluation_data_description => i, :name => 'T+'

i = JobEvaluationDataDescription.create :name => 'CONTRIBUCIÓN INDIVIDUAL: Ejecución o supervisión de actividades específicas en cuanto a su objetivo o contenido, con conocimiento apropiado de otras actividades relacionadas. <br />CONTRIBUYENTES INDIVIDUALES: Los empleados individuales a los que se asignan específicamente tareas completas simples o complejas, combinaciones de tareas o actividades funcionales, normalmente no requieren “Competencia” gerencial, tal y como se aplica este término en las tablas guía.  Los contribuyentes individuales con posiciones profesionales (Contabilidad, Finanzas, Ingeniería, Personal, etc.) tienen que saber cómo se relaciona su trabajo con el de otros que desempeñan la misma función. <br />SUPERVISORES DE PRIMERA LINEA: Los Supervisores, aunque normalmente se consideran como el primer nivel de la gerencia, desde la perspectiva de las Tablas Guía, suelen lograr resultados a través de otras personas por medio de la intervención personal directa en el trabajo que se está haciendo.  Se centran en la asignación de trabajos, programación del trabajo, supervisión mientras se está haciendo, revisión de los resultados para determinar su puntualidad, calidad y efectividad de costos.'
JobEvaluationData::create :kind => JobEvaluationData::KH_GERENCIA, :job_evaluation_data_description => i, :name => 'I-'
JobEvaluationData::create :kind => JobEvaluationData::KH_GERENCIA, :job_evaluation_data_description => i, :name => 'I'
JobEvaluationData::create :kind => JobEvaluationData::KH_GERENCIA, :job_evaluation_data_description => i, :name => 'I+'

i = JobEvaluationDataDescription.create :name => 'HOMOGÉNEA: Integración, organización o coordinación, de manera operativa o conceptual, de actividades que son relativamente homogéneas en su naturaleza y objetivos. <br />CONTRIBUYENTES INDIVIDUALES: Algunos contribuyentes individuales deben conocer y utilizar el proceso de gerencia para asesorar a los Gerentes de funciones acerca de cómo gerenciar sus propias áreas.  Actúan como consultores que ejercen destrezas conceptuales de “Competencia”, en lugar de destrezas de “Competencia Gerencial” de líneas/operativas.<br />GERENTES DE PRIMERA LINEA:  Estos son puestos que supervisan el trabajo de profesionales cuyas actividades de trabajo no requieren una supervisión diaria, incluyendo asignación, programación, control, etc.  El Gerente de primera línea debe concentrarse en asuntos a largo plazo, necesidades de fuerza laboral, planes operativos, presupuestos, etc.<br />GERENTES DE SEGUNDA LINEA: Gerencian las funciones o subfunciones a través de supervisores subordinados;  hacen la integración de las actividades que tienen un propósito funcional común y se aseguran de la coordinación externa con otras áreas funcionales.'
JobEvaluationData::create :kind => JobEvaluationData::KH_GERENCIA, :job_evaluation_data_description => i, :name => 'II-'
JobEvaluationData::create :kind => JobEvaluationData::KH_GERENCIA, :job_evaluation_data_description => i, :name => 'II'
JobEvaluationData::create :kind => JobEvaluationData::KH_GERENCIA, :job_evaluation_data_description => i, :name => 'II+'

i = JobEvaluationDataDescription.create :name => 'HETEROGÉNEA: Integración o dirección operacional o conceptual de actividades que son diversas en naturaleza y objetivos en un área gerencial importante, o coordinación de una función estratégica para toda la compañía. Estos son generalmente puestos de Alta Gerencia que buscan la integración de todas las funciones dentro de un área significativa de actividades, (todas las actividades financieras de una compañía Grande), o la integración de funciones no relacionadas, a veces con objetivos y metas en conflicto.  Estos gerentes multifuncionales tienen que resolver las prioridades en conflicto, asignar recursos financieros y humanos entre los niveles subordinados de gerencia que están compitiendo por recursos limitados y cuyo interés es el logro de sus propios objetivos.'
JobEvaluationData::create :kind => JobEvaluationData::KH_GERENCIA, :job_evaluation_data_description => i, :name => 'III-'
JobEvaluationData::create :kind => JobEvaluationData::KH_GERENCIA, :job_evaluation_data_description => i, :name => 'III'
JobEvaluationData::create :kind => JobEvaluationData::KH_GERENCIA, :job_evaluation_data_description => i, :name => 'III+'

i = JobEvaluationDataDescription.create :name => 'IV'
JobEvaluationData::create :kind => JobEvaluationData::KH_GERENCIA, :job_evaluation_data_description => i, :name => 'IV-'
JobEvaluationData::create :kind => JobEvaluationData::KH_GERENCIA, :job_evaluation_data_description => i, :name => 'IV'
JobEvaluationData::create :kind => JobEvaluationData::KH_GERENCIA, :job_evaluation_data_description => i, :name => 'IV+'

i = JobEvaluationDataDescription.create :name => 'V'
JobEvaluationData::create :kind => JobEvaluationData::KH_GERENCIA, :job_evaluation_data_description => i, :name => 'V-'
JobEvaluationData::create :kind => JobEvaluationData::KH_GERENCIA, :job_evaluation_data_description => i, :name => 'V'
JobEvaluationData::create :kind => JobEvaluationData::KH_GERENCIA, :job_evaluation_data_description => i, :name => 'V+'

i = JobEvaluationDataDescription.create :name => 'BÁSICAS:  Cortesía, tacto y eficicacia en el trato con otros en las relaciones diarias de trabajo incluyendo contactos para requerir o dar información. Este es el nivel básico de habilidades interpersonales utilizado por la mayoría de los individuos en el desempeño de sus trabajos.  Incluirá la habilidad de comunicarse con los colegas de trabajo, supervisores y otros miembros de la organización para solicitar o transmitir información, hacer preguntas, pedir aclaraciones con tacto.  El fracaso para ejercer este nivel de competencia “hará olas,” producirá problemas y en su debido tiempo interferirá con un desempeño efectivo en el puesto.'
JobEvaluationData::create :kind => JobEvaluationData::KH_RELACIONES_HUMANAS, :job_evaluation_data_description => i, :name => '1'

i = JobEvaluationDataDescription.create :name => 'IMPORTANTES: Las habilidades para comprender y/o influir sobre las personas, utilizadas en forma alternativa o combinada,  son importantes para alcanzar los objetivos del puesto, induciendo a los otros a la acción o a la comprensión. Este nivel de habilidades interpersonales es requerido en puestos que interactúan regularmente con otros, dentro de la organización, con clientes o con el público.  Frecuentemente se requieren aptitudes como persuasión o firmeza, así como sensibilidad para con el punto de vista de los demás para influenciar conductas, cambiar una opinión o invertir una situación.  Los requisitos para contacto con el público no necesariamente demandan este nivel de habilidades para las relaciones humanas, especialmente si el propósito es proporcionar o solicitar información.<br />Los puestos que asignan trabajo, supervisan y revisan el trabajo de otros empleados suelen requerir al menos este nivel de destreza.'
JobEvaluationData::create :kind => JobEvaluationData::KH_RELACIONES_HUMANAS, :job_evaluation_data_description => i, :name => '2'

i = JobEvaluationDataDescription.create :name => 'CRÍTICAS: Las habilidades para comprender, seleccionar, desarrollar y motivar a las personas utilizadas en forma alternativa o combinada, son importantes en el más alto grado. Suele requerirse el nivel  más alto de habilidades interpersonales para puestos que tienen significativo grado de interacción con otras personas en cualquier nivel dentro o fuera de la organización.  Estas posiciones requieren una comprensión bien desarrollada del comportamiento humano y aquellos factores que lo influencian o provocan cambios de conducta.  Los puestos que requieren aptitudes para la negociación suelen encontrarse en este nivel, pero deben tenerse en consideración las bases de poder que están siendo utilizadas.  En negociaciones entre compradores y vendedores de productos, servicios, conceptos o ideas, pueden requerirse menos habilidades para las relaciones humanas del “comprador” que tiene la posibilidad para decir “no”, que del vendedor que debe convertir el “no” en un “sí”.<br />Este nivel de destrezas suele ser requerido por posiciones que son responsables por el desarrollo, motivación, evaluación y remuneración de otros empleados.'
JobEvaluationData::create :kind => JobEvaluationData::KH_RELACIONES_HUMANAS, :job_evaluation_data_description => i, :name => '3'

i = JobEvaluationDataDescription.create :name => 'RUTINA ESTRICTA: Reglas simples e instrucciones detalladas. Instrucciones u órdenes, generalmente verbales, que suele  especificar en detalle la secuencia y tiempo de las tareas que deben llevarse a cabo con poca o ninguna libertad para que el ocupante del cargo considere procedimientos alternativos.'
JobEvaluationData::create :kind => JobEvaluationData::SP_AMBITO, :job_evaluation_data_description => i, :name => 'A'
JobEvaluationData::create :kind => JobEvaluationData::SP_AMBITO, :job_evaluation_data_description => i, :name => 'A+'

i = JobEvaluationDataDescription.create :name => 'RUTINA: Rutinas establecidas e instrucciones diarias. Las instrucciones suelen proporcionar libertad para considerar variaciones en la secuencia de tareas con base en situaciones encontradas dentro del escenario de trabajo.'
JobEvaluationData::create :kind => JobEvaluationData::SP_AMBITO, :job_evaluation_data_description => i, :name => 'B'
JobEvaluationData::create :kind => JobEvaluationData::SP_AMBITO, :job_evaluation_data_description => i, :name => 'B+'

i = JobEvaluationDataDescription.create :name => 'SEMI-RUTINA: Procedimientos y precedentes algo diversificados. Aunque las tareas que deben realizarse tienen procedimientos o precedentes específicos establecidos, existe cierta libertad (por las circunstancias cambiantes del trabajo), para considerar el procedimiento o precedente más apropiado para seguir.'
JobEvaluationData::create :kind => JobEvaluationData::SP_AMBITO, :job_evaluation_data_description => i, :name => 'C'
JobEvaluationData::create :kind => JobEvaluationData::SP_AMBITO, :job_evaluation_data_description => i, :name => 'C+'

i = JobEvaluationDataDescription.create :name => 'ESTANDARIZADO:  Procedimientos sustancialmente diversificados y estándares especializados. Las prioridades cambiantes y las diferentes situaciones encontradas en el ambiente de trabajo permiten que el ocupante del puesto tenga libertad para considerar cuál entre los muchos procedimientos debe seguirse y en qué secuencia para lograr los resultados necesarios.'
JobEvaluationData::create :kind => JobEvaluationData::SP_AMBITO, :job_evaluation_data_description => i, :name => 'D'
JobEvaluationData::create :kind => JobEvaluationData::SP_AMBITO, :job_evaluation_data_description => i, :name => 'D+'

i = JobEvaluationDataDescription.create :name => 'CLARAMENTE DEFINIDO: Políticas y principios claramente definidos. Las políticas están claramente definidas, pero son menos limitantes que los procedimientos.  El “qué” está bien establecido; el “cómo” es ampliamente determinado por el propio juicio del ocupante del puesto.  Los principios implican principios subyacentes a disciplinas como ingeniería, derecho, contabilidad.'
JobEvaluationData::create :kind => JobEvaluationData::SP_AMBITO, :job_evaluation_data_description => i, :name => 'E'
JobEvaluationData::create :kind => JobEvaluationData::SP_AMBITO, :job_evaluation_data_description => i, :name => 'E+'

i = JobEvaluationDataDescription.create :name => 'AMPLIAMENTE DEFINIDO: Políticas amplias  y objetivos específicos. Las políticas están claramente definidas, pero son menos limitantes que los procedimientos.  El “qué” está bien establecido; el “cómo” es ampliamente determinado por el propio juicio del ocupante del puesto.  Los principios implican principios subyacentes a disciplinas como ingeniería, derecho, contabilidad.'
JobEvaluationData::create :kind => JobEvaluationData::SP_AMBITO, :job_evaluation_data_description => i, :name => 'F'
JobEvaluationData::create :kind => JobEvaluationData::SP_AMBITO, :job_evaluation_data_description => i, :name => 'F+'

i = JobEvaluationDataDescription.create :name => 'GENÉRICAMENTE DEFINIDO: Políticas generales y objetivos finales. Estos puestos establecen las políticas generales y los objetivos funcionales de la organización.  El objetivo se especifica solamente en términos muy generales, tales como “aumento de las operaciones internacionales” o “entrar a nuevos mercados”'
JobEvaluationData::create :kind => JobEvaluationData::SP_AMBITO, :job_evaluation_data_description => i, :name => 'G'
JobEvaluationData::create :kind => JobEvaluationData::SP_AMBITO, :job_evaluation_data_description => i, :name => 'G+'

i = JobEvaluationDataDescription.create :name => 'ABSTRACTAMENTE DEFINIDO: Leyes generales de la naturaleza de la ciencia, la filosofía de los negocios, y patrones culturales. Esta singular posición dentro de la organización, guiada por los accionistas, tenedores de políticas, propietarios, establece la dirección estratégica de la misma, congruente con sus estatutos y los requisitos para la supervivencia y la continuidad de la organización.'
JobEvaluationData::create :kind => JobEvaluationData::SP_AMBITO, :job_evaluation_data_description => i, :name => 'H'
JobEvaluationData::create :kind => JobEvaluationData::SP_AMBITO, :job_evaluation_data_description => i, :name => 'H+'

i = JobEvaluationDataDescription.create :name => 'REPETITIVO: Situaciones idénticas, en las  que la solución requiere una simple elección entre cosas aprendidas. Estos puestos aplican conocimientos y habilidades directamente al trabajo.  Cada situación en casi la misma que la anterior, y se toman las decisiones correctas a través de selecciones simples, Ej.: ordenar operaciones.'
JobEvaluationData::create :kind => JobEvaluationData::SP_DESAFIO, :job_evaluation_data_description => i, :name => '1'
JobEvaluationData::create :kind => JobEvaluationData::SP_DESAFIO, :job_evaluation_data_description => i, :name => '1+'

i = JobEvaluationDataDescription.create :name => 'CON MODELOS: Situaciones similares, en las que la solución requiere una elección discriminada entre cosas aprendidas que generalmente siguen un modelo definido. Estos puestos confrontan situaciones de selección múltiple, pero han aprendido cuál es la selección más adecuada para la situación a través de exposición o experiencia previas.'
JobEvaluationData::create :kind => JobEvaluationData::SP_DESAFIO, :job_evaluation_data_description => i, :name => '2'
JobEvaluationData::create :kind => JobEvaluationData::SP_DESAFIO, :job_evaluation_data_description => i, :name => '2+'

i = JobEvaluationDataDescription.create :name => 'INTERPOLATIVO: Situaciones diferenciadas que requieren de investigación para encontrar soluciones o nuevas aplicaciones dentro de cosas aprendidas. Estos puestos se enfrentan con problemas que “se esconden en grietas” y se resuelven “leyendo entre líneas”.  Las soluciones resultan de la comparación de elementos de problemas con puntos de referencia dentro de la propia experiencia y la selección de soluciones que se relacionan con experiencias anteriores.'
JobEvaluationData::create :kind => JobEvaluationData::SP_DESAFIO, :job_evaluation_data_description => i, :name => '3'
JobEvaluationData::create :kind => JobEvaluationData::SP_DESAFIO, :job_evaluation_data_description => i, :name => '3+'

i = JobEvaluationDataDescription.create :name => 'ADAPTATIVO: Situaciones variables que requieren un pensamiento analítico, interpretativo, evaluativo y/o constructivo. La situación que debe resolverse incluye circunstancias, hechos y problemas que difieren de aquellos encontrados en el pasado.  El ocupante del puesto tiene que considerar varios cursos de acción posibles y ponderar sus consecuencias antes de emprender o recomendar otros pasos.'
JobEvaluationData::create :kind => JobEvaluationData::SP_DESAFIO, :job_evaluation_data_description => i, :name => '4'
JobEvaluationData::create :kind => JobEvaluationData::SP_DESAFIO, :job_evaluation_data_description => i, :name => '4+'

i = JobEvaluationDataDescription.create :name => 'SIN PRECEDENTES: Situaciones nuevas o sin precedentes que requieren el desarrollo de conceptos nuevos y propuestas imaginativas. Estas posiciones confrontan con lo desconocido.  Las situaciones enfrentadas tienen poco o ningún precedente. El ocupante del cargo debe originar nuevos conceptos o enfoques sin la orientación de otras personas.'
JobEvaluationData::create :kind => JobEvaluationData::SP_DESAFIO, :job_evaluation_data_description => i, :name => '5'
JobEvaluationData::create :kind => JobEvaluationData::SP_DESAFIO, :job_evaluation_data_description => i, :name => '5+'

i = JobEvaluationDataDescription.create :name => 'LIMITADA: Los puestos están sujetos a instrucciones explícitas que cubren tareas simples. La naturaleza de las tareas o del trabajo está totalmente confinada, las instrucciones son exactas y/o la supervisión es constante.  Con frecuencia las normas de seguridad requieren que una tarea (activación del botón de inicio de una máquina peligrosa)  se desempeñe de una sola manera.'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'L-'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'L'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'L+'

i = JobEvaluationDataDescription.create :name => 'RESTRICTA: Estos puestos están sujetos a instrucciones prescriptas que cubren  las tareas asignadas y/o supervisión inmediata. Estos puestos reciben instrucciones explícitas, en forma oral o escrita, que determinan paso a paso las tareas que deben completarse para alcanzar un resultado final específico.  No se permiten desvíos sin solicitar autorización previa.'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'A-'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'A'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'A+'

i = JobEvaluationDataDescription.create :name => 'CONTROLADA: Estos puestos están sujetos a instrucciones y rutinas de trabajo establecidas y/o supervisión directa. Estos puestos tienen libertad para reacomodar la secuencia dada para completar las variadas tareas u obligaciones con base en las situaciones de trabajo cambiantes, prioridades del flujo de trabajo, etc.'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'B-'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'B'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'B+'

i = JobEvaluationDataDescription.create :name => 'ESTANDARIZADA: Estos puestos están total o parcialmente sujetos a prácticas y procedimientos estandarizados y/o instrucciones generales de trabajo y/o a supervisión sobre el avance del trabajo y sus resultados. Estos puestos suelen contener una mayor variedad de tareas y obligaciones, y entienden claramente los resultados cotidianos que se esperan.  No se permite a los ocupantes del puesto se desvíen de las prácticas y procedimientos estándar, pero puede permitirse que establezcan su propias prioridades, sujetas a aprobación.'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'C-'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'C'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'C+'

i = JobEvaluationDataDescription.create :name => 'GENERICAMENTE REGULADA: Estos puestos están total o parcialmente sujetos a prácticas y procedimientos cubiertos por precedentes o políticas muy definidas y/o a revisión del supervisor. Estos puestos están autorizados a determinar sus propias prioridades, y pueden desviarse de procedimientos y prácticas establecidos en la medida que los resultados finales cumplan con los estándares de aceptación (Ej.: calidad, volumen, puntualidad, etc.)  La supervisión de las actividades de trabajo suele ser indirecta y la revisión de los resultados de trabajo generalmente ocurre después de los hechos.'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'D-'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'D'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'D+'

i = JobEvaluationDataDescription.create :name => 'CON DIRECCIÓN: Estos puestos por su naturaleza o tamaño, están sujetos a prácticas y procedimientos amplios cubiertos por precedentes y políticas funcionales y/o realización de una actividad operacional  circunscripta y/o dirección gerencial. Estos puestos, frecuentemente gerentes de áreas funcionales o colaboradores individuales de alta jerarquía, tienen el grado de independencia necesario para alcanzar resultados operativos, ya que las actividades son congruentes con planes y objetivos operacionales aprobados y con políticas y precedentes funcionales.  La orientación gerencial dada a estos puestos establece los resultados esperados.  Estos puestos determinan cómo y cuándo se alcanzarán los resultados.'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'E-'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'E'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'E+'

i = JobEvaluationDataDescription.create :name => 'CON ORIENTACIÓN: Estos puestos por su naturaleza o tamaño, están sujetos en forma amplia a políticas funcionales y metas  y/o dirección gerencial general. Los puestos de este nivel suelen reportarse al jefe de un área operativa importante en organizaciones grandes y se les concede amplia discreción ya que sus actividades son congruentes con políticas y precedentes operativos dentro de esa función.  Las acciones que tendrán impacto sobre otras áreas funcionales u operativas suelen requerir aprobación antes de ser implementadas.'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'F-'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'F'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'F+'

i = JobEvaluationDataDescription.create :name => 'CON GUIAS GENERALES: Estos puestos están esencialmente  sujetos únicamente a  políticas y guías  gerenciales  general. Estos puestos establecen políticas funcionales, generalmente como resultado de pertenecer a la Alta Gerencia Operativa o a comités de políticas.  Este es un nivel de toma de decisiones importantes en la organización que determina los resultados organizacionales que deben ser alcanzados.'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'G-'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'G'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'G+'

i = JobEvaluationDataDescription.create :name => 'H'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'H-'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'H'
JobEvaluationData::create :kind => JobEvaluationData::AC_LIBERTAD, :job_evaluation_data_description => i, :name => 'H+'

i = JobEvaluationDataDescription.create :name => 'Usar si el Cargo no tiene magnitud y se trata de un cargo operativo o administrativo'
JobEvaluationData::create :kind => JobEvaluationData::AC_MAGNITUD, :job_evaluation_data_description => i, :name => '0-'
JobEvaluationData::create :kind => JobEvaluationData::AC_MAGNITUD, :job_evaluation_data_description => i, :name => '0'
JobEvaluationData::create :kind => JobEvaluationData::AC_MAGNITUD, :job_evaluation_data_description => i, :name => '0+'

i = JobEvaluationDataDescription.create :name => 'Usar si el Cargo no tiene magnitud y se trata de un cargo Técnico o profesional'
JobEvaluationData::create :kind => JobEvaluationData::AC_MAGNITUD, :job_evaluation_data_description => i, :name => '1-'
JobEvaluationData::create :kind => JobEvaluationData::AC_MAGNITUD, :job_evaluation_data_description => i, :name => '1'
JobEvaluationData::create :kind => JobEvaluationData::AC_MAGNITUD, :job_evaluation_data_description => i, :name => '1+'

i = JobEvaluationDataDescription.create :name => 'Desde 550.000 - Hasta 2.200.000'
JobEvaluationData::create :kind => JobEvaluationData::AC_MAGNITUD, :job_evaluation_data_description => i, :name => '2-'

i = JobEvaluationDataDescription.create :name => 'Desde 2.200.000 - Hasta 3.850.000'
JobEvaluationData::create :kind => JobEvaluationData::AC_MAGNITUD, :job_evaluation_data_description => i, :name => '2'

i = JobEvaluationDataDescription.create :name => 'Desde 3.850.000 - Hasta 5.500.000'
JobEvaluationData::create :kind => JobEvaluationData::AC_MAGNITUD, :job_evaluation_data_description => i, :name => '2+'

i = JobEvaluationDataDescription.create :name => 'Desde 5.500.000 - Hasta 22.000.000'
JobEvaluationData::create :kind => JobEvaluationData::AC_MAGNITUD, :job_evaluation_data_description => i, :name => '3-'

i = JobEvaluationDataDescription.create :name => 'Desde 22.000.000 - Hasta 38.500.000'
JobEvaluationData::create :kind => JobEvaluationData::AC_MAGNITUD, :job_evaluation_data_description => i, :name => '3'

i = JobEvaluationDataDescription.create :name => 'Desde 38.500.000 - Hasta 55.000.000'
JobEvaluationData::create :kind => JobEvaluationData::AC_MAGNITUD, :job_evaluation_data_description => i, :name => '3+'

i = JobEvaluationDataDescription.create :name => 'Desde 55.000.000 - Hasta 220.000.000'
JobEvaluationData::create :kind => JobEvaluationData::AC_MAGNITUD, :job_evaluation_data_description => i, :name => '4-'

i = JobEvaluationDataDescription.create :name => 'Desde 220.000.000 - Hasta 385.000.000'
JobEvaluationData::create :kind => JobEvaluationData::AC_MAGNITUD, :job_evaluation_data_description => i, :name => '4'

i = JobEvaluationDataDescription.create :name => 'Desde 385.000.000 - Hasta 550.000.000'
JobEvaluationData::create :kind => JobEvaluationData::AC_MAGNITUD, :job_evaluation_data_description => i, :name => '4+'

i = JobEvaluationDataDescription.create :name => 'Desde 550.000.000 - Hasta 2.200.000.000'
JobEvaluationData::create :kind => JobEvaluationData::AC_MAGNITUD, :job_evaluation_data_description => i, :name => '5-'

i = JobEvaluationDataDescription.create :name => 'Desde 2.200.000.000 - Hasta 3.850.000.000'
JobEvaluationData::create :kind => JobEvaluationData::AC_MAGNITUD, :job_evaluation_data_description => i, :name => '5'

i = JobEvaluationDataDescription.create :name => 'Desde 3.850.000.000 - Hasta 5.500.000.000'
JobEvaluationData::create :kind => JobEvaluationData::AC_MAGNITUD, :job_evaluation_data_description => i, :name => '5+'

i = JobEvaluationDataDescription.create :name => 'Desde 5.500.000.000 - Hasta 22.000.000.000'
JobEvaluationData::create :kind => JobEvaluationData::AC_MAGNITUD, :job_evaluation_data_description => i, :name => '6-'

i = JobEvaluationDataDescription.create :name => 'Desde 22.000.000.000 - Hasta 38.500.000.000'
JobEvaluationData::create :kind => JobEvaluationData::AC_MAGNITUD, :job_evaluation_data_description => i, :name => '6'

i = JobEvaluationDataDescription.create :name => 'Desde 38.500.000.000 - Hasta 55.000.000.000'
JobEvaluationData::create :kind => JobEvaluationData::AC_MAGNITUD, :job_evaluation_data_description => i, :name => '6+'

i = JobEvaluationDataDescription.create :name => 'PRIMARIO: Control del impacto sobre los resultados finales, donde cualquier  responsabilidad compartida con otros es secundaria. Tales impactos se encuentran comúnmente en posiciones operativas y gerenciales que tienen “responsabilidad lineal” por áreas claves de resultados finales, bien sea grandes o pequeñas.  Por ejemplo, un supervisor puede tener“responsabilidad primaria” por la producción o los resultados (valor agregado) de una unidad dentro del contexto de recursos disponibles (Ej.: recursos de personal y gastos controlables);  mientras que el jefe de fabricación puede tener un impacto primario sobre el valor agregado total de la fabricación de productos o o del costo de los bienes producidos.  La clave aquí es que el puesto existe para tener el impacto de control sobre ciertos resultados finales de magnitud dada y que la responsabilidad no se comparte con otras personas (“yo soy el responsable”).'
JobEvaluationData::create :kind => JobEvaluationData::AC_IMPACTO, :job_evaluation_data_description => i, :name => 'P-'
JobEvaluationData::create :kind => JobEvaluationData::AC_IMPACTO, :job_evaluation_data_description => i, :name => 'P'
JobEvaluationData::create :kind => JobEvaluationData::AC_IMPACTO, :job_evaluation_data_description => i, :name => 'P+'

i = JobEvaluationDataDescription.create :name => 'COMPARTIDO: Participan con otros (excepto sus subordinados y superiores), dentro o fuera de s unidad organizacional, al actuar. Este tipo de impacto no es totalmente controlador en relación con la magnitud del resultado.  Una regla básica es que el “compartir” no puede existir verticalmente en una organización (Ej.: entre superior y subordinado).  Los impactos compartidos pueden existir entre puestos pares y/o funciones y sugieren un grado de “sociedad” o de “responsabilidad conjunta” por el área total de resultados.  Por ejemplo, puede existir responsabilidad compartida entre un ingeniero y una función en fabricación por un producto exitoso.'
JobEvaluationData::create :kind => JobEvaluationData::AC_IMPACTO, :job_evaluation_data_description => i, :name => 'S-'
JobEvaluationData::create :kind => JobEvaluationData::AC_IMPACTO, :job_evaluation_data_description => i, :name => 'S'
JobEvaluationData::create :kind => JobEvaluationData::AC_IMPACTO, :job_evaluation_data_description => i, :name => 'S+'

i = JobEvaluationDataDescription.create :name => 'CONTRIBUTORIO: Servicios de interpretación, asesoramiento o ayuda  para ser utilizados por otros, al actuar. Este tipo de impacto es apropiado donde los puestos son responsables por la rendición de “asesoramiento y consejos” significativo además de información y/o análisis y cuando las decisiones son probablemente tomadas en virtud de tal consejo.  Tales impactos son comúnmente encontrados en el personal o en las funciones de soporte que influyen significativamente en las decisiones relacionadas con varias magnitudes de recursos.'
JobEvaluationData::create :kind => JobEvaluationData::AC_IMPACTO, :job_evaluation_data_description => i, :name => 'C-'
JobEvaluationData::create :kind => JobEvaluationData::AC_IMPACTO, :job_evaluation_data_description => i, :name => 'C'
JobEvaluationData::create :kind => JobEvaluationData::AC_IMPACTO, :job_evaluation_data_description => i, :name => 'C+'
i = JobEvaluationDataDescription.create :name => 'REMOTO: Procesamiento de información, registro o eventuales servicios, para ser utilizados por otros en relación con algún resultado final importante. La actividad del puesto puede ser compleja, pero su impacto sobre la organización en general es relativamente menor.  Estos puestos suelen estar involucrados con la recolección o el procesamiento de información, típicamente utilizada en puestos que tienen un impacto más directo sobre la organización.'
JobEvaluationData::create :kind => JobEvaluationData::AC_IMPACTO, :job_evaluation_data_description => i, :name => 'R-'
JobEvaluationData::create :kind => JobEvaluationData::AC_IMPACTO, :job_evaluation_data_description => i, :name => 'R'
JobEvaluationData::create :kind => JobEvaluationData::AC_IMPACTO, :job_evaluation_data_description => i, :name => 'R+'
