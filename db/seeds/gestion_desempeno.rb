# -*- encoding : utf-8 -*-
i = JobType.create name: 'Estrategico'
ActionVerb.create name: 'Planificar', job_type: i
ActionVerb.create name: 'Definir', job_type: i
ActionVerb.create name: 'Dirigir', job_type: i
ResultVerb.create name: 'Garantizar', job_type: i

i = JobType.create name: 'Estrategico Táctico'
ActionVerb.create name: 'Organizar', job_type: i
ActionVerb.create name: 'Conducir', job_type: i
ActionVerb.create name: 'Controlar', job_type: i
ResultVerb.create name: 'Garantizar', job_type: i

i = JobType.create name: 'Táctico'
ActionVerb.create name: 'Programar', job_type: i
ActionVerb.create name: 'Estructurar', job_type: i
ActionVerb.create name: 'Operacionalizar', job_type: i
ActionVerb.create name: 'Guiar', job_type: i
ResultVerb.create name: 'Asegurar', job_type: i

i = JobType.create name: 'Táctico Operativo'
ActionVerb.create name: 'Proyectar', job_type: i
ActionVerb.create name: 'Establecer', job_type: i
ActionVerb.create name: 'Coordinar', job_type: i
ActionVerb.create name: 'Aconsejar', job_type: i
ActionVerb.create name: 'Encausar', job_type: i
ActionVerb.create name: 'Verificar', job_type: i
ActionVerb.create name: 'Inspeccionar', job_type: i
ActionVerb.create name: 'Analizar', job_type: i
ActionVerb.create name: 'Investigar', job_type: i
ResultVerb.create name: 'Contribuir', job_type: i
ResultVerb.create name: 'Lograr', job_type: i

i = JobType.create name: 'Operativo de Supervisión'
ActionVerb.create name: 'Supervisar', job_type: i
ActionVerb.create name: 'Distribuir', job_type: i
ActionVerb.create name: 'Revisar', job_type: i
ActionVerb.create name: 'Comprobar', job_type: i
ActionVerb.create name: 'Asignar', job_type: i
ResultVerb.create name: 'Velar', job_type: i
ResultVerb.create name: 'Mantener', job_type: i
ResultVerb.create name: 'Lograr', job_type: i


i = JobType.create name: 'Operativo de Ejecución'
ActionVerb.create name: 'Preparar', job_type: i
ActionVerb.create name: 'Ordenar', job_type: i
ActionVerb.create name: 'Clasificar', job_type: i
ActionVerb.create name: 'Archivar', job_type: i
ActionVerb.create name: 'Contrastar', job_type: i
ActionVerb.create name: 'Realizar', job_type: i
ActionVerb.create name: 'Ejecutar', job_type: i
ActionVerb.create name: 'Hacer', job_type: i
ResultVerb.create name: 'Lograr', job_type: i


Profile.create name: 'RI3'
Profile.create name: 'RI2'
Profile.create name: 'RI1'
Profile.create name: '0'
Profile.create name: 'P1'
Profile.create name: 'P2'
Profile.create name: 'P3'
Profile.create name: 'P4'

InferentialFunction.create name: 'las Estrategias'
InferentialFunction.create name: 'las Políticas Generales'
InferentialFunction.create name: 'las Políticas Funcionales'
InferentialFunction.create name: 'los Planes'
InferentialFunction.create name: 'los Procedimientos'
InferentialFunction.create name: 'la Gerencia'
InferentialFunction.create name: 'la Subgerencia'
InferentialFunction.create name: 'las actividades'
InferentialFunction.create name: 'los recursos'

InferentialResult.create name: 'el cumplimiento'
InferentialResult.create name: 'el seguimiento'
InferentialResult.create name: 'la disponibilidad'
InferentialResult.create name: 'la existencia'
InferentialResult.create name: 'la obtención'
InferentialResult.create name: 'el registro'


Magnitude.create kind: Magnitude::FINANCIERAS, name: 'Ingreso Anual MM US$'
Magnitude.create kind: Magnitude::FINANCIERAS, name: 'Gasto Anual MM US$'
Magnitude.create kind: Magnitude::FINANCIERAS, name: 'Inversión Anual MM US$'
Magnitude.create kind: Magnitude::FINANCIERAS, name: 'Presupuesto Anual MM US$'
Magnitude.create kind: Magnitude::FINANCIERAS, name: 'Presupuesto Operativo Anual MM US$'

Magnitude.create kind: Magnitude::NO_FINANCIERAS, name: 'Nº Reportes directos'
Magnitude.create kind: Magnitude::NO_FINANCIERAS, name: 'Nº Reportes totales'
Magnitude.create kind: Magnitude::NO_FINANCIERAS, name: 'Contratistas'
