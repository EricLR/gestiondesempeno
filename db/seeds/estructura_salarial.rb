# -*- encoding : utf-8 -*-
i = IncomeStructure.create name: 'Sueldo Base (SB)', acronym: 'SB'

IncomeStructureDetail.create name: 'Sueldo Base', income_structure: i

i = IncomeStructure.create name: 'Salario Base Fijo Mensual (SBFM)', acronym: 'SBFM'

IncomeStructureDetail.create name: 'SB', income_structure: i
IncomeStructureDetail.create name: 'Gratificación Mensual Legal', income_structure: i
IncomeStructureDetail.create name: 'Gratificación Mensual Voluntaria', income_structure: i
IncomeStructureDetail.create name: 'Asignaciones', income_structure: i

i = IncomeStructure.create name: 'Salario Fijo Garantizado Mensual (SFGM)', acronym: 'SFGM'

IncomeStructureDetail.create name: 'SBFM', income_structure: i
IncomeStructureDetail.create name: 'Aguinaldos', income_structure: i
IncomeStructureDetail.create name: 'Vacaciones', income_structure: i

i = IncomeStructure.create name: 'Total en Dinero (TD)', acronym: 'TD'

IncomeStructureDetail.create name: 'SFGM', income_structure: i
IncomeStructureDetail.create name: 'Bonos', income_structure: i
IncomeStructureDetail.create name: 'Comisiones', income_structure: i
IncomeStructureDetail.create name: 'Premios', income_structure: i

i = IncomeStructure.create name: 'Compensación Directa Total (CDT)', acronym: 'CDT'

IncomeStructureDetail.create name: 'TD', income_structure: i
IncomeStructureDetail.create name: 'Incentivo de Largo Plazo', income_structure: i

i = IncomeStructure.create name: 'Total Remuneración (TR)', acronym: 'TR'

IncomeStructureDetail.create name: 'CDT', income_structure: i
IncomeStructureDetail.create name: 'Beneficios expresados en dinero', income_structure: i

i = IncomeStructure.create name: 'Total Compensación', acronym: 'TC'

IncomeStructureDetail.create name: 'TR', income_structure: i
IncomeStructureDetail.create name: 'Intangibles', income_structure: i
