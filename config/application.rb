# -*- encoding : utf-8 -*-
require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require 'action_controller/railtie'
require 'dm-rails/railtie'
require 'action_mailer/railtie'
require 'active_resource/railtie'
require "sprockets/railtie"
#require 'lib/cache_ssi'
# require 'rails/test_unit/railtie'


if defined?(Bundler)
  # If you precompile assets before deploying to production, use this line
  # Bundler.require(*Rails.groups(:assets => %w(development test)))
  # If you want your assets lazily compiled in production, use this line
  Bundler.require(:default, :assets, Rails.env)
end



module GestionDesempeno
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Custom directories with classes and modules you want to be autoloadable.
    #config.autoload_paths += %W(#{config.root}/vendor/gems/dm-crud/lib)
    # config.autoload_paths += %W(#{Rails.root}/lib)
    config.autoload_paths += %W(#{Rails.root}/lib)
    # Only load the plugins named here, in the order given (default is alphabetical).
    # :all can be used as a placeholder for all plugins not explicitly named.
    # config.plugins = [ :crudify ]

    # Activate observers that should always be running.
    # config.active_record.observers = :cacher, :garbage_collector, :forum_observer

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    config.i18n.default_locale = :es

    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = "utf-8"

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:password]

    # Enable the asset pipeline
    config.assets.enabled = true
    config.assets.precompile += %w{templates/*}

    # Version of your assets, change this if you want to expire all your assets
    config.assets.version = '1.0'

    # :niveles, :factores_claves, :factores_claves_4_niveles
    config.tipo_conductas      = :factores_claves#_4_niveles
    config.cliente             = :rvelasco #:maserrazuriz #:uandes #disal

    # agregado para controlar la competencia que utilizara
    # saville - otro - no usara

    config.competencias        = :saville #:saville #:otro #:no_usara


    config.solo_autoevaluacion = false
    config.url                 = "http://127.0.0.1:8888"
    config.external_url        = "http://127.0.0.1:4000"
    config.organigrama_url     = "#{config.external_url}/organigrama"
    config.mailing_url         = "#{config.external_url}/comunicacion"
    config.encuesta_url        = "http://127.0.0.1/survey/"
    config.email_descripcion   = "andres.villagran@rvelasco.cl"
    if Rails.env.production?
        config.java_oo         = "java -jar lib/jodconverter-cli-2.2.2.jar"
    else
        config.java_oo         = "java -jar lib/jodconverter-cli-2.2.2.jar"
    end

    #ActiveSupport::Dependencies.explicitly_unloadable_constants << 'Crud'
    #ActiveSupport::Dependencies.autoload_once_paths.delete(File.expand_path(File.dirname(__FILE__))+'/vendor/gems/dm-crud/lib')
    #ActiveSupport::Dependencies.autoload_paths << "vendor/gems/dm-crud/lib"
    #config.autoload_paths += %W(#{config.root}/vendor/gems/dm-crud/lib)

    Haml::Template::options[:ugly] = true

    if Rails.env.production?
      config.action_controller.asset_host = 'http://localhost:3000'
      config.action_mailer.delivery_method = :smtp
      config.action_mailer.asset_host = config.action_controller.asset_host

      config.action_mailer.smtp_settings = {
          :address              => "smtp.gmail.com",
          :port                 => 587,
          :user_name            => 'sergio.olivares@rvelasco.cl',
          :password             => 'pendragon1079',
          :authentication       => 'plain',
          :enable_starttls_auto => true
      }
    else
      config.action_mailer.delivery_method = :sendmail
    end

    require Rails.root+'lib/dm-update-or-create'
  end
end
