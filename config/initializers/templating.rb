# -*- encoding : utf-8 -*-
require 'handlebars_assets'

::HandlebarsAssets.configure do |config|
  config.haml_enabled = false
  # and/or
  config.hamlbars_extensions = []
end
