# -*- encoding : utf-8 -*-
# Be sure to restart your server when you modify this file.

require 'action_dispatch/middleware/session/dalli_store'
#GestionDesempeno::Application.config.session_store :cookie_store, key: '_GestionDesempeno_session'
if Rails.env.production?
  GestionDesempeno::Application.config.session_store :dalli_store, :memcache_server => ['localhost'], :namespace => 'uandes-sessions', key: '_fundation_session', expire_after: 20.minutes
else
  # GestionDesempeno::Application.config.session_store :cookie_store, key: '_GestionDesempeno_session'
  GestionDesempeno::Application.config.session_store :dalli_store, :memcache_server => ['localhost'], :namespace => 'uandes-sessions', key: '_fundation_session', expire_after: 8.hours
end

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rails generate session_migration")
# GestionDesempeno::Application.config.session_store :active_record_store
