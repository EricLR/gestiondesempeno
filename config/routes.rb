# -*- encoding : utf-8 -*-
GestionDesempeno::Application.routes.draw do

  get "main/index"

  resources :comments do
    member do

    end
  end

  match 'organizacion' => 'organization#index'

  namespace :organization, :path => 'organizacion' do
    resources :jobs, :path => 'cargos'
    resources :positions, :path => 'puestos'
    resources :jobscharts, :path => 'organigrama-cargos'
    resources :positionscharts, :path => 'organigrama-puestos'
    resources :users, :path => 'personas'
  end

  post 'asignar_descripcion' => 'Jobsdescription::Jobs#asignar_descripcion'
  post 'informar_jefes' => 'Jobsdescription::Jobs#informar_jefes'
  post 'comentarios' => 'Jobsdescription::Jobs#comentarios'
  #post 'informar_jefes/:descripcion', to: 'Jobsdescription::Jobs#informar_jefes', as: 'informar_jefes'

  namespace :jobsdescription, :path => 'descripcion-de-cargos' do
    resources :jobs, :path => 'administracion' do
      # get 'asignar_descripcion', to: 'jobs#asignar_descripcion', as: 'asignar_descripcion'
      member do
        # post 'asignar_descripcion'
        get 'delete_main_result'
        get 'delete_dimension'
        get 'cargar'
        post 'cargar_process'
        get 'description_edit'
        put 'comment_save'
        put 'description_status_save'
      end
      collection do
        get 'multiupload'
        post 'multiupload_save'
        get 'list'
        get 'workflow'
        post 'workflow_save'
        post 'asociar'
        post 'update_dynamic_tables'

      end
    end
  end

  resources :desempenoaprobe do
    collection do
      get 'aprobarmetasporcorreo'
      get 'aprobarcompetenciaporcorreo'
    end
  end

  namespace :performance, :path => 'gestion-desempeno' do
    resources :planifications, :path => 'planificacion' do
      collection do
        post  'planification_duplicar'
        get  'kpi'
        post 'kpisave'
        get  'competences'
        post 'competences_save'
        get  'kpi_aprove'
        get  'competences_aprove'
      end
      member do

        get  'kpiedit'
        put  'kpiupdate'
        get  'kpidelete'
        get  'competences_delete'
      end
    end
    resources :monitoring, :path => 'seguimiento', :as => 'seguimiento' do
      collection do

        get  'kpi', :kind => 'seguimiento'
        post 'kpisave'
        get  'kpidelete'
        get  'competences', :kind => 'seguimiento'
        post 'competences_save'
      end
      member do
        get  'kpiedit'

        put  'kpiupdate'
        get  'competences_edit'
        put  'competences_update'
      end
    end
    resources :monitoring, :path => 'evaluacion', :as => 'evaluacion' do
      collection do
        get  'planilla'

        post 'planilla_save'
        get  'kpi', :kind => 'evaluacion'
        post 'kpisave'
        get  'competences', :kind => 'evaluacion'
        post 'competences_save'
      end
      member do
        get  'kpiedit'
        put  'kpiupdate'
        get  'competences_edit'
        put  'competences_update'

        get  'competences_close'
        get  'competences_close_aprove'
        get  'competences_close_refuse', path: 'evaluacion/:id/competences_close_refuse'

        get  'kpi_close'
        get  'kpi_close_aprove'
        get  'kpi_close_refuse', path: 'evaluacion/:id/kpi_close_refuse'

        get  'mt_close'
        get  'mt_open'

      end
    end

    resources :agreements, :path => 'acuerdos-y-comentarios'
    resources :evaluations, :path => 'evaluacion-jefatura-y-final' do
      collection do
        get 'matriz_evaluacion'
      end
    end

    resources :informes do
      collection do
        post 'planificaciones'
        post 'evaluaciones'
        post 'evaluaciones_notas'
        get 'evaluaciones_notas'
        get 'evaluaciones_competencias'
        post 'evaluaciones_competencias'
        post 'evaluaciones_finales'
        post 'estado_evaluaciones'
        post 'informe_cuadrantes'
        get 'personal'
        post 'personal'
        get 'personal_informe'
        post 'personal_informe_feedback_save'
      end
    end

  end

  namespace :compensacion, path: 'administracion-de-la-compensacion' do
    resources :compensation do
      collection do
        get 'excel_puestos'
        get 'masa_salarial'
        get 'masa_salarial_simulacion'
        post 'masa_salarial_update'
        post 'excel_procesa_datos'
        get 'seleccion'
        get 'definir_estructuras'
        post 'eliminar_excel'


      end
    end
    resources :market_data, path: 'datos-de-mercado', as: 'market_data' do
      collection do
        get 'mantenedor'
        post 'guardar_mantenedor'
        get 'equidad', path: 'equidad-interna'
        get 'ei_save_option'
        get 'ei_informacion', path: 'equidad-interna-datos'
        post 'ei_guarda_puntos'
        get 'comp_graficos'
        get 'calculo_personal_comparativo', path: 'calculo-personal/comparar/:user_id/con/:comparar_tipo'
        post 'guardar_calculo_personal', path: 'calculo-personal/guardar/:user_id'
        get 'calculo_personal', path: 'calculo-personal(/user/:user_id)'
        get 'bandas_equidad_interna', path: 'bandas-equidad-interna'


        get 'equidad_eliminar_puntos', path: 'eliminar-puntos/:income_id/:option/:delete_key'
        get 'equidad_agregar_puntos', path: 'agregar-puntos(/:income_id(/:option))'
      end
    end
    resources :market_percentile, path: 'mercado/mantenedor-percentiles'

    match 'estudios-de-mercado/competitividad' => 'market_studies#competitivity', as: 'market_studies_competitivity'

  end

  namespace :dotacion, path: 'administracion-de-la-dotacion' do
    resources :dotacion do
      collection do
        post 'save_data'
        get 'diferencias'
        get 'clean_data'
        get 'plan'
        get 'carga'
        get 'excel'
        post 'excel_procesa'
      end
    end
  end

  resources :job_evaluation_datas
  resources :job_evaluation_data_descriptions

  namespace :mantenedores do
    resources :roles do
      member do
        get 'delete'
        post 'update'
      end
    end
    resources :jobtypes do
      member do
        get 'delete'
      end
    end
    resources :actionverbs do
      member do
        get 'delete'
      end
    end
    resources :jd_tables do
      collection do
        post 'save'
      end
      member do
        get 'delete'
      end
    end
    resources :periods do
      member do
        get 'delete'
      end
    end

    resources :competence_types do
      member do
        get 'delete'
      end
    end

    resources :competences do
      member do
        get 'delete'
        get 'list'
      end

    end


    resources :conducts do
      member do
        get 'delete'
        get 'list'
      end
    end


    resources :userperiods

    resources :insert_planifications do
      collection do
        get 'json_data'
        get 'json_jobs'
        post 'save'
        post 'delete'
        post 'execute'
      end
    end

    resources :jobroles

    resources :users

    resources :jobs

    resources :competences_upload

    resources :resultverbs


    resources :incomestructuredetails
    resources :incomestructures

    resources :inferential_results
    resources :inferential_functions
    resources :magnitudes do
        collection do
          post 'get_dimension'
        end
    end
    resources :dicisions
    resources :recomendations
    resources :knowledges
    resources :experiences
    resources :internal_situations
    resources :external_situations
    resources :experiencia_laborals
    resources :experiencia_en_cargos
    resources :tipo_movilidads
    resources :frecuencias
    resources :idiomas
    resources :nivel_hablados
    resources :nivel_escritos
    resources :nivel_lecturas
    resources :estudios
    resources :certificacions
    resources :seguridads
    resources :calidads
    resources :medio_ambientes
    resources :conocimiento_organizacionals
    resources :habilidades_funcionaless
    resources :caracteristicas_ambientales_cargos
    resources :areas
    resources :empresa_tipos
    resources :sector_experiences
    resources :sectors
    resources :tamano_empresas
    resources :modelo_competencias

    resources :utils

    match 'home' => 'home#index', as: 'home'

  end

  resources :job_evaluations do
    collection do
      get 'get_description'
      get 'select_position'
      delete 'destroy'
      post 'guardacomentario'
    end
  end

  resources :users do
    collection do
      get 'login'
      post 'login'
      get 'logout'
      post 'update_pass'
      post 'update_password'
    end
  end

  match 'utils(/:action)' => 'utils#index'

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.

  root :to => 'home#login'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
