  # -*- encoding : utf-8 -*-
set :application, "Essential" #Nombre de la aplicación
set :repository,  "git@bitbucket.org:desarrollorv/gestiondesempeno.git" #Repositorio que se clonará
set :branch, 'master' #Branch perteneciente al repositorio

set :scm, :git #Controlador de versiones que se usará
set :use_sudo, false
set :stage, 'production'
set :rvm_ruby_string, '2.3.1@test-gestion231'
#set :rvm_type, :system    # :user is the default
#set :rvm_ruby_string, ENV['GEM_HOME'].gsub(/.*\//,"") # Read from local system
#require "rvm/capistrano"

cliente = {path: 'test-gestion', port: 30019, subdomain: 'test-gestion'}
server = '159.89.158.32'
user   = 'www-data:www-data'

#role :web, "root@#{server}.rvelasco.cl"                          # Your HTTP server, Apache/etc
#role :app, "root@#{server}.rvelasco.cl"                          # This may be the same as your `Web` server
role :web, "root@#{server}"                          # Your HTTP server, Apache/etc
role :app, "root@#{server}"                          # This may be the same as your `Web` server

set :deploy_to, "/mnt/sites/#{cliente[:path]}"

set :current_path, "#{deploy_to}/current"
set :releases_path, "#{deploy_to}/releases"
set :shared_path, "#{deploy_to}/shared"
#set :git_enable_submodules, 1


##### REVISAR QUE SESSION STORE SEA DALLI

# PASOS
# 1) cap deploy:setup
# 2) cap deploy:update
# 3) cap deploy:copy_config
# 4) cap deploy:start_script
# 5) cap deploy:nginx_config
# Todos: cap deploy:setup && cap deploy:update && cap deploy:copy_config && cap deploy:start_script && cap deploy:nginx_config


set :shared_children, shared_children << 'tmp/sockets'

namespace :deploy do
  task :finalize_update do
    %w{logs tmp}.each do |i|
      run "rm -rf #{release_path}/#{i} && ln -s #{shared_path}/system/#{i} #{release_path}/#{i}"
    end
    %w{convert compensations dotacion}.each do |i|
      run "mkdir -p #{shared_path}/system/public/#{i}"
      run "rm -rf #{release_path}/public/#{i} && ln -s #{shared_path}/system/public/#{i} #{release_path}/public/#{i}"
    end
    ['database.yml', 'application.rb'].each do |i|
      run "rm -rf #{release_path}/config/#{i} && ln -s #{shared_path}/system/config/#{i} #{release_path}/config/#{i}"
    end
    ['dm-crud'].each do |i|
    	run "rm -rf #{release_path}/vendor/gems/#{i} && ln -s #{shared_path}/system/#{i} #{release_path}/vendor/gems/#{i}"
    end
    ['session_store.rb'].each do |i|
      run "rm -rf #{release_path}/config/initializers/#{i} && ln -s #{shared_path}/system/config/initializers/#{i} #{release_path}/config/initializers/#{i}"
    end
    # run "cd #{release_path} && ruby server_setup.rb start_script -d #{cliente[:path]} -p #{cliente[:port]}"
    run "touch #{current_path}"
    run "chown -R #{user} #{current_path}"
    run "touch #{release_path}/tmp/restart.txt"
  end

  desc 'Create start script'
  task :start_script, roles: :app do
    run "cd #{current_path} && ruby server_setup.rb start_script -d #{cliente[:path]} -p #{cliente[:port]}"
  end

  desc 'Copy config files'
  task :copy_config, roles: :app, except: {no_release: true} do
    run "cd #{shared_path}/system && cp -rf /mnt/sites/configs/gestion/* ."
  end

  desc 'Generate ngingx config'
  task :nginx_config, roles: :app, except: {no_release: true} do
    run "cd #{current_path} && ruby server_setup.rb config_nginx -d #{cliente[:path]} -p #{cliente[:port]} -s #{cliente[:subdomain]}"
  end

  desc "Start the application"
  task :start, :roles => :app, :except => { :no_release => true } do
    run "cd #{current_path} && screen -m -d -S #{cliente[:port]}-#{cliente[:subdomain]} -L rvm use 1.9.3 && ./start.sh"
  end

  # desc "Stop the application"
  # task :stop, :roles => :app, :except => { :no_release => true } do
  #   run "cd #{current_path} && RAILS_ENV=#{stage} bundle exec pumactl -S #{shared_path}/system/tmp/sockets/puma.state stop"
  # end

  # desc "Restart the application"
  # task :restart, :roles => :app, :except => { :no_release => true } do
  #   run "cd #{current_path} && RAILS_ENV=#{stage} bundle exec pumactl -S #{shared_path}/system/tmp/sockets/puma.state restart"
  # end

  # desc "Status of the application"
  # task :status, :roles => :app, :except => { :no_release => true } do
  #   run "cd #{current_path} && RAILS_ENV=#{stage} bundle exec pumactl -S #{shared_path}/system/tmp/sockets/puma.state stats"
  # end

  desc "Compile assets"
  task :assets, roles: :app, except: { no_release: true} do
    run "cd #{current_path} && rake assets:clean && rake assets:precompile"
  end

  desc "Bundle install"
  task :bundle_install, roles: :app, except: { no_release: true} do
    run "cd #{current_path} && bundle install"
  end

  desc "db:autoupgrade"
  task :db_upgrade, roles: :app, except: { no_release: true} do
    run "cd #{current_path} && rake db:autoupgrade"
  end
end

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
# namespace :deploy do
#   task :start do ; end
#   task :stop do ; end
#   task :restart, :roles => :app, :except => { :no_release => true } do
#     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
#   end
# end
