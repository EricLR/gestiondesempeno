# -*- encoding : utf-8 -*-
module EquidadInternaHelper

  def calcular_resumen_equidad data, tipo, income_id
    counter = 0
    data.each do |x|
      current_value = 0

      x[:incomes].each_with_index do |v, k|
        current_value += v[1][:income_value]
        break if v[0].to_i == income_id.to_i
      end
      
      tmp = x[:incomes][income_id.to_i]
      

      next if tmp[:banda_inferior].blank?||tmp[:banda_superior].blank?

      if tipo.to_s == 'rango'
        counter += 1 if( current_value >= tmp[:banda_inferior] && current_value <= tmp[:banda_superior] )
      elsif tipo.to_s == 'superior'
        counter += 1 if( current_value > tmp[:banda_superior] )
      elsif tipo.to_s == 'inferior'
        counter += 1 if( current_value < tmp[:banda_inferior] )
      else
        counter += 1
      end
    end

    counter
  end


end
