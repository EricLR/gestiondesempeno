# -*- encoding : utf-8 -*-
module ApplicationHelper

  def selected_position
    session[:evaluated_id] ? Position.get(session[:evaluated_id]) : nil
  end

	def controller_is?(value, module_name = nil)
    status = @controller.downcase == value.to_s.downcase

    if status && !module_name.nil? && !@module_name.nil?
      status = module_name.downcase == @module_name.downcase
    end

    status
  end

  def current_controller_is?(value, extra = true, cls = 'active')
    controller_is?(value) && extra ? cls : nil
  end

  def action_is?(value)
    value = value.to_s
    @action.downcase == value.downcase
  end




  def tipo_modelo? value = :no_usara
     return true if Rails.configuration.competencias == value
     return false
  end

  def job_evaluated
      Job.get(session[:job_evaluated_id]).blank? ? nil : Job.get(session[:job_evaluated_id])
  end

  def current_action_is?(value, class_name = 'current')
    values = value.class == Array ? value : [value]
    status = false
    values.each{|x| status = action_is?(x) if status.eql?(false)}

    status ? class_name : nil
  end

  def active_module_is?(controller, action, extra=true)
    (current_controller_is?(controller, true, true) && current_action_is?(action, true) && extra) ? 'active' : nil
  end

  def module_is?(value)
    return false if @module_name.nil?
    return @module_name.downcase == value.downcase
  end

  def sidebar
    render partial: 'partials/sidebar'
  end

  def menu
    render partial: 'partials/menu'
  end
  #parciales nuevo diseño
  def actions
    render partial: 'partials/actions'
  end

  def main
    render partial: 'partials/main'
  end

  def userprofile
    render partial: 'partials/userprofile'
  end

  def is_home?
    controller_is?('home')
  end

  def is_organization?
    controller_is?('organization') ||
    controller_is?('positions', 'organization') ||
    controller_is?('jobscharts', 'organization') ||
    controller_is?('positionscharts', 'organization') ||
    controller_is?('jobs', 'organization') ||
    controller_is?('users', 'organization')
  end

  def is_cargos?
    controller_is?('jobs', 'jobsdescription') ||
      (module_is?('mantenedores') && (
        controller_is?(:actionverbs) ||
        controller_is?(:resultverbs) ||
        controller_is?(:inferential_functions) ||
        controller_is?(:inferential_results) ||
        controller_is?(:magnitudes) ||
        controller_is?(:dicisions) ||
        controller_is?(:recomendations) ||
        controller_is?(:knowledges) ||
        controller_is?(:experiences) ||
        controller_is?(:internal_situations) ||
        controller_is?(:external_situations) ||
        controller_is?(:experiencia_laborals) ||
        controller_is?(:experiencia_en_cargos) ||
        controller_is?(:tipo_movilidads) ||
        controller_is?(:frecuencias) ||
        controller_is?(:idiomas) ||
        controller_is?(:nivel_hablados) ||
        controller_is?(:nivel_escritos) ||
        controller_is?(:nivel_lecturas) ||
        controller_is?(:estudios) ||
        controller_is?(:certificacions) ||
        controller_is?(:seguridads) ||
        controller_is?(:calidads) ||
        controller_is?(:medio_ambientes) ||
        controller_is?(:conocimiento_organizacionals) ||
        controller_is?(:habilidades_funcionaless) ||
        controller_is?(:caracteristicas_ambientales_cargos) ||
        controller_is?(:empresa_tipos) ||
        controller_is?(:sector_experiences) ||
        controller_is?(:sectors) ||
        controller_is?(:tamano_empresas)



      )
    )
  end

  def is_mantenedores_cargos?
    controller_is?('actionverbs') ||
    controller_is?('resultverbs') ||
    controller_is?('inferential_functions') ||
    controller_is?('inferential_results') ||
    controller_is?('magnitudes') ||
    controller_is?('dicisions') ||
    controller_is?('recomendations') ||
    controller_is?('knowledges') ||
    controller_is?('experiences') ||
    controller_is?('internal_situations') ||
    controller_is?('external_situations')

  end

  def is_mantenedores_cargos_perfiles?
    controller_is?('experiencia_laborals') ||
    controller_is?('experiencia_en_cargos') ||
    controller_is?('tipo_movilidads') ||
    controller_is?('frecuencias') ||
    controller_is?('idiomas') ||
    controller_is?('nivel_hablados') ||
    controller_is?('nivel_escritos') ||
    controller_is?('nivel_lecturas') ||
    controller_is?('estudios') ||
    controller_is?('certificacions') ||
    controller_is?('seguridads') ||
    controller_is?('calidads') ||
    controller_is?('medio_ambientes') ||
    controller_is?('conocimiento_organizacionals') ||
    controller_is?('habilidades_funcionaless') ||
    controller_is?('caracteristicas_ambientales_cargos') ||
    controller_is?('empresa_tipos') ||
    controller_is?('sector_experiences') ||
    controller_is?('sectors') ||
    controller_is?('tamano_empresas')
  end



  def is_mantenedores_gestion_desempeno?
    controller_is?('insert_planifications') ||
    controller_is?('periods') ||
    controller_is?('userperiods')


  end

  def is_companies?
    controller_is?('companies')
  end
  def is_performance?
    controller_is?('planifications', 'performance') ||
    controller_is?('monitoring', 'performance') ||
    controller_is?('agreements', 'performance') ||
    controller_is?('evaluations', 'performance') ||
    controller_is?('informes', 'performance') ||
    controller_is?('insert_planifications') ||
    controller_is?('periods') ||
    controller_is?('userperiods')
  end
  def is_compensacion?
    module_is?('compensacion')||current_controller_is?('incomestructures')||current_controller_is?('market_data')
  end

  def is_mantenedores?
    module_is?('mantenedores') && !(
        controller_is?(:actionverbs) ||
        controller_is?(:resultverbs) ||
        controller_is?(:inferential_functions) ||
        controller_is?(:inferential_results) ||
        controller_is?(:magnitudes) ||
        controller_is?(:dicisions) ||
        controller_is?(:recomendations) ||
        controller_is?(:knowledges) ||
        controller_is?(:experiences) ||
        controller_is?(:internal_situations) ||
        controller_is?(:external_situations) ||
        controller_is?(:experiencia_laborals) ||
        controller_is?(:experiencia_en_cargos) ||
        controller_is?(:tipo_movilidads) ||
        controller_is?(:frecuencias) ||
        controller_is?(:idiomas) ||
        controller_is?(:nivel_hablados)||
        controller_is?(:nivel_escritos) ||
        controller_is?(:nivel_lecturas) ||
        controller_is?(:estudios) ||
        controller_is?(:certificacions) ||
        controller_is?(:seguridads) ||
        controller_is?(:calidads) ||
        controller_is?(:medio_ambientes) ||
        controller_is?(:conocimiento_organizacionals) ||
        controller_is?(:habilidades_funcionaless) ||
        controller_is?(:caracteristicas_ambientales_cargos) ||
        controller_is?(:empresa_tipos) ||
        controller_is?(:sector_experiences) ||
        controller_is?(:sectors) ||
        controller_is?(:tamano_empresas)||
        controller_is?(:insert_planifications) ||
        controller_is?(:periods) ||
        controller_is?(:userperiods)||
        controller_is?(:modelo_competencias) ||
        controller_is?(:areas) ||
        controller_is?(:competence_types) ||
        controller_is?(:competences) ||
        controller_is?(:conducts)
    )
  end

  def is_mantenedores_competencias?
    controller_is?('modelo_competencias') ||
    controller_is?('areas') ||
    controller_is?('competence_types') ||
    controller_is?('competences') ||
    controller_is?('conducts')
  end

  def is_evaluacion?
    controller_is?('job_evaluations') ||
    controller_is?('job_evaluation_datas') ||
    controller_is?('job_evaluation_data_descriptions')
  end

  def editar_link ruta
    %{
      <a href="#{ruta}">
        <i class="icon-pencil" />
        Editar
      </a>
    }
  end

  def crud_bar options
    str = %{
      <ul class='nav nav-tabs not-print'>
    }
    options.each do |i|
      str << "<li#{' class="active"' unless i[2].blank?}>"

      str << crud_links(i)

      str += "</li>"
    end

      str << %{
        </ul>
      }

    raw str
  end

  def crud_options options

    str  = %{
      <div class="dropdown">
        <a class="data-toggle" data-toggle="dropdown">
          #{options[0]}
          <span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
    }
    options[1..-1].each do |i|
      str << '<li>'
      str << crud_links( i )
      str << '</li>'
    end
    str << %{
        </ul>
      </div>
    }

    raw str
  end

  def crud_links i
    str = ""
      if i[0].is_a?(String)
        str << %{
          <a href='#{i[1]}' data-loading-text="Cargando...">
            #{i[0]}
          </a>
        }
      elsif i[0] == :add
        str << %{
          <a href='#{i[1]}' data-loading-text="Cargando...">
            <span class='glyphicon glyphicon-plus'></i>
            Agregar
          </a>
        }
      elsif i[0] == :edit
        str << %{
          <a href='#{i[1]}' data-loading-text="Cargando...">
            <span class='glyphicon glyphicon-edit'></i>
            Editar
          </a>
        }
      elsif i[0] == :delete
        str << %{
          <a href='#{i[1]}' data-confirm="¿Está seguro de querer eliminar?" data-method="delete" data-loading-text="Cargando...">
            <span class='glyphicon glyphicon-trash'></i>
            Eliminar
          </a>
        }
      elsif i[0] == :show
        str << %{
          <a href='#{i[1]}' data-loading-text="Cargando...">
            <span class='glyphicon glyphicon-search'></i>
            Ver
          </a>
        }
      end
    raw str
  end

  def ckeditor
    str = ""
    str << "<link href='/stylesheets/ckeditor.css' rel='stylesheet' />"
    str << "<script src='/javascripts/ckeditor.js' />"
    str << "<script src='/javascripts/config.js' />"
    raw str
  end

  def btn_guardar txt = 'Guardar'
    raw %{
      <button class="btn btn-primary" name="button" type="submit" data-disable-with="Enviando...">
        <i class="glyphicon glyphicon-check"></i>
        #{txt}
      </button>
    }
  end

  def btn_subir txt = 'Subir'
    raw %{
      <button class="btn btn-primary" name="button" type="submit"  data-disable-with="Enviando...">
        <i class="glyphicon glyphicon-upload"></i>
        #{txt}
      </button>
    }
  end

  def notices
    render partial: 'partials/notices'
  end

  def decimales value
    number_to_currency value, unit: ''
  end

  def for_select cls, arr, first=""

    fnl = first.blank? ? [] : [ [first, ""] ]

    cls.const_get(arr).each do |i|
      fnl << [i, i.to_s.humanize.titleize, cls.const_get(i)]
    end
    fnl
  end

  def css_pdf(*sources)
    css_dir = WickedPdfHelper.root_path.join('public', 'stylesheets')
    css_text = sources.collect { |source|
      source = WickedPdfHelper.add_extension(source, 'css')
      "<style type='text/css'>#{File.read(css_dir.join(source))}</style>"
    }.join("\n")
    css_text.respond_to?(:html_safe) ? css_text.html_safe : css_text
  end


end
