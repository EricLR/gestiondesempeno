# -*- encoding : utf-8 -*-
module JobEvaluationsHelper
    def select_type type,selected,management
        unless management.blank?
            options_for_select JobEvaluationData.all(:kind => type, :management_type_id => management).map {|i| [i.name, i.id] }, selected
        else
            options_for_select JobEvaluationData.all(:kind => type).map {|i| [i.name, i.id] }, selected
        end
    end

    def selector type, item,management = ""
        size = "font-size:9px !important;"
        str = select :job_evaluation, type.to_s<<'_id', select_type(JobEvaluationData.const_get(type.to_s.upcase), value(item.send(type), :id),management), :style=> size,  :prompt => '-'

        str << raw("<script>")

        str << raw("$('#job_evaluation_#{type.to_s}_id').change(
                    function(){
                        $elm = $(this);
                        $.get('#{get_description_job_evaluations_path}?id=' + $(this).val(), function(content){
                            //$elm.parent().next().text(content);
                            $('#form_cargos').submit();

                        });

                    });")

        str << raw("</script>")

        str
    end
end
