# -*- encoding : utf-8 -*-
module RoleHelper

  def role_cann? pers
    return current_user.role.permissions.include? pers
  end

  def is_admin?
    return current_user.role.name.eql?("Administrador")
  end

  def mod_org
    return current_user.role.organizacion
  end

  def mod_gest
    return current_user.role.gestion_desempeno
  end

  def mod_desc
      return current_user.role.descripcion_cargos
  end

  def mod_eval
      return current_user.role.evaluacion_cargos
  end

  def mod_comp
      return current_user.role.adm_compensacion
  end

  def mod_dot
      return current_user.role.plan_dotacion
  end

  def mod_pres
      return current_user.role.adm_presupuesto
  end

end
