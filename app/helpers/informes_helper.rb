# -*- encoding : utf-8 -*-
module InformesHelper
    def draw_planificaciones(items, parent = nil, level = 0)
        
        str = ""
        
        if items

            items.each do |i|

                #str << %{ #{i.id} - }
                # tmp = 0
                # tmp_item = i
                # val = [tmp_item.id]
                # while tmp < level
                #     val << tmp_item.parent.id

                #     tmp_item = tmp_item.parent
                #     tmp += 1
                # end
                str << render( partial: 'performance/informes/evaluaciones_row', locals: {item: i})
                #str << %{ val: #{val.reverse.join('.')} - }

                #str << %{ #{i.name} <br />}

                if i.children.size > 0
                    str << draw_planificaciones(i.children, i, level+1)
                end
            end         
        else 
            unless parent.nil? 
                str << %{ #{i.id}/#{level} = #{i.name} <br />}
            end



            
        end 

        str
    end
end
