# -*- encoding : utf-8 -*-
module PerformanceHelper
    def planification_competences_class planification, competence
        if planification.blank? && competence.blank?
            return 'error'
        else
            return 'warning' if planification.blank? 
            return 'warning' if competence.blank? 

            unless planification.blank? && competence.blank?
                return 'success' if planification.closed && competence.closed

                return 'warning'
            end
        end
    end
    
    def solo_autoevaluacion?
        begin
            Rails.configuration.solo_autoevaluacion
        rescue
            return false
        end
    end
end
