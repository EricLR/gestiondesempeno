# -*- encoding : utf-8 -*-
class JobEvaluationDataDescriptionsController < ApplicationController
  before_filter :store_location, :only => :index
  #protect_from_forgery :except         => :destroy

  crud :job_evaluation_data_description, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_job_evaluation_data_descriptions_path'
end
