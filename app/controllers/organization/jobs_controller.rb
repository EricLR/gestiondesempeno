# -*- encoding : utf-8 -*-
class Organization::JobsController < ApplicationController
  before_filter :store_location, :only => [:index]
  protect_from_forgery :except => :destroy

  def index
    @items = Job.all(:order => [:boss_id.desc, :id.asc], :status => Job::ENABLED)
  end
  
  def new
    @item = Job.new
  end
  
  def create
    data = params[:job]

    @item = Job.new 
    
    data[:boss_id] = nil      if data[:boss_id].blank?
    data[:job_type_id] = nil  if data[:job_type_id].blank?
    data[:profile_id] = nil   if data[:profile_id].blank?
    
    @item.attributes = data
    if @item.save
      flash[:notice] = 'Se guardó correctamente el cargo'
    else
      flash[:error] = 'Ocurrió un error al guardar el cargo'
    end
    
    redirect_to organization_jobs_path
  end
  
  def edit
    @item = Job.get params[:id]
  end
  
  def update
    data = params[:job]
    @item = Job.get params[:id]
    
    data[:boss_id] = nil      if data[:boss_id].blank?
    data[:job_type_id] = nil  if data[:job_type_id].blank?
    data[:profile_id] = nil   if data[:profile_id].blank?
    
    if @item.update data
      flash[:notice] = 'Se guardó correctamente el cargo'
    else
      flash[:error] = 'Ocurrió un error al guardar el cargo'
      Rails.logger.info "ERRORES======"
       @item.errors.each do |e|
         Rails.logger.info e
       end
       Rails.logger.info "======"
    end
    redirect_to organization_jobs_path
  end
  
  def destroy
    @item = Job.get params[:id]
    positions = Position.all(job: @item)
    if @item.children.blank? && positions.blank?
      @item.status = Job::DISABLED
      if @item.save
        flash[:notice] = 'Se eliminó correctamente el cargo'
      else
        flash[:error] = 'Ocurrió un error al guardar el cargo'
      end
    elsif positions.size > 0
      flash[:error] = 'No se puede eliminar el cargo ya que existen personas asociadas a este'
    else
      flash[:error] = 'No puede eliminar el cargo ya que tiene dependientes'
    end
    
    redirect_to organization_jobs_path
  end

end
