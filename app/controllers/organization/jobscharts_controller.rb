# -*- encoding : utf-8 -*-
class Organization::JobschartsController < ApplicationController
  
  def index
    @items = Job.all(:order => [:boss_id.desc, :id.asc], :status => Job::ENABLED)
  end
  
end
