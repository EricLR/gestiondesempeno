# -*- encoding : utf-8 -*-
class Organization::UsersController < ApplicationController

	before_filter :store_location, :only => [:index]

	crud :user, :title_attribute => :rut, :redirect_to_url => 'organization_users_path'

	def successful_action
        if !params[:user][:rut].blank?
            @item.update rut: params[:user][:rut].gsub(".", "")
        end
        if !params[:user][:password].blank?
            @item.update password:  Digest::MD5.hexdigest(params[:user][:password])
        end		
		if params.has_key?(:position) and !params[:position].blank?
			job_id = params[:position][:job_id]

			unless job_id.blank?
				position = Position.first_or_create( {user_id: @item.id}, {job_id: job_id})

				position.update job_id: job_id if position.job_id != job_id
			end

		end
	end
end
