# -*- encoding : utf-8 -*-
class Organization::PositionsController < ApplicationController

	before_filter :store_location, :only => [:index]
    protect_from_forgery :except => :destroy

	crud :position, :redirect_to_url => 'organization_positions_path', :use_class_name_as_title => true
  
	def destroy

		if @item.destroy!
		  successful_destroy
		else
		  failed_destroy
		end

	end

end
