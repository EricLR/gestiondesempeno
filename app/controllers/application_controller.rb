# -*- encoding : utf-8 -*-
require 'dm-rails/middleware/identity_map'
class ApplicationController < ActionController::Base
    # use Rails::DataMapper::Middleware::IdentityMap, :rvelasco
    use Rails::DataMapper::Middleware::IdentityMap
    protect_from_forgery except: [:masa_salarial]

    helper_method :current_user_id, :current_user, :evaluator, :evaluated, :current_period, :current_role, :current_role_can?, :value, :conectado?, :is_admin?
    helper_method :show_sidebar?, :'role_can?', :current_job, :'cliente?'


    before_filter :init
    before_filter :must_login


    # TODO teng queasdkalsd
    # FIXME funcionma lmalala mlamsld
    def init

        DataMapper::Model.raise_on_save_failure = false

        my_class_name = self.class.name
        if my_class_name.index("::").nil? then
          @module_name = nil
        else
          @module_name = my_class_name.split("::").first
        end
        @controller = controller_name
        @action = action_name

    end

    def oo_convert input_file, output_file
        system "#{GestionDesempeno.java_oo} #{input_file} #{output_file}"
    end

    protected

    def controller_is?(value, module_name = nil)
        status = @controller.downcase == value.downcase

        module_name = module_name.downcase unless module_name.nil?

        if status
          status = module_name == @module_name.downcase unless @module_name.nil?
        end

        status
    end

    def action_is?(value)
        @action.downcase == value.downcase
    end

    def must_login
        if !(controller_is?('users') && (action_is?('login') || action_is?('logout'))) && current_user.nil?
            reset_session
            redirect_to login_users_path
        end
    end

    def current_user
        return nil unless session.has_key?(:user)

        if session[:user].blank?
            session[:user] = User.get(session[:user].id)
        end

        return session[:user]
    end

    def conectado?
        current_user.nil? ? false : true
    end

    def current_user_id
        current_user.id
    end

    def current_period
        Period.current
    end

    def cliente? es
        Rails.configuration.cliente.to_s.eql? (es).to_s
    end
    def current_role

        role = Role.first(:name.like => current_user.role.name)
        #puts role.inspect.red.bold
        #if Rails.env.production?
        #    return session[:role] unless session[:role].blank?
        #end

        #if session[:admin_role].blank?
        #    session[:admin_role] = Role.first :name.like => 'Admin%'
        #end

        ## if Rails.env.development?
        ##     admin_role = Role.get 1 #Role.first :name.like => 'Admin%'
        ## else
        ##     admin_role = session[:admin_role]
        ## end



        #if session[:user_kind].eql?(:admin)
        #    return admin_role
        #end
        #if session[:default_role].blank?
        #    session[:default_role] = Role.first :name.like => 'Limitado%'
        #end
        #default_role = session[:default_role]
        #role = current_user.position && current_user.position.job && current_user.position.job.role ? current_user.position.job.role : default_role
        #session[:role] = role

        role
    end

    def current_job
        current_user.position && current_user.position.job ? current_user.position.job : nil
    end

    def role_can? permission
        role = current_role#Rails.env.development? ? Role.first( :name.like => 'Admin%' ) : current_role

        role.blank? ? false : role.can?( permission )
    end

    def is_admin?
        current_role_can? :mantenedores
    end

    def show_sidebar?
        !controller_is?('organization')
    end

    def current_role_can?(prop, extra_hide = false)
        return false if current_role.nil?
        return false if extra_hide

        return current_role.send(prop)
    end

    def evaluator
        Position.first :user_id => current_user.user_id
    end

    def evaluated
        Position.get session[:evaluated_id]
    end

    def store_location
        logger.info "> Store Location: #{request.path}"
        session[:return_to] = request.path
    end

    def redirect_back_or_default default = '/'
        default = '/' if default.nil?
        logger.info "> Return to: #{session[:return_to]}"
        redirect_to(session[:return_to] || default)
        session[:return_to] = nil
    end

    def value item, property, default = '', default2 = ''
        val = item.nil? ? (default.class != String ? default2 : default) : item.send(property)
        if !item.nil? && default.class != String
          val = item.send(property).nil? ? default2 : item.send(property).send(default)
        end

        return val
    end

    def sel items, opts = {default: nil}
        return opts[:default] if items.blank?
        default = opts.delete :default
        tmp = items.select { |e| t = opts.map { |m| e[m.first] == m.last }.uniq; t.blank? ? default : t.first == true }


    end

    def dm_to_a dm_collection, fields = []
        dm_collection.map do |x|
            if fields.blank?
                x.attributes
            else
                tmp = {}
                fields.each{|v| tmp[v.to_sym] = x[v]}

                tmp
            end
        end
    end

    def dm_a model, options = {}
        options[:object_relation] = false if options[:object_relation].nil?

        #model_opts   = options[:only].blank? ? {} : {fields: options[:only]}
        model_opts    = options.except(:child).except(:parent).except(:object_relation)
        dm_collection = model.all model_opts

        #onlys = (tmp=options.delete(:only)).blank? ? [] : [tmp]
        data  = dm_to_a dm_collection, model_opts[:fields]

        dm_a_data dm_collection, data, options

        data
        #dm_a_data dm_collection, true, options
    end
    def dm_a_data( collection, data, options, nivel = 1 )
        collection_name = collection.model.to_s

        # 1er nivel
        unless options[:child].blank?
            options[:child].each do |c1_name, c1_opts|

                if collection.respond_to? c1_name
                    model_opts    = c1_opts.except(:child).except(:parent).except(:object_relation).except(:subchild)

                    tmp   = collection.__send__(c1_name).all model_opts

                    tmp_a = dm_to_a(tmp, model_opts[:fields])



                    objs = dm_a_get_data data, tmp_a, collection_name, c1_name, c1_opts, :child

                    unless c1_opts[:subchild].blank?
                        c1_opts[:subchild].each do |c2_name, c2_opts|
                            if tmp.respond_to? c2_name
                                tmp2   = tmp.__send__(c2_name, c2_opts)
                                tmp2_a = tmp2.to_a

                                dm_a_get_data data, tmp_a, collection_name, c2_name, c2_opts, :child, tmp2_a
                            end
                        end
                    end


                end
            end
        end

        unless options[:parent].blank?
            options[:parent].each do |c1_name, c1_opts|

                if collection.respond_to? c1_name
                    model_opts    = c1_opts.except(:child).except(:parent).except(:object_relation)

                    tmp   = collection.__send__(c1_name).all model_opts
                    tmp_a = dm_to_a(tmp, model_opts[:fields])

                    objs = dm_a_get_data data, tmp_a, collection_name, c1_name, c1_opts, :parent

                    unless c1_opts[:subparent].blank?
                        c1_opts[:subparent].each do |c2_name, c2_opts|
                            if tmp.respond_to? c2_name
                                tmp2   = tmp.__send__(c2_name)
                                tmp2_a = tmp2.to_a

                                dm_a_get_data data, tmp_a, collection_name, c2_name, c2_opts, :parent, tmp2_a
                            end
                        end
                    end


                end
            end
        end
    end

    def dm_a_get_data data, collection, parent_name, name, options, kind = :child, extra = false

        tmp = []
        parent_name += "_" unless parent_name.blank?

        data.each_with_index do |item, k|
            #logger.info "Item: #{item} Kind: #{kind}".red
            if kind == :child
                if extra != false

                    obj = collection.select{|x| x["#{parent_name.to_s.singularize.downcase}id".to_sym] == item[:id]}.first
                    next if obj.blank?
                    obj = extra.select{|x| x.id == obj["#{name.to_s.singularize.downcase}_id".to_sym]}.first

                else
                    obj = collection.select{|x| x["#{parent_name.to_s.singularize.downcase}id".to_sym] == item[extra ? extra : :id]}.first

                end
            end
            if kind == :parent
                if extra != false
                    # logger.info "KEY: #{name.to_s.singularize.downcase}_id EXTRA: #{extra}".bold.green
                    obj = collection.select{|x| x[:id] == item["#{name.to_s.singularize.downcase}_id".to_sym]}.first
                    next if obj.blank?
                    obj = extra.select{|x| x.id == obj["#{name.to_s.singularize.downcase}_id".to_sym]}.first

                else
                    #logger.info "KEY: #{name.to_s.singularize.downcase}_id".bold.green
                    obj = collection.select{|x| x[:id] == item["#{name.to_s.singularize.downcase}_id".to_sym]}.first

                end
            end




            unless obj.blank?
                tmp << obj

                if options[:object_relation]
                    data[k][name.to_sym] = obj.as_json options
                else
                    data[k] = item.merge Hash[obj.as_json(options).map{|k,v| ["#{name}_#{k}".to_sym,v]}]
                end
            end
        end

        tmp
    end









    ###### CACHE SSI
  DEFAULT_TIME = Rails.env.production? ? 14.days : 0.seconds

  def do_cache?
      DEFAULT_TIME > 0.minutes
  end

  def do_cache template, opts = {layout: false}
      # log "________ PARAMS: #{params.inspect}".red
      # log "PATH: #{request.path_info + "?" + params.slice("filtro", "orden").reduce(""){ |s,(k,v)| s += "#{k}=#{v}&" }}".bold
      # if Padrino.env.eql? :production
      content = nil
      use_cache = opts[:cache].blank? ? true : opts.delete(:cache)
      use_cache = false unless do_cache?

      # str = request.path_info + "?" + params.slice("filtro", "orden").sort.reduce(""){ |s,(k,v)| s += "#{k}=#{v}&" }

      logger.info "___--------- FILE CACHE PATH: #{get_cache_file_path}".red

      content = use_cache ? get_cache : nil


      if content.blank?
          if block_given?
              yield
              #content = render(template)
              content = render_to_string(template, opts)
              save_cache content
          end
      end


      CacheSsi::process_includes(content)
      # unless params.empty?
      #   str = request.path_info + "?" + params.slice("filtro", "orden").reduce(""){ |s,(k,v)| s += "#{k}=#{v}&" }

      #   cache_key str
      # end

      # expires_in time
      # end
  end

  def get_cache_file_path
      uri       = request.env['PATH_INFO'] # Ruta completa para el cache
      filetype  = uri[-3, uri.length] == 'pdf' ? '.pdf' : '.html' ### REVISAR, NO SIRVE...

      file_name = File.join( Rails.root, 'tmp', 'cache', uri )+filetype # Archivo que guarda el contenido
      if file_name[-(filetype.length+1), file_name.length] == "/#{filetype}"
         file_name = file_name.gsub(filetype, "index#{filetype}")
      end

      qs = request.query_parameters.to_query
      unless qs.blank?
        file_name += "?#{qs}"
      end

      cache_dir = File.dirname( file_name ) # Donde se guarda el archivo

      FileUtils.mkdir_p cache_dir # Creo la carpeta y subcarpetas correspondientes

      return file_name
  end

  def get_cache
      file_name = get_cache_file_path
      content   = nil

      if File.exists?(file_name)
        content = File.read(file_name)

        content = CacheSsi.valid? content


      end

      content
  end

  def save_cache content, time = DEFAULT_TIME
      file_name = get_cache_file_path
      FileUtils.rm_rf get_cache_file_path
      void_at = (Time.now+time).to_i

      File.write(file_name, "[cache:#{void_at}]\n#{content}")

      content
  end

  def inc_ssi file
      "<!--#include file=\"#{file}\" -->".html_safe
  end

  ##### END CACHE SSI

  def planilla_data evaluated_id, evaluator_id = nil, period_id = nil
      calculo = lambda do |x|
        return 0 if x < 84.0
        return 1 if x >= 84.0 and x < 94.0
        return 2 if x >= 94.0 and x < 104.0
        return 3 if x >= 104.0 and x < 114.0
        return 4 if x >= 114.0
      end
      #logger.info "evaluated #{evaluated_id} evaluator #{evaluator_id}".blue.bold
      #period_id = current_period.id unless current_period.blank?
      #evaluator_id = evaluated_id if evaluator_id.blank?

      #period_id    = period_id #current_period.id
      if evaluator_id == evaluated_id
          p_kpi        = Planification.all(evaluated_id: evaluated_id, evaluator_id: evaluator_id, period_id: period_id)
      elsif evaluator_id != evaluated_id && !evaluator_id.blank?
          p_kpi        = Planification.all(evaluated_id: evaluated_id, evaluator_id: evaluator_id, period_id: period_id)
      else
          p_kpi        = Planification.all(evaluated_id: evaluated_id, period_id: period_id)
      end
      #logger.info "p_kpi #{p_kpi.inspect}".green.bold
      _kpi         = p_kpi.key_performance_indicator
      if evaluator_id == evaluated_id
          p_competences = PlanificationCompetence.all(evaluated_id: evaluated_id, evaluator_id: evaluator_id, period_id: period_id)
          #logger.info "p_competences #{p_competences.inspect}".blue.bold
      elsif evaluator_id != evaluated_id && !evaluator_id.blank?
          p_competences = PlanificationCompetence.all(evaluated_id: evaluated_id, evaluator_id: evaluator_id, period_id: period_id)
      else
          p_competences = PlanificationCompetence.all(evaluated_id: evaluated_id, period_id: period_id)
      end
      competences   = p_competences.competences
      #logger.info "competences #{competences.inspect}".blue.bold
      conducts      = competences.conducts
      comp_types    = competences.competence_type
      t_kpis        = TracingKpi.all  kind: TracingKpi::EVALUACION,
                      closed: 1,
                      #evaluator_id: evaluator_id,
                      evaluated_id: evaluated_id,
                      period_id: period_id

    #   logger.info "t_kpis: #{t_kpis.inspect}".yellow
      kpi = []
      _kpi.each do |_k|
        _p_k = p_kpi.find{|x| x.key_performance_indicator_id == _k.id}
        _tmp_kpi = _k.attributes

        _tmp_2 = t_kpis.select{|x| x.planification_id.to_i == _p_k[:id].to_i}.sort_by(&:created_at)
        # logger.info "OMG: #{_tmp_2.inspect}".red
        _tmp_kpi = _tmp_kpi.merge(_tmp_2.last.attributes) unless _tmp_2.blank?
        _tmp_kpi[:extra] = {tmp2: _tmp_2, p_k: _p_k, search: t_kpis.select{|x| x.planification_id.to_i == _p_k[:id].to_i}}
        _tmp_kpi[:id] = _p_k[:id]

        #logger.info "_tmp_2[:increase] #{_tmp_2 unless _tmp_2.blank?}".red.bold
        #logger.info "asdasdasdasd #{_p_k.key_performance_indicator.ponderation}".red.bold
        increase_tmp =0
        _tmp_2.each do |intmp|
            if _p_k.key_performance_indicator.code == 5
                (calculo.(intmp[:increment].to_f) * _p_k.key_performance_indicator.ponderation.to_f) / 100
            else
                (intmp[:increment].to_f * _p_k.key_performance_indicator.ponderation.to_f) / 100
            end

        end
        _tmp_kpi[:increment] = _tmp_2[0][:increment] unless _tmp_2.blank?
        _tmp_kpi[:code] = _p_k.key_performance_indicator.code
        kpi << _tmp_kpi
      end

      t_comps     = TracingCompetence.all(kind: TracingCompetence::EVALUACION,
                          evaluated_id: evaluated_id,
                          closed: 1,
                          period_id: period_id)

      comp_data = []

      comp_types.each do |i|
        tmp = {id: i[:id], name: i[:name], evaluable: i[:evaluable], competences: []}
        competences.select{|x| x[:competence_type_id] == i[:id]}.each do |c|
          _p_c = p_competences.find{|x| x.competence_id == c.id}
          #logger.info "P_ competencias #{_p_c.inspect}".green.bold
          _t   = c.attributes
          _ct  = t_comps.select{|_c| _c.planification_competence_id == _p_c.id}

          _t   = _t.merge(_ct.last.attributes) unless _ct.blank?
          _t[:id] = _p_c[:id]
          _t[:ponderation] =  _p_c[:ponderation]
          # Select:  && (_p_c.conduct.blank? ? false : JSON.parse(_p_c.conduct).include?(cd.number.to_s))
          # Revisar, da error cuando el json es inválido
          #_t[:conducts] = conducts.select{|cd| cd.competence_id == c.id}.map(&:description).join('</li><li>')
          #unless _t[:conducts].blank?
          #  _t[:conducts] = "<ul><li>#{_t[:conducts]}</li></ul>"
          #end
          tmp[:competences] << _t

        end
        comp_data << tmp
      end
      comment = Agreement.all evaluator_id: evaluator_id,
                  evaluated_id: evaluated_id,
                  period_id: period_id


      {kpi: {planification: p_kpi, data: kpi}, competences: {planification: p_competences, data: comp_data}, last_comment: comment.blank? ? '' : comment.last.detalle}
    end
    def planilla_data_nota evaluated_id, evaluator_id = nil, period_id = nil

      calculo_porcentaje = lambda do |x|
        #return 0 if x < 84.0
        #return 1 if x >= 84.0 and x < 94.0
        #return 2 if x >= 94.0 and x < 104.0
        #return 3 if x >= 104.0 and x < 114.0
        #return 4 if x >= 114.0
        return 80	 if x == 1.0
        return 81	 if x == 1.1
        return 82	 if x == 1.2
        return 83	 if x == 1.3
        return 84	 if x == 1.4
        return 85	 if x == 1.5
        return 86	 if x == 1.6
        return 87	 if x == 1.7
        return 88	 if x == 1.8
        return 89	 if x == 1.9
        return 90	 if x == 2.0
        return 91	 if x == 2.1
        return 92	 if x == 2.2
        return 93	 if x == 2.3
        return 94	 if x == 2.4
        return 95	 if x == 2.5
        return 96	 if x == 2.6
        return 97	 if x == 2.7
        return 98	 if x == 2.8
        return 99	 if x == 2.9
        return 100 if x == 	3.0
        return 101 if x == 	3.1
        return 102 if x == 	3.2
        return 103 if x == 	3.3
        return 104 if x == 	3.4
        return 105 if x == 	3.5
        return 106 if x == 	3.6
        return 107 if x == 	3.7
        return 108 if x == 	3.8
        return 109 if x == 	3.9
        return 110 if x == 	4.0
        return 111 if x == 	4.1
        return 112 if x == 	4.2
        return 113 if x == 	4.3
        return 114 if x == 	4.4
        return 115 if x == 	4.5
        return 116 if x == 	4.6
        return 117 if x == 	4.7
        return 118 if x == 	4.8
        return 119 if x == 	4.9
        return 120 if x == 	5.0
      end

      data = planilla_data evaluated_id, evaluator_id, period_id


      rendimiento = 0
      kpi_aprobe = nil
      unless data[:kpi][:data].blank?
        vals        = data[:kpi][:data].map{|x|
                          kpi_aprobe= data[:kpi][:data][0][:aprobe]
                          #if (x[:code] != 5 ) #DESCOMENTAR
                              #(calculo.(x[:increment].to_f) * x[:ponderation].to_f) / 100 #DESCOMENTAR
                          #else  #DESCOMENTAR
                              #(x[:increase].to_f * x[:ponderation].to_f) / 100 #DESCOMENTAR
                          #end    #DESCOMENTAR
                          (x[:increment].to_f * x[:ponderation].to_f) / 100
                          #logger.info "INCREMENT #{x[:increment].to_f}  PONDERATIO #{x[:ponderation].to_f}".red.bold
                          #x[:increment].to_f  #COMENTAR
                      }
        #data[:kpi][:data].map{|x| logger.info "INCREMENT #{x[:increment]} *  PONDERATIO #{x[:ponderation]} / 100 ".red.bold}
        #logger.info "vals #{vals.inspect}".red.bold

        rendimiento = vals.sum #DESCOMENTAR PARA 2018
        #rendimiento = vals.sum / vals.length #COMENTAR PARA 2018
      end
      rend_cal = rendimiento


      competencias = []
      competencia_aprobe = nil
      unless data[:competences][:data].blank?
        vals = 0
        data[:competences][:data].each do |c|
          next unless c[:evaluable]
          competencia_aprobe = c[:competences][0][:aprobe]
          vals = c[:competences].map{|x| ((x[:increase].to_f != 0.0 ? x[:increase].to_f + 1 : x[:increase].to_f) * x[:ponderation].to_f) / 100}
          #c[:competences].map{|x| logger.info "#{x.inspect} ".green.bold}
          #tmp  = vals.sum / vals.size.to_f
          competencias << vals.sum
        end
      end
      #logger.info "competencias #{competencias.sum} ".blue.bold
      #logger.info "kpi #{rend_cal} ".blue.bold
      tot_competencias = (competencias.sum)
      comp_cal = tot_competencias

      calculo_palabras = lambda do |x|
        return "NO EVALUADO"           if x == 0.0
        return "Bajo lo esperado"      if x >  0.0 and x < 1.5
        return "Próximo a lo esperado" if x >= 1.5 and x < 2.5
        return "Resultado esperado"    if x >= 2.5 and x < 3.5
        return "Sobre lo esperado"     if x >= 3.5 and x < 4.5
        return "Excepcional"           if x >= 4.5
      end
      #matriz = [
      #  ['P0R0', 'Diamante en bruto', 'Estrella emergente', 'Futuro líder'],
      #  ['P1R0', 'Dilema', 'Futuro prometedor', 'Estrella emergente'],
      #  ['P2R0', 'Bajo desarrollo', 'Dilema', 'Profesional Experimentado']
      #]

      #tipo = Estamento.first(id: Position.first(user_id: evaluated_id).job.estamento_id)
      #if !tipo.blank? && tipo.id == 14
      #    resul= comp_cal #(rend_cal.to_f * 0.7) + (comp_cal.to_f*0.3)
      #elsif !tipo.blank? && tipo.id == 9
      #    resul= rend_cal #(rend_cal.to_f * 0.7) + (comp_cal.to_f*0.3)
      #else
      #    resul= (rend_cal.to_f * 0.7) + (comp_cal.to_f*0.3)
      #end
      resul= (rend_cal.to_f * 0.7) + (comp_cal.to_f*0.3)
      #logger.info "competencias nota:  #{comp_cal.round(1)}".red.bold
      #logger.info "competencias nota:  #{comp_cal.round(2)}".red.bold
      #if comp_cal.round(2) == 2.92
      #    logger.info "calculo_porcentaje.(comp_cal.round(1)) #{calculo_porcentaje.(comp_cal.round(1))}".yellow.bold
      #end

      {
        #tipo_cargo: tipo,
        evaluated_id: evaluated_id,
        competence_aprobe: competencia_aprobe,
        kpi_aprobe: kpi_aprobe,
        rendimiento: rendimiento.round(2),
        rendimiento_calculado: calculo_palabras.(rend_cal.round(2)),
        competencias: tot_competencias.round(2),
        competencias_calculado: calculo_palabras.(comp_cal.round(2)),
        competencias_porcentaje: calculo_porcentaje.(comp_cal.round(1)),
        resultado: resul.round(2),
        resultado_calculado: calculo_palabras.(resul.round(2))
      }
    end
end
