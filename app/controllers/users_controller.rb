# -*- encoding : utf-8 -*-
class UsersController < ApplicationController
  before_filter :store_location, only: :edit

  # METODOS DE CONEXIÓN
  def login
    if params.has_key?(:login)
      login = params[:login]

      if current_user.nil?
        data = Account.authenticate login[:rut], login[:password]
        # data = User.login login[:rut].gsub(".", ""), login[:password] if data.blank?

        if data
          session[:user] = data
          session[:user_kind] = data.role

          if data.first_time
            flash[:error] = "Si es primera vez que ingresa debe cambiar su contraseña"
            must_login
          else
            redirect_to root_path
          end
        else
          flash[:error] = "Los datos ingresados no son correctos"
          must_login
        end
      end
    end
  end
=begin
    # METODOS DE CONEXIÓN
    def login
        if p = params[:login]
            if !params[:login].include?('password_new')
                if !params[:login].include?('rut_recover')
                    session[:user_kind] = nil
                    if current_user.nil?
                        rut = p[:rut]
                        if rut.include?("@") # ES UN CORREO
                            session[:user_kind] = :admin
                            data = Account.authenticate rut, p[:password]
                        else # ES RUT
                            data = User.login p[:rut].gsub(".", ""), p[:password]
                            session[:user_kind] = :user
                        end

                        if data
                            session[:user] = data
                            if data.first_time
                                flash[:error] = "Si es primera vez que ingresa debe cambiar su contraseña"
                                must_login
                            else
                                redirect_to root_path
                            end
                            session[:user] = data
                            #redirect_to is_admin? ? root_path : performance_planifications_path
                            #redirect_to root_path
                        else
                            flash[:error] = "Los datos ingresados no son correctos"
                            must_login
                        end
                    end
                else
                    recover_pass p
                end
            else
                update_pass p
            end
        end
    end
=end

  def recover_pass par
    user = User.first(:rut => par[:rut_recover].gsub(".", ""))

    if !user.blank?
      if((!user.email.blank?) || (user.email.length > 13))
        if !user.first_time
          passasd = [*('a'..'z'),*('0'..'9')].shuffle[0,6].join
          user.update password: Digest::MD5.hexdigest(passasd)

          #Correo
          RecoverMailer.enviar_solicitud(user, passasd).deliver
          #Correo

          flash[:notice] = "Su nueva contraseña a sido enviada al correo #{user.email}"
          must_login
        else
          flash[:error] = "Si es primera vez que ingresa debe cambiar su contraseña"
          must_login
        end
      else
        flash[:error] = "Este usuario no posee un Email registrado en el sistema"
        must_login
      end
    else
      flash[:error] = "Rut no corresponde"
      must_login
    end
  end

  def logout
      reset_session
      session[:user] = nil unless current_user.nil?
      redirect_to root_path
  end

  def edit
      @item = current_user
  end

  def update
      data = params[:user]
      password = data.delete :password
      password_confirmation = data.delete :password_confirmation
      current_user.update data

      if !password.blank? && password == password_confirmation
          current_user.update password: Digest::MD5.hexdigest(password)
      end

      redirect_back_or_default
  end

  def update_pass prms
      if user = User.first(:rut => prms[:rut].gsub(".", ""), :password => Digest::MD5.hexdigest(prms[:password]))#Digest::MD5.hexdigest(params[:old_password]).eql?( current_user.password )
          if prms[:password_new].eql?(prms[:password_rep])
              if prms[:password_new].length > 4
                  user.update({password: Digest::MD5.hexdigest( prms[:password_new] ), first_time: false})
                  flash[:notice] = "Cambio exitoso. Ingrese con su nueva contraseña"
                  must_login
              else
                  flash[:error] = "La contraseña debe tener mas de 4 caracteres"
                  must_login
              end
          else
              flash[:error] = "Las contraseñas ingresadas no coinciden"
              must_login
          end
      else
          flash[:error] = "Rut o Contraseña anterior no corresponde"
          must_login
      end
  end

  def update_password
      status  = 'success'
      mensaje = ''
      prms = params[:user_pass]

      if Digest::MD5.hexdigest(prms[:password_old]).eql?( current_user.password )
          if prms[:password].eql?(prms[:password_repeat])
              if prms[:password].length > 0
                  current_user.update password: Digest::MD5.hexdigest( prms[:password] )
                  mensaje = 'Contraseña modificada. Puede cerrar esta ventana.'
              else
                  status  = 'error'
                  mensaje = 'La contraseña debe tener más de 4 caracteres'
              end
          else
              status  = 'error'
              mensaje = 'Las nuevas contraseñas no son idénticas'
          end
      else
          status  = 'error'
          mensaje = 'La contraseña antigua no coincide con la asociada al usuario'
      end

      render :json => {status: status, mensaje: mensaje}
  end
end
