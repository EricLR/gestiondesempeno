# -*- encoding : utf-8 -*-
class Performance::EvaluationsController < ApplicationController
    before_filter :store_location, :only => [:index]
    #before_filter :has_evaluated?, :only => [:index, :matriz_evaluacion]

    crud :evaluation, :use_class_name_as_title => true

    def index
        evaluator_id = (Position.first(user_id: current_user.id).id
        @item = Evaluation.find_or_create period_id: current_period.id, evaluated_id: evaluated.id, evaluator_id: evaluator_id)

        @item.kpi_result = 0

        TracingKpi.all(:evaluated => evaluated, :kind => TracingKpi::EVALUACION, period_id: current_period.id).each do |i|
            kpi = i.planification.key_performance_indicator
            result = kpi.ponderation == 0 ? 0 : (i.increment * kpi.ponderation/100)

            @item.kpi_result += result

        end

        logger.info ">>> COMPETENCIAS"

        @item.competence_result = 0
        TracingCompetence.all(evaluated_id: evaluated.id, period_id: current_period.id, kind: TracingCompetence::EVALUACION).each do |i|
          if Rails.configuration.tipo_conductas.eql? :factores_claves
            pond_value = i.planification_competence.ponderation.to_f / 100.0
            # logger.info "Ponderacion: #{pond_value} resultado evaluacion: #{((i.evaluation_result*100)/5)} total ponderado: #{((i.evaluation_result*100)/5) * pond_value}".red
            @item.competence_result +=  ((i.increase*100)/5) * pond_value
          elsif Rails.configuration.tipo_conductas.eql? :niveles
            pond_value = i.planification_competence.ponderation.to_f / 100.0
            # logger.info "Ponderacion: #{pond_value} real: #{i.evaluation_result} resultado evaluacion: #{(i.evaluation_result.eql?(1) ? 0 : 100)} total ponderado: #{(i.evaluation_result.eql?(1) ? 0 : 100) * pond_value}".red
            @item.competence_result +=  (i.increase.eql?(1) ? 100 : 0) * pond_value
          end
        end

=begin
        TracingCompetence.all(:evaluated => evaluated, :kind => TracingCompetence::EVALUACION).each do |i|
            pond_value = i.planification_competence.ponderation

            result = pond_value > 0 ? i.increase * pond_value/100 : 0

            @item.competence_result += result
        end
=end

        @item.save

        @ponderation_total = @item.kpi_ponderation + @item.competence_ponderation + @item.boss_ponderation
    end

    #### PARA INFORME DE AUTO-EVALUACIONES, SQL:
    ## SELECT rut, concat(first_name, ' ', last_name) AS 'Nombre', j.name AS 'Cargo', (SELECT IF(COUNT(id)>0, 'Si', 'No') FROM tracing_kpis WHERE evaluator_id = evaluated_id AND evaluator_id = p.id) AS 'Autoevaluacion'
    # FROM `maserrazuriz-datos`.`users` u
    # INNER JOIN `maserrazuriz-datos`.`positions` p ON u.id = p.user_id
    # INNER JOIN `maserrazuriz-datos`.`jobs` j ON j.id = p.job_id

    def matriz_evaluacion
      session[:evaluated_id] = params[:evaluated_id]
      logger.info "PARAMS EVALUATED_ID #{params[:evaluated_id]}".yellow.bold
      u = Position.first(user_id: params[:evaluated_id].to_i)
      update_opts= {evaluated_id: 0, evaluator_id: 0,kpi_result:0,kpi_final_result:"",
                    competence_result:0,competence_final_result:"",kpi_competence_result:0,
                    final_result:"", evaluator_closed:0, evaluated_closed:0}
      @data = planilla_data_nota u.user_id, nil, current_period.id #, current_user.position.id
      if !@data.blank?
          unless @data[:resultado].to_i == 0
              puts @data.inspect.red.bold
              ev = Evaluation.first(evaluated_id: @data[:evaluated_id], period_id: current_period.id)
              if ev.blank?
                  ev = Evaluation.new(evaluated_id: @data[:evaluated_id],period_id: current_period.id)
                  ev.evaluator_id = u.boss_position.user_id
                  ev.kpi_result= @data[:rendimiento]
                  ev.kpi_final_result= @data[:rendimiento_calculado]
                  ev.competence_result = @data[:competencias]
                  ev.competence_final_result = @data[:competencias_calculado]
                  ev.competence_ponderation = @data[:competencias_porcentaje]
                  ev.kpi_competence_result = @data[:resultado]
                  ev.final_result = @data[:resultado_calculado]
                  ev.evaluator_closed = 1
                  s = -1
                  if @data[:kpi_aprobe] == 1 && @data[:competence_aprobe] == 1
                    s = 1
                  else
                    s = 0
                  end
                  ev.evaluated_closed = s
                  if ev.save
                  else
                    logger.info "ERRORES====CREAR==".red.bold
                      ev.errors.each do |e|
                        logger.info "#{e}".red.bold
                      end
                  logger.info "======".red.bold
                  end
              else
                update_opts[:evaluated_id] = @data[:evaluated_id]
                update_opts[:evaluator_id] = u.boss_position_id
                update_opts[:kpi_result] = @data[:rendimiento]
                update_opts[:kpi_final_result] = @data[:rendimiento_calculado]
                update_opts[:competence_result] = @data[:competencias]
                update_opts[:competence_final_result] = @data[:competencias_calculado]
                update_opts[:kpi_competence_result] = @data[:resultado]
                update_opts[:final_result] = @data[:resultado_calculado]
                update_opts[:evaluator_closed] = 1
                s = -1
                if @data[:kpi_aprobe] == 1 && @data[:competence_aprobe] == 1
                  s = 1
                else
                  s = 0
                end
                update_opts[:evaluated_closed] = s
                if ev.update update_opts
                else
                  logger.info "ERRORES===Actualizar===".red.bold
                    ev.errors.each do |e|
                      logger.info "#{e}".red.bold
                    end
                logger.info "======".red.bold
                end
              end
          end
      end

    end

    def successful_action
      if @item.kpi_ponderation > 0 && @item.kpi_result > 0
        @item.kpi_final_result = @item.kpi_result * (@item.kpi_ponderation / 100)
      end

      if @item.competence_ponderation > 0 && @item.competence_result > 0
        @item.competence_final_result = @item.competence_result * @item.competence_ponderation/100
      end

      @item.kpi_competence_result = @item.kpi_final_result + @item.competence_final_result

      @item.final_result = @item.kpi_competence_result + (@item.boss_ponderation > 0 ? @item.boss_result * @item.boss_ponderation/100 : 0)

      @item.save
    end

    protected
    def has_evaluated?
      redirect_to performance_planifications_path if evaluated.nil?
    end
end
