# -*- encoding : utf-8 -*-
class Performance::MonitoringController < ApplicationController

  before_filter :store_location, :only => [:kpi, :competences]
	before_filter :has_evaluated?, :except => [:index]

	def planilla
		@autoevaluacion = nil
		if !params[:evaluated_id].blank? && !params[:evaluator_id].blank?
			_evaluated = Position.get params[:evaluated_id]
			_evaluator = Position.get params[:evaluator_id]
		else
			_evaluated = evaluated
			_evaluator = evaluator
		end
		if _evaluator != _evaluated
			@autoevaluacion = planilla_data _evaluated.id
			@funcional = nil
			if !_evaluated.job.blank? and !_evaluated.job.functional_boss_id.blank?
				@funcional = planilla_data _evaluated.id, Position.first(job_id: _evaluated.job.functional_boss_id)
			end
		end
		@user_evaluator = _evaluator.user
		@user_evaluated = _evaluated.user
		@data = planilla_data _evaluated.id, _evaluator.id
	end

	def planilla_informe
		@autoevaluacion = nil
		if !params[:evaluated_id].blank? && !params[:evaluator_id].blank?
			_evaluated = Position.get params[:evaluated_id]
			_evaluator = Position.get params[:evaluator_id]
		else
			_evaluated = evaluated
			_evaluator = evaluator
		end
		if _evaluator != _evaluated
			@autoevaluacion = planilla_data _evaluated.id
			@funcional = nil
			if !_evaluated.job.blank? and !_evaluated.job.functional_boss_id.blank?
				@funcional = planilla_data _evaluated.id, Position.first(job_id: _evaluated.job.functional_boss_id)
			end
		end
		@user_evaluator = _evaluator.user
		@user_evaluated = _evaluated.user
		@data = planilla_data _evaluated.id, _evaluator.id
	end

	def planilla_save
		period_id = current_period.id
		unless params[:tracing_kpi].blank?
			params[:tracing_kpi].each do |p_kpi|
				logger.info "P_KPI: #{p_kpi.inspect}".red

				kpi = TracingKpi.new 	trace_date: Date.today,
										planification_id: p_kpi[0],
										increase: p_kpi[1],
										kind: TracingKpi::EVALUACION,
										evaluator_id: evaluator.id,
										evaluated_id: evaluated.id,
										period_id: period_id
				kpi.save

			end
		end

		unless params[:tracing_competence].blank?
			params[:tracing_competence].each do |p_comp|
				logger.info "P_COMP: #{p_comp.inspect}".red

				tc = TracingCompetence.new 	trace_date: Date.today,
											planification_competence_id: p_comp[0],
											increase: p_comp[1],
											kind: TracingCompetence::EVALUACION,
											evaluator_id: evaluator.id,
											evaluated_id: evaluated.id,
											period_id: period_id
				tc.save

			end
		end

		unless params[:comment].blank?
			Agreement.create 	evaluator_id: evaluator.id,
								evaluated_id: evaluated.id,
								period_id: period_id,
								fecha: Date.today,
								detalle: params[:comment],
								meta_o_conducta: 'Evaluación completa',
								acuerdo_o_comentario: 'Comentario'
		end

		redirect_to planilla_performance_evaluacion_index_path
	end
	################### KPI #####################
	def kpi
      #logger.info "EVALUATED : #{evaluated.inspect}".yellow.bold
			opts = {:evaluated_id => evaluated.user_id,
	            :order => [:key_performance_indicator_id.asc], :period => current_period}
	    @items = Planification.all(opts)

	    @item = TracingKpi.new
	end

	def kpisave


      comprueba = TracingKpi.first(evaluated_id: params[:tracing_kpi][:evaluated_id] ,period: current_period ,evaluator_id: params[:tracing_kpi][:evaluator_id],planification_id: params[:tracing_kpi][:planification_id], kind: 'evaluacion')
      comprueba = TracingKpi.first(evaluated_id: params[:tracing_kpi][:evaluated_id] ,period: current_period ,planification_id: params[:tracing_kpi][:planification_id] , kind: 'evaluacion') if comprueba.blank?
      #logger.info "COMPRUEBA #{comprueba.inspect}".red.bold
      if params[:tracing_kpi][:increment].to_i > 100
        flash[:error] = "Solo puede realizar una evaluación o seguimiento entre 0% y 100% "
        redirect_to params[:tracing_kpi][:kind] == 'seguimiento' ?
  	    	kpi_performance_seguimiento_index_path :
  	    	kpi_performance_evaluacion_index_path
      else
        if comprueba.blank?
            item = TracingKpi.new params[:tracing_kpi]
            item.period = current_period
            if item.save
            else
              Rails.logger.info "ERRORES======"
                item.errors.each do |e|
                  Rails.logger.info e
                end
            Rails.logger.info "======"
            end
        else
          flash[:error] = "Solo puede realizar una evaluación por meta "

        end

        redirect_to params[:tracing_kpi][:kind] == 'seguimiento' ?
          kpi_performance_seguimiento_index_path :
          kpi_performance_evaluacion_index_path
      end


	end

	def kpiedit
	    @item = KeyPerformanceIndicator.get params[:id]
	    @position = evaluated
	end

	def kpiupdate
	    planification = Planification.first_or_create :evaluator_id => evaluator.user_id, :evaluated_id => evaluated.user_id, :period => current_period

	    item = KeyPerformanceIndicator.get params[:id]
	    item.attributes = kpiparse
	    item.planification = planification

	    begin
	      item.save
	    rescue
	      Rails.logger.info "ERRORES======"
	        item.errors.each do |e|
	          Rails.logger.info e
	        end
	        Rails.logger.info "======"
	    end

		redirect_to kpi_performance_planifications_path
	end

	################## COMPETENCIAS ##############
	def competences
    logger.info "evaluado :#{evaluated.user_id.inspect}".blue.bold
		@items = PlanificationCompetence.all(:evaluated_id => evaluated.user_id, period_id: current_period.id)

		@item = TracingCompetence.new

		render 'competences_detail' if params[:detalle]
	end

	def competences_save
    puts params.inspect.yellow.bold
		data     = params[:tracing_competence]
		conducts = data.delete 'increase'
		acuerdo  = data.delete 'acuerdo'
    unless conducts.blank?
    		conducts.each do |k, i|

    			item            = TracingCompetence.new data
    			item.period     = current_period
    			item.conduct_id = k
    			item.increase   = i
          #logger.info "DATA #{data.inspect}".red.bold

          comprueba = TracingCompetence.first(evaluated_id: data[:evaluated_id] ,period: current_period ,planification_competence_id: data[:planification_competence_id], kind: 'evaluacion')

          if comprueba.blank?
        			if item.save
        				Rails.logger.info "------- PASO SIN PROBLEMAS ---------"
                ag = Agreement.create 	meta_o_conducta: "#{PlanificationCompetence.first(id: data[:planification_competence_id]).competence.name} ", evaluated_id: data[:evaluated_id], evaluator_id: data[:evaluator_id]
                optag = {acuerdo_o_comentario: 'Comentario',
                           fecha: data[:trace_date],
                           detalle: acuerdo[:comentario],
                           period: current_period,
                           evaluated_id: data[:evaluated_id],
                           evaluator_id: data[:evaluator_id]}
        				ag.update optag unless acuerdo[:comentario].blank?
        			else
        				Rails.logger.info "ERRORES======"
        		        item.errors.each do |e|
        		          Rails.logger.info e
        		        end
        				Rails.logger.info "======"
        			end
          else
              flash[:error] = "Solo puede realizar una Evaluación para cada competencia "
          end
    		end
    else
        flash[:error] = "Debe seleccionar una nota de evaluación"
    end
		redirect_to params[:tracing_competence][:kind] == 'seguimiento' ?
	    	competences_performance_seguimiento_index_path :
	    	competences_performance_evaluacion_index_path
	end

  ##########################################################
  ################ META TRANSVERSAL #######################
  def meta_transversal
      #logger.info "params : #{evaluated.inspect}".yellow.bold
      opts = {:evaluated_id => evaluated.user_id,:period => current_period}
      @items = MetaTransversal.all(opts)

      @item = TracingMt.new
  end

  def meta_transversal_save
      logger.info "PARAMETROS MT : #{params.inspect}".red.bold



      comprueba = TracingMt.first(evaluated_id: params[:tracing_mt][:evaluated_id] ,period: current_period ,evaluator_id:     params[:tracing_mt][:evaluator_id],meta_transversal_id: params[:tracing_mt][:meta_transversal_id])
      comprueba = TracingMt.first(evaluated_id: params[:tracing_mt][:evaluated_id] ,period: current_period ,meta_transversal_id: params[:tracing_mt][:meta_transversal_id]) if comprueba.blank?
      #logger.info "COMPRUEBA #{comprueba.inspect}".red.bold
      if params[:tracing_mt][:increment].to_i > 100
        flash[:error] = "Solo puede realizar una evaluación entre 0% y 100% "
        redirect_to meta_transversal_performance_evaluacion_index_path
      else
        if comprueba.blank?
            item = TracingMt.new params[:tracing_mt]
            item.period = current_period
            if item.save
            else
              Rails.logger.info "ERRORES======"
                item.errors.each do |e|
                  Rails.logger.info e
                end
            Rails.logger.info "======"
            end
        else
            comprueba.update params[:tracing_mt]
            #flash[:error] = "Solo puede realizar una evaluación o seguimiento al día para cada meta "
        end

        redirect_to meta_transversal_performance_evaluacion_index_path

      end


  end
  def mt_close
      logger.info "PARAMS MT CLOSE #{params.inspect}".red.bold
      TracingMt.all(evaluated_id: params[:id].to_i).update(closed: true)

      flash[:notice] = "Evaluación Cerrada"
      redirect_to	meta_transversal_performance_evaluacion_index_path
  end

  def mt_open
      logger.info "PARAMS MT OPEN #{params.inspect}".red.bold
      TracingMt.all(evaluated_id: params[:id].to_i).update(closed: false)

      flash[:notice] = "Evaluación Abierta"
      redirect_to	meta_transversal_performance_evaluacion_index_path
  end
  ###################Fin Meta Transversal ###################
  ########################################################

  # APROBAR COMPETENCIAS
  def competences_close
      TracingCompetence.all(evaluated_id: params[:id].to_i).update(closed: true)
      evaluated = Position.first(user_id: params[:id].to_i)
      mail = evaluated.user.email
      CompetenceMailer.competence_closed(mail).deliver
      flash[:notice] = "Evaluación Cerrada"
      redirect_to	competences_performance_evaluacion_index_path
  end
  def competences_close_aprove
      TracingCompetence.all(evaluated_id: params[:id].to_i).update(aprobe: 1)
      evaluated = Position.first(user_id: params[:id].to_i)
      mail = evaluated.boss_position.user.email
      evaluator_name = evaluated.boss_position.user.first_name+" "+evaluated.boss_position.user.last_name
      evaluated_name = evaluated.user.first_name+" "+evaluated.user.last_name
      CompetenceMailerEval.competence_aprobe(mail,evaluator_name,evaluated_name).deliver
      flash[:notice] = "Evaluación Aprobada"
      redirect_to	competences_performance_evaluacion_index_path
  end
  def competences_close_refuse
      #logger.info "PARAMETROS: #{params[:competences_close_refuse][:apelacion].inspect}".red.bold
      TracingCompetence.all(evaluated_id: params[:id].to_i).update(aprobe: 0)
      apelacion =  params[:competences_close_refuse][:apelacion]
      TracingCompetence.all(evaluated_id: params[:id].to_i).update(apelacion: apelacion)
      evaluated = Position.first(user_id: params[:id].to_i)
      mail = evaluated.boss_position.user.email
      evaluator_name = evaluated.boss_position.user.first_name+" "+evaluated.boss_position.user.last_name
      evaluated_name = evaluated.user.first_name+" "+evaluated.user.last_name
      CompetenceMailerEval.competence_refuse(mail,evaluator_name,evaluated_name,apelacion).deliver
      flash[:notice] = "Evaluación Rechazada"
      redirect_to	competences_performance_evaluacion_index_path
  end
  #APROBAR Metas Individuales
  def kpi_close
      TracingKpi.all(evaluated_id: params[:id].to_i).update(closed: true)
      evaluated = Position.first(user_id: params[:id].to_i)
      mail = evaluated.user.email
      MetasMailer.metas_closed(mail).deliver
      flash[:notice] = "Evaluación Cerrada"
      redirect_to	kpi_performance_evaluacion_index_path
  end
  def kpi_close_aprove
      TracingKpi.all(evaluated_id: params[:id].to_i).update(aprobe: 1)
      evaluated = Position.first(user_id: params[:id].to_i)
      mail = evaluated.boss_position.user.email
      evaluator_name = evaluated.boss_position.user.first_name+" "+evaluated.boss_position.user.last_name
      evaluated_name = evaluated.user.first_name+" "+evaluated.user.last_name
      MetasMailerEval.metas_aprobe(mail,evaluator_name,evaluated_name).deliver
      flash[:notice] = "Evaluación Aprobada"
      redirect_to	kpi_performance_evaluacion_index_path
  end
  def kpi_close_refuse
      #logger.info "PARAMETROS: #{params[:kpi_close_refuse][:apelacion].inspect}".red.bold
      TracingKpi.all(evaluated_id: params[:id].to_i).update(aprobe: 0)
      apelacion =  params[:kpi_close_refuse][:apelacion]
      TracingKpi.all(evaluated_id: params[:id].to_i).update(apelacion: apelacion)
      evaluated = Position.first(user_id: params[:id].to_i)
      mail = evaluated.boss_position.user.email
      evaluator_name = evaluated.boss_position.user.first_name+" "+evaluated.boss_position.user.last_name
      evaluated_name = evaluated.user.first_name+" "+evaluated.user.last_name
      MetasMailerEval.metas_refuse(mail,evaluator_name,evaluated_name,apelacion).deliver
      flash[:notice] = "Evaluación Rechazada"
      redirect_to	kpi_performance_evaluacion_index_path
  end

	private

		def has_evaluated?
			return true if !params[:evaluated_id].blank? && !params[:evaluator_id].blank?
		  redirect_to performance_planifications_path unless session.has_key?(:evaluated_id) && session[:evaluated_id].to_i > 0
		end


end
