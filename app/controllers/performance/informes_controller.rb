# -*- encoding : utf-8 -*-
class Performance::InformesController < ApplicationController
    before_filter :store_location
    require 'writeexcel'

    def index

    end

    def planificaciones_excel
        # Create a new Excel Workbook
        file = "tmp/informe_planificaciones.xls"
        workbook = WriteExcel.new file

        # Add worksheet(s)
        worksheet  = workbook.add_worksheet

        # Add and define a format
        format = workbook.add_format
        format.set_bold
        format.set_color('black')
        format.set_align('center')

        # write a formatted and unformatted string.
        worksheet.write(0, 0, 'Rut', format)  # cell B2
        worksheet.write(0, 1, 'Nombre', format)  # cell B2
        worksheet.write(0, 2, 'Cargo', format)  # cell B2
        worksheet.write(0, 3, 'Empresa', format)  # cell B2
        worksheet.write(0, 4, 'Vicepresidencia', format)  # cell B2
        worksheet.write(0, 5, 'Metas Desempeño Individual', format)  # cell B2
        worksheet.write(0, 6, 'Competencias', format)  # cell B2
        #worksheet.write(0, 6, 'Nivel de Competencia', format)  # cell B2

        # write a number and formula using A1 notation
        c = 1

        @items.each do |item|
            jnombre = item[:job].name
            unombre = item[:user].full_name
            urut    = item[:user].rut
            jempresa = item[:job].branch_office.company.name unless item[:job].branch_office.company.blank?
            jvic = item[:job].vicepresidencia.name unless item[:job].vicepresidencia.blank?
            pl      = item[:planification]
            cm      = item[:plan_competences]
            cmar      = item[:plan_competences_ar]

            planificacion = pl.blank? ? "No Iniciado" : pl.closed ? "Finalizado" : !pl.evaluator_closed ? "Falta cierre evaluador" : "Falta cierre colaborador"
            competencias  = cm.blank? ? "No Iniciado" : cm.closed ? "Finalizado" : !cm.evaluator_closed ? "Falta cierre evaluador" : "Falta cierre colaborador"
            comcarga = Competence.first(id: cm.competence_id).name unless cm.blank?
            compe = ""
            lista = [urut, unombre, jnombre,jempresa,jvic, planificacion, competencias]
            nun = 5
            #unless cmar.blank?
            #   cmar.each do |xx|

            #       compe = ""
            #       compe += xx.competence.name
            #       compe += " Nivel: #{Conduct.first(id:JSON(xx.conduct)[0].to_i).number} \t" unless xx.blank?#.map{|i| "#{(c = Conduct.get(i)).blank? ? '' : "#{c.number}"}" }.join("") unless cm.blank?
            #       lista << compe
            #       worksheet.write(0, nun, 'Competencia / Nivel', format)
            #       nun+=1
            #   end
            #end
            #worksheet.write(c, 0, [urut, unombre, jnombre, planificacion, competencias,comcarga,nivelconduc])
            #worksheet.write(c, 0, lista)
            #worksheet.write(c, 0, [urut, unombre, jnombre,jempresa, planificacion, competencias])
            worksheet.write(c, 0, lista)
            c+=1

        end
        # write to file
        workbook.close
        send_file file, type: 'application/octet-stream'
    end

    def planificaciones
        @periodo         = Period.get params[:informe_planificaciones][:period]
        if @periodo.blank?
            flash[:error] = "Debe seleccionar un periodo para generar el informe"
            redirect_to performance_informes_path
        else
            #positions        = Position.all
            if current_role_can? :mantenedores
                jobs             = Job.activos
            else
                jobs             = [current_user.position.job] + return_children(current_user.position.job.children.to_a)
            end
            users            = User.all#(role:"Limitado")
            planifications   = Planification.all period_id: @periodo.id
            plan_competences = PlanificationCompetence.all period_id: @periodo.id

            @items = []

            users.each do |p|#positions.each do |p|
                unless jobs.select{|x| x.id == p.position.job_id}.first.blank?
                    @items << {
                        position:               p.position,
                        job:                    jobs.select{|x| x.id == p.position.job_id}.first,
                        user:                   p,#users.select{|x| x.id == p.id}.first,
                        planification:          planifications.select{|x| x.evaluated_id == p.id }.first,
                        plan_competences:       plan_competences.select{|x| x.evaluated_id == p.id }.first,
                        plan_competences_ar:    plan_competences.select{|x| x.evaluated_id == p.id }
                    }
                end
            end
            if params[:button].eql?("excel")
                planificaciones_excel
            else
                @items
            end
        end


    end

    def informes_pdf

    end

    def evaluaciones
        @periodo            = Period.get params[:informe_evaluaciones][:period]

        if @periodo.blank?
            flash[:error] = "Debe seleccionar un periodo para generar el informe"
            redirect_to performance_informes_path
        else

            #positions           = Position.all
            if current_role_can? :mantenedores
                jobs             = Job.activos
            else
                jobs             = [current_user.position.job] + return_children(current_user.position.job.children.to_a)
            end
            users               = User.all#(role:"Limitado")
            planifications      = Planification.all period_id: @periodo.id
            plan_competences    = PlanificationCompetence.all period_id: @periodo.id
            tracing_kpi         = TracingKpi.all period_id: @periodo.id
            tracing_competences = TracingCompetence.all period_id: @periodo.id

            @items = []

            users.each do |p|#positions.each do |p|
                unless jobs.select{|x| x.id == p.position.job_id}.first.blank?
                    @items << {
                        position:               p.position,
                        job:                    jobs.select{|x| x.id == p.position.job_id}.first,
                        user:                   p,#users.select{|x| x.id == p.id}.first,
                        planification:          planifications.select{|x| x.evaluated_id == p.id },
                        plan_competences:       plan_competences.select{|x| x.evaluated_id == p.id },
                        tracing_kpi:            tracing_kpi.select{|x| x.evaluated_id == p.id && x.kind.eql?('seguimiento')},
                        tracing_competences:    tracing_competences.select{|x| x.evaluated_id == p.id && x.kind.eql?('seguimiento')},
                        evaluacion_kpi:         tracing_kpi.select{|x| x.evaluated_id == p.id && x.kind.eql?('evaluacion')},
                        evaluacion_competences: tracing_competences.select{|x| x.evaluated_id == p.id && x.kind.eql?('evaluacion')}
                    }
                end
            end
        end
    end

    def evaluaciones_notas
        session[:period] = Period.get(params[:informe_evaluaciones][:period]) unless params[:informe_evaluaciones].blank?
        users_opts = {}
        jobs_opts = {}
        @periodo  = session[:period].blank? ? Period.get(params[:informe_evaluaciones][:period]) : session[:period]
        if @periodo.blank?
            flash[:error] = "Debe seleccionar un periodo para generar el informe"
            redirect_to performance_informes_path
        else
            puts params.inspect.red.bold
            users_opts[:sex]             = params[:sex] unless params[:sex].blank?
            #users_opts[:role]            = "Limitado"
            jobs_opts[:boss_id]          = Job.first(name: params[:job]).id unless params[:job].blank?
            jobs_opts[:empresa]          = Company.first(name: params[:empresa]) unless params[:empresa].blank?
            jobs_opts[:vicepresidencia]  = Vicepresidencia.first(name: params[:vicepresidencia]) unless params[:vicepresidencia].blank?
            jobs_opts[:job_class]        = JobClass.first(name: params[:estamento]) unless params[:estamento].blank?
            puts jobs_opts.inspect.blue.bold

            jobs             = Job.all(jobs_opts)#(centro_gasto_id:  params[:informe_evaluaciones][:gerencia].to_i, status:1)
            puts jobs.inspect.green.bold
            #System.exit(0)
            users               = User.all(users_opts)
            planifications      = Planification.all period_id: @periodo.id
            plan_competences    = PlanificationCompetence.all period_id: @periodo.id
            tracing_kpi         = TracingKpi.all period_id: @periodo.id
            tracing_competences = TracingCompetence.all period_id: @periodo.id
            ev                  = Evaluation.all(period_id: @periodo.id)
            vps = Vicepresidencia.all
            @vp = vps


            @items = []
            @ev = ev
            @notas_empresa = []
            @notas_empresa[0]=0
            @notas_empresa[1]=0
            @notas_empresa[2]=0
            @notas_empresa[3]=0
            @notas_empresa[4]=0
            @ev.each do |x|
                @notas_empresa[0] +=1 if x.competence_result.round(0) == 1
                @notas_empresa[1] +=1 if x.competence_result.round(0) == 2
                @notas_empresa[2] +=1 if x.competence_result.round(0) == 3
                @notas_empresa[3] +=1 if x.competence_result.round(0) == 4
                @notas_empresa[4] +=1 if x.competence_result.round(0) == 5
            end


            @ev_vice = []
            @ev_vice_polo =[]
            @ev_vice_names = []
            @ev_vice_avg = []

            @ev_vice_compe = []
            @ev_vice_polo_compe =[]
            @ev_vice_avg_compe = []

            @metas_avg = []
            @compe_avg = []
            @dotacionvp = []
            vps.each do |vp|
                @ev_vice[vp.id-1] = []
                @ev_vice_compe[vp.id-1] = []
                @metas_avg[vp.id-1] = []
                @compe_avg[vp.id-1] = []
                @dotacionvp[vp.id-1] = 0
                @ev_vice_names[vp.id-1] = []
                users.each do |p|
                    next if p.position.job.vicepresidencia != vp || jobs.select{|x| x.id == p.position.job_id}.first.blank?
                    @dotacionvp[vp.id-1] +=1
                    @ev_vice_names[vp.id-1] << vp.name unless @ev_vice_names[vp.id-1].include?(vp.name) || ev.select{|x|  x.evaluated_id == p.id}.first.blank?
                    @ev_vice[vp.id-1] <<  ev.select{|x|  x.evaluated_id == p.id}.first.kpi_result unless ev.select{|x|  x.evaluated_id == p.id}.first.blank? || ev.select{|x|  x.evaluated_id == p.id}.first.kpi_result == 0.0
                    @ev_vice_compe[vp.id-1] <<  ev.select{|x|  x.evaluated_id == p.id}.first.competence_ponderation unless ev.select{|x|  x.evaluated_id == p.id}.first.blank? || ev.select{|x|  x.evaluated_id == p.id}.first.competence_ponderation == 0.0 || ev.select{|x|  x.evaluated_id == p.id}.first.competence_ponderation.blank?
                end
            end
            @nvp = @ev_vice_names

            vps.each do |vp|
                mayor = 0
                menor = 100
                avg = []
                mayor_compe = 0
                menor_compe = 120
                avg_compe = []
                @ev_vice[vp.id-1].each do |evp|
                    if evp > mayor
                        mayor = evp
                    end
                    if evp < menor
                        menor = evp
                    end
                    avg << evp
                end
                @ev_vice_polo << [menor,mayor] unless @ev_vice[vp.id-1].blank?
                @ev_vice_avg << (avg.sum / avg.length).round(2) unless avg.blank?
                @metas_avg[vp.id-1] << (avg.sum / avg.length).round(2) unless avg.blank?

                @ev_vice_compe[vp.id-1].each do |evp|
                    if evp > mayor_compe
                        mayor_compe = evp
                    end
                    if evp < menor_compe
                        menor_compe = evp
                    end
                    avg_compe << evp
                end
                @ev_vice_polo_compe << [menor_compe,mayor_compe] unless @ev_vice_compe[vp.id-1].blank?
                @ev_vice_avg_compe << (avg_compe.sum / avg_compe.length).round(2) unless avg_compe.blank?
                @compe_avg[vp.id-1] << (avg_compe.sum / avg_compe.length).round(2) unless avg_compe.blank?
            end
            @ev_vice_names= @ev_vice_names.reject(&:empty?)

            users.each do |p|#positions.each do |p|
                next if ev.select{|x|  x.evaluated_id == p.id}.first.blank? || jobs.select{|x| x.id == p.position.job_id}.first.blank?

                @items << {
                    position:               p.position,
                    job:                    p.position.job,#jobs.select{|x| x.id == p.position.job_id}.first,
                    user:                   p,#users.select{|x| x.id == p.id}.first,
                    evaluacion:             ev.select{|x|  x.evaluated_id == p.id}.first

                    #planification:          planifications.select{|x| x.evaluated_id == p.id },
                    #plan_competences:       plan_competences.select{|x| x.evaluated_id == p.id },
                    #tracing_kpi:            tracing_kpi.select{|x| x.evaluated_id == p.id && x.kind.eql?('seguimiento')},
                    #tracing_competences:    tracing_competences.select{|x| x.evaluated_id == p.id && x.kind.eql?('seguimiento')},
                    #evaluacion_kpi:         tracing_kpi.select{|x| x.evaluated_id == p.id && x.kind.eql?('evaluacion')},
                    #evaluacion_competences: tracing_competences.select{|x| x.evaluated_id == p.id && x.kind.eql?('evaluacion')}
                }

            end
        end
    end


    def evaluaciones_competencias
        session[:period] = Period.get(params[:informe_evaluaciones][:period]).id unless params[:informe_evaluaciones].blank?
        users_opts = {}
        jobs_opts = {}

        @periodo  = session[:period].blank? ? Period.get(params[:informe_evaluaciones][:period]).id : session[:period]
        #render text: do_cache('evaluaciones_competencias', cache: true){
        if @periodo.blank?
            flash[:error] = "Debe seleccionar un periodo para generar el informe"
            redirect_to performance_informes_path
        else
            users_opts[:sex]             = params[:sex] unless params[:sex].blank?
            #users_opts[:role]            = "Limitado"
            jobs_opts[:boss_id]          = Job.first(name: params[:job]).id unless params[:job].blank?
            jobs_opts[:empresa]          = Empresa.first(name: params[:empresa]) unless params[:empresa].blank?
            jobs_opts[:vicepresidencia]  = Vicepresidencia.first(name: params[:vicepresidencia]) unless params[:vicepresidencia].blank?
            jobs_opts[:job_class]        = JobClass.first(name: params[:estamento]) unless params[:estamento].blank?
            jobs_opts[:status]=1

            jobs                = Job.all(jobs_opts)#(centro_gasto_id:  params[:informe_evaluaciones][:gerencia].to_i, status:1)
            users               = User.all(users_opts)
            #plan_competences    = PlanificationCompetence.all period_id: @periodo
            tracing_competences = TracingCompetence.all period_id: @periodo,kind: TracingCompetence::EVALUACION

            @competences = Competence.all

            user_aux = []
            users.each do |p|
                next if jobs.select{|x| x.id == p.position.job_id}.first.blank?
                user_aux << p
            end
            calculo_porcentaje = lambda do |x|
              return 80	 if x == 0
              return 90	 if x == 1
              return 100 if x == 2
              return 110 if x == 3
              return 120 if x == 4
            end

            @items = []
            @compe_names=[]
            @notas_competencia_sobre=[]
            @notas_competencia_bajo=[]

            @notas_competencia_polos=[]
            @notas_competencia_avg=[]

            @notas_competencia_polos_sobre=[]

            contador = 0
            @competences.each do |comp|

                @compe_names[contador]=[]
                @notas_competencia_bajo[contador]=[]
                @notas_competencia_sobre[contador]=[]
                @notas_competencia_polos_sobre[contador]=[]
                @notas_competencia_polos[contador]=[]
                @notas_competencia_avg[contador]=[]
                user_aux.each do |p|
                    #next if jobs.select{|x| x.id == p.position.job_id}.first.blank?
                    @compe_names[contador] << comp.name.capitalize unless @compe_names[contador].include?(comp.name.capitalize)
                    tmps = tracing_competences.select{|x|  x.evaluated_id == p.id && x.planification_competence.competence_id == comp.id}.first.increase unless tracing_competences.select{|x|  x.evaluated_id == p.id && x.planification_competence.competence_id == comp.id}.blank?
                    tmps2 = calculo_porcentaje.(tmps) < 100 ? calculo_porcentaje.(tmps)*-1 : calculo_porcentaje.(tmps) unless tmps.blank?

                    if !tmps2.blank? && tmps2 < 0
                        #puts tmps2.inspect.red
                        @notas_competencia_bajo[contador] << tmps2 unless tmps2.blank?
                    elsif !tmps2.blank? && tmps2 > 0
                        #puts tmps2.inspect.green
                        @notas_competencia_sobre[contador] << tmps2 unless tmps2.blank?
                    end
                end
                contador +=1
            end

            contador2 = 0
            @competences.each do |comp|

                mayor = 0
                menor = 130
                avg = []
                mayor_sobre = 0
                menor_sobre = 130
                total = @notas_competencia_sobre[contador2].length + @notas_competencia_bajo[contador2].length
                result1 = (@notas_competencia_bajo[contador2].length * 100) / total unless total == 0
                result2 = (@notas_competencia_sobre[contador2].length * 100) / total unless total == 0
                if result1.blank?
                  result1 = 0
                end
                if result2.blank?
                  result2 = 0
                end

                @notas_competencia_polos[contador2] <<  result1*-1
                @notas_competencia_polos_sobre[contador2] << result2

                @notas_competencia_bajo[contador2].each do |nc|
                    nc = nc*-1
                    avg << nc
                end
                @notas_competencia_sobre[contador2].each do |nc|
                    avg << nc
                end
                @notas_competencia_avg[contador2] << (avg.sum / avg.length).round(2) unless avg.blank?
                contador2 +=1
            end
            @compe_names.each do |x|
                @compe_names.delete(x) if x.blank?
            end


            #users.each do |p|#positions.each do |p|
            #    next if ev.select{|x|  x.evaluated_id == p.id}.first.blank? || jobs.select{|x| x.id == p.position.job_id}.first.blank?

            #    @items << {
            #        position:               p.position,
            #        job:                    p.position.job,#jobs.select{|x| x.id == p.position.job_id}.first,
            #        user:                   p,#users.select{|x| x.id == p.id}.first,
            #        evaluacion:             ev.select{|x|  x.evaluated_id == p.id}.first

            #        #planification:          planifications.select{|x| x.evaluated_id == p.id },
            #        #plan_competences:       plan_competences.select{|x| x.evaluated_id == p.id },
            #        #tracing_kpi:            tracing_kpi.select{|x| x.evaluated_id == p.id && x.kind.eql?('seguimiento')},
            #        #tracing_competences:    tracing_competences.select{|x| x.evaluated_id == p.id && x.kind.eql?('seguimiento')},
            #        #evaluacion_kpi:         tracing_kpi.select{|x| x.evaluated_id == p.id && x.kind.eql?('evaluacion')},
            #        #evaluacion_competences: tracing_competences.select{|x| x.evaluated_id == p.id && x.kind.eql?('evaluacion')}
            #    }

            #end
        end
        respond_to do |format|
            format.html
            format.pdf do
                render :pdf => "Informe Por Competencias ", # pdf will download as my_pdf.pdf
                       :layout => 'pdf', # uses views/layouts/pdf.haml
                       :javascript_delay => 10000,
                        orientation:'Landscape',
                       #:zoom => 1,
                       #:footer => { :right => '[page] de [topage]', :font_size => 9 },
                       :show_as_html => params[:debug].present? # renders html version if you set debug=true in URL
            end
        end
        #}.html_safe
    end

    def evaluation_calc period
        if !period.blank?
            usrs = User.all#(role:"Limitado")
            update_opts= {evaluated_id: 0, evaluator_id: 0,kpi_result:0,kpi_final_result:"",
                          competence_result:0,competence_final_result:"",kpi_competence_result:0,
                          final_result:"", evaluator_closed:0, evaluated_closed:0}
            usrs.each do |u|
                next if u.rut == '1-9'
                next if u.position.boss_position.blank?
                data = planilla_data_nota u.id , nil,period #u.position.boss_position.user_id, period

                if !data.blank?
                    unless data[:competencias] == 0.0 &&  data[:rendimiento] == 0.0
                        ev = Evaluation.first(evaluated_id: data[:evaluated_id], period_id: period)
                        puts data.inspect.red.bold
                        if ev.blank?
                            ev = Evaluation.new(evaluated_id: data[:evaluated_id],period_id: period)
                            ev.evaluator_id = u.position.boss_position.user_id  #u.boss_position_id
                            ev.kpi_result= data[:rendimiento]
                            ev.kpi_final_result= data[:rendimiento_calculado]
                            ev.competence_result = data[:competencias]
                            ev.competence_ponderation = data[:competencias_porcentaje]
                            ev.competence_final_result = data[:competencias_calculado]
                            ev.kpi_competence_result = data[:resultado]
                            ev.final_result = data[:resultado_calculado]
                            ev.kpi_aprobe         = data[:kpi_aprobe]
                            ev.competence_aprobe  = data[:competence_aprobe]
                            ev.evaluator_closed = 1
                            s = -1
                            if data[:kpi_aprobe] != -1 || data[:competence_aprobe] != -1
                              s = 1
                            else
                              s = 0
                            end
                            ev.evaluated_closed = s
                            ev.kpi_aprobe         = data[:kpi_aprobe]
                            ev.competence_aprobe  = data[:competence_aprobe]
                            if ev.save
                      	    else
                      	    	logger.info "ERRORES====CREAR==".red.bold
                      	        ev.errors.each do |e|
                      	          logger.info "#{e}".red.bold
                      	        end
                      			logger.info "======".red.bold
                      	    end
                        else
                          update_opts[:evaluated_id]            = data[:evaluated_id]
                          update_opts[:evaluator_id]            = u.position.boss_position.user_id #u.boss_position_id
                          update_opts[:kpi_result]              = data[:rendimiento]
                          update_opts[:kpi_final_result]        = data[:rendimiento_calculado]
                          update_opts[:competence_ponderation]  = data[:competencias_porcentaje]
                          update_opts[:competence_result]       = data[:competencias]
                          update_opts[:competence_final_result] = data[:competencias_calculado]
                          update_opts[:kpi_competence_result]   = data[:resultado]
                          update_opts[:final_result]            = data[:resultado_calculado]
                          update_opts[:evaluator_closed]        = 1
                          s = -1
                          if data[:kpi_aprobe] != -1 || data[:competence_aprobe] != -1
                            s = 1
                          else
                            s = 0
                          end
                          update_opts[:evaluated_closed]   = s
                          update_opts[:kpi_aprobe]         = data[:kpi_aprobe]
                          update_opts[:competence_aprobe]  = data[:competence_aprobe]

                          if ev.update update_opts
                            logger.info "ACTUALIZADO : #{data[:evaluated_id]}".green.bold
                          else
                            logger.info "ERRORES===Actualizar===".red.bold
                              ev.errors.each do |e|
                                logger.info "#{e}".red.bold
                              end
                          logger.info "======".red.bold
                          end
                        end
                    end
                end
            end
        end
    end
    def evaluaciones_finales_excel
        # Create a new Excel Workbook
        file = "tmp/informe_planilla_nota_final.xls"
        workbook = WriteExcel.new file

        # Add worksheet(s)
        worksheet  = workbook.add_worksheet

        # Add and define a format
        format1 = workbook.add_format
        format1.set_bold
        format1.set_color('white')
        format1.set_bg_color('green')
        format1.set_align('center')


        format2 = workbook.add_format
        format2.set_bold
        format2.set_color('white')
        format2.set_bg_color('blue')
        format2.set_align('center')

        # write a formatted and unformatted string.
        worksheet.write(0, 0, 'Rut', format1)  # cell B2
        worksheet.write(0, 1, 'Nombre', format1)  # cell B2
        worksheet.write(0, 2, 'Cargo', format1)  # cell B2
        worksheet.write(0, 3, 'Empresa', format1)  # cell B2
        worksheet.write(0, 4, 'VP', format1)  # cell B2
        worksheet.write(0, 5, 'Nivel Organizacional', format1)  # cell B2
        worksheet.write(0, 6, 'Periodo Evaluación', format2)  # cell B2
        worksheet.write(0, 7, 'Metas Individuales', format2)  # cell B2
        worksheet.write(0, 8, 'Competencias', format2)  # cell B2
        worksheet.write(0, 9, 'Cumplimiento Metas (%)', format2)  # cell B2
        worksheet.write(0, 10, 'Cumplimiento Competencias (%) ', format2)  # cell B2
        worksheet.write(0, 11, 'Categoria Evaluación Competencias', format2)  # cell B2
        worksheet.write(0, 12, 'Evaluación Metas Aprobada', format2)  # cell B2
        worksheet.write(0, 13, 'Evaluación Competencias Aprobada', format2)  # cell B2
        worksheet.write(0, 14, 'Apelación Metas', format2)  # cell B2
        worksheet.write(0, 15, 'Apelación Competencias', format2)  # cell B2

        # write a number and formula using A1 notation
        c = 1
        logger.info "@items #{@items.inspect}".green.bold
        #Process.exit(0)
        @items.each do |item|
            urut      = item[:user].rut
            unombre   = item[:user].name
            job       = item[:job].name
            empresa   = item[:job].empresa.name         unless  item[:job].empresa.blank?
            vp        = item[:job].vicepresidencia.name unless  item[:job].vicepresidencia.blank?
            estamento = item[:job].job_class.name       unless  item[:job].job_class.blank?
            p_eval    = item[:job].p_eval               unless  item[:job].p_eval.blank?
            apelaci_competence       = TracingCompetence.first(evaluated_id: item[:user].id,period_id: @periodo.id ).apelacion unless TracingCompetence.first(evaluated_id: item[:user].id , period_id:@periodo.id).blank?
            apelaci_kpi       = TracingKpi.first(evaluated_id: item[:user].id,period_id:@periodo.id).apelacion unless TracingKpi.first(evaluated_id: item[:user].id,period_id:@periodo.id).blank?

            unless item[:evaluacion].blank?
                metas                   = item[:evaluacion].kpi_result.to_i == 0  ? "NO EVALUADO" : "#{item[:evaluacion].kpi_result.to_i}%"
                competencias_porcentaje = item[:evaluacion].competence_ponderation.to_i == 0  ? "NO EVALUADO" : "#{item[:evaluacion].competence_ponderation.to_i}%"
                competencia_cat         = item[:evaluacion].competence_final_result
                if item[:evaluacion].kpi_aprobe == 1
                    ev_kpi_apr= "SI"
                elsif  item[:evaluacion].kpi_aprobe == 0
                    ev_kpi_apr= "NO"
                else
                    ev_kpi_apr= item[:evaluacion].kpi_result.to_i == 0  ? "NO EVALUADO" : "PENDIENTE"
                end
                if item[:evaluacion].competence_aprobe == 1
                    ev_competence_apr= "SI"
                elsif  item[:evaluacion].competence_aprobe == 0
                    ev_competence_apr= "NO"
                else
                    ev_competence_apr=  item[:evaluacion].competence_ponderation.to_i == 0  ? "NO EVALUADO" : "PENDIENTE"
                end
            else
              metas                       = "NO EVALUADO"
              competencias_porcentaje     = "NO EVALUADO"
              competencia_cat             = "NO EVALUADO"
              ev_kpi_apr                  = "NO EVALUADO"
              ev_competence_apr           = "NO EVALUADO"

            end

            lista = [urut,unombre,job,empresa,vp,estamento,p_eval,metas,competencias_porcentaje,competencia_cat,ev_kpi_apr,ev_competence_apr,apelaci_kpi,apelaci_competence]
            nun = 5

            worksheet.write(c, 0, lista)
            c+=1

        end
        # write to file
        workbook.close
        send_file file, type: 'application/octet-stream'

    end


    def evaluaciones_finales
        logger.info "PERIODO #{params.inspect}".red.bold
        @periodo = Period.first(id: params[:informe_finales][:period])
        logger.info "PERIODO #{@periodo.inspect}".red.bold

        if @periodo.blank?
            flash[:error] = "Debe seleccionar un periodo para generar el informe"
            redirect_to performance_informes_path
        else
            #opts = {period_id: @periodo.id}
            #opts[:final_result.gte] = params[:informe_finales][:minimo] unless params[:informe_finales][:minimo].blank?
            #opts[:final_result.lte] = params[:informe_finales][:maximo] unless params[:informe_finales][:maximo].blank?

            #tmp_eval = Evaluation.all(period_id: @periodo.id)
            #if !@periodo.evaluation_closed #&& tmp_eval.blank?
            evaluation_calc @periodo.id
            #end

            @evaluaciones = Evaluation.all(period_id: @periodo.id)
            #logger.info "@evaluaciones: #{@evaluaciones.inspect}".red.bold
            #@users    = @evaluaciones.evaluated
            #@jobs         = @users.positions.jobs
            #@users        = @positions.users

            @items = []
            #*********************************SOLO QUIENES TIENEN EVALUACIONES CERRADAS *************
            if !params[:button].eql?("excel")
                @evaluaciones.each do |i|
                    user = User.first(id: i.evaluated_id)
                    next if user.blank?
                    @items << {
                        evaluacion: i,
                        positions:  user.position,
                        job:        user.position.job,
                        user:       user
                    }
                end
            end
            #***************************************************************************************

            #*********************************TODA LA DOTACION**************************************
            if params[:button].eql?("excel")
                @positions = Position.all
                @jobs         = @positions.jobs
                @users        = @positions.users
                @positions.each do |i|
                    next if i.user.rut =='1-9' ||  i.user.rut =='1' ||  i.user.rut =='madmin'
                    evaluaciones = @evaluaciones.find{|x| x.evaluated_id == i.user_id}
                    @items << {
                        evaluacion: evaluaciones,
                        positions:  i,
                        job:        @jobs.find{|x| x.id == i.job_id},
                        user:       @users.find{|x| x.id == i.user_id}
                    }
                end
            end
            #***************************************************************************************
            if params[:button].eql?("excel")
                evaluaciones_finales_excel #@periodo.id
            else
                #@items.each do |item|
                #  Rails.logger.info "NOTA FINAL: #{item.inspect}".green.bold
                #end
                @items
            end
        end
    end

    def estado_evaluaciones

        @periodo            = Period.get params[:informe_avance][:period]

        if @periodo.blank?
            flash[:error] = "Debe seleccionar un periodo para generar el informe"
            redirect_to performance_informes_path
        else
            calculo_evaluaciones current_user.position, @periodo, true, true
        end
    end

    def informe_cuadrantes

        @periodo            = Period.get params[:informe_cuadrantes][:period]

        if @periodo.blank?
            flash[:error] = "Debe seleccionar un periodo para generar el informe"
            redirect_to performance_informes_path
        else

            calculo_evaluaciones current_user.position, @periodo, true, true


        end
    end

    def personal
        if current_period.blank?
            session[:notice] = 'No existe un período actual, el administrador debe crearlo'
            redirect_to performance_planifications_path
        else
            evaluated_id        = params[:evaluated_id]
            @periodo            = params[:periodos].blank? ? @periodo :  params[:periodos]
            logger.info "PERIODO #{@periodo.inspect}".yellow.bold
            if @periodo.blank?
                @periodo = current_period.id
            end
            logger.info "PERIODO #{@periodo}".yellow.bold
            jobs                = Job.activos
            #users               = User.all(role:"Limitado")
            positions           = Position.first user_id: evaluated_id
            #mt_plan             = MetaTransversal.all period_id: @periodo,evaluated_id: evaluated_id
            #tracing_mt          = TracingMt.all period_id:@periodo,evaluated_id:evaluated_id
            planifications      = Planification.all period_id: @periodo, evaluated_id: evaluated_id
            plan_competences    = PlanificationCompetence.all period_id: @periodo, evaluated_id: evaluated_id
            tracing_kpi         = TracingKpi.all period_id: @periodo, evaluated_id: evaluated_id
            tracing_competences = TracingCompetence.all period_id: @periodo, evaluated_id: evaluated_id
            agreements          = Agreement.all period_id: @periodo, evaluated_id: evaluated_id
            evaluacion          = Evaluation.all period_id: @periodo, evaluated_id: evaluated_id



            @items = {}

            #positions.each do |p|
            p=positions
            @items = {
                position:               p,
                job:                    jobs.select{|x| x.id == p.job_id}.first,
                user:                   p.user,#users.select{|x| x.id == p.user_id}.first,
                planification:          planifications.select{|x| x.evaluated_id == p.user_id },
                plan_competences:       plan_competences.select{|x| x.evaluated_id == p.user_id },
                tracing_kpi:            tracing_kpi.select{|x| x.evaluated_id == p.user_id},
                tracing_competences:    tracing_competences.select{|x| x.evaluated_id == p.user_id},
                agreements:             agreements.select{|x| x.evaluated_id == p.user_id},
                #mt_plan: mt_plan,
                #tracing_mt: tracing_mt,
                evaluation: evaluacion.first

            }

            #end

            #@items = @items.first unless @items.blank?
            respond_to do |format|
                format.html
                format.pdf do
                    render :pdf => "Essential - #{User.first(id:params[:evaluated_id]).name}", # pdf will download as my_pdf.pdf
                           :layout => 'pdf', # uses views/layouts/pdf.haml
                           :zoom => 1,
                           :footer => { :right => '[page] de [topage]', :font_size => 9 }
                        #:show_as_html => params[:debug].present? # renders html version if you set debug=true in URL
                end
            end
        end
    end

    def personal_informe
        @periodo   = current_period
        @evaluated = Position.get params[:evaluated_id]
        if @periodo.blank?
            flash[:error] = "No hay un período activo. Solicitar habilitar uno a RRHH"
            redirect_to performance_informes_path
        elsif @evaluated.blank?
            flash[:error] = "Seleccionó un colaborador inexistente. Contacte a RRHH"
            redirect_to performance_informes_path
        else
            evaluator_id = params[:ver].blank? ? current_user.position.id : params[:evaluator_id]
            @comentario_eval = Agreement.first_or_create evaluated_id: params[:evaluated_id], evaluator_id: evaluator_id, meta_o_conducta: 'eval'
            @comentario_auto = Agreement.first_or_create evaluated_id: params[:evaluated_id], evaluator_id: evaluator_id, meta_o_conducta: 'auto'
            calculo_evaluaciones @evaluated, @periodo, false
        end

        respond_to do |format|
            format.html
            format.pdf do
                render :pdf => "Ricardo Velasco HR Strategic Partners - #{params[:evaluated_id]}", # pdf will download as my_pdf.pdf
                       :layout => 'pdf', # uses views/layouts/pdf.haml
                       :zoom => 1,
                       :footer => { :right => '[page] de [topage]', :font_size => 9 }
                    #:show_as_html => params[:debug].present? # renders html version if you set debug=true in URL
            end
        end
    end

    def personal_informe_feedback_save

        data = params[:data]
        @comentario_eval = Agreement.first_or_create evaluated_id: data[:evaluated_id].to_i, evaluator_id: data[:evaluator_id].to_i, meta_o_conducta: 'eval'
        @comentario_auto = Agreement.first_or_create evaluated_id: data[:evaluated_id].to_i, evaluator_id: data[:evaluator_id].to_i, meta_o_conducta: 'auto'

        logger.info "eval: #{@comentario_eval.inspect} auto: #{@comentario_auto.inspect}"
        unless data[:comment_eval].blank?
            @comentario_eval.detalle = data[:comment_eval]
            @comentario_eval.save!
        end
        unless data[:comment_auto].blank?
            @comentario_auto.detalle = data[:comment_auto]
            @comentario_auto.save!
        end
        flash[:notice] = 'Feedback actualizado'
        logger.info "-> eval: #{@comentario_eval.inspect} auto: #{@comentario_auto.inspect}"

        render json: {status: 'ok'}
    end

    private
    def return_children items
        rtr = []
        unless items.blank?
            items.each do |i|
                rtr += [i]
                if i.children.size > 0
                    rtr += return_children(i.children.to_a)
                end
            end
        end

        rtr
    end

    def calculo_evaluaciones position, periodo, todos = true, informe = false
        escala_eval = InsertPlanification::ESCALA_EVAL
        ## Falta que si es Admin vea todo
        if informe
            jobs       = Job.activos.map(&:attributes)
            jobs_linea = jobs
        else

            # Selecciono solo el cargo y sus subordinados - 10/05/2016 - AV
            jobs_linea  = [position.job]
            jobs_linea += position.job.children.to_a if todos
            #jobs_linea += position.boss_position.job.position unless todos
            jobs        = [position.job]
            if todos
                jobs += return_children(position.job.children.to_a)
            else
                unless position.job.boss.blank?
                    jobs_linea += [position.job.boss]
                    jobs       += [position.job.boss]
                end
            end
            #jobs       = Job.activos.map(&:attributes) if jobs.blank?
            jobs_linea  = jobs if jobs_linea.blank?
        end
        positions_linea   = Position.all(job_id: jobs_linea.map{|x| x[:id]})
        positions         = Position.all job_id: jobs.map{|x| x[:id]}
        users             = User.all(role:"Limitado")
        all_positions_ids = ( positions_linea.map(&:id) + positions.map(&:id) ).uniq


        t_no_incluir = [9, 10]
        no_incluir = Competence.all( fields: [:id], competence_type_id: t_no_incluir ).map(&:id)
        #########################

        ####### DESEMPEÑO #######
        t_comp_desemp = [8]
        comp_desemp = Competence.all( fields: [:id], competence_type_id: t_comp_desemp ).map(&:id)
        # + Todos los kpi

        ####### POTENCIAL #######
        t_comp_potencial = [12, 11, 13]
        t_comp_potencial = Competence.all( fields: [:id], competence_type_id: t_comp_potencial ).map(&:id)

        #########################

        planifications      = Planification.all period_id: periodo.id, evaluated_id: all_positions_ids
        plan_competences    = PlanificationCompetence.all period_id: periodo.id, :competence_id.not => no_incluir, evaluated_id: all_positions_ids
        @plan_data          = {kpi: planifications.key_performance_indicator, competences: plan_competences.competences, planifications: planifications, plan_competences: plan_competences}
        tracing_kpi         = TracingKpi.all period_id: periodo.id, order: :created_at.desc
        tracing_competences = TracingCompetence.all period_id: periodo.id, order: :created_at.desc
        @tracing_data       = {kpi: tracing_kpi, competences: tracing_competences}

        @items = []

        sel_evaluables = lambda{|arreglo, key, added, evaluated_id| arreglo.select{|x| added.include?(x[key]) ? false : (added << x[key]; x[:evaluated_id] == evaluated_id) }}
        escala_lam = lambda{|resultado, tipo|
            potencial = tipo.to_s.eql?('potencial')
            if resultado <= 1.49
                return potencial ? "Bajo Potencial" : "No Satisfactorio"
            elsif resultado <=2.49 && resultado >= 1.5
                return "En Desarrollo"
            elsif resultado <= 3.49 && resultado >= 2.5
                return "Competente"
            elsif resultado
                return potencial ? "Alto Potencial" : "Alto Desempeño"
            end
        }
        cuadrantes = [[9, 7, 5], [8, 6, 3], [4, 2, 1]]
        cuadrantes_colores = [
            ['rgb(230, 46, 37)', 'rgb(235, 100, 37)', 'rgb(253, 250, 41)'],
            ['rgb(235, 104, 37)', 'rgb(253, 250, 41)', 'rgb(188, 230, 32)'],
            ['rgb(253, 250, 41)', 'rgb(180, 226, 37)', 'rgb(130, 202, 63)']
        ]
        cuadrante_lam = lambda{|potencial, desempeno|

            if potencial <= 1.79
                p_val = 0
            elsif potencial <= 3.19 && potencial >= 1.8
                p_val = 1
            else
                p_val = 2
            end


            if desempeno <= 1.99
                d_val = 0
            elsif desempeno <= 3.49 && desempeno >= 2.00
                d_val = 1
            else
                d_val = 2
            end

            {txt: "Cuad. #{cuadrantes[p_val][d_val]}", color: cuadrantes_colores[p_val][d_val]}
        }

        @only_data = []
        positions_linea.each do |p|
            unless (j_tmp = jobs.find{|x| x[:id] == p.job_id}).blank?

                pos_eval        = (planifications.select{|x| x[:evaluator_id] == p.id}.map(&:evaluated_id)+plan_competences.select{|x| x[:evaluator_id] == p.id}.map(&:evaluated_id)).uniq
                pos_arr         = positions.select{|x| pos_eval.include? x.id}
                users_data      = []
                total_evaluados = 0
                tmp_tr_kpi      = tracing_kpi.select{|x| x[:evaluator_id] == p.id }.sort_by(&:created_at).reverse
                tmp_tr_comp     = tracing_competences.select{|x| x[:evaluator_id] == p.id }.sort_by(&:created_at).reverse
                users_ids       = pos_arr.map do |p_i|
                    next if !todos and position.job.id != p_i.job_id
                    evaltd  = 0
                    _t = tmp_tr_kpi.find{|_t| _t[:evaluated_id] == p_i.id}
                    evaltd += 1 unless _t.blank?
                    _t = tmp_tr_comp.find{|_t| _t[:evaluated_id] == p_i.id}
                    evaltd += 1 unless _t.blank?

                    total_evaluados += 1 if evaltd > 0

                    added = []
                    #### Guardo desempeño
                    tkp        = sel_evaluables.(tmp_tr_kpi, :planification_id, added, p_i.id)

                    added_    = []
                    tcompD     = sel_evaluables.(tmp_tr_comp, :planification_competence_id, added_, p_i.id)

                    tcomp_sumD = tcompD.sum(&:increase).to_f

                    tkp_sum    = tkp.sum(&:increase).to_f+tcomp_sumD
                    ## Código extraño, no funcionaba de sin validaciones extras
                    tkp_prom   = (tkp_sum/(tkp.size+tcompD.size).to_f)
                    if tkp_prom.nan?
                        tkp_prom = 0
                    else
                        tkp_prom += 1.0
                    end
                    # tkp_prom = tkp_prom.round
                    # Validaciones para situar en la matriz
                    tkp_result = escala_lam.(tkp_prom, :desempeno)

                    added        = []
                    tcomp        = tmp_tr_comp.select{|x| added.include?(x.planification_competence_id) ? false : (added << x.planification_competence_id; x[:evaluated_id] == p_i.id) }
                    tcomp_sum    = tcomp.sum(&:increase).to_f
                    ## Código extraño, no funcionaba de sin validaciones extras
                    tcomp_prom = (tcomp_sum/tcomp.size.to_f)
                    if tcomp_prom.nan?
                        tcomp_prom = 0
                    else
                        tcomp_prom += 1.0
                    end
                    # tcomp_prom = tcomp_prom.round
                    tcomp_result = escala_lam.(tcomp_prom, :potencial)


                    cuad = cuadrante_lam.(tcomp_prom, tkp_prom)
                    _u_data = {
                        user:                 users.find{|x| x.id == p_i.user_id},
                        job:                  jobs.find{|x| x[:id] == p_i.job_id},
                        evaluados:            evaltd,
                        position_id:          p_i.id,
                        tracing_kpi:          tkp,
                        promedio_kpi:         (fnd = escala_eval.find{|x| x[:value] == tkp_prom}).blank? ? '' : fnd[:name],
                        tracing_competences:  tcomp,
                        promedio_competences: (fnd = escala_eval.find{|x| x[:value] == tcomp_prom}).blank? ? '' : fnd[:name],
                        result_desempeno:     tkp_result,
                        desempeno_prom_org:   tkp_prom,
                        desempeno_prom:       tkp_prom.round(1),
                        result_potencial:     tcomp_result,
                        potencial_prom:       tcomp_prom.round(1),
                        potencial_prom_org:   tcomp_prom,
                        resultado:            cuad[:txt],
                        resultado_color:      cuad[:color]

                    }
                    users_data << _u_data
                    if evaltd > 0
                        @only_data << _u_data
                    end

                    p_i[:user_id]
                end
                e_p_arr = users_data.map{|x| x[:potencial_prom_org]}
                e_d_arr = users_data.map{|x| x[:desempeno_prom_org]}
                @items << {
                    position:             p,
                    job:                  jobs.find{|x| x[:id] == p.job_id},
                    user:                 users.find{|x| x.id == p.user_id},
                    # planification:          planifications.select{|x| x.evaluated_id == p.id },
                    # plan_competences:       plan_competences.select{|x| x.evaluated_id == p.id },
                    # tracing_kpi:            tracing_kpi.select{|x| x.evaluated_id == p.id && x.kind.eql?('seguimiento')},
                    # tracing_competences:    tracing_competences.select{|x| x.evaluated_id == p.id && x.kind.eql?('seguimiento')},
                    # evaluacion_kpi:         tracing_kpi.select{|x| x.evaluated_id == p.id && x.kind.eql?('evaluacion')},
                    # evaluacion_competences: tracing_competences.select{|x| x.evaluated_id == p.id && x.kind.eql?('evaluacion')},
                    posiciones_a_evaluar: pos_eval,
                    users_a_evaluar:      users.select{|x| users_ids.include? x.id },
                    users_data:           users_data,
                    total_evaluados:      total_evaluados,
                    eval_potencial_prom:  e_p_arr.blank? ? 0.0 : (e_p_arr.reduce(:+)/e_p_arr.size.to_f),
                    eval_desempeno_prom:  e_d_arr.blank? ? 0.0 : (e_d_arr.reduce(:+)/e_d_arr.size.to_f),
                }
            end
        end

        @only_data = @only_data.uniq
        p_p_arr = @only_data.map{|x| x[:potencial_prom]}
        d_p_arr = @only_data.map{|x| x[:desempeno_prom]}

        logger.info "calc min: #{@only_data.map{|x| x[:potencial_prom_org]}.min}".red

        @extra_data = {
            potencial_prom: p_p_arr.reduce(:+)/p_p_arr.size.to_f,
            desempeno_prom: d_p_arr.reduce(:+)/d_p_arr.size.to_f,
            potencial_min: @only_data.map{|x| x[:potencial_prom_org]}.min.to_f,
            potencial_max: @only_data.map{|x| x[:potencial_prom_org]}.max.to_f,
            desempeno_min: @only_data.map{|x| x[:desempeno_prom_org]}.min.to_f,
            desempeno_max: @only_data.map{|x| x[:desempeno_prom_org]}.max.to_f
        }
        @extra_data[:potencial_dist] = ((@extra_data[:potencial_max]/@extra_data[:potencial_min])**(1.0/3.0).to_f)-1.0
        @extra_data[:desempeno_dist] = ((@extra_data[:desempeno_max]/@extra_data[:desempeno_min])**(1.0/3.0).to_f)-1.0
        logger.info "potencial_dist: #{@extra_data[:potencial_dist]} desempeno_dist: #{@extra_data[:desempeno_dist]}".red
        @escala_lam    = escala_lam
        @cuadrante_lam = cuadrante_lam

        @norm_cuadrante_lam = lambda{|potencial, desempeno|
            r = @extra_data[:potencial_dist]
            #p1, p2, p3 = (1.0+r), (1.0+r)**2.0, (1.0+r)**3.0
            p1 = @extra_data[:potencial_min] * (r+1.0)
            p2 = p1 * (1.0+r)
            p3 = p2 * (1.0+r)


            if potencial <= p1
                p_val = 0
            elsif potencial <= p2 && potencial > p1
                p_val = 1
            else
                p_val = 2
            end
            r = @extra_data[:desempeno_dist]
            #d1, d2, d3 = (1+r), (1+r)**2, (1+r)**3
            d1 = @extra_data[:desempeno_min] * (r+1.0)
            d2 = d1 * (1.0+r)
            d3 = d2 * (1.0+r)

            if desempeno <= d1
                d_val = 0
            elsif desempeno <= d2 && desempeno > d1
                d_val = 1
            else
                d_val = 2
            end
            logger.info "p1: #{p1} p2: #{p2} p3: #{p3} d1: #{d1} d2: #{d2} d3: #{d3}"
            {txt: "Cuad. #{cuadrantes[p_val][d_val]}", color: cuadrantes_colores[p_val][d_val]}
        }
    end


end
