# -*- encoding : utf-8 -*-
class Performance::AgreementsController < ApplicationController
  
  	before_filter :store_location, :only => [:index]  
	before_filter :has_evaluated?

	crud :agreement, :redirect_to_url => 'performance_agreements_path', :use_class_name_as_title => true

	def index
		@items = Agreement.all(:evaluated_id => evaluated.id, period_id: current_period.id)

		@item = Agreement.new
	end

	def successful_create
		Historial.do Historial::CREACION, @item
		after_success
	end

	def successful_update
		Historial.do Historial::ACTUALIZACION, @item, :auto => true
		after_success
	end

	private

	def has_evaluated?
	    redirect_to performance_planifications_path unless session.has_key?(:evaluated_id) && session[:evaluated_id].to_i > 0
	end
	  
	def evaluated
	    Position.get session[:evaluated_id]
	end

end
