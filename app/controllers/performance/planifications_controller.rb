# -*- encoding : utf-8 -*-
class Performance::PlanificationsController < ApplicationController

  before_filter :store_location, :only => [:kpi, :competences, :kpiedit]
  before_filter :has_evaluated?, :except => [:index,:planification_duplicar]

    def index
        if current_user.role.name == 'Administrador'
            puts current_user.inspect.red.bold

            @data = Position.all
        else
            user_pos = current_user.user.position
            @data    = user_pos.children_positions
            @data << current_user.user.position
        end
        curr_pos = @data.map(&:id).uniq

        ###############################################################################
        ###############################################################################
        # comprobacion de gestion de desempeño segun organigrama
        ###############################################################################
        ###############################################################################
        #@data.each do |da|

        #  next if (da.boss_position_id.blank? || da.user.first_name =="Puesto Disponible")

        #  plf = Planification.all(evaluated_id: da.user_id,period_id: current_period.id)
        #  plfc = PlanificationCompetence.all(evaluated_id: da.user_id,period_id: current_period.id)
        #  next if plf.blank?
        #  plf.each do |f|  #recorro las Planifications para saber si el jefe contenido en el position id de cada usuario esta correcto en Metas Individuales
        #      if f.evaluator_id == da.boss_position.user_id
        #          next # siguiente registro
        #      else
        #          f.evaluator_id = da.boss_position.user_id # asigno el jefe correcto   ######  (f.destroy # destruyo las que no corresponden)
        #          f.save
        #          #logger.info "ACtualizadooooo pibeee-------------------------------------------------------------".bold.blue
        #      end
        #  end
        #  next if plfc.blank?
        #  plfc.each do |f| #recorro las PlanificationCompetences para saber si el jefe contenido en el position id de cada usuario esta correcto en competencias
        #      if f.evaluator_id == da.boss_position.user_id
        #          next # siguiente registro
        #      else
        #          f.evaluator_id = da.boss_position.user_id # asigno el jefe correcto   ######  (f.destroy # destruyo las que no corresponden)
        #          f.save
        #          #logger.info "ACtualizadooooo pibeee-------------------------------------------------------------".bold.blue
        #      end
        #  end
        #end
        ###############################################################################
        ###############################################################################
        ###############################################################################

        #logger.info "================ PLAN SELECT".red
        #ind_plan = (user.role == 'Administrador') ? Planification.all.map(&:evaluated_id) : Planification.all(evaluator_id: user_pos.user_id, :evaluated_id.not => curr_pos).map(&:evaluated_id)
        #ind_comp = (user.role == 'Administrador') ? PlanificationCompetence.all.map(&:evaluated_id) : PlanificationCompetence.all(evaluator_id: user_pos.id, :evaluated_id.not => curr_pos.user_id).map(&:evaluated_id)
        #indirectos = Position.all id: (ind_plan+ind_comp).uniq
        #logger.info "================ INDIRECTOS #{indirectos.inspect}".red

        #@data += indirectos
        # Falta incluir en data los matriciales
        @items = @data
        logger.info "=> store_location: #{session[:redirect_to]}"

        if params.has_key?('evaluated_id')
            session[:evaluated_id] = params[:evaluated_id]
            redirect_to kpi_performance_planifications_path
        else
            session[:evaluated_id] = nil
        end

        @item = Job.get session[:evaluated_id]
    end


  def planification_duplicar
      flash[:notice] = ""
      flash[:error] = ""
      if params[:origen].blank?
          flash[:error] = "Seleccione la planificación que desea duplicar"
      else
          if params[:duplicar].blank?
              flash[:error] = "Seleccione a quien desea copiar la planificación citada"
          else

              plorigen = Planification.all(evaluated_id: params[:origen].to_i)
              #logger.info "*********************COPIATRES : #{params[:duplicar].inspect}".red.bold
              dupli = params[:duplicar]

              dupli.each do |dup|
                  us = Position.first(id: dup.to_i)
                  asdsd = false
                  #logger.info "USUARIO A DUPLICAR: #{us.inspect}".yellow.bold
                  if !Planification.first(evaluated_id: us.id).blank?
                      flash[:error] += "#{us.user.first_name} #{us.user.last_name} Rut : #{us.user.rut} Ya posee planificaciones cargadas <br/>"
                      #redirect_to performance_planifications_path
                  else
                      plorigen.each do |org|
                          #logger.info "PLANIFICACION ORIGEN : #{org.inspect}".yellow.bold
                          item = KeyPerformanceIndicator.first(id: org.key_performance_indicator_id)

                          kpinew = KeyPerformanceIndicator.create(description: item.description)
                          #logger.info "kpinew :   #{kpinew.inspect}".blue.bold
                          kpinew.start_date     = item.start_date
                          kpinew.end_date       = item.end_date
                          kpinew.code           = item.code
                          kpinew.last_date_gold = item.last_date_gold
                          kpinew.increase_gold  = item.increase_gold
                          kpinew.gold           = item.gold
                          kpinew.unit           = item.unit
                          kpinew.ponderation    = item.ponderation


                          #logger.info "KPI NEWWWW #{kpinew.inspect}".red.bold

                          if kpinew.save
                              plnew = Planification.create(evaluated_id: us.id)

                              plnew.closed = false
                              plnew.evaluated_closed = false
                              plnew.evaluator_closed = false

                              plnew.evaluator_id = us.boss_position_id
                              plnew.key_performance_indicator_id = kpinew.id
                              plnew.period_id = org.period_id
                              asdsd = plnew.save
                              if asdsd

                              else

                                flash[:error] += "Error al duplicar para #{us.user.first_name} #{us.user.last_name} Rut : #{us.user.rut} <br/>"
                                Rails.logger.info "ERRORES======".blue.bold
                                item.errors.each do |e|
                                  Rails.logger.info "#{e}".red.bold
                                end
                                Rails.logger.info "ERRORES======".blue.bold
                              end
                          else

                              flash[:error] += "Error al duplicar para #{us.user.first_name} #{us.user.last_name} Rut : #{us.user.rut} <br/>"
                              Rails.logger.info "ERRORES======".blue.bold
                              item.errors.each do |e|
                                Rails.logger.info "#{e}".red.bold
                              end
                              Rails.logger.info "ERRORES======".blue.bold
                          end
                      end
                  end
                  flash[:notice] += "Duplicado correctamente para #{us.user.first_name} #{us.user.last_name} Rut : #{us.user.rut} <br/>" if asdsd
              end
          end
      end
      redirect_to performance_planifications_path
  end
  ################### KPI #####################
  def kpi
    opts = {:evaluated_id => evaluated.user_id,
            :order => [:key_performance_indicator_id.asc], :period => current_period}
    @items = Planification.all(opts)
    @position = evaluated
    #unless @items.blank?
    #    c = 0
    #    @items.each do |plc|
    #        c += plc.ponderation #= (100.to_f / @items.count.to_f).round(2)
    #        #plc.save
    #    end
    #    if (c > 100.to_f)
    #        logger.info "Ponderacion total : #{c}".blue.bold
    #        @items.each do |plc|
    #            plc.ponderation = (100.to_f / @items.count.to_f).round(2)
    #            plc.save
    #        end
    #      # logger.info "Ponderacion total despues de regular: #{@items.count * @items.first.ponderation}".red.bold
    #    end
    #end
    @item = KeyPerformanceIndicator.new
  end

  def kpisave
    #TODO: Cambiar evaluator ID por la session
    planification = Planification.new :evaluator_id => evaluated.boss_position.user_id, :evaluated_id => evaluated.user_id if current_user.role.name == "Administrador"
    planification = Planification.new :evaluator_id => current_user.user_id, :evaluated_id => evaluated.user_id if current_user.role.name != "Administrador"
    planification.period = current_period

    item = KeyPerformanceIndicator.new
    item.attributes = kpiparse



    if item.save
      planification.key_performance_indicator = item
      planification.save
      puts planification.inspect.green.bold
    else
      Rails.logger.info "ERRORES======".red.bold
        item.errors.each do |e|
          Rails.logger.info e
        end
      Rails.logger.info "======".red.bold
    end

    redirect_to kpi_performance_planifications_path
  end

  def kpiedit
    @item = KeyPerformanceIndicator.get params[:id]
    @position = evaluated
  end

  def kpiupdate

    item = KeyPerformanceIndicator.get params[:id]
    planification = Planification.first :evaluated_id => evaluated.user_id,
                                        :period_id => current_period.id,
                                        :key_performance_indicator_id => item.id
    if planification.blank?
      planification = Planification.first :evaluated_id => evaluated.user_id,
                                          :period_id => current_period.id,
                                          :key_performance_indicator_id => item.id
    end

    item.attributes = kpiparse

    begin
      item.save
    rescue
      Rails.logger.info "ERRORES======"
        item.errors.each do |e|
          Rails.logger.info e
        end
        Rails.logger.info "======"
    end

    redirect_to kpi_performance_planifications_path
  end

  def kpidelete

    item = KeyPerformanceIndicator.get params[:id]
    logger.info "evaluated.id #{evaluated.id} "
    planification = Planification.first :evaluated_id => evaluated.user_id,
                                        :period_id => current_period.id,
                                        :key_performance_indicator_id => item.id
    if planification.blank?
      planification = Planification.first :evaluated_id => evaluated.user_id,
                                          :period_id => current_period.id,
                                          :key_performance_indicator_id => item.id
    end
    planification.key_performance_indicator_id = nil
    item.destroy
    planification.destroy

    redirect_back_or_default
  end

  def kpi_aprove
   is_evaluated = evaluated.user_id == current_user.user_id
   items = Planification.all :evaluated_id => evaluated.user_id, :period_id => current_period.id
   evaluator_id = nil

   items.each do |i|
     if is_evaluated
       i.evaluated_closed = true
     else
       i.evaluator_closed = true
     end

     if i.evaluator_closed == i.evaluated_closed && i.evaluator_closed == true
       i.closed = true
     end

     i.save
     evaluator_id = i.evaluator_id
   end

   evaluator = Position.first user_id: evaluator_id

   if !is_evaluated #jefe cierra planificacion
       #MetasMailer
       mail = evaluated.user.email
       link = "#{request.base_url}" + aprobarmetasporcorreo_desempenoaprobe_index_path(id:evaluated.user_id)
       MetasMailer.enviar_solicitud(mail,link).deliver
   else #colaborador aprueba planificacion
       #MetasMailerEval
       mail = evaluator.user.email
       MetasMailerEval.enviar_solicitud(mail, evaluated.user.full_name).deliver
   end

   redirect_back_or_default
 end

  #################### COMPETENCES #######################
  def competences
    @items = PlanificationCompetence.all(:evaluated_id => evaluated.user_id,
                                          :period => current_period)

    unless @items.blank?

        ponderacion = 0
        @items.each do |x|
            ponderacion += x.ponderation.to_i
        end
        logger.info "Ponderacion total : #{ponderacion}".blue.bold
        if (ponderacion > 100.to_f ||  ponderacion < 100.to_f )

            @items.each do |plc|
                plc.ponderation = (100.to_f / @items.count.to_f).round(2)
                plc.save
            end
              logger.info "Ponderacion total despues de regular: #{@items.count * @items.first.ponderation}".red.bold
        end
    end

    @item = PlanificationCompetence.new


    @areas = Area.all.to_json
    @tipos = CompetenceType.all.to_json
    @competencias = Competence.all.to_json
    @conductas = Conduct.all.to_json
  end

  def competences_save
    item = PlanificationCompetence.new params[:planification_competence]
    item.evaluator_id = evaluated.boss_position.user_id
    puts params.inspect.red.bold
    #System.exit(0)
    if !params[:competence].blank? && !params[:conducts].blank?
      item.competence_id = params[:competence]
      item.conduct = params[:conducts].to_json
    elsif !params[:competencia].blank? && !params[:conductas].blank?
      item.competence_id = params[:competencia]
      item.conduct = params[:conductas].to_json
    else
      flash[:error] = "No ingresar datos nulos"
    end

    item.period = current_period

    if item.save
      plans = PlanificationCompetence.all(:evaluated_id => params[:planification_competence][:evaluated_id])
      ponderacion = (100.to_f / plans.count.to_f).round(2)
      logger.info "ponderacion : #{ponderacion}".blue
      plans.update(:ponderation => ponderacion)
    else
      Rails.logger.info "ERRORES======"
        item.errors.each do |e|
          Rails.logger.info e
          flash[:error] = "No ingresar datos nulos"
        end
        Rails.logger.info "======"
    end

    redirect_to :action => 'competences'
  end

  def competences_delete
    item = PlanificationCompetence.get params[:id]
    item.destroy

    if item.destroyed?
      plans = PlanificationCompetence.all(:evaluated_id => item.evaluated_id)
      ponderacion = (100.to_f / plans.count.to_f).round(2)
      logger.info "ponderacion : #{ponderacion}".blue
      plans.update(:ponderation => ponderacion)
    end

    unless item.destroyed?
        session[:error] = "Existen seguimientos y/o evaluaciones asignados a estas competencias, no se puede eliminar"
    end

    redirect_back_or_default
  end

  def competences_aprove
    is_evaluated = evaluated.user_id == current_user.user_id

    items = PlanificationCompetence.all :evaluated_id => evaluated.user_id, :period_id => current_period.id

    items.each do |i|

      if is_evaluated
        i.evaluated_closed = true
      else
        i.evaluator_closed = true
      end

      if i.evaluator_closed == i.evaluated_closed && i.evaluator_closed == true
        i.closed = true
      end

      i.save

    end

    if !is_evaluated #jefe cierra planificacion
        #MetasMailer
        mail = evaluated.user.email
        link = "#{request.base_url}" + aprobarcompetenciaporcorreo_desempenoaprobe_index_path(id:evaluated.user_id)
        CompetenceMailer.enviar_solicitud(mail,link).deliver
    else # colaborador aprueba planificacion
        #MetasMailerEval
        mail = evaluator.user.email
        CompetenceMailerEval.enviar_solicitud(mail, evaluated.user.full_name).deliver
    end

    redirect_back_or_default
  end

  private

  def has_evaluated?
    logger.info "has_evaluated? #{session[:evaluated_id]} evaluated: #{evaluated.inspect}"



    session[:notice] = nil

    session[:notice] = 'No ha seleccionado a un colaborador' if (!(session.has_key?(:evaluated_id) && session[:evaluated_id].to_i > 0) or evaluated.nil?)
    session[:notice] = 'No existe un período actual, el administrador debe crearlo' if current_period.nil?

    redirect_to performance_planifications_path unless session[:notice].blank?
  end

  def kpiparse
    data = params[:key_performance_indicator]
    data[:ponderation] = data[:ponderation].to_i

    data
  end

end
