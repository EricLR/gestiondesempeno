# -*- encoding : utf-8 -*-
class UtilsController < ApplicationController

	def index
		render :json => {:status => 'hola!'}
	end

	def competence_types
		render :json => CompetenceType.all.map {|i| [i.id, i.name]}
	end

	def competences
		where = {visible: true}

		where[:competence_type_id] = params[:competence_type_id] if params[:competence_type_id]

		render :json => Competence.all(where).map { |i| [i.id, i.name, i.description]}
	end

	def conducts
		where = {visible: true}
		where[:competence_id] = params[:competence_id] if params[:competence_id]

		render :json => Conduct.all(where).map { |i| [i.id, i.number, i.description] }
	end

	def monitoring_conducts
		if Rails.configuration.competencias == :saville
				pl = PlanificationCompetence.get(params[:competences])
				aux = JSON(pl.conduct).map{ |i| i = Conduct.get(i); [i.description] }
				cond = ""
				aux.each do |x|
						cond += " - #{x[0]}"
				end
				@items = [[pl.id, cond]]
				puts @items.inspect.blue.bold
		else
				@items = JSON(PlanificationCompetence.get(params[:competences]).conduct).map { |i| i = Conduct.get(i); [i.id, i.description] }
				#puts @items.inspect.green.bold
		end
		render layout: nil
	end

end
