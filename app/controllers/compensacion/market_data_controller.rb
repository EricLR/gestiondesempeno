# -*- encoding : utf-8 -*-
class Compensacion::MarketDataController < ApplicationController
    before_filter :store_location, :only => [:index, :equidad]
    helper_method :grafico_equidad_interna

    def index
        render text: do_cache('index', cache: true){
            # jobs_col       = Job.all
            # jobs           = dm_to_a jobs_col
            # users          = dm_to_a jobs_col.positions.users
            calculate params: params
            #@incomes_ids     = @incomes_show#@equidad_configs.map{|x| x[:income].id}
        }.html_safe
    end

    def equidad


        @incomes_sel        = IncomeStructure.all order: :position.asc
        @banda_inferior     = current_user.global_config(:ei_banda_inferior)||20
        @banda_superior     = current_user.global_config(:ei_banda_superior)||20
        @graficos_guardados = EquidadConfig.all

        if params[:option].blank?
          tmp_opt = @graficos_guardados.select{|x| x.income_id == params[:income_id].to_i}
          params[:option] = EquidadPunto::ECUACIONES_PARAMS[tmp_opt.first.value.to_i].downcase.to_s unless tmp_opt.blank?
        end

    end

    def equidad_agregar_puntos
        kind   = params[:option]||'linear'
        query  = "#{kind}_sections_#{params[:income_id]}"
        puntos = current_user.global_config query
        puntos = puntos.blank? ? nil : JSON::parse(puntos)
        tmp    = puntos.length == 0 ? [] : puntos

        if tmp.count == 1 && tmp[0].last == 1
            tmp = []
        end


        current_user.set_global_config query, (tmp + [[params[:bloque_inicio].to_i, params[:bloque_fin].to_i, 0]]).to_json

        flash[:notice] = 'Bloque agregado correctamente'

        redirect_to equidad_compensacion_market_data_path+'?income_id='+params[:income_id]+'&option='+params[:option]
    end

    def equidad_eliminar_puntos
        kind   = params[:option]||'linear'
        query  = "#{kind}_sections_#{params[:income_id]}"
        puntos = current_user.global_config query
        puntos = puntos.blank? ? nil : JSON::parse(puntos)
        tmp    = []


        unless puntos.blank?
            puntos.each_with_index do |p, k|
                if k.to_i != params[:delete_key].to_i
                    tmp << p
                end
            end
        end

        tmp = [] if tmp.count == 1 && tmp[0].last == 1

        current_user.set_global_config query, tmp.to_json

        flash[:notice] = 'Bloque eliminado correctamente'

        redirect_to equidad_compensacion_market_data_path+'?income_id='+params[:income_id]+'&option='+params[:option]
    end

    def ei_save_option
        current_user.set_global_config :ei_equation, params[:option]
        FileUtils.rm_rf("#{Rails.root}/tmp/cache/administracion-de-la-compensacion/datos-de-mercado")

        redirect_to equidad_compensacion_market_data_path+'?income_id='+params[:income_id]+'&option='+params[:option]
    end

    def ei_informacion
        #render text: do_cache('ei_informacion', cache: true){
        calculate params: params
        #ecuacion        = EquidadPunto.get_ecuacion(current_user.global_config(:ei_equation))
        @equidad_puntos  = EquidadPunto.all(tipo: EquidadPunto::DATOS_LINEA).to_a
        #@equidad_puntos = EquidadPunto.all ecuacion: ecuacion, tipo: EquidadPunto::DATOS_LINEA
        @equidad_configs = EquidadConfig.data.to_a
        @incomes_ids     = @equidad_configs.map{|x| x[:income].id}
        #}.html_safe
    end

    def ei_guarda_puntos
        ecuacion = EquidadPunto.get_ecuacion(params[:ecuacion])

        save = params[:save].blank? ? false : true
        logger.info "SAVE: #{save} ECUACION: #{ecuacion}".red

        if save
            EquidadPunto.all(income_structure_id: params[:income_id], ecuacion: ecuacion).destroy

            EquidadConfig.set "income_structure_id:#{params[:income_id]}", ecuacion.to_s
        end


        datos          = []
        datos_linea    = []
        datos_superior = []
        datos_inferior = []


        params[:datos].each do |d|
            p  = d.last
            eq = EquidadPunto.new   income_structure_id: params[:income_id].to_i,
                                    ecuacion: ecuacion,
                                    tipo: EquidadPunto::DATOS,
                                    x: p[0].to_f, y: p[1].to_f
            eq.save if save

            datos   << eq


        end unless params[:datos].blank?

        params[:linea].each do |l|
            p  = l.last
            eq = EquidadPunto.new   income_structure_id: params[:income_id].to_i,
                                    ecuacion: ecuacion,
                                    tipo: EquidadPunto::DATOS_LINEA,
                                    x: p[0].to_f, y: p[1].to_f
            eq.save     if save
            datos_linea << eq

        end unless params[:linea].blank?


        params[:superior].each do |d|
            p = d.last

            eq = EquidadPunto.new   income_structure_id: params[:income_id].to_i,
                                    ecuacion: ecuacion,
                                    tipo:     EquidadPunto::DATOS_SUPERIOR,
                                    x:        p[0].to_f, y: p[1].to_f
            eq.save        if save
            datos_superior << eq
        end unless params[:superior].blank?
        params[:inferior].each do |d|
            p = d.last

            eq = EquidadPunto.new   income_structure_id: params[:income_id].to_i,
                                    ecuacion: ecuacion,
                                    tipo:     EquidadPunto::DATOS_INFERIOR,
                                    x:        p[0].to_f, y: p[1].to_f
            eq.save        if save
            datos_inferior << eq

        end  unless params[:inferior].blank?

        # params[:puntos].each do |p|
        #     EquidadPunto.create income_structure_id: params[:income_id],
        #                         ecuacion: params[:ecuacion],
        #                         tipo: params[:tipo],
        #                         x: p[0], y: p[1]
        # end

        render json: {status: 'ok'}.merge(EquidadPunto.calcula_resumen(params[:income_id].to_i, ecuacion, {datos: datos, linea: datos_linea, superior: datos_superior, inferior: datos_inferior}))
    end

    #crud :market_data, :use_class_name_as_title => true, :redirect_to_url => 'compensation_market_data_path'
    def comp_graficos
        @incomes_sel        = IncomeStructure.all order: :position.asc
        @graficos_guardados = EquidadConfig.all
        calculate  params: params
        @incomes_arr = IncomeStructure.all id: @incomes_ids
    end
    def mantenedor
        @percentiles = MarketPercentile.all(order: :position.asc)
        @estructuras = IncomeStructure.all(order: :position.asc)


        @data = []
        MarketData.all(order: [:income_structure_id.asc, :nivel.asc]).each do |md|
            tmp = md.attributes
            tmp.delete(:created_at)
            tmp.delete(:updated_at)
            tmp[:income_structure] = @estructuras.select{|x| x.id == md.income_structure_id}.first.name

            mpds = MarketPercentileData.all market_data: md

            mpd_s = mpds.select{|x| x.selected == true}.first
            unless mpd_s.blank?
                mp                   = @percentiles.select{|x| x.id == mpd_s.market_percentile_id}.first
                tmp[:estadigrafo]    = mp.name
                tmp[:estadigrafo_id] = mp.id
            end


            @percentiles.each do |i|
                mp_v = mpds.select{|x| x.market_percentile_id == i.id}.first
                tmp["percentile_#{i.id}".to_sym] = mp_v.blank? ? '' : mp_v.value

            end

            @data << tmp
        end



    end

    def guardar_mantenedor
        data    = params[:data]
        ids     = []
        mk_data = MarketData.all

        data.each{|item_id, item| ids << item[:id].to_i unless ids.include?(item[:id].to_i)}

        mk_data.each do |item|
            unless ids.include? item.id
                item.market_percentile_data.destroy!
                item.destroy!
            end
        end

        data.each do |item_id, item|

            structure = IncomeStructure.first name: item[:income_structure]

            ids << item[:id] unless ids.include?(item[:id])



            market_data = MarketData.get(item[:id])
            market_data = MarketData.new if market_data.blank?

            market_data.nivel            = item[:nivel]
            market_data.banda_superior   = item[:banda_superior]
            market_data.banda_inferior   = item[:banda_inferior]
            market_data.income_structure = structure
            market_data.save


            ## Proceso los datos de percentiles
            item.each do |k, v|

                if k.include?("percentile_")
                    percentile = MarketPercentile.get k.gsub("percentile_", "").to_i
                    if percentile
                        mpd = MarketPercentileData.first_or_create( {market_data: market_data, market_percentile: percentile})

                        mpd.value    = v
                        mpd.selected = (percentile.name == item[:estadigrafo])
                        mpd.save
                    end
                end
            end
        end

        render json: {success: true}
    end

    def bandas_equidad_interna # MANTENEDOR

    end


    def calculo_personal

        users_opts = {params: params}

        job_opts = {status: Job::ENABLED}
        unless users_opts[:params].blank?
            params = users_opts.delete(:params)

            users_opts[:sex]             = params[:sex] unless params[:sex].blank?
            users_opts[:name]             = params[:job] unless params[:job].blank?
            users_opts[:estamento]       = params[:estamento] unless params[:estamento].blank?
            users_opts[:jornada]         = params[:jornada] unless params[:jornada].blank?
            users_opts[:dependency_type] = params[:dependency_type] unless params[:dependency_type].blank?
            users_opts[:ceco]            = params[:ceco] unless params[:ceco].blank?
            users_opts[:subceco]         = params[:subceco] unless params[:subceco].blank?
        end

        opts = {has_compensation_data: true}
        opts.merge!(users_opts)
        #opts = users_opts.merge ({has_compensation_data: true})
        # ,
        #                     subchild: {
        #                         job: {fields: [:id, :name, :nivel]}.merge(job_opts)
        #                     }
        # jobs = dm_a Job, job_opts
        # @jobs = jobs
        # Completed 200 OK in 155029ms (Views: 1365.3ms | Models: 68.390ms)
        if params[:user_id].blank?
            # Muestra listado para seleccionar usuario
            @items = []
            # dm_a(User, opts).each do |item|
            #     job = jobs.find{|x| x[:id] == item[:position_job_id]}

            #     next if job.blank?
            #     job_item = {}
            #     job.each{|i| job_item[("job_"+i.first.to_s).to_sym] = i.last}
            #     @items << item.merge(job_item)
            # end
            User.list_with_jobs(opts).each do |u|
                #puts u.inspect.red.bold
                @items << {id: u.id, rut: u.rut, name: u.full_name, job_name: u.job_name, job_level: u.job_level}
            end

        else # Con el usuario seleccionado, hago la simulación
            calculo_personal_datos params[:user_id], opts

        end
    end
    def guardar_calculo_personal
        current_user.set_global_config "calculo_personal_#{params[:user_id]}", params[:datos]
        current_user.set_global_config "calculo_personal_ratios_#{params[:user_id]}", params[:ratios]
        current_user.set_global_config "calculo_personal_mercados_#{params[:user_id]}", params[:mercados]
        current_user.set_global_config "calculo_personal_mercados_ratios_#{params[:user_id]}", params[:mercados_ratios]
        current_user.set_global_config "calculo_personal_empresas_#{params[:user_id]}", params[:empresas]
        current_user.set_global_config "calculo_personal_empresas_ratios_#{params[:user_id]}", params[:empresas_ratios]
        current_user.set_global_config "calculo_personal_totales_#{params[:user_id]}", params[:totales]
        current_user.set_global_config "calculo_personal_sumatorias_#{params[:user_id]}", params[:sumatorias]

        render json: {status: "ok"}
    end


    def calculo_personal_comparativo

        user_id = params[:user_id].to_i
        opts = {:id.not => user_id}

        user     = User.get user_id
        position = user.position
        job      = position.job

        @user     = user.attributes
        @user_obj = user
        @position = position
        @job      = job
        @user     = user.attributes
        @user_obj = user
        @position = position
        @job      = job
        @income = IncomeStructure.all fields: [:id , :banda_inferior , :banda_superior]
        @user[:job_name] = job.name
        logger.info "income #{@income.inspect} ".green.bold
        par = {:params=>{"action"=>"comp_graficos", "controller"=>"compensacion/market_data", "option"=>"linear"}}
        calculate par
        calculo_personal

        case params[:comparar_tipo]
            when "nivel"
                jobs = Job.all level: job.level
                opts[:id] = jobs.positions.users(has_compensation_data: true).map(&:id)
            when "pares"
                opts[:id] = job.positions.users(has_compensation_data: true).map(&:id)
            when "nivel-superior"
                nivel_superior = Job.first fields: [:level], :level.gt => job.level, limit: 1, order: :level.asc

                opts[:id] = nivel_superior.blank? ? [] : Job.all(fields: [:id], level: nivel_superior.level).positions.user(fields: [:id], has_compensation_data: true).map(&:id)

            when "jefaturas"

                opts[:id] = job.boss.blank? ? [] : job.boss.positions.users(has_compensation_data: true).map(&:id)
            when "cargo"
                opts[:id] = Job.all(:name.like => @job.name).positions.users(has_compensation_data: true).map(&:id)
            when "centro-costos"
                opts[:id] = Job.all(:centro_costos.like => @job.centro_costos).positions.users(has_compensation_data: true).map(&:id)

        end



        @incomes, @incomes_show, @masa_salarial, @users, @data = CompensationUtils::masa_salarial( opts )

        datos_guardados_calculo user_id
        logger.info "@datos guardados #{@datos_guardados}"

        respond_to do |format|

              format.html
              format.pdf do
                render :pdf => "my_pdf", # pdf will download as my_pdf.pdf
                  #:javascript_delay => 10000,
                  :layout => 'pdf_comparativo', # uses views/layouts/pdf.haml
                  :show_as_html => params[:debug].present? # renders html version if you set debug=true in URL
              end
          end
        #if @datos_guardados.blank?
        #    flash[:error] = "Debe realizar una simulación antes de comparar"
        #    redirect_to calculo_personal_compensacion_market_data_path(params[:user_id])
        #end

        # @masa_salarial = [user_masa_salarial]+@masa_salarial





        # opts = {
        #         has_compensation_data: true,
        #         fields:          [:id, :rut, :first_name, :last_name],
        #         child:           {
        #             position: {
        #                 fields: [:id, :user_id, :job_id],
        #                 subchild: {
        #                     job: {fields: [:id, :name, :nivel]}
        #                 }
        #             }
        #         }
        #     }
        # calculo_personal_datos params[:user_id], opts



    end

    private
    def calculate users_opts = {}

        fields   = [:id, :name, :level]

        job_opts = {fields: fields, status: Job::ENABLED, :level.gte => 0}
        #income_search = {order: :position.asc}
        unless users_opts[:params].blank?
            #income_search = {id: users_opts[:params][:income_id].to_i, order: :position.asc}
            params = users_opts.delete(:params)
            users_opts[:gender]        = params[:gender] unless params[:gender].blank?
            job_opts[:job_class]       = JobClass.first(name: params[:job_class]) unless params[:job_class].blank?
            job_opts[:jornada]         = params[:jornada] unless params[:jornada].blank?
            job_opts[:level]           = params[:level] unless params[:level].blank?
            job_opts[:name]            = params[:job] unless params[:job].blank?
        end
        logger.info "Job opts: #{{fields: [:id, :name, :level]}.merge(job_opts)}"

        user_fields = [:id, :rut, :first_name, :last_name]

        users = dm_a User, {
                                has_compensation_data: true,
                                fields:          user_fields,
                                child:           {
                                    position: {
                                        fields: [:id, :user_id, :job_id]
                                    }
                                }
                            }.merge(users_opts)
        jobs = dm_a Job, job_opts
        @items = []

        users.each do |item|
            job = jobs.select{|x| x[:id] == item[:position_job_id]}

            next if job.blank?
            job_item = {}
            job.first.each{|i| job_item[("job_"+i.first.to_s).to_sym] = i.last}
            @items << item.merge(job_item)
        end


        @incomes      = IncomeStructure.all order: :position.asc #income_search
        @income_data  = @incomes.income_structure_data

        @market_data            = dm_a MarketData, fields: [:id, :nivel, :banda_inferior, :banda_superior, :income_structure_id]
        @market_percentile      = dm_a MarketPercentile, {fields: [:id, :name]}
        @market_percentile_data = dm_a MarketPercentileData, {
                                        selected: true,
                                        fields: [:id, :value, :market_data_id, :market_percentile_id]
                                    }
        @market_data.each_with_index do |i, k|
            tmp = @market_percentile_data.select{|x| x[:market_data_id] == i[:id]}.first
            unless tmp.blank?
                @market_data[k][:market_percentile_data] = tmp
                tmp2 = @market_percentile.select{|x| x[:id] == tmp[:market_percentile_id]}.first
                unless tmp2.blank?
                    @market_data[k][:market_percentile] = tmp2
                end
            end

        end

        @incomes_show = []
        @incomes_ids  = []

        @items.each_with_index do |v, k|

            @incomes.each_with_index do |iv, ik|
                @items[k][:incomes] = {} if @items[k][:incomes].blank?

                tmp = @income_data.select{|x| x.position_id == @items[k][:position_id] && x.income_structure_id == iv.id}


                @incomes_show << ik if !@incomes_show.include?(ik)
                aux = 0
                tmp.each do |tmps|
                    if tmps.value
                        aux += tmps.value.to_i
                        mkd_tmp = @market_data.select{|x|
                                x[:income_structure_id] == iv.id &&
                                x[:nivel] == v[:job_level]
                                #!v[:nivel].blank? &&
                                #x[:nivel] == v[:nivel]
                            }


                        i_hash = {
                            income_id: iv.id,
                            income_value: (tmps.blank? ? [] : aux),#tmps.value.to_i),
                            income_detail: (tmps.blank? ? [] : tmps.detail),
                            market_percentile_data: (mkd_tmp.blank? ? [] : mkd_tmp.first)
                        }
                        unless (mpd = i_hash[:market_percentile_data]).blank?
                            if !mpd[:market_percentile_data].blank? && !mpd[:market_percentile_data][:value].blank?
                                mkpdv = mpd[:market_percentile_data][:value]
                                mkbi  = mpd[:banda_inferior]
                                mkbs  = mpd[:banda_superior]

                                i_hash[:banda_inferior] = (mkpdv*(100-mkbi))/100
                                i_hash[:banda_superior] = (mkpdv*(100+mkbs))/100
                                i_hash[:value]          = mkpdv.to_i
                                #next if mkpdv.to_i <= 0
                            end
                        end
                        @incomes_ids << tmps.income_structure_id if !@incomes_ids.include?(tmps.income_structure_id)
                        @items[k][:incomes][ik] = i_hash
                    end
                end

            end
        end

        @market_lines       = []
        @market_lines_bands = []


        @incomes_show.each do |isk|

            is = @incomes[isk].id
            #puts is.inspect.red
            market_data             = @market_data.select{|i| i[:income_structure_id] == is.to_i}
            #puts market_data.inspect.blue
            next if is.blank?
            unless market_data.blank?
                @market_lines[is]       = market_data.map{|x| [x[:nivel], x[:market_percentile_data][:value]] unless x[:market_percentile_data].blank?}

                @market_lines[is]       = @market_lines[is].sort_by{|x| x.first.nil? ? 0 : x.first} unless @market_lines[is].blank?
                @market_lines_bands[is] = market_data.map{|x| [x[:banda_inferior], x[:banda_superior]]}
            end
        end
        puts @market_lines.inspect.green


    end

    def calculo_personal_datos user_id, opts
        user_id = user_id.to_i

        datos_guardados_calculo user_id

        opts[:id] = user_id

        #@user = User.list_with_jobs opts
        @user = User.list_with_jobs_compe opts

        if @user.blank?
            user  = User.first( opts )
            @user = user.attributes
        end

        @position = Position.first user_id: @user.class.eql?(Array) ? @user.first : @user[:id]
        @equidad_puntos  = dm_a EquidadPunto, {tipo: EquidadPunto::DATOS_LINEA, fields: [:ecuacion, :x, :y, :tipo, :income_structure_id]}
        #@equidad_puntos = EquidadPunto.all ecuacion: ecuacion, tipo: EquidadPunto::DATOS_LINEA
        @equidad_configs = EquidadConfig.data

        redirect_to calculo_personal_compensacion_market_data_path if @user.blank?

        #@user         = @user.first
        job_sel = @position.job #@jobs.select{|x| x[:id] == @user[:position_job_id]}.first
        unless job_sel.blank?
            @user[:job_level] = job_sel[:level]
            @user[:job_name] = job_sel[:name]
        end
        @incomes      = IncomeStructure.all order: :position.asc
        @incomes_show = []
        #binding.pry
        data                    = @incomes.income_structure_data(position_id: @position.id, fields: [:id, :value, :detail, :periodicity, :periodicity_detail])
        counter                 = 0
        @items                  = []
        @market_data            = MarketData.all nivel: @user[:job_level], income_structure_id: @incomes.map(&:id)
        @market_percentile_data = @market_data.market_percentile_data(selected: true)
        sumatorias              = 0

        # Recorro incomes
        @incomes.each_with_index do |income, ik|

            # Selecciono incomestructuredata según el income actual
            i_d = data.select{|x| x.income_structure_id == income.id}

            # Guardo los que mostraré

            @incomes_show << income.id if !@incomes_show.include?(income.id) && !i_d.select{|x| x.value.to_i >= 0}.blank?
            unless i_d.blank?
                #i_d      = i_d.first
                ecuacion        = @equidad_configs.select{|x| x[:income].id == income.id}
                ecuacion        = ecuacion.first[:ecuacion] unless ecuacion.blank?
                equidad         = @equidad_puntos.select{|x| x[:x] == @user[:job_level] && x[:income_structure_id] == income.id && x[:ecuacion] == ecuacion}.uniq
                market_tmp      = @market_data.select{|x| x.income_structure_id == income.id}
                market_tmp      = market_tmp.blank? ? nil : market_tmp.first

                if market_tmp.blank?
                    market_data_tmp = nil
                else
                    market_data_tmp = @market_percentile_data.select{|x| x.market_data_id == market_tmp.id}
                    market_data_tmp = market_data_tmp.blank? ? nil : market_data_tmp.first
                end

                equidad_tmp = equidad.blank? ? nil : equidad.first
                total_tmp =0

                i_d.each do |idd|
                    #puts i_d.inspect.green.bold
                    #puts "============================================================"
                    counter += idd.value.to_f
                    total_tmp   = idd[:detail].sum{|x| x.second.blank?||!IncomeStructureData.exception?(x.first) ? 0 : x.second/(idd[:periodicity_detail][x.first].blank? ? 1 : IncomeStructureData::PERIDIOCIDAD[idd[:periodicity_detail][x.first].to_i])}
                    sumatorias += total_tmp.to_f
                end
                @items << {
                    income:                     income,
                    detalle:                    i_d,
                    data:                       i_d,
                    counter:                    counter,
                    sumatoria:                  i_d.first.blank? ? 0 : i_d.first.value.to_i,
                    total:                      total_tmp,
                    total_sum:                  sumatorias,
                    ecuacion:                   ecuacion,
                    equidad:                    equidad_tmp,
                    market:                     market_tmp,
                    market_ratio:               equidad_tmp.blank?||market_data_tmp.blank? ? nil : ( (sumatorias * 100) / equidad_tmp[:y].to_i ).round,
                    market_data:                market_data_tmp,
                    market_data_bands:          market_tmp.blank? ? nil : [market_tmp.banda_inferior, market_tmp.banda_superior],
                    market_data_banda_inferior: market_tmp.blank? ? nil : market_tmp.banda_inferior,
                    market_data_banda_superior: market_tmp.blank? ? nil : market_tmp.banda_superior,
                    market_data_ratio:          (market_data_tmp.blank?||market_data_tmp.value.blank?) ? nil :  ( (sumatorias * 100) / (market_data_tmp.value == 0 ? 1 : market_data_tmp.value) ).round
                }

                #end

            end
        end
    end

    def grafico_equidad_interna income_id, kind = "linear", params = []
        unless income_id.blank?
            @sel_income = @incomes_sel.select{|x| x.id == income_id.to_i}.first

            calculate params: params
            # @market_data = MarketData.all income_structure: @sel_income,

            @data              = []
            @data_promedio     = []
            @data_mediana      = []
            @data_linear       = []
            @ecuaciones_linear = {}
            @lineas            = []
            # @market_data.each do |md|
            #     if md[:income_structure_id].to_i == income_id.to_i
            #         md_tmp = md[:market_percentile_data][:value].to_i
            #         md_bi  = md[:banda_inferior].to_i
            #         md_bs  = md[:banda_superior].to_i
            #         lineas << {
            #                 nivel: md[:nivel],
            #                 inferior: (md_tmp*(100-md_bi)/100),
            #                 superior: (md_tmp*(100+md_bs)/100),
            #                 valor: md_tmp}
            #     end
            # end
            # @items.each do |item|
            #   # sum        = 0
            #   # encontrado = false
            #   # @incomes.each do |i|
            #   #     next if encontrado
            #   #     #tmp = item[:incomes].select{|k, v| v[:income_value].blank? ? false : (v[:income_value][:income_structure_id] == i.id)}

            #   #     sum += #tmp.first.last[:income_value] unless tmp.blank?

            #   #     encontrado = true if i.id == @sel_income.id

            #   # end
            #   # next if sum <= 0
            #   @data << [item[:job_level].to_i, item[:incomes]value]]
            # end

            @items.each do |item|
                sum        = 0
                encontrado = false
                logger.info "ITEM: #{item}".red
                @incomes.each do |i|
                    next if encontrado
                    #tmp = item[:incomes].select{|k, v| k == i.id}
                    tmp = item[:incomes].find{|x| x.last[:income_id] == i.id}

                    next if tmp.blank?

                    sum += tmp.last[:income_value] #if !item[:incomes].blank? && !item[:incomes][i.id].blank?# unless tmp.blank?

                    encontrado = true if i.id == @sel_income.id

                end

                @data << [item[:job_level].to_i, sum] if item[:job_level].to_i > 0
            end



            grouped_data = @data.group_by(&:first)
            grouped_data.each do |k, v|
              tmp  = v.map(&:last)
              tmp2 = @data_promedio.blank? ? [] : @data_promedio.map(&:first)
              # PROMEDIO
              @data_promedio << [k, tmp.mean.round(2)] unless tmp2.include?(k)

              tmp2 = @data_mediana.blank? ? [] : @data_mediana.map(&:first)
              # MEDIANA
              @data_mediana << [k, tmp.median.round(2)] unless tmp2.include?(k)

            end

            @data_promedio = @data_promedio.sort_by(&:first)
            @data_mediana = @data_mediana.sort_by(&:first)

            niveles       = @data.map(&:first).sort!
            conf_sections = current_user.global_config("#{kind}_sections_#{@sel_income.id}")

            @secciones    = conf_sections.blank? ? [] : JSON::parse(conf_sections)



            if @secciones.blank?

                @secciones = [[niveles.first, niveles.last, 1]]
                current_user.set_global_config "#{kind}_sections_#{@sel_income.id}", @secciones.to_json

            end



            @secciones.each_with_index do |seccion, idx|
                sec    = [seccion.first, seccion.second].sort
                x_min  = sec.first
                x_max  = sec.second
                data_x = []
                data_y = []
                @data.sort_by(&:first).each do |dta|
                    if dta.first >= x_min && dta.first <= x_max
                        data_x << dta.first
                        data_y << dta.second
                    end
                end

                # Calculo regresión lineal
                linefit = LineFit.new
                linefit.setData data_x, data_y
                @intercept, @slope = linefit.coefficients
                @rSquared          = (lftmp = linefit.rSquared ).blank? ? 0 : lftmp.round(2)
                @intercept         = (lftmp = @intercept).blank? ? 0 : lftmp.round(2)
                @slope             = (lftmp = @slope).blank? ? 0 : lftmp.round(2)



                @ecuaciones_linear[idx] = {r2: @rSquared, intercept: @intercept, slope: @slope }

                begin
                    (x_min..x_max).each do |x|
                      y = (@slope * x) + @intercept
                      @data_linear << [x, y]
                    end
                rescue
                end

            end

        end

        return @data, @items, @data_linear, @intercept, @slope, @rSquared, @sel_income, @lineas, @market_data, income_id, @data_promedio, @data_mediana, @secciones, @ecuaciones_linear
  end

  def datos_guardados_calculo user_id
        @datos_guardados           = datos_json "calculo_personal_#{user_id}", true
        @ratios_guardados          = datos_json "calculo_personal_ratios_#{user_id}"
        @mercados_guardados        = datos_json "calculo_personal_mercados_#{user_id}"
        @mercados_ratios_guardados = datos_json "calculo_personal_mercados_ratios_#{user_id}"
        @empresas_guardados        = datos_json "calculo_personal_empresas_#{user_id}"
        @empresas_ratios_guardados = datos_json "calculo_personal_empresas_ratios_#{user_id}"
        @totales_guardados         = datos_json "calculo_personal_totales_#{user_id}"
        @sumatorias_guardados      = datos_json "calculo_personal_sumatorias_#{user_id}"
  end

  def datos_json str, null_to_array = false
        tmp = current_user.global_config str
        unless tmp.blank?
            tmp = JSON::parse( tmp )
            if null_to_array
                tmp2 = []
                tmp.each do |t|
                    tmp2 << (t.blank? ? [] : t)
                end
            else
                tmp2 = tmp
            end
        end
        tmp2.blank? ? [] : tmp2.map{|x| x.blank? ? 0 : x}
  end

end
