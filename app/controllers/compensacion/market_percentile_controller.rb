# -*- encoding : utf-8 -*-
class Compensacion::MarketPercentileController < ApplicationController
    before_filter :store_location, :only => :index

    crud :market_percentile, :use_class_name_as_title => true, :redirect_to_url => 'market_percentile_path'
end
