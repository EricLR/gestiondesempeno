# -*- encoding : utf-8 -*-
class Compensacion::CompensationController < ApplicationController
    UPLOAD_BASE_PATH = "compensations"
    UPLOAD_PATH = File.join(Rails.root, "public", UPLOAD_BASE_PATH)

    skip_before_filter :must_login, :only => [:masa_salarial]

    def index
        #redirect_back_or_default '/' unless role_cann ("cp_captura_xls")

        @file = current_user.config :compensation_file

        unless params[:clean].blank?
            #session[:file_path] = ""
            @file = current_user.set_config :compensation_file, nil
        end
        unless params[:file].blank?
            #session[:file_path] = File.join(UPLOAD_PATH, params[:file])
            @file = current_user.set_config :compensation_file, File.join(UPLOAD_PATH, params[:file])
        end

        if @file.blank?
            @last  = Compensation.last
            @files = Dir.entries File.join(UPLOAD_PATH)
        else # Para procesar
            # session[:file_path] = ""
            @estructuras = IncomeStructure.all

        end
    end

    def seleccion
        #session[:file_path] = ""

        unless params[:file].blank?
            current_user.set_config :compensation_file, params[:file]
            current_user.set_config :compensation_rel_column, []
            current_user.set_config :compensation_rel_period, []
            #session[:file_path] = params[:file]
            redirect_to definir_estructuras_compensacion_compensation_index_path
        end
        @last  = Compensation.last
        @files = Dir.entries File.join(UPLOAD_PATH)
        @files = @files.sort
        @file = current_user.config :compensation_file
    end

    def definir_estructuras

        tmp         = current_user.config :compensation_rel_column
        rel_col     = tmp.blank? ? [] : JSON.parse( tmp )
        @rel_col    = {}
        tmp         = current_user.config :compensation_rel_period
        @rel_period = tmp.blank? ? {} : JSON.parse(tmp)

        rel_col.each{|i| @rel_col[i[0].to_i] = i[1]}

        @file = current_user.config :compensation_file

        if @file.blank? #||!File.exists?(@file)
            flash[:error] = 'El archivo previamente almacenado no existe'
            redirect_to seleccion_compensacion_compensation_index_path
        else
            begin
                file = File.join(UPLOAD_PATH, @file)
                if file.include?('.xlsx')
                    xls = Roo::Excelx.new file
                elsif @file.include?('.xls')
                    xls = Roo::Excel.new file
                end

                length       = 1
                continua     = true
                columnas     = []
                contiene_rut = false

                while continua do
                    tmp = xls.cell(1, length)
                    if tmp.blank? # No hay celda, detengo
                        continua = false
                    else # Existe celda, la almaceno
                        columnas << tmp
                        contiene_rut = true if tmp.downcase.include?("rut")
                        length += 1
                    end
                end

                unless contiene_rut
                    flash[:error] = "El archivo '#{@file}' no tiene la columna RUT que es obligatoria. Corrija los problemas y vuelva a intentar"
                    FileUtils.rm(file)

                    redirect_to compensacion_compensation_index_path
                else

                    #session[:columnas]  = columnas
                    #session[:file_path] = file
                    @file                = current_user.set_config :compensation_file, @file
                    @columnas            = current_user.set_config :compensation_columns, columnas
                end

                if @file.blank?
                    flash[:notice] = "Debe seleccionar la planilla a utilizar"
                    redirect_to seleccion_compensacion_compensation_index_path
                end
                @estructuras = IncomeStructure.all
            rescue
                flash[:error] = "El archivo a utilizar no existe, cargue otro archivo."
                redirect_to seleccion_compensacion_compensation_index_path
            end


        end

    end

    def excel_puestos
        #xls   = XlsxWriter.new
        file = "tmp/excel-puestos.xls"
        xls = WriteExcel.new file
        sheet = xls.add_worksheet("Compensaciones")

        income_structures = IncomeStructure.all(order: :position.asc)


        # sheet.add_row(['Rut', 'Nivel', 'Puntos'] + income_structures.map{|x| x.name}) # TODO: Falta todo lo dinámico (Sueldo Base, etc)

        sheet.write_row(0,0, ['Rut', 'Nivel', 'Puntos', 'Sueldo Base (SB)', 'Gratificación Legal (Mensual)',
                                                 'Gratificación Voluntaria', 'Asignación de Caja (Mensual)',
                                                 'Asignación de Turno (Mensual)', '………', 'Aguinaldo de Fiestas Patrias (Anual)',
                                                 'Aguinaldo de Fin de Año (Anual)', 'Vacaciones (Anual)', 'Gratificación (Anual)',
                                                 'Bono expresado en SB', 'Bono por Desempeño Target (Anual)',
                                                 'Bono por Desempeño Real (Anual)', 'Promedio Beneficios (Mensual)'])

        users = dm_a User, {

                                fields:            [:id, :rut],
                                child: {
                                    position: {
                                        fields: [:id, :job_id, :user_id],
                                        subchild: {
                                            job: {fields: [:id, :level, :puntos]}
                                        }
                                    }
                                }
                            }

        # jobs      = Job.all(:status => Job::ENABLED)
        # positions = jobs.positions
        # users     = positions.users

        c = 1
        users.each do |user|
            unless user[:job_id].blank?
                sheet.write_row(c, 0, [user[:rut], user[:job_nivel], user[:job_puntos]])
                c += 1
            end
        end
        xls.close

        # FileUtils.mv xls.path, 'tmp/file.xlsx'
        send_file file, type: 'application/octet-stream'
    end



    def create
        if file = params[:upload][:file]

            name    = file.original_filename
            tmpfile = file.tempfile
            file    = File.join(UPLOAD_PATH, Time.now.strftime("%Y-%m-%d-%H%M%S").to_s + "_" + name.gsub(" ", ""))

            File.open(file, "wb") { |f| f.write tmpfile.read }

            if name.include?('.xlsx')
                xls = Roo::Excelx.new file
            elsif name.include?('.xls')
                xls = Roo::Excel.new file
            end

            length   = 1
            continua = true
            columnas = []
            contiene_rut = false
            while continua do
                tmp = xls.cell(1, length)
                logger.info "TMP: #{tmp}".red
                if tmp.blank? # No hay celda, detengo
                    continua = false
                else # Existe celda, la almaceno
                    columnas << tmp
                    contiene_rut = true if tmp.downcase.include?("rut")
                    length += 1
                end
            end

            unless contiene_rut
                flash[:error] = "El archivo '#{name}' no tiene la columna RUT que es obligatoria. Corrija los problemas y vuelta a intentar"
                FileUtils.rm(file)

                redirect_to compensacion_compensation_index_path
            else

                #session[:columnas]  = columnas
                #session[:file_path] = file
                @file                = current_user.set_config :compensation_file, file
                @columnas            = current_user.set_config :compensation_columns, columnas
                redirect_to seleccion_compensacion_compensation_index_path
            end




            # first = true
            # income_structure_ids = []

            # rnd_num = rand(1000)
            # file_path = ""

            # CSV.foreach(file.tempfile.path, :col_sep => ";") do |l|
            #     if first # CABECERAS DEL CSV
            #         file_path = "#{file.tempfile.path}-#{rnd_num}"

            #         l.each do |i| # BUSCANDO LOS NOMBRES DE LAS ESTRUCTURAS
            #             if i.downcase != 'rut'
            #                 structure = IncomeStructureDetail.first name: i
            #                 income_structure_ids << (structure ? structure.id : nil )
            #             end
            #         end
            #         first = false

            #     else # PROCESANDO DATOS

            #         if( user = User.first rut: l[0] ) # Usuario existe
            #             i = Compensation.create user: user, position: user.position, update_name: file_path

            #             (subl = l[1..-1]).each_index do |k|
            #                 CompensationDetail.create compensation: i, income_structure_detail_id: income_structure_ids[k], value: subl[k].gsub('.', '').to_i if( !k.nil? and !subl[k].nil? )
            #             end

            #         end
            #     end

            # end

            # # Guardo hash de acronimos para poder ver si el CompensationDetail que estoy viendo debe ser calculado
            # acronyms = IncomeStructure.all.map {|i| [i.id, i.acronym, i.income_structure_detail.map {|ic| ic.id}]}
            # # Recorro todos los insertados para realizar los cálculos de la sumatoria de los SB, SBFM, etc.
            # (comps = Compensation.all).each do |c|
            #     acronyms.each do |i|
            #         total = 0
            #         c.compensation_details(income_structure_detail_id: i[2]).each do |cd| # Obtengo todos 'gastos' que pertenecen a la estructura
            #             total += cd.value
            #         end

            #         CompensationDetail.create compensation: c, income_structure_detail: IncomeStructureDetail.first(name: i[1]), value: total
            #     end
            # end

        end


    end

    def eliminar_excel
        file = File.join(UPLOAD_PATH, params[:file])
        if File.exists?(file)
            File.delete(file)
            flash[:notice] = "Archivo eliminado correctamente"
        else
            flash[:error] = "El archivo no existe, intente nuevamente"
        end
        render json: {status: 'success'}
    end

    def excel_procesa_datos
      cols            = params[:rel_columna]
      @peridiocidad   = params[:peridiocidad_columna]
      cols_normalizar = params[:normalizar]
      current_user.set_config :compensation_rel_column, cols.to_json
      current_user.set_config :compensation_rel_period, @peridiocidad.to_json

      UserConfig.clean_config "calculo_personal_%"
      UserConfig.clean_config "%_sections_%"
      UserConfig.clean_config "horas"

      if cols.select{|k,v| v.include?( "rut" ) }.blank?
        flash[:error] = "No seleccionó la columna RUT que es obligatoria"
        redirect_to definir_estructuras_compensacion_compensation_index_path
      else
        file = current_user.config :compensation_file
        # Opcion 1
        if file.blank?
          flash[:notice] = "Debe seleccionar una planilla antes de trabajar con los datos"
          redirect_to seleccion_compensacion_compensation_index_path
        end
        # Opcion 2
        # file = Dir.entries( File.join(UPLOAD_PATH) ).last
        if params[:rel_columna].select{|k,v| v.include?( "rut" ) }.blank?
          flash[:error] = "No seleccionó la columna RUT que es obligatoria"
          redirect_to seleccion_compensacion_compensation_index_path
        end

        file = File.join(UPLOAD_PATH, file) unless file.include?(UPLOAD_PATH)
        logger.info "file #{file}"

        if file.include?('.xlsx')
            xls = Roo::Excelx.new file
        elsif file.include?('.xls')
            xls = Roo::Excel.new file
        end

        length          = 1
        continua        = true
        columnas        = []
        horas_semanales = params[:estructura][:horas].to_f

        IncomeStructureData.all.destroy
        FileUtils.rm_rf("#{Rails.root}/tmp/cache/administracion-de-la-compensacion")
        User.all.update has_compensation_data: false

        niveles_isd = {nil => {}}
        niveles     = Job.all(fields: [:level]).map{|x| x[:level].blank? ? 0 : x[:level]}.uniq.sort

        niveles.each do |n|
          niveles_isd[n] = {}
        end

        user_error = []
        2.upto(xls.count).each do |i|
          col_rut  = cols.select{|k,v| v == "rut"}.first.first
          user     = User.first rut: xls.cell(i, col_rut.to_i+1)

          if user.blank? || user.position.blank?
            user_error.push(col_rut: xls.cell(i, col_rut.to_i+1))
          end

          next if user.blank? || user.position.blank?
          #user.update has_compensation_data: true
          user.has_compensation_data = 1
          user.save
          if user.save
          else
              user.errors.each do |e|
                puts "=====================================".yellow
                puts e.inspect.red.bold
              end
          end
          position = user.position
          job      = position.job
          hsem     = cols.key('hsem')
          current_user.set_config 'horas', horas_semanales

          cols.each do |j, v|
            case v
            when "nivel" then
              col_nivel  = xls.cell(i, j.to_i+1)
              job.level  = col_nivel  if col_nivel != job.level
            when "puntos" then
              col_puntos = xls.cell(i, j.to_i+1)
              job.puntos = col_puntos if col_puntos != job.puntos
            else
              if !["Seleccione / No corresponde", "cargo", "hsem"].include?(v) && v.to_i > 0
                #if cliente?(:uandes)
                #  p_mdo = params[:mercado][j]

                #  unless p_mdo[:con_datos].blank?
                #    job.nivel = 0 if job.nivel.blank?
                #    niveles_isd[job.nivel][v.to_i] = {} if niveles_isd[job.nivel][v.to_i].blank?
                #    niveles_isd[job.nivel][v.to_i][p_mdo[:estadigrafo]] = [] if niveles_isd[job.nivel][v.to_i][p_mdo[:estadigrafo]].blank?
                #    niveles_isd[job.nivel][v.to_i][p_mdo[:estadigrafo]] << xls.cell(i, j.to_i+1).to_f
                #    next
                #  end

                #  hsem_val                                         = xls.cell(i, hsem.to_i+1)
                #  in_st                                            = IncomeStructure.get v.to_i
                #  isd                                              = IncomeStructureData.first_or_create( {position: position, income_structure: in_st}, {value: 0, detail: {}, periodicity_detail: {}})
                #  _isd_tmp                                         = xls.cell(i, j.to_i+1).to_f
                #  isd.detail[xls.cell(1, j.to_i+1)]                = _isd_tmp #_isd_tmp.blank? ? _isd_tmp : _isd_tmp/IncomeStructureData::PERIDIOCIDAD[@peridiocidad[j].to_i]
                #  valor_calculado                                  = (xls.cell(i, j.to_i+1).to_f/IncomeStructureData::PERIDIOCIDAD[@peridiocidad[j].to_i])
                #  calculo_resultado                                = isd.value.to_f+valor_calculado
                #  resultado                                        = (cols_normalizar.blank?||cols_normalizar[j].blank?) ? calculo_resultado : ((calculo_resultado/hsem_val)*horas_semanales)
                #  # if calculo_resultado                          != resultado
                #  isd.detail[xls.cell(1, j.to_i+1)+" (Resultado)"] = resultado
                #  isd.detail[xls.cell(1, j.to_i+1)+" (Original)"]  = calculo_resultado
                #  isd.detail['(hsem_val)']                         = hsem_val
                #  isd.detail['(normalizado)']                      = !(cols_normalizar.blank?||cols_normalizar[j].blank?)
                #  # Guardar si almacenó proyección
                #  # end
                #  isd.value                                        = cliente?(:uandes) ? resultado.to_s : calculo_resultado.to_s#
                #  isd.periodicity_detail[xls.cell(1, j.to_i+1)]    = @peridiocidad[j]
                #  isd.save!
                # else
                in_st = IncomeStructure.get v.to_i
                isd   = IncomeStructureData.first( {position: position, income_structure: in_st}, {value: 0, detail: {}, periodicity_detail: {}})
                #puts isd.inspect.blue
                isd   = IncomeStructureData.new( {position: position, income_structure: in_st, value: 0, detail: {}, periodicity_detail: {}}) if isd.blank?
                isd.detail[xls.cell(1, j.to_i+1)]             = xls.cell(i, j.to_i+1)
                resultado                                     = isd.value.to_f+(xls.cell(i, j.to_i+1).to_f/IncomeStructureData::PERIDIOCIDAD[@peridiocidad[j].to_i])
                #puts resultado.inspect.yellow
                isd.value                                     = resultado.to_s
                isd.periodicity_detail[xls.cell(1, j.to_i+1)] = @peridiocidad[j].to_i
                #puts isd.inspect.red
                isd.insertar
                #if !isd.save

                #end

                puts isd.inspect.green
                # end
                # TODO: Falta almacenar el valor original, quizás en  IncomeStructureData para mostrar más facil en masa salarial
                # if hsem
                #     in_st2                                         = IncomeStructure.first_or_create({name: "#{in_st.name} - Normalizado"})
                #     isd2                                           = IncomeStructureData.first_or_create( {position: position, income_structure: in_st2}, {value: 0, detail: {}, periodicity_detail: {}})
                #     isd2.detail[xls.cell(1, j.to_i+1)]             = xls.cell(i, j.to_i+1)
                #     resultado2                                     = isd2.value.to_f+(xls.cell(i, j.to_i+1).to_f/IncomeStructureData::PERIDIOCIDAD[@peridiocidad[j].to_i])
                #     isd2.value                                     = resultado2.to_s
                #     isd2.periodicity_detail[xls.cell(1, j.to_i+1)] = @peridiocidad[j].to_i
                #     isd2.save
                # end
                # binding.pry
              end
            end
          end

          job.save
        end

        if user_error.length > 0
          msj = ""
          user_error.each do |x|
            msj += x[:col_rut]+" /  "
          end
          flash[:error] = "No existen en la base de datos el/los siguientes rut: <h3>#{msj}</h3>"
          redirect_to seleccion_compensacion_compensation_index_path
          return
        end

        ##### Procesar datos de mercado
        p_mercado = params[:mercado].select{|x,v| !v[:con_datos].blank?}

        p_mercado.each do |k, v|
          estadigrafo_id      = v[:estadigrafo]
          income_structure_id = params[:rel_columna][k.to_s].to_i
          in_st               = IncomeStructure.get income_structure_id

          niveles_isd.each do |n, arr|
            n = 0 if n.nil?
            a = arr[income_structure_id]

            if a.blank?
              a = [0]
            else
              a = a[estadigrafo_id]
              a = [0] if a.blank?
            end

            a = a.reject{|x| x == 0||x.nil?}
            a = [0] if a.blank?
            promedio = a.reduce(:+) / a.size.to_f

            market_data = MarketData.first_or_create( {income_structure_id: income_structure_id, nivel: n} )
            market_percentile_data = MarketPercentileData.first_or_create( {market_data_id: market_data.id, market_percentile_id: estadigrafo_id}, {value: promedio, selected: true} )

            unless market_percentile_data.new?
              market_percentile_data.update value: promedio, selected: true
            end
          end
        end

        EquidadConfig.all.destroy
        EquidadPunto.all.destroy

        DataMapper.repository(:rvelasco) do
          UserConfig.all(:name.like => "%_sections_%").destroy
        end

        session[:file_path] = ''
        redirect_to masa_salarial_compensacion_compensation_index_path
      end
    end

    def masa_salarial
        #render text: do_cache((cliente?(:uandes) ? 'masa_salarial_uandes' : 'masa_salarial'), cache: true){
        @incomes, @incomes_show, @masa_salarial, @users, @data, @incomes_cols = CompensationUtils::masa_salarial params: params
        #}.html_safe
    end

    def masa_salarial_simulacion
        #@incomes, @incomes_show, @masa_salarial, @users, @data = CompensationUtils::masa_salarial params: params
        params[:filtro] = 'todas'
        @incomes, @incomes_show, @masa_salarial, @users, @data, @incomes_cols = CompensationUtils::masa_salarial params: params
        @equidad_configs = EquidadConfig.data
        @incomes_ids     = @equidad_configs.map{|x| x[:income].id}
    end

    def masa_salarial_update
        params[:masa_salarial_columns].each do |i|
            item = IncomeStructure.first acronym: i[0]

            item.update in_masa_salarial: i[1]
        end

        redirect_to masa_salarial_compensacion_compensation_index_path
    end

end
