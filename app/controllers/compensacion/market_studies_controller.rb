# -*- encoding : utf-8 -*-
class Compensacion::MarketStudiesController < ApplicationController

    def competitivity
    	render text: do_cache('competitivity', cache: true){
	        last = Compensation.last

	        @items    = last ? Compensation.all( update_name: last.update_name ) : []
	        @selected = nil
	        @income   = (params[:competitivity] && @selected = params[:competitivity][:income_structure_id]) ? IncomeStructure.get(@selected) : IncomeStructure.first
	    }.html_safe
    end

end
