# -*- encoding : utf-8 -*-
class JobEvaluationsController < ApplicationController
    include ApplicationHelper
    def index
        if job_evaluated.nil?
            redirect_to select_position_job_evaluations_path
        else
            rango = lambda do |x|
              return 1 if x == "A"
              return 2 if x == "B"
              return 3 if x == "C"
              return 4 if x == "D"
              return 5 if x == "E"
              return 6 if x == "F"
              return 7 if x == "G"
              return 8 if x == "H"
            end
            tmp = []
            @item = JobEvaluation.first :position_id => job_evaluated.id
            @boss = JobEvaluation.first :position_id => job_evaluated.boss_id
            if @item.nil?
                @item = JobEvaluation.new :position_id => job_evaluated.id
            end
            unless @item.kh_tecnico.blank? || @item.kh_gerencia.blank? || @item.kh_relaciones_humanas.blank?
                khcom = JobEvaluationArrays.buscarcombi(JobEvaluationArrays::KHCOMBI, [@item.kh_tecnico.name,@item.kh_gerencia.name,@item.kh_relaciones_humanas.name])
                if khcom.blank?
                    tmp << "Combinación de <strong>Saber Como</strong> Invalida o Poco Probable"
                end
            end
            unless @item.sp_ambito.blank? || @item.sp_desafio.blank?
                spcom = JobEvaluationArrays.buscarcombi(JobEvaluationArrays::SPCOMBI, [@item.sp_ambito.name,@item.sp_desafio.name])
                if spcom.blank?
                    tmp << "Combinación de <strong>Solución de Problemas</strong> Invalida o Poco Probable"
                end
            end
            ###################################################################################################
            #validaciones para validar en cada update
            unless @item.new?
                if @item.perfil.blank? || @item.perfil != job_evaluated.profile.acronim
                    tmp << "Perfil de Evaluación:  #{@item.perfil.blank? ? "<strong>Invalido!</strong>" : "<strong>"+ @item.perfil+"</strong>" } // Cargo: <strong>#{ job_evaluated.profile.acronim}</strong>"
                end
            end
            if session[:gestion_cargo] || session[:gestion_cargo].blank?
                if !@item.kh_tecnico.blank? && !@item.kh_tecnico.management_type_id.blank? && @item.kh_tecnico.management_type_id != job_evaluated.management_type_id
                    tmp << "Indicador de Conocimiento Tecnico no corresponde <br> Indicador: <strong>#{@item.kh_tecnico.management_type.name}</strong> Cargo: <strong>#{job_evaluated.management_type.name}</strong>
                            <br>Indicadores recomendados: #{@item.kh_tecnico.indicators_list(job_evaluated.management_type_id)} "
                end
            end
            ########################################
            ##continuar agregando condiciones aqui##
            #Muestra de condicion  rango.(@item.kh_tecnico.name[0..1]) == rango.(@item.sp_ambito.name[0..1])
            aux = 0
            c1 = !@item.kh_tecnico.blank? && !@item.sp_ambito.blank?
            if c1
                c2 = rango.(@item.sp_ambito.name[0..0]) <= rango.(@item.kh_tecnico.name[0..0])-2
                c3 = rango.(@item.sp_ambito.name[0..0]) >= rango.(@item.kh_tecnico.name[0..0])+1

                aux = -1 if rango.(@item.sp_ambito.name[0..0]) == rango.(@item.kh_tecnico.name[0..0])-1
                aux =  1 if rango.(@item.sp_ambito.name[0..0]) == rango.(@item.kh_tecnico.name[0..0])
            end
            if ( c1 && c2 || c1 && c3)
              tmp << "Indicador de Ámbito <strong>debe ser igual o 1 nivel menor</strong> al indicador de Conocimiento Técnico"
            end
            ######################################################################################

            cc1 = !@item.ac_libertad.blank? && !@item.sp_ambito.blank?
            if cc1
                cc2 = rango.(@item.ac_libertad.name[0..0]) <= rango.(@item.sp_ambito.name[0..0])-2
                cc3 = rango.(@item.ac_libertad.name[0..0]) >= rango.(@item.sp_ambito.name[0..0])+1
            end
            if aux == 1
                if ( cc1 && cc2 || cc1 && cc3)
                  tmp << "Indicador de <strong>Libertad para actuar</strong> debe <strong>ser igual o 1 nivel menor</strong> al indicador de <strong>Ámbito</strong>"
                end
            elsif aux == -1
              if ( cc1 && cc2 || cc1 && cc3)
                tmp << "Indicador de <strong>Libertad para actuar</strong> debe <strong>SER IGUAL</strong> al indicador de <strong>Ámbito</strong>"
              end
            end
            ########################################

            exp = ["Contribuyente sin experiencia","Contribuyente Con Experiencia", "Supervisor"]
            #puts  session[:tfunc].inspect.yellow.bold
            unless @boss.blank?
                if !@item.kh_gerencia.blank? && !@boss.kh_gerencia.blank? && @item.kh_gerencia.name == @boss.kh_gerencia.name
                  tmp << "Indicador de <strong>Conocimiento Gerencial NO debe ser igual</strong> al del cargo superior <br> Evaluado: <strong> #{@item.kh_gerencia.name} </strong>
                         Superior: <strong> #{@item.kh_gerencia.name}  </strong>"
                end
                if !@item.sp_ambito.blank? && !@boss.sp_ambito.blank? && @item.sp_ambito.name == @boss.sp_ambito.name
                  tmp << "Indicador de <strong>Ámbito de pensamiento NO debe ser igual</strong> al del cargo superior <br> Evaluado: <strong> #{@item.sp_ambito.name} </strong>
                         Superior: <strong> #{@item.sp_ambito.name}  </strong>"
                end
                if !@item.ac_libertad.blank? && !@boss.ac_libertad.blank? && @item.ac_libertad.name == @boss.ac_libertad.name
                  tmp << "Indicador de <strong>Libertad para actuar NO debe ser igual</strong> al del cargo superior <br> Evaluado: <strong> #{@item.ac_libertad.name} </strong>
                         Superior: <strong> #{@item.ac_libertad.name}  </strong>"
                end
            end
            if session[:tfunc].blank?
                #puts  session[:exp].inspect.yellow.bold
                if session[:exp]== "1" && @item.kh_gerencia.name != "I-"
                  tmp << "Indicador de Conocimiento Gerencial no corresponde <br> Indicador: <strong> #{@item.kh_gerencia.name} </strong>
                          <br> Recomendado para #{exp[session[:exp].to_i-1]}: <strong> I- </strong>"
                end
                if session[:exp]== "2" && @item.kh_gerencia.name != "I"
                  #puts "ENTROOOO".red.bold
                  tmp << "Indicador de Conocimiento Gerencial no corresponde <br> Indicador: <strong> #{@item.kh_gerencia.name} </strong>
                          <br> Recomendado para #{exp[session[:exp].to_i-1]}: <strong> I </strong>"
                end
                if session[:exp]== "3" && @item.kh_gerencia.name != "I+"
                  tmp << "Indicador de Conocimiento Gerencial no corresponde <br> Indicador: <strong> #{@item.kh_gerencia.name} </strong>
                          <br> Recomendado para #{exp[session[:exp].to_i-1]}: <strong> I+ </strong>"
                end
            else
                signo=""
                case session[:nfunc]
                when "1"
                    signo = "-"
                when "3"
                    signo = "+"
                end
                case session[:tfunc]
                when "1"
                    if @item.kh_gerencia.name != "II#{signo}"
                        tmp << "Indicador de Conocimiento Gerencial no corresponde <br> Indicador: <strong> #{@item.kh_gerencia.name} </strong>
                                <br> Recomendado para #{exp[session[:exp].to_i-1]}: <strong> II#{signo}</strong>"
                    end
                when "2"
                    if @item.kh_gerencia.name != "III#{signo}"
                        tmp << "Indicador de Conocimiento Gerencial no corresponde <br> Indicador: <strong> #{@item.kh_gerencia.name} </strong>
                                <br> Recomendado para #{exp[session[:exp].to_i-1]}: <strong> III#{signo}</strong>"
                    end
                when "3"
                    puts @item.kh_gerencia.inspect.yellow.bold
                    if @item.kh_gerencia.name != "IV#{signo}"
                        tmp << "Indicador de Conocimiento Gerencial no corresponde <br> Indicador: <strong> #{@item.kh_gerencia.name} </strong>
                                <br> Recomendado para #{exp[session[:exp].to_i-1]}: <strong> IV#{signo} </strong>"
                    end
                end

            end
            ########################################
            @item.errores = tmp.to_json
            @item.save!
            ######################################################################################################
        end
    end

    def guardacomentario
        jev = JobEvaluation.first :position_id => job_evaluated.id
        jev.comentario = params[:job_evaluation][:comentario]
        jev.save!
        redirect_to job_evaluations_path

    end

    def destroy
        @item = JobEvaluation.first :position_id => job_evaluated.id
        @item.destroy
        redirect_to select_position_job_evaluations_path << "?id=#{job_evaluated.id}"
    end

    def select_position
        if params[:id]
            session[:job_evaluated_id] = params[:id]
            redirect_to job_evaluations_path
        end
        job_evaluated = nil
        session[:prset] = nil
        session[:exp]   = nil
        session[:tfunc] = nil
        session[:nfunc] = nil
        session[:gestion_cargo] = nil

        @items = Job.all
    end

    def create
        item = JobEvaluation.first :position_id => job_evaluated.id
        unless params[:job_evaluation][:perfil_select] == "undefined" || params[:job_evaluation][:perfil_select].blank?
            job = job_evaluated
            job.profile_id = Profile.first(acronim: params[:job_evaluation][:perfil_select]).id
            job.save!
            session[:prset]                         = params[:job_evaluation][:perfil_select]
            session[:gestion_cargo]                 = params[:job_evaluation][:gestion_cargo]
            params[:job_evaluation][:perfil_select] = nil
            params[:job_evaluation][:gestion_cargo] = nil
        else
            session[:gestion_cargo]                 = params[:job_evaluation][:gestion_cargo]
            params[:job_evaluation][:gestion_cargo] = nil
            params[:job_evaluation][:perfil_select] = nil
        end
        puts params[:job_evaluation].inspect.red.bold
        unless params[:job_evaluation][:experiencia] == "undefined" || params[:job_evaluation][:experiencia].blank?

            session[:exp]   = params[:job_evaluation][:experiencia]
            session[:tfunc] = params[:job_evaluation][:tipo_funcion] == "undefined" ?  nil : params[:job_evaluation][:tipo_funcion]
            session[:nfunc] = params[:job_evaluation][:n_funcion] == "undefined" ?  nil : params[:job_evaluation][:n_funcion]
            params[:job_evaluation][:experiencia] = nil
            params[:job_evaluation][:tipo_funcion] = nil
            params[:job_evaluation][:n_funcion] = nil

        else
            params[:job_evaluation][:experiencia] = nil
            params[:job_evaluation][:tipo_funcion] = nil
            params[:job_evaluation][:n_funcion] = nil
        end

        prms = params[:job_evaluation]
        puts prms.inspect.green.bold
        prms.each_key do |k|
            prms.delete(k) if prms[k].blank?
        end

        if item.nil?
            item = JobEvaluation.new prms
            item.errores = ""
            item.save
        else
            item.update prms
            puts item.errors.inspect.blue
        end

        if item.kh_tecnico && item.kh_gerencia && item.kh_relaciones_humanas
            item.kh_puntos = JobEvaluationArrays.kh_puntos(value(item.kh_tecnico, :name), value(item.kh_gerencia, :name), value(item.kh_relaciones_humanas, :name))

            item.kh_nivel = item.kh_puntos ? JobEvaluationArrays.kh_nivel(item.kh_puntos) : nil
            item.save!
        else
            item.kh_puntos = nil
        end

        if item.sp_ambito && item.sp_desafio

            item.sp_porcentaje = JobEvaluationArrays.sp_porcentaje(value(item.sp_ambito, :name), value(item.sp_desafio, :name))

            if item.kh_puntos && item.sp_porcentaje
                item.sp_puntos = JobEvaluationArrays.sp_puntos item.kh_puntos, item.sp_porcentaje
            end
            item.save!
        end

        if item.ac_libertad && item.ac_magnitud && item.ac_impacto
            item.ac_puntos = JobEvaluationArrays.ac_puntos value(item.ac_libertad, :name), value(item.ac_magnitud, :name), value(item.ac_impacto, :name)

            item.save!
        end

        if item.kh_puntos && item.sp_puntos && item.ac_puntos
            item.total = item.kh_puntos + item.sp_puntos + item.ac_puntos
            item.nivel = JobEvaluationArrays.nivel(item.total)
            item.save!
        end

        if item.sp_puntos && item.ac_puntos
            item.perfil = JobEvaluationArrays.perfil(item.ac_puntos, item.sp_puntos)

            item.save!
        end

        redirect_to job_evaluations_path
    end

    def get_description
        item = JobEvaluationData.find params[:id]

        render :text => item.job_evaluation_data_description.name
    end
end
