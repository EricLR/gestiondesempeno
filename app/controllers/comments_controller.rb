# -*- encoding : utf-8 -*-
class CommentsController < ApplicationController
  def index; end

  def show
    dw = DescriptionWorkflow.first(job_id: params[:id])
    @comments = dw.comments unless dw.blank?
    @comment = Comment.new
  end

  def create
    c_params = params[:comment]
    dw = DescriptionWorkflow.first(job_id: c_params[:job_id])
    comment = Comment.create(detalle: c_params[:detalle], description_workflow_id: dw.id, author_id: current_user.user.id)

    redirect_to comment_path(c_params[:job_id])
  end
end
