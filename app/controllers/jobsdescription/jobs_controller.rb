# -*- encoding : utf-8 -*-

class Jobsdescription::JobsController < ApplicationController # :nodoc:
  def index
    if current_user.role.name.eql?('Administrador') || current_user.role.name.eql?('Analista Jefe')
      @items = (tmp = Job.first(status: Job::ENABLED, boss_id: nil); tmp.blank? ? [] : tmp.jerarquia)
    else
      @items = (tmp = Job.first(status: Job::ENABLED, id: current_user.user.position.job_id); tmp.blank? ? [] : tmp.jerarquia)
      DescriptionWorkflow.all(fields: [:job_id], unique: true, order: nil).each do |dw|
        job = Job.first(id: dw.job_id)
        @items << job unless @items.include?(job)
      end
    end

    @analistas = Account.all(role: Role.first(name: 'Analista'))
  end

  def list
    @items = JobDescription.all

    if !params[:asignar].blank?
      # @description = JobDescription.get params[:asignar]
      @jobs = Job.activos
    else
      @jobs = Job.all job_description_id: @items.map(&:id), fields: %i[id job_description_id]
    end
  end

  def asignar_descripcion
    description = Job.get(params[:job_id].to_i).job_description
    analista = User.get(params[:analista].to_i)
    jefe = Position.first(job_id: params[:job_id].to_i).user

    begin
      dw = DescriptionWorkflow.new(
        user_id: current_user.user.id,
        analista_id: analista.id,
        jefe_id: jefe.id,
        job_id: params[:job_id].to_i,
        job_description_id: description.id,
        status: DescriptionWorkflow::ASIGNADO
      )

      if dw.save
        render json: {statusssss: 'Asignado'}
      else
        puts "errors -----> #{dw.errors.inspect}".yellow
        render json: {statusssss: 'Error!!!!'}
      end
    rescue
      render json: {statusssss: 'Error!!!!'}
    end
  end

  def informar_jefes
    if params.include?('job_id')
      if DescriptionWorkflow.first(job_id: params[:job_id]).update(status: params[:status])
        render json: {status: 'Se ha modificado el estatus'}
      else
        render json: {status: 'error'}
      end
    end
    #workflow = DescriptionWorkflow.get params[:id]
  end

  def comentarios

  end

  def workflow
    opts = {}

    unless role_can?(:dc_administrador) # Usuarios!
      opts[:analista_id] = current_user.position.id
    end

    @descriptions        = JobDescription.all opts
    @items               = @descriptions.jobs
    @job_items           = @items.map { |j| jd = j.job_description; j.attributes.merge({ status: jd.status, analista: jd.analista, analista_id: jd.analista_id }) }
    @analistas_jobs      = Job.with_permission(:dc_analista)
    @analistas_positions = @analistas_users = []
    @analistas           = []

    unless @analistas_jobs.blank?
      @analistas_positions = @analistas_jobs.positions
      @analistas_users     = @analistas_positions.users
    end

    @analistas_positions.each do |ap|
      tmp = {}
      usr = @analistas_users.find { |p| p.id == ap.user_id }
      job = @analistas_jobs.find { |j| j.id == ap.job_id }
      tmp.merge! value job, :attributes
      tmp.merge! value usr, :attributes
      tmp[:id]     = ap.id
      tmp[:nombre] = "#{job.name + ':' unless job.blank?} #{usr.name unless usr.blank?}"
      @analistas << tmp
    end

    if !params[:asignar].blank?
      # @description = JobDescription.get params[:asignar]
      @jobs = Job.activos
    else
      @jobs = Job.all job_description_id: @items.map(&:id), fields: %i[id job_description_id]
    end
  end

  def workflow_save
    params[:seleccion][:job_description].each do |i, _k|
      job = Job.get i
      jd = job.job_description
      jd.update! analista_id: params[:seleccion][:analista_id], status: JobDescription::ASIGNADO
    end

    flash[:notice] = 'Asignaciones almacenadas'
    redirect_to workflow_jobsdescription_jobs_path
  end

  def asociar
    counter = 0

    params[:job].each do |job_id, values|
      job   = Job.get job_id
      jd_id = values[:job_description_id].to_i

      if job.job_description_id != jd_id
        counter += 1 if job.update(job_description_id: jd_id)
      end
    end

    if counter > 0
      flash[:notice] = "Se asoció correctamente #{counter} descripciones"
    else
      flash[:error] = 'No se realizaron cambios. Probablemente no ingresó las descripciones asociadas.'
    end

    redirect_to list_jobsdescription_jobs_path
  end

  def show
    DataMapper.repository(:rvelasco) do
      @item        = Job.get params[:id]
      @description = @item.job_description
      @tablas       = JdTable.all_tables
      @tablas_datos = JdTableData.all_data @description.id, @tablas
    end

    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Ricardo Velasco HR Strategic Partners - #{params[:id]}", # pdf will download as my_pdf.pdf
          layout: 'pdf', # uses views/layouts/pdf.haml
          zoom: 1,
          footer: { right: '[page] de [topage]', font_size: 9 },
          show_as_html: params[:debug].present? # renders html version if you set debug=true in URL
      end
    end
  end

  def new
    @item = Job.new
  end

  def create
    data = params[:job]

    DataMapper.repository(:rvelasco) do
      @item = Job.new
      @item.attributes = data
    end

    process_decisions @item
    process_recomendations @item
    @item.save
    process_main_results @item.id
    redirect_to jobsdescription_jobs_path
  end

  def edit
    @is_description = false
    session[:job_id] = params[:id]

    DataMapper.repository(:rvelasco) do
      @item         = Job.get params[:id].to_i
      @description  = @item.job_description
      @items        = @item.generation
      @tablas       = JdTable.all_tables
      @tablas_datos = JdTableData.all_data @description.id, @tablas
    end

    @areas = Area.all.to_json
    @tipos = CompetenceType.all.to_json
    @competencias = Competence.all.to_json
    @conductas = Conduct.all.to_json
    @tipoempresa = EmpresaTipo.all.to_json
    @tamanoempresa = TamanoEmpresa.all.to_json
    @sector = Sector.all.to_json
    @experienciasector = SectorExperience.all.to_json
    dw = DescriptionWorkflow.first(job_id: params[:id])
    @comments = dw.blank? ? nil : dw.comments
  end

  def description_edit
    @is_description = true
    # session[:job_id] = params[:id]
    @item        = Job.get params[:id]
    @description = @item.job_description # JobDescription.get params[:id]
    @comentarios = Comment.job_description @item.id
    @analistas_jobs      = Job.with_permission(:dc_analista)
    @analistas_positions = @analistas_users = []
    @analistas           = []

    unless @analistas_jobs.blank?
      @analistas_positions = @analistas_jobs.positions
      @analistas_users     = @analistas_positions.users
    end

    @analistas_positions.each do |ap|
      tmp = {}
      usr = @analistas_users.find { |p| p.id == ap.user_id }
      job = @analistas_jobs.find { |j| j.id == ap.job_id}
      tmp.merge! value job, :attributes
      tmp.merge! value usr, :attributes
      tmp[:id]     = ap.id
      tmp[:nombre] = "#{job.name + ':' unless job.blank?} #{usr.name unless usr.blank?}"
      @analistas << tmp
    end

    DataMapper.repository(:rvelasco) do
      # @items        = @item.generation
      @tablas       = JdTable.all_tables
      @tablas_datos = JdTableData.all_data @description.id, @tablas
    end

    render 'edit'
  end

  def description_status_save
    description = JobDescription.get params[:id]

    if role_can?(:dc_administrador) # Administrador modifica
      description.status      = params[:job_description][:status]
      description.analista_id = params[:job_description][:analista_id] unless params[:job_description][:analista_id].blank?
      description.save
      DescriptionMailer.enviar_solicitud(params[:email_envio], params[:job_id], true, request.base_url).deliver
      flash[:notice] = 'Modificaciones almacenadas'
    else # Modificacion de asesor
      flash[:notice] = 'Se ha enviado un correo solicitando aprobación'
      DescriptionMailer.enviar_solicitud(params[:email_envio], params[:job_id], true, request.base_url).deliver

      if params[:job_description][:status].to_i == JobDescription::REVISION_PENDIENTE
        description.status = JobDescription::REVISION_PENDIENTE
        description.save
      end

    end

    redirect_to description_edit_jobsdescription_job_path(params[:job_id])
  end

  def comment_save
    Comment.create(
      detalle: params[:comentarios],
      commentable_id: params[:id],
      commentable_type: JobDescription.to_s,
      author_id: current_user.position.id
    )

    flash[:notice] = 'Comentario almacenado'
    redirect_to description_edit_jobsdescription_job_path(params[:id])
  end

  def update
    data = params[:job]
    saved = false
    job_description_data = {}

    DataMapper.repository(:rvelasco) do
      @item = Job.get params[:id]

      %i[aditional_responsability mission others].each do |p|
        job_description_data[p] = data.delete p
      end

      if saved = @item.update(data)
      else
        Rails.logger.info 'ERRORES======'
         @item.errors.each do |e|
           Rails.logger.info e
         end
         Rails.logger.info '======'
      end
    end

    if saved
      jd = JobDescription.get @item.job_description_id
      jd = JobDescription.create() if jd.blank?
      jd.update job_description_data
      @item.update(job_description_id: jd.id)
    end

    process_decisions @item
    process_recomendations @item
    process_main_results @item.job_description_id
    process_dimensions @item.job_description_id
    process_knowledges @item
    process_experiences @item
    process_internal_situations @item
    process_external_situations @item
    process_internal_contacts @item
    process_external_contacts @item
    process_perfiles (JobDescription.first id: @item.job_description_id)
    process_comments params[:detalle], @item

    redirect_to edit_jobsdescription_job_path(params[:id])+params[:tab_state]
  end

  def process_comments(detalle, item)
    dw = DescriptionWorkflow.first job_id: params[:id]
    comment = Comment.create(
      description_workflow: dw,
      detalle: detalle,
      author_id: current_user.user_id
    )
  end

  def update_dynamic_tables
    params[:values].each do |table_id, tmp|
      tmp.each do |col_id, tmp1|
        tmp1.each do |row_id, tmp2|
          JdTableData.update_or_create(
            {
              jd_table_id: table_id,
              jd_column_id: col_id,
              jd_row_id: row_id,
              job_description_id: params[:job_description_id]
            },
            {value: tmp2}
          )
        end
      end
    end

    render json: {status: 'ok'}
  end

  def delete_main_result
    item = MainResult.get params[:id]

    unless item.blank?
      item.destroy
    else
      flash[:error] = 'No existe Principal Resultado a eliminar'
    end

    redirect_to edit_jobsdescription_job_path(session[:job_id])+'#principales-resultados'
  end

  def delete_dimension
    item = Dimension.get params[:id]

    unless item.blank?
      item.destroy
    else
      flash[:error] = 'No existe Dimensión a eliminar'
    end

    redirect_to edit_jobsdescription_job_path(session[:job_id])+'#dimensiones'
  end

  def multiupload; end

  def multiupload_save
    files = params[:files]
    data = JobdescriptionProcess::process( files.first )

    if data[:status]
      flash[:notice] = data[:info]
      @descripcion              = data[:descripcion]
      @desc_funcional           = data[:desc_funcional]
      @decisions_recomendations = data[:decisions_recomendations]
      @relaciones               = data[:relaciones]
      @perfil                   = data[:perfil]
      @contexto                 = data[:contexto]
      @dimensiones              = data[:dimensiones]
      @titulo_cargo             = data[:titulo_cargo]
    end

    rtrn = {files: files.map{|x| {name: ActiveSupport::Inflector.transliterate(x.original_filename), info: data[:info], status: data[:status], job_id: data[:job_id] ? data[:job_id] : nil}}}
    render json: rtrn, content_type: request.format
  end

  def cargar
    @item = Job.get params[:id]
  end

  def cargar_process
    upload    = params[:job][:file]
    data = JobdescriptionProcess::process( upload, params[:id] )

    @descripcion              = ''
    @desc_funcional           = []
    @decisions_recomendations = []
    @relaciones               = {internas: [], externas: []}
    @perfil                   = {conocimiento: [], experiencia: []}
    @contexto                 = []
    @dimensiones              = []
    @situaciones              = []
    @titulo_cargo             = []
    @tablas                   = []
    @dinamicas                = []

    if data[:status]
      flash[:notice] = data[:info]
      @descripcion              = data[:descripcion]
      @desc_funcional           = data[:desc_funcional]
      @decisions_recomendations = data[:decisions_recomendations]
      @relaciones               = data[:relaciones]
      @perfil                   = data[:perfil]
      @contexto                 = data[:contexto]
      @dimensiones              = data[:dimensiones]
      @situaciones              = data[:situaciones]
      @titulo_cargo             = data[:titulo_cargo]
      @tablas                   = data[:tables]
      @dinamicas                = data[:dinamicas]
    else
      flash[:error] = 'Ocurrió un problema al cargar los datos'
    end
    # render 'cargar_process_uandes'
    # redirect_to jobsdescription_job_path(params[:id])
  end

  private

  def process_main_results(job_description_id)
    if params[:main_results]
      # arego variable para mantener control del nivel de importancia en caso de no ingresarse un valor
      c=0
      logger.info "Main results: #{params[:main_results]}".red.bold
      params[:main_results].sort_by{ |i| i['number'].to_i}.each do |i|
        # en caso de no existir el numero de importancia en el parametro se toma el ultimo valor en la bd y se suma 1
        if i['number'].blank?
          c =  MainResult.first(job_description_id: job_description_id, limit: 1, order: [:number.desc])
          if c == nil
            c= 1
            i['number'] = c
          else
            c= c.number + 1
            i['number'] = c
          end
        end

        item = MainResult.new( i )
        item.job_description_id = job_description_id
        item.save
      end
    end

    if params[:main_result]
      logger.info "Main result sort: #{params[:main_result].sort_by{ |i| i.last['number'].to_i }}"
      params[:main_result].sort_by{ |i| i.last['number'].to_i }.each do |m|
        i = m.last
        item = MainResult.get( m.first )
        item.update i
      end
    end
  end

  def process_dimensions(job_description_id)
    if params[:dimensions]
      Dimension.all(job_description_id: job_description_id).destroy

      params[:dimensions].each do |i|
        item = Dimension.new i
        item.job_description_id = job_description_id
        item.save
      end
    else
      items = Dimension.all job_description_id: job_description_id
      items.destroy
    end
  end

  def process_decisions(item)
    item.dicisions_value = params[:dicisions]
  end

  def process_recomendations(item)
    item.recomendations_value = params[:recomendations]
  end

  def process_knowledges(item)
    if params[:knowledges]
      item.knowledges_value = params[:knowledges]
      item.save
    else
      item.knowledges_value = nil
      item.save
    end
  end

  def process_experiences(item)
    if params[:experiences]
      item.experiences_value = params[:experiences]
    else
      item.experiences_value = nil
    end

    item.save
  end

  def process_internal_situations(item)
    if params[:internal_situations]
      item.internal_situations_value = params[:internal_situations]
    else
      item.internal_situations_value = nil
    end

    item.save
  end
  def process_external_situations(item)
    if params[:external_situations]
      item.external_situations_value = params[:external_situations]
    else
      item.external_situations_value = nil
    end
    item.save
  end

  def process_internal_contacts(item)
    if params[:internal_contacts]
      item.internal_contacts_value = params[:internal_contacts]
    else
      item.internal_contacts_value = []
    end
    item.save
  end

  def process_external_contacts(item)
    if params[:external_contacts]
      item.external_contacts_value = params[:external_contacts]
    else
      item.external_contacts_value = []
    end

    item.save
  end

  def process_idiomas(item)
    if params[:idiomas]
      item.idiomas_value = params[:idiomas]
    else
      item.idiomas_value = []
    end

    item.save
  end

  def process_competencias(item)
    if params[:competencias]
      item.competencias_value = params[:competencias]
    else
      item.competencias_value = []
    end

    item.save
  end

  def process_empresa_anterior(item)
    if params[:empresa_anterior]
      item.datos_empresa_anterior_value = params[:empresa_anterior]
    else
      item.datos_empresa_anterior_value = []
    end

    item.save
  end

  def process_certificaciones(item)
    if params[:certificaciones]
      item.certificaciones_value = params[:certificaciones]
    else
      item.certificaciones_value = []
    end

    item.save
  end

  # nuevos 2016/05/16
  def process_perfiles(item)
    item.anios_experiencia_laboral = params[:anios_experiencia_laboral].blank? ? nil : params[:anios_experiencia_laboral]
    item.anios_experiencia_cargo = params[:anios_experiencia_cargo].blank? ? nil : params[:anios_experiencia_cargo]
    item.estudio_basico = params[:estudio_basico].blank? ? nil : params[:estudio_basico]
    item.anios_estudio = params[:anios_estudio].blank? ? nil : params[:anios_estudio]
    item.conocimiento_organizacional = params[:conocimiento_organizacional].blank? ? nil : params[:conocimiento_organizacional]
    item.habilidades_funcionales = params[:habilidades_funcionales].blank? ? nil : params[:habilidades_funcionales]
    item.definicion = params[:definicion].blank? ? nil : params[:definicion]
    item.conductas = params[:conductas].blank? ? nil : params[:conductas]
    item.seguridad = params[:seguridad].blank? ? nil : params[:seguridad]
    item.calidad = params[:calidad].blank? ? nil : params[:calidad]
    item.medio_ambiente = params[:medio_ambiente].blank? ? nil : params[:medio_ambiente]
    item.caracteristicas_ambientales = params[:caracteristicas_ambientales].blank? ? nil : params[:caracteristicas_ambientales]
    #item.frecuencia = params[:frecuencia].blank? ? nil : params[:frecuencia]
    item.save
    item.movilidad_value = params[:movilidad].blank? ? [] : params[:movilidad]
    item.ultima_actualizacion_value = Time.now.strftime('%Y/%m/%d %T')
    process_empresa_anterior(item)
    process_idiomas(item)
    process_competencias(item)
    process_certificaciones(item)
  end
end
