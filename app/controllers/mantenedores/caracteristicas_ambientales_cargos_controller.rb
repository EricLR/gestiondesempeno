# -*- encoding : utf-8 -*-
class Mantenedores::CaracteristicasAmbientalesCargosController < ApplicationController
  before_filter :store_location, :only => :index
  protect_from_forgery :except         => :destroy

  crud :caracteristicas_ambientales_cargo, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_caracteristicas_ambientales_cargo_path'
end
