# -*- encoding : utf-8 -*-
class Mantenedores::TipoMovilidadsController < ApplicationController
  before_filter :store_location, :only => :index
  protect_from_forgery :except         => :destroy

  crud :tipo_movilidad, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_tipo_movilidad_path'
end
