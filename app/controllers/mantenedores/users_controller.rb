# -*- encoding : utf-8 -*-
class Mantenedores::UsersController < ApplicationController
    RUT              = 1
    NOMBRES          = 2
    APELLIDOS        = 3
    SEXO             = 4
    FECHA_NACIMIENTO = 5
    NOMBRE_CARGO     = 6
    EMAIL            = 7
    FONO             = 8
    PASSWORD         = 9
    FECHA_INGRESO    = 10

    def index
        @last = User.last

    end

    def create
        errores = ""

        begin

            if file = params[:upload][:file]

                name = file.original_filename
                directory = "public/cargos"

                path = File.join(Rails.root, directory, name)

                File.open(path, "wb") { |f| f.write file.read }

                if path.include?('.xlsx')
                    xls = Roo::Excelx.new path
                elsif path.include?('.xls')
                    xls = Roo::Excel.new path
                end

                @data = xls



                
                User.transaction do

                    2.upto(@data.count).each do |i|
                        cargo = @data.cell(i, NOMBRE_CARGO)
                        job   = Job.first(name: cargo)

                        if job.blank?
                            errores << 'El cargo: ' << cargo << ' no existe.<br />'
                            break
                        else
                            user = User.first_or_create(
                                                            { 
                                                                rut:          @data.cell(i, RUT) 
                                                            },{ 
                                                                first_name:   @data.cell(i, NOMBRES), 
                                                                last_name:    @data.cell(i, APELLIDOS),
                                                                sex:          (@data.cell(i, SEXO) == 'Masculino' ? User::MASCULINO : User::FEMENINO),
                                                                birthdate:    @data.cell(i, FECHA_NACIMIENTO),
                                                                email:        @data.cell(i, EMAIL),
                                                                phone_office: @data.cell(i, FONO).to_i,
                                                                password:     Digest::MD5.hexdigest( @data.cell(i, PASSWORD).to_i.to_s ),
                                                                contractdate: @data.cell(i, FECHA_INGRESO)
                                                            }
                                                        )
                            
                            params = {job: job, user: user}

                            unless job.boss.blank?
                                params[:boss_position] = Position.first job: job.boss
                            end

                            Position.first_or_create params
                            
                        end



                        
                    end

                end
                
                
            end  
            session[:error]  = errores.blank? ? nil : errores
            session[:notice] = 'Se ha cargado correctamente los usuarios' if session[:error].blank?
        rescue Exception => e
            session[:error] = 'Ocurrió un error, revise que el archivo sea correcto'
        end  
        
        redirect_to mantenedores_users_path
    end


end
