# -*- encoding : utf-8 -*-
class Mantenedores::JdTablesController < ApplicationController
  before_filter :store_location, :only => :index
  protect_from_forgery :except => :destroy

  crud :jd_table, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_jd_tables_path'

  def index
  	@datos = JdTable::all_tables
  	
 	# @datos = [
	# 	{
	# 		table: {id: 1, name: 'Tabla 1'},
	# 		rows: [{name: ':D'}, {name: 'WOW'}],
	# 		cols: [{name: 'Col 1'},{name: 'Columna'}]
	# 	},
	# 	{
	# 		table: {id: 2, name: 'Tabla 2'},
	# 		rows: [{name: 'Row1'}, {name: 'Row2'}, {name: 'Row3'}],
	# 		cols: [{name: 'Col 1'}, {name: 'Col 2'}, {name: 'Col 3'}]
	# 	}
  end
	# ]
  
  def save
  	logger.info "PARAMS: #{params}"
  	update_records = lambda do |model, data, relationship=nil|
  		rel = relationship.blank? ? {} : {jd_table_id: relationship}
  		if data[:id].blank? # Nuevo
  			item = model.create( {name: data[:name]}.merge(rel))
  		else
  			item = model.get data[:id]
  		end
  		item.update( name: data[:name] ) if( item && item.name != data[:name])
  		if !rel.blank? && rel[:jd_table_id] != item.jd_table_id
  			item.update rel
  		end
  		
  		item
  	end
  	t_ids = []
  	r_ids = []
  	c_ids = []
  	params[:data].each do |k, item|
  		tmp_t = item[:table]
  		tmp_r = item[:rows]
  		tmp_c = item[:cols]
  		table = update_records.(JdTable, tmp_t)
  		tmp_r.each do |kr, row|
  			nr = update_records.(JdRow, row, table.id)
  			r_ids << nr.id
  		end unless tmp_r.blank?
  		tmp_c.each do |kc, col|
  			nc = update_records.(JdColumn, col, table.id)
  			c_ids << nc.id
  		end unless tmp_c.blank?
  		t_ids << table.id
  	end unless params[:data].blank?
    
    
  	
  	tables = JdTable.all fields: [:id]
  	tables.each do |t|
  		unless t_ids.include?(t.id)
        begin
          JdRow.all(jd_table_id: t.id).destroy
          JdColumn.all(jd_table_id: t.id).destroy
          t.destroy  
        rescue Exception => e
          
        end
  			
  		end
  	end
  	
  	rows = JdRow.all fields: [:id]
  	rows.each do |row|
  		unless r_ids.include? row.id
        begin
          row.destroy  
        rescue Exception => e
          
        end
  			
  		end
  	end
  	
  	cols = JdColumn.all fields: [:id]
  	cols.each do |col|
  		unless c_ids.include? col.id
        begin
          col.destroy  
        rescue Exception => e
          
        end
  			
  		end
  	end
  	
  	
  	render json: {status: 'ok', data: JdTable::all_tables}
  end
end
