# -*- encoding : utf-8 -*-
class Mantenedores::ExternalSituationsController < ApplicationController
    before_filter :store_location, :only => :index
    protect_from_forgery :except         => :destroy

    crud :external_situation, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_external_situations_path'
end
