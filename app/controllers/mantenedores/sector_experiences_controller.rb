# -*- encoding : utf-8 -*-
class Mantenedores::SectorExperiencesController < ApplicationController
  before_filter :store_location, :only => :index
  protect_from_forgery :except         => :destroy

  crud :sector_experience, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_sector_experience_path'
end
