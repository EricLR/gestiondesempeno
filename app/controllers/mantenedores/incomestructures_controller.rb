# -*- encoding : utf-8 -*-
class Mantenedores::IncomestructuresController < ApplicationController
    before_filter :store_location, :only => :index

    crud :income_structure, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_incomestructures_path', except: [:destroy]

    def index
        @items = IncomeStructure.all order: :position.asc
    end
    def destroy
    	@item = IncomeStructure.get params[:id]
    	
    	if !@item.income_structure_data.blank?||!@item.income_structure_detail.blank?
    		flash[:error] = 'Hay datos asociados a la estructura, por lo que no se puede eliminar'
    	else
    		flash[:notice] = 'Estructura eliminada' if @item.destroy!
    		
    	end
    	
    	redirect_to mantenedores_incomestructures_path
    end
end
