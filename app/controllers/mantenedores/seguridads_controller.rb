# -*- encoding : utf-8 -*-
class Mantenedores::SeguridadsController < ApplicationController
  before_filter :store_location, :only => :index
  protect_from_forgery :except         => :destroy

  crud :seguridad, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_seguridad_path'
end
