# -*- encoding : utf-8 -*-
class Mantenedores::JobtypesController < ApplicationController
  before_filter :store_location, :only => :index
  protect_from_forgery :except => :destroy

  crud :job_type, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_jobtypes_path'
  
end
