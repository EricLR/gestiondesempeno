# -*- encoding : utf-8 -*-
class Mantenedores::ExperiencesController < ApplicationController
    before_filter :store_location, :only => :index
    protect_from_forgery :except         => :destroy

    crud :experience, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_experiences_path'
end
