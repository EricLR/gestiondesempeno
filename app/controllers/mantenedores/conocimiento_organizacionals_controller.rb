# -*- encoding : utf-8 -*-
class Mantenedores::ConocimientoOrganizacionalsController < ApplicationController
  before_filter :store_location, :only => :index
  protect_from_forgery :except         => :destroy

  crud :conocimiento_organizacional, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_conocimiento_organizacional_path'
end
