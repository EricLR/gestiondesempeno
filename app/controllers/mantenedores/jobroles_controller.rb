# -*- encoding : utf-8 -*-
class Mantenedores::JobrolesController < ApplicationController
    before_filter :store_location, :only => :index

    crud :job, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_insert_planifications_path'
end
