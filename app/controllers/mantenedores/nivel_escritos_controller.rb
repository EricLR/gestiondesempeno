# -*- encoding : utf-8 -*-
class Mantenedores::NivelEscritosController < ApplicationController
  before_filter :store_location, :only => :index
  protect_from_forgery :except         => :destroy

  crud :nivel_escrito, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_nivel_escrito_path'
end
