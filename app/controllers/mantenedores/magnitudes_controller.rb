# -*- encoding : utf-8 -*-
class Mantenedores::MagnitudesController < ApplicationController
    before_filter :store_location, :only => :index
    protect_from_forgery :except         => :destroy

    crud :magnitude, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_magnitudes_path'



    def index
        @items = Magnitude.all(:kind.not => nil)
    end

    def get_dimension
        id   = params[:id]
        name = params[:name]
        kind = params[:kind] == "financieras" ? Magnitude::FINANCIERAS : Magnitude::NO_FINANCIERAS

        item = Magnitude.get(id)
        item = Magnitude.first name: name if item.blank?
        item = Magnitude.create name: name, kind: kind if item.blank?
        render json: item
    end
    
    def after_fail
        flash[:error] << " Probablemente existen Descripciones que tienen asociada esta Magnitud."
        redirect_back_or_default
    end

end
