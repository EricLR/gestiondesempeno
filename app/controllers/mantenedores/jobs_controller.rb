# -*- encoding : utf-8 -*-
class Mantenedores::JobsController < ApplicationController
    NOMBRE_CARGO           = 1
    DEPENDENCIA_JERARQUICA = 2
    TIPO_DEPENDENCIA       = 3
    TIPO                   = 4
    PERFIL                 = 5
    ESTAMENTO              = 6
    JORNADA                = 7
    CENTRO_COSTO           = 8
    
    def index
        @last = Job.last
    end

    def create
        errores = ""

        begin

            rol = Role.first name: 'Usuarios'

            if file = params[:upload][:file]

                name = file.original_filename
                directory = "public/cargos"

                path = File.join(Rails.root, directory, name)

                File.open(path, "wb") { |f| f.write file.read }

                if path.include?('.xlsx')
                    xls = Roo::Excelx.new path
                elsif path.include?('.xls')
                    xls = Roo::Excel.new path
                end

                @data = xls
                
                2.upto(@data.count).each do |i|
                    boss = Job.first(name: @data.cell(i, DEPENDENCIA_JERARQUICA))
                    Job.create  name: @data.cell(i, NOMBRE_CARGO),
                                dependency_type: @data.cell(i, TIPO_DEPENDENCIA),
                                jornada: @data.cell(i, JORNADA),
                                boss: boss,
                                job_type: JobType.first_or_create(name: @data.cell(i, TIPO)),
                                profile: Profile.first_or_create(name: @data.cell(i, PERFIL)),
                                estamento: @data.cell(i, ESTAMENTO),
                                centro_costos: @data.cell(i, CENTRO_COSTO),
                                role: rol


                end


                session[:notice] = 'Se guardaron correctamente los cargos.'
            end
        rescue Exception => e
            session[:error] = 'Ocurrió un error al cargar el archivo'
        end
        redirect_to mantenedores_jobs_path
    end
end
