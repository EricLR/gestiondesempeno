# -*- encoding : utf-8 -*-
class Mantenedores::NivelLecturasController < ApplicationController
  before_filter :store_location, :only => :index
  protect_from_forgery :except         => :destroy

  crud :nivel_lectura, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_nivel_lectura_path'
end
