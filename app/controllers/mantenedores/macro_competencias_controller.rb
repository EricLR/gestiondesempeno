class Mantenedores::MacroCompetenciasController < ApplicationController
  before_filter :store_location, :only => :index
  protect_from_forgery :except         => :destroy

  crud :macro_competencia, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_macro_competencia_path'
end
