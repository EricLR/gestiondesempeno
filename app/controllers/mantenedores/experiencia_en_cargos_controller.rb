# -*- encoding : utf-8 -*-
class Mantenedores::ExperienciaEnCargosController < ApplicationController
  before_filter :store_location, :only => :index
  protect_from_forgery :except         => :destroy

  crud :experiencia_en_cargo, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_experiencia_en_cargo_path'
end
