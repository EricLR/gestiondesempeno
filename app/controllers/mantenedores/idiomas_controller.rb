# -*- encoding : utf-8 -*-
class Mantenedores::IdiomasController < ApplicationController
  before_filter :store_location, :only => :index
  protect_from_forgery :except         => :destroy

  crud :idioma, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_idioma_path'
end
