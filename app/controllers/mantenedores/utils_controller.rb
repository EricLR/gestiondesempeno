# -*- encoding : utf-8 -*-
class Mantenedores::UtilsController < ApplicationController
    def index
        
    end

    def create
        gestion_db = DataMapper.repository.adapter.options
        datos_db   = DataMapper.repository(:rvelasco).adapter.options
        
        cmd        = "bunzip2 < db/sql/2015-07-25_gestion.bz2 | mysql -u #{gestion_db[:username]} #{('--password='+gestion_db[:password]) unless gestion_db[:password].blank?} #{gestion_db[:database]}"
        cmd2       = "bunzip2 < db/sql/2015-07-25_datos.bz2 | mysql -u #{datos_db[:username]} #{('--password='+datos_db[:password]) unless datos_db[:password].blank?} #{datos_db[:database]}"
        
        #completed = system 'bundle exec rake db:drop && bundle exec rake db:create && bundle exec rake db:automigrate && bundle exec rake db:seed'
        logger.info "CMD: #{cmd}".red
        completed = system(cmd)
        if completed
            logger.info "CMD2: #{cmd2}".red
            completed = system(cmd2) 
        end
        #completed = system "mysql -u #{gestion_db[:username]} #{('-p '+gestion_db[:password]) unless gestion_db[:password].blank?} #{gestion_db[:database]} < 2015-07-25_gestion.bz2"
        
        if completed
            session[:notice] = 'Se limpió correctamente el sistema'
        else
            session[:error] = 'Ocurrió un error durante la limpieza'
        end
        
        redirect_to mantenedores_utils_path
    end
end
