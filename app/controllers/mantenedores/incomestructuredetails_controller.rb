# -*- encoding : utf-8 -*-
class Mantenedores::IncomestructuredetailsController < ApplicationController
    before_filter :store_location, :only => :index

    crud :income_structure_detail, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_insert_planifications_path'

    def show
        @income = IncomeStructure.get params[:id]
        @items = IncomeStructureDetail.all income_structure: @income
    end
end
