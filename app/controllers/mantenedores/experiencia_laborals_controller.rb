# -*- encoding : utf-8 -*-
class Mantenedores::ExperienciaLaboralsController < ApplicationController
  before_filter :store_location, :only => :index
  protect_from_forgery :except         => :destroy

  crud :experiencia_laboral, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_experiencia_laboral_path'
end
