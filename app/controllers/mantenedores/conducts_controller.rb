# -*- encoding : utf-8 -*-
class Mantenedores::ConductsController < ApplicationController
    before_filter :store_location, :only => :index
    protect_from_forgery :except => :destroy

    crud :conduct, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_conducts_path'

    def show
        #@income = IncomeStructure.get params[:id]
        #@items = IncomeStructureDetail.all income_structure: @income
    end

    def list
       @items = Conduct.all competence_id: params[:id] 

       render 'index'
    end

end
