# -*- encoding : utf-8 -*-
class Mantenedores::MedioAmbientesController < ApplicationController
  before_filter :store_location, :only => :index
  protect_from_forgery :except         => :destroy

  crud :medio_ambiente, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_medio_ambiente_path'
end
