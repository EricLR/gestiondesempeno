# -*- encoding : utf-8 -*-
class Mantenedores::ConductasController < ApplicationController
  before_filter :store_location, :only => :index
  protect_from_forgery :except         => :destroy

  crud :conducta, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_conducta_path'
end
