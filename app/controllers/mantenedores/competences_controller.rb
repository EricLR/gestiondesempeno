# -*- encoding : utf-8 -*-
class Mantenedores::CompetencesController < ApplicationController
    before_filter :store_location, :only => :index
    protect_from_forgery :except => :destroy

    crud :competence, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_competences_path'

    def show
        #@income = IncomeStructure.get params[:id]
        #@items = IncomeStructureDetail.all income_structure: @income
    end


    def list
        @items = Competence.all competence_type_id: params[:id]

        render 'index'
    end

    def after_fail
        flash[:error] = "No se pudo eliminar el registro, existen conductas que dependen de este tipo."

        redirect_back_or_default
    end
end
