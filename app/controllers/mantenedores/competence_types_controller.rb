# -*- encoding : utf-8 -*-
class Mantenedores::CompetenceTypesController < ApplicationController
    before_filter :store_location, :only => :index
    protect_from_forgery :except => :destroy

    crud :competence_type, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_competence_type_path'

    def show
        #@income = IncomeStructure.get params[:id]
        #@items = IncomeStructureDetail.all income_structure: @income
    end

    def after_fail
        flash[:error] = "No se pudo eliminar el registro, existen competencias que dependen de este tipo."

        redirect_back_or_default
    end
end
