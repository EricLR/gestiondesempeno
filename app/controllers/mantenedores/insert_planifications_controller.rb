# -*- encoding : utf-8 -*-
class Mantenedores::InsertPlanificationsController < ApplicationController

	before_filter :store_location, :only => [:index]
	crud :insert_planification, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_insert_planifications_path'


	def new
		@item = KeyPerformanceIndicator.new
	end
	
	def json_data
		render json: InsertPlanification.all.reverse.to_json
	end
	
	def json_jobs
		items    = []
		# profiles = Profile.all
		
		Job::activos.each do |j|
			tmp = j.attributes
			
			# _p = profiles.find{|x| x[:profile_id] == tmp[:profile_id]}
			# tmp[:profile_name] = _p.blank? ? '' : _p[:name]
			
			items << tmp
		end
		
		
		
		render json: items.to_json
	end
	
	def save
		data = {
			description: 	 params[:description], 
			jobs:            params[:jobs].map{|k, i| i}, 
			competences_ids: params[:competences].blank? ? [] : params[:competences].map{|k, i| i}, 
			kpi_ids:         params[:kpis].blank? ? [] : params[:kpis].map{|k, i| i}, 
			user_id:         current_user_id
		}
		ip = InsertPlanification.get params[:id]
		if ip.blank?
			ip = InsertPlanification.create data
		else
			ip.update data
		end
		render json: {status: 'ok'}
	end
	
	def execute
		ip = InsertPlanification.get params[:id]
		render json: {status: ip.realizar_carga ? 'ok' : 'error'}
	end
	
	def delete
		item = InsertPlanification.get params[:item_id]
		item.destroy!
		
		render json: {status: 'ok'}
	end

	def create
		kpi = KeyPerformanceIndicator.create params[:key_performance_indicator]

		ip = InsertPlanification.create :key_performance_indicator_id => kpi.id, :user_id => current_user_id

		Position.all.each do |i|
			if i.job && i.job.boss

				jp = Position.first(:job_id => i.job.boss.id)
				if jp
					p = Planification.create :evaluated_id => i.id, :evaluator_id => jp.id,
										 :period_id => current_period.id, :key_performance_indicator_id => kpi.id
				end
			end
		end

		redirect_back_or_default
	end

end
