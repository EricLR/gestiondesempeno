# -*- encoding : utf-8 -*-
class Mantenedores::InferentialResultsController < ApplicationController
    before_filter :store_location, :only => :index
    protect_from_forgery :except => :destroy

    crud :inferential_result, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_inferential_results_path'
end
