# -*- encoding : utf-8 -*-
class Mantenedores::ResultverbsController < ApplicationController
    before_filter :store_location, :only => :index
    protect_from_forgery :except => :destroy

    crud :result_verb, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_resultverbs_path'
end
