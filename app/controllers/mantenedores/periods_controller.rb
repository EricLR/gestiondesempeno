# -*- encoding : utf-8 -*-
class Mantenedores::PeriodsController < ApplicationController
	crud :period, :title_attribute => :name, :redirect_to_url => 'mantenedores_periods_path'
	before_filter :store_location, :only => :index

	def successful_update
		flash[:notice] = t('crud.updated', :what => set_what)
        
		if @item.planification_closed
			Planification.all(:period_id => @item.id).update(:closed => true)
			PlanificationCompetence.all(:period_id => @item.id).update(:closed => true)
		end
		if @item.evaluation_closed
			TracingKpi.all(:period_id => @item.id).update(:closed => true)
			TracingCompetence.all(:period_id => @item.id).update(:closed => true)
		end

		after_success
	end

	
end
