# -*- encoding : utf-8 -*-
class Mantenedores::DicisionsController < ApplicationController
    before_filter :store_location, :only => :index
    protect_from_forgery :except         => :destroy

    crud :dicision, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_dicisions_path'
end
