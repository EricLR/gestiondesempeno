# -*- encoding : utf-8 -*-
class Mantenedores::EmpresaTiposController < ApplicationController
  before_filter :store_location, :only => :index
  protect_from_forgery :except         => :destroy

  crud :empresa_tipo, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_empresa_tipo_path'
end
