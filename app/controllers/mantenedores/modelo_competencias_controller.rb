class Mantenedores::ModeloCompetenciasController < ApplicationController

  def index
    @areas = Area.all
    @macrocompetencias = CompetenceType.all
    @competencias = Competence.all
    @conductas = Conduct.all
    #logger.info "conductas modelo #{@conductas.to_json}"
  end


end
