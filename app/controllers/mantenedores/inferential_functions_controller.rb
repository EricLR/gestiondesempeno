# -*- encoding : utf-8 -*-
class Mantenedores::InferentialFunctionsController < ApplicationController
    before_filter :store_location, :only => :index
    protect_from_forgery :except => :destroy

    crud :inferential_function, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_inferential_functions_path'
end
