# -*- encoding : utf-8 -*-
class Mantenedores::CompetencesUploadController < ApplicationController
    before_filter :store_location, :only => :index
    TIPO       = 1
    NOMBRE     = 2
    DEFINICION = 3
    NIVEL      = 4
    CONDUCTA   = 5

    def index
        
    end

    def create
        errores = ""


        if file = params[:upload][:file]

            first = true
            file_path = ""

            name      = file.original_filename
            directory = "public/competencias"

            path      = File.join(Rails.root, directory, name)

            File.open(path, "wb") { |f| f.write file.read }

            if path.include?('.xlsx')
                xls   = Roo::Excelx.new path
            elsif path.include?('.xls')
                xls   = Roo::Excel.new path
            end

            @data = xls
            begin
                ultimo_tipo, ultima_competencia = nil

                2.upto(@data.count).each do |i|
                    tipo       = @data.cell(i, TIPO)
                    nombre     = @data.cell(i, NOMBRE)
                    definicion = @data.cell(i, DEFINICION)
                    nivel      = @data.cell(i, NIVEL)
                    conducta   = @data.cell(i, CONDUCTA)

                    unless tipo.blank?
                        ultimo_tipo = CompetenceType.first_or_create( name: tipo )
                    end 
                    unless nombre.blank?
                        ultima_competencia = Competence.first_or_create( {name: nombre, competence_type: ultimo_tipo}, {description: definicion} )
                    end

                    unless conducta.blank?
                        Conduct.first_or_create( {description: conducta, competence: ultima_competencia}, {number: nivel} )
                    end
                end
            end


            # CSV.foreach(file.tempfile.path, :col_sep => ";") do |l|
            #     if first # CABECERAS DEL CSV
            #         #file_path = "#{file.tempfile.path}-#{rnd_num}"

            #         first = false
            #     else # PROCESANDO DATOS
            #         case params[:upload][:kind]
            #             when "competences_type"
            #                 CompetenceType.first_or_create( {id: l[CT_ID]}, {name: l[CT_NAME]})
            #             when "competences"
            #                 if (competence_type = CompetenceType.get l[CO_ID_CT]).nil?
            #                     errores << "El tipo de competencia ID: #{l[CO_ID_CT]} no existe<br />"
            #                 else
            #                     Competence.first_or_create(  {id: l[CO_ID]}, 
            #                                                 {   name: l[CO_NAME], description: l[CO_DESCRIPTION],
            #                                                     competence_type_id: competence_type.id
            #                                                 })
            #                 end
            #             when "conducts"
            #                 if (competence = Competence.get l[CD_ID_CO]).nil?
            #                     errores << "La competencia ID: #{l[CD_ID_CO]} no existe<br />"
            #                 else
            #                     Conduct.first_or_create( {id: l[CD_ID]},
            #                                             {
            #                                                 number: l[CD_NUMBER],
            #                                                 description: l[CD_DESCRIPTION],
            #                                                 competence_id: competence.id
            #                                             })
            #                 end


            #         end

            #     end
            
            # end
            
        end    
        session[:error] = errores.blank? ? nil : errores
        session[:notice] = 'Se ha cargado correctamente los datos' if session[:error].blank?
        redirect_back_or_default
    end
end
