# -*- encoding : utf-8 -*-
class Mantenedores::HabilidadesFuncionalessController < ApplicationController
  before_filter :store_location, :only => :index
  protect_from_forgery :except         => :destroy

  crud :habilidades_funcionales, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_habilidades_funcionales_path'
end
