# -*- encoding : utf-8 -*-
class Mantenedores::NivelHabladosController < ApplicationController
  before_filter :store_location, :only => :index
  protect_from_forgery :except         => :destroy

  crud :nivel_hablado, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_nivel_hablado_path'
end
