# -*- encoding : utf-8 -*-
class Mantenedores::TamanoEmpresasController < ApplicationController
  before_filter :store_location, :only => :index
  protect_from_forgery :except         => :destroy

  crud :tamano_empresa, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_tamano_empresa_path'
end
