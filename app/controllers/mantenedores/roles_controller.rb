# -*- encoding : utf-8 -*-
class Mantenedores::RolesController < ApplicationController
    before_filter :store_location, :only => :index
    after_filter :proccess_create, only: :create
    protect_from_forgery :except         => :destroy

    crud :role, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_roles_path'

    def proccess_create
        @item.update permissions: Role.parse_permissions(params[:role][:permissions])
    end

    def update
  	     item = Role.get params[:id]
         item = Role.new if item.blank?
         values = params[:role]
         values[:permissions] = Role.parse_permissions(values[:permissions])
         logger.info "Values: #{values}"

         if item.update values
             flash[:notice] = "Rol #{item.new? ? 'creado' : 'actualizado'}"
         else
             flash[:error] = "No se actualizó el rol"
             logger.info "Errores: #{item.errors.inspect}"
         end

         redirect_back_or_default
     end
end
