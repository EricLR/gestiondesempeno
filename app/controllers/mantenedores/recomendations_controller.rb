# -*- encoding : utf-8 -*-
class Mantenedores::RecomendationsController < ApplicationController
    before_filter :store_location, :only => :index
    protect_from_forgery :except         => :destroy

    crud :recomendation, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_recomendations_path'
end
