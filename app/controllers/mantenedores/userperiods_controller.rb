# -*- encoding : utf-8 -*-
class Mantenedores::UserperiodsController < ApplicationController
	before_filter :store_location, :only => :index
	
	crud :user_period, :use_class_name_as_title => true, :redirect_to_url => 'mantenedores_userperiods_path'

	def successful_action
		if @item.planification_open
			Planification.all(:period_id => @item.period_id, :evaluated_id => @item.position_id).each {|i| i.open}
			PlanificationCompetence.all(:period_id => @item.period_id, :evaluated_id => @item.position_id).each {|i| i.open}
		else
			Planification.all(:period_id => @item.period_id, :evaluated_id => @item.position_id).each {|i| i.close}
			PlanificationCompetence.all(:period_id => @item.period_id, :evaluated_id => @item.position_id).each {|i| i.close}
		end

		if @item.evaluation_open
			TracingKpi.all(:period_id => @item.period_id, :user_id => @item.position.user.id).update(:closed => false)
			TracingCompetence.all(:period_id => @item.period_id, :user_id => @item.position.user.id).update(:closed => false)
		else
			TracingKpi.all(:period_id => @item.period_id, :evaluated_id => @item.position.id).update(:closed => true)
			TracingCompetence.all(:period_id => @item.period_id, :evaluated_id => @item.position.id).update(:closed => true)
		end

	end

	def successful_action_destroy
		# Cerrando planificaciones
		Planification.all(:period_id => @item.period_id, :evaluated_id => @item.position_id).each {|i| i.close}
		PlanificationCompetence.all(:period_id => @item.period_id, :evaluated_id => @item.position_id).each {|i| i.close}

		# Cerrando Evaluaciones
		TracingKpi.all(:period_id => @item.period_id, :evaluated_id => @item.position.id).update(:closed => true)
		TracingCompetence.all(:period_id => @item.period_id, :evaluated_id => @item.position.id).update(:closed => true)
	end
end
