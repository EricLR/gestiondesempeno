# -*- encoding : utf-8 -*-
class Dotacion::DotacionController < ApplicationController
	UPLOAD_BASE_PATH = "dotacion"
    UPLOAD_PATH = File.join(Rails.root, "public", UPLOAD_BASE_PATH)
    
	def index
		@work_data = DotacionDato::calcular_dotacion_real		
	end
	
	def carga
		
	end
	
	def excel
		xls   = XlsxWriter.new
        sheet = xls.add_sheet("Dotacion")


        # sheet.add_row(['Rut', 'Nivel', 'Puntos'] + income_structures.map{|x| x.name}) # TODO: Falta todo lo dinámico (Sueldo Base, etc)

        sheet.add_row(['Nombre Cargo', 'Jefe', 'Ocupante'])# +DotacionDato::MESES)

        # jobs        = Job.all fields: [:id, :name, :jornada], status: Job::ENABLED, order: :boss_id.desc
        # positions   = jobs.positions
        # users 		
        jobs = Job.all(fields: [:id, :name, :boss_id], status: Job::ENABLED)
        users = dm_a User, {

                                fields:            [:id, :rut, :first_name, :last_name],
                                child: {
                                    position: {
                                        fields: [:id, :job_id, :user_id],
                                        subchild: {
                                            job: {fields: [:id, :nivel, :puntos, :name, :boss_id]}
                                        }
                                    }
                                }
                            }
		logger.info "Users: #{users.inspect}".bold.red
        users.each do |user|
        	job = jobs.select{|x| x.id == user[:job_boss_id]}
        	unless job.blank?
        		sheet.add_row([user[:job_name], job.first.name, "#{user[:last_name]} #{user[:first_name]}"])
        	end
            # sheet.add_row([job.name, job.jornada]+[positions.select{|x| x.job_id == job.id}.count]*DotacionDato::MESES.length)
            
        end



        send_file xls.path, type: 'application/octet-stream'
	end
	
	def excel_procesa
		if file = params[:upload][:file]

            name    = file.original_filename
            tmpfile = file.tempfile
            file    = File.join(UPLOAD_PATH, Time.now.strftime("%Y-%m-%d-%H%M%S").to_s + "_" + name.gsub(" ", ""))

            File.open(file, "wb") { |f| f.write tmpfile.read }

            if name.include?('.xlsx')
                xls = Roo::Excelx.new file
            elsif name.include?('.xls')
                xls = Roo::Excel.new file
            end
            
            
            

        end
	end
	
	def plan
		@data = DotacionDato::calcular_dotacion_real
		@work_data = DotacionDato::plan.map{|x| tmp = JSON::parse(x.datos); tmp[:updated_at] = l(x.updated_at); tmp}
	end
	
	def diferencias
		work_data = DotacionDato::plan.map{|x| tmp = JSON::parse(x.datos); tmp[:updated_at] = l(x.updated_at, format: :date); tmp}
		if work_data.blank?
			flash[:error] = "No hay datos de dotación almacenados para revisar las diferencias"
			redirect_to plan_dotacion_dotacion_index_path 
		end
		
		data = DotacionDato::calcular_dotacion_real
		
		@work_data = []
		
		work_data.each do |wd|
			
			org = data.select{|x| !wd['job']['id'].blank? && x[:job][:id] == wd['job']['id'].to_i}.first
			next if org.blank?
			tmp = {job: wd['job'], updated_at: wd[:updated_at]}
			
			DotacionDato::MESES_CORTO.each do |mc|
				tmp["#{mc}_plan"] = wd[mc].to_i
				tmp["#{mc}_real"] = org[mc.to_sym].to_i
				tmp["#{mc}_diff"] = wd[mc].to_i - org[mc.to_sym].to_i
			end
			
			@work_data << tmp
		end
		
		
	end
	
	def save_data
		unless params[:data].blank?
			
			DotacionDato.all(tipo: DotacionDato::PLAN).destroy
			
			params[:data].each do |data|
				DotacionDato.create posicion: data.first, datos: data.last.to_json, tipo: DotacionDato::PLAN
			end	
		end
		
		render json: {success: true}
	end
	
	def clean_data
		if params[:confirm] == 'true'
			DotacionDato.all(tipo: DotacionDato::PLAN).destroy
			flash[:notice] = "Planificación eliminada correctamente"
		end
		
		redirect_to dotacion_dotacion_index_path
	end
end
