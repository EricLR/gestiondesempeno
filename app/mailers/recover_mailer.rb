# -*- encoding : utf-8 -*-
class RecoverMailer < ActionMailer::Base
    default :from => "contacto@rvelasco.cl "

    def enviar_solicitud user, pass
        @user = user
        @pass = pass
        attachments.inline['logo.png'] = File.read('public/images/logos/logo.png')
        mail to: user.email, subject: "Recuperación de Password - Essential"
    end
end
