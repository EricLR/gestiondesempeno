# -*- encoding : utf-8 -*-
class DescriptionMailer < ActionMailer::Base
  default from: "no-responder@rvelasco.cl"

  def enviar_solicitud email, job, solicitud = false, url = nil
    @url = url
    @job = Job.get(job)
    @jd  = @job.job_description
    mail to: email, subject: "[#{JobDescription::ESTADOS[@jd.status]}] Solicitud Modificación de descripción"
  end
end
