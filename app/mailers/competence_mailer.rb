# -*- encoding : utf-8 -*-
class CompetenceMailer < ActionMailer::Base
  default :from => "contacto@rvelasco.cl "

  def enviar_solicitud email,link
    @link = link
    #email = "eric.labra@rvelasco.cl"
    attachments.inline['Prop1.png'] = File.read('public/images/logos/Prop1.png')
    mail to: email, subject: "Aviso de planificaciones de competencias"
  end

  def competence_closed email
    attachments.inline['Prop1.png'] = File.read('public/images/logos/Prop1.png')
    mail to: email, subject: "Aviso Cierre Evaluación de Competencias"
  end


end
