# -*- encoding : utf-8 -*-
class MetasMailer < ActionMailer::Base
  default :from => "contacto@rvelasco.cl "

  def enviar_solicitud email,link
    @link = link
    attachments.inline['Prop1.png'] = File.read('public/images/logos/Prop1.png')
    mail to: email, subject: "Aviso de planificaciones de metas"
  end

  def metas_closed email

    attachments.inline['Prop1.png'] = File.read('public/images/logos/Prop1.png')
    mail to: email, subject: "Aviso Cierre evaluación de metas individuales"
  end

end
