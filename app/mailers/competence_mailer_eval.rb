# -*- encoding : utf-8 -*-
class CompetenceMailerEval < ActionMailer::Base
    default :from => "contacto@rvelasco.cl "

    def enviar_solicitud email, user
        @user = user
        attachments.inline['Prop1.png'] = File.read('public/images/logos/Prop1.png')
        mail to: email, subject: "Aviso de aprobación de la planificación de competencias"
    end

    def competence_aprobe email,evaluator,user
        @evaluator = evaluator
        @user = user
        attachments.inline['Prop1.png'] = File.read('public/images/logos/Prop1.png')
        mail to: email, subject: "Aviso aprobación de evaluación de Competencias"
    end

    def competence_refuse email,evaluator,user,apelacion
        @evaluator = evaluator
        @user = user
        @apelacion = apelacion
        attachments.inline['Prop1.png'] = File.read('public/images/logos/Prop1.png')
        mail to: email, subject: "Aviso rechazo de evaluación de Competencias"
    end
end
