# -*- encoding : utf-8 -*-
class JobClass
  include DataMapper::Resource
  include DataMapper::Validate
  def self.repository(name = nil, &block)
    super(:rvelasco, &block)
  end
  def self.default_repository_name
    :rvelasco
  end


  property :id,               Serial
  property :name,             String

  timestamps :at

  has n, :jobs
end
