# -*- encoding : utf-8 -*-
class PlanificationCompetence < Historiable
    include DataMapper::Resource
  	include DataMapper::Timestamp

    LEVEL_NOT_FOUND = 99
    if Rails.configuration.tipo_conductas.eql? :factores_claves_4_niveles
      LEVELS = ['Alto Desemepeño', 'Competente', 'En Desarrollo', 'No Satisfactorio']
    else
      LEVELS = ['Bajo lo esperado', 'Proximo a lo esperado', 'Resultado esperado', 'Sobre Lo esperado', 'Excepcional']
    end
    LEVELS[99] = ''

  	property :id,           Serial

  	property :start_date,	Date
  	property :end_date,		Date
  	property :ponderation,	Float
    property :conduct,      Text, lazy: false # JSON

    property :closed,             Boolean, :default => false
    property :evaluator_closed,   Boolean, :default => false
    property :evaluated_closed,   Boolean, :default => false
    property :detail,             Text, lazy: false

  	belongs_to :evaluator, 'User'
  	belongs_to :evaluated, 'User'

  	belongs_to :competence#, require: false
   #belongs_to :conducts
   belongs_to :period, require: false



    timestamps :at


    has n, :tracing_competences

    def self.for_select(evaluated_id, period_id = nil)
      period_id = Period.last[:id] if period_id == nil
      # id, descripción, meta, actual
      #TODO: Realizar búsqueda por evaluado en self.all( XXX )
      self.all(:evaluated_id => evaluated_id, period_id: period_id).map {|i| [i.competence.id, i.competence.name, i.id]}
    end

    def open
      self.update( :evaluator_closed => false, :evaluated_closed => false, :closed => false )
    end

    def close
      self.update( :evaluator_closed => true, :evaluated_closed => true, :closed => true )
    end

    def all_trace_dates
      td = TracingCompetence.all( fields: [:trace_date],
                                  planification_competence_id: id,
                                  evaluated_id: evaluated_id,
                                  unique: true, order: [:trace_date.desc] )

      td.map {|x| x.trace_date}
    end

    def evaluation_results
      result = []

      all_trace_dates.each do |td|

        total = 0
        (tcs = tracing_competences.all(fields: [:increase], trace_date: td, order: [:trace_date.desc])).each do |tc|
          total += (tc.increase + 1)
        end

        result << {date: td, result: (total.to_f / tcs.length).round}
      end

      result
    end

    def last_trace_date
      td = all_trace_dates.first

      td.nil? ? nil : td
    end

    def evaluation_result
      return LEVEL_NOT_FOUND if last_trace_date.nil?
      Rails.logger.info "LAST TRACE DATE: #{last_trace_date}".blue
      total = 0
      (tcs = tracing_competences.all(fields: [:increase], trace_date: last_trace_date)).each do |tc|
        total += (tc.increase + 1)
      end
      Rails.logger.info "TOTAL : #{total} count: #{tcs.length} redondeado: #{(total / tcs.length).round}".yellow

      (total.to_f / tcs.length).round
    end

end
