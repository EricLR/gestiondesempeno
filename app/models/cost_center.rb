# -*- encoding : utf-8 -*-
class CostCenter
	include DataMapper::Resource
	include DataMapper::Validate

	property :id,   Serial
	property :name, String

	timestamps :at

	has n, :jobs
end
