# -*- encoding : utf-8 -*-
class MarketPercentileData
  include DataMapper::Resource
  include DataMapper::Timestamp
  
  property :id,         Serial
  property :value,      Integer
  property :selected,   Boolean, default: false
  
  timestamps :at

  belongs_to :market_data
  belongs_to :market_percentile


end
