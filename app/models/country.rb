# -*- encoding : utf-8 -*-
class Country
	include DataMapper::Resource
	include DataMapper::Validate

	property :id,        Serial
	property :name,      String

	timestamps :at

	has n, :companies
end
