# -*- encoding : utf-8 -*-
class Empresa
	include DataMapper::Resource
  	include DataMapper::Validate
    def self.repository(name = nil, &block)
      super(:rvelasco, &block)
    end
    def self.default_repository_name
      :rvelasco
    end

  	property :id,           Serial
  	property :name,         String#, unique: true <- No es necesario, ID ya lo mantiene único
   # Recordar agregar migraciones Y PROPIEDADES en los modelos.
   property :color,        String
   #property :logo,         String
   #property :banner,       String
   property :tipo_letra,   String

  	timestamps :at


end
