# -*- encoding : utf-8 -*-
class Utils
	def self.job_description_copy
		jobs = Job.todos
		
		jobs.each do |job|
			desc = JobDescription.first name: job.name
			
			if desc.blank? # No existe y creo
				
				desc = JobDescription.create 	name: job.name,
												mission: job.mission,
												dicisions: job.dicisions,
												recomendations: job.recomendations,
												internal_impact: job.internal_impact,
												external_impact: job.external_impact,
												main_challenge: job.main_challenge,
												internal_contacts: job.internal_contacts,
												external_contacts: job.external_contacts,
												aditional_responsability: job.aditional_responsability,
												others: job.others,
												knowledge: job.knowledge,
												experience: job.experience,
												habilidades: job.habilidades
				job.update job_description_id: desc.id
			end
			## HAY QUE PERMITIR JOB NULO EN BASE DE DATOS (SEQUELPRO)
			MainResult.all(job: job).each do |x|
				if x.job_description_id.blank?
					x.update job_id: nil, job_description_id: desc
				end
			end
			
			JobCompetence.all(job: job).each do |x|
				if x.job_description_id.blank?
					x.update job_id: nil, job_description_id: desc
				end
			end
		
			Dimension.all(job: job).each do |x|
				if x.job_description_id.blank?
					x.update job_id: nil, job_description_id: desc
				end
			end
		
		end
	end
end
