# -*- encoding : utf-8 -*-
class NivelEscrito

  include DataMapper::Resource

  property :id, Serial
  property :name, String

end
