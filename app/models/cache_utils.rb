#encoding: utf-8
class CacheUtils
  def self.masa_salarial
    base_url = "administracion-de-la-compensacion/compensation/masa_salarial"
    params = {}


    parametros = [
        {param: :sex, name: 'Sexo', values: [User::FEMENINO, User::MASCULINO]},
        {param: :job, name: 'Cargo', values: Job.activos.map { |e| e.name }.uniq}
    ]



    unless [:uandes].include? Rails.configuration.cliente
        # NO UANDES
        parametros = parametros.concat [
            {param: :estamento, name: 'Estamento', values: Job.lista_estamentos},
            {param: :jornada, name: 'Jornada', values: []},
            {param: :dependency_type, name: 'Tipo de Dependencia', values: []},
        ]
    else
        # SI UANDES
        # {param: :contractdate, name: 'Fecha Alta', values: []},
        # {param: :estamento, name: 'Estamento', values: []},
        # {param: :estamento, name: 'Estamento', values: []},
        ceco_val    = User.uniq_attr(:ceco, has_compensation_data: true)
        subceco_val = User.uniq_attr(:subceco, has_compensation_data: true)
        grado_val   = User.uniq_attr(:grado, has_compensation_data: true)
        parametros  = parametros.concat [
            {param: :ceco, name: 'Centro de Costos', values: ceco_val},
            {param: :subceco, name: 'Sub Centro de Costos', values: subceco_val},
            {param: :grado, name: 'Grado Académico', values: grado_val},
        ]
    end



    #puts "Parametros: #{parametros.inspect}".red
    bases = {}
    parametros.each do |i|
      puts "i: #{i.inspect}".red
      bases[i[:param]] = [] if bases[i[:param]].blank?
      i[:values].each do |v|
        bases[i[:param]] << {i[:param] => v}
      end
    end
    # puts "Bases: #{bases.inspect}"
    # binding.pry

    hydra = Typhoeus::Hydra.new max_concurrency: 2

    call_cache base_url, hydra
    bases.each do |b|
      puts "B: #{b.inspect}".yellow
      b.last.each do |bi|
        call_cache base_url+"?#{bi.to_query}", hydra
      end
       #Typhoeus::Request.get "#{Rails.configuration.url}/administracion-de-la-compensacion/compensation/masa_salarial?"+b.to_query, userpwd: '1-9:1234'
    end

    hydra.run

  end

  def self.call_cache _url, hydra
    puts "Generando cache de: #{Rails.configuration.url}/#{_url}".red
    hydra.queue Typhoeus::Request.new( "#{Rails.configuration.url}/#{_url}", userpwd: '1-9:1234' )
  end
end
