# -*- encoding : utf-8 -*-
class Competence < Historiable
	include DataMapper::Resource
	include DataMapper::Timestamp

	property :id,           Serial
	property :name,         String, :length => 255
	property :description,  Text
    property :visible,      Boolean, default: true


	has n, :conducts
	has n, :job_competences
	has n, :planification_competences
	belongs_to :competence_type

end
