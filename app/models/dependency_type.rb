# -*- encoding : utf-8 -*-
class DependencyType
  include DataMapper::Resource

  # property <name>, <type>
  property :id, Serial
  property :name, String

  timestamps :at

  has n, :job
end
