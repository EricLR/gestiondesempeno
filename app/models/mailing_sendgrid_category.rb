# -*- encoding : utf-8 -*-
class MailingSendgridCategory
	include DataMapper::Resource
    include DataMapper::Timestamp

    property :id,           Serial
    
    timestamps :at
    
    belongs_to :mailing
    
    def category_name
        "#{id}-#{created_at.strftime("%m%d%Y")}"
    end
end
