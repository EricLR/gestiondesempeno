# -*- encoding : utf-8 -*-
class JdTable
    include DataMapper::Resource
    include DataMapper::Timestamp
    
	def self.repository(name = nil, &block); super(:default, &block); end
	def self.default_repository_name; :default; end
	  
    
    property :id,               Serial
    property :name,             String, :length => 255
    property :position,         Integer, default: 1
    
    has n, :jd_table_data, 'JdTableData'
    has n, :jd_row
    has n, :jd_column
    
    def self.all_tables
    	ordr   = {order: [:id.asc]}#{order: [:position.desc, :name.desc]}
	  	tables = JdTable.all ordr
	  	rows   = tables.jd_rows ordr
	  	cols   = tables.jd_columns ordr
	  	
	  	data = lambda{|item| { id: item.id, name: item.name, position: item.position }}
	  	
	  	datos = []
	  	
	  	tables.each do |table|
	  		datos << { 
	  			table: data.(table),
	  			rows: rows.select{|x| x.jd_table_id.eql? table.id}.map{|x| data.(x) },
	  			cols: cols.select{|x| x.jd_table_id.eql? table.id}.map{|x| data.(x) }
	  		}
	  	end
	  	
	  	datos
    end
end
