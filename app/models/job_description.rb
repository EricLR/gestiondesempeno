# -*- encoding : utf-8 -*-
class JobDescription < StringList
  include DataMapper::Resource
  include DataMapper::Timestamp

  DEPENDENCY_TYPE_ARRAY = ['', 'Linea', 'Staff']
  ESTAMENTO_ARRAY       = ['', 'Gerentes', 'Mandos Medios', 'Profesionales y Técnicos', 'Administrativos', 'Agentes CC', 'Operativos']
  JORNADA_ARRAY         = ['', 'Jornada completa', 'Media jornada']
  CENTRO_COSTOS_ARRAY   = ['']

  PERIODOS = ['Diario', 'Semanal', 'Mensual', 'Trimestral', 'Semestral', 'Anual', 'Periódica']

  ENABLED  = 1
  DISABLED = 0

  # NUEVO                = 0
  # ASIGNADO             = 1
  # REVISION_PENDIENTE   = 2
  # CORRECCION_PENDIENTE = 3
  # JEFATURA_PENDIENTE   = 4
  # JEFATURA_CORRECCION  = 5
  # JEFATURA_APROBADO    = 6
  # APROBADO             = 7
  NUEVO      = 0
  ASIGNADO   = 1
  REVISION   = 2
  CORRECCION = 3
  APROBACION = 4

  ESTADOS = ['Nuevo', 'Asignado', 'Revisión', 'Corrección', 'Aprobado']

  ADMINISTRADOR = 0
  ANALISTA      = 1
  JEFATURA      = 2

  ROLES = ['Administrador', 'Analista', 'Jefatura']


  # ESTADOS = ['Nuevo', 'Asignado', 'Revisión Pendiente', 'Corrección Pendiente', 'Jefatura Pendiente', 'Corrección Jefatura', 'Aprobado Jefatura', 'Aprobado']


  property :id,               Serial
  property :name,             String, :length => 255

  property :mission,          Text

  # 5. Autoridad
  property :dicisions,        Text
  property :recomendations,   Text

  # 6. Contexto
  property :internal_impact,            Text # Inciden internamente para el logro de los resultados esperados del cargo
  property :external_impact,            Text # Inciden externamente para el logo de los resultados esperados del cargo
  property :main_challenge,             Text # El principal desafío del cargo es
  property :internal_contacts,          Text # El cargo requiere contactos internos con (quien y para qué)
  property :external_contacts,          Text # El cargo requiere contactos externos con (quién y para qué)
  property :aditional_responsability,   Text # El cargo adicionalmiente es responsabe de
  property :others,                     Text # Otros del cargo

  # 7. Principales conocimientos, experiencias
  property :knowledge,                  Text # Conocimientos de
  property :experience,                 Text # Experiencia en

  property :habilidades,                Text

 #Campos para perfiles 23/05/2016

  property :anios_experiencia_laboral,   Text
  property :anios_experiencia_cargo,     Text
  property :estudio_basico,              Text
  property :anios_estudio,               Text
  property :conocimiento_organizacional, Text
  property :habilidades_funcionales,     Text
  property :definicion,                  Text
  property :conductas,                   Text
  property :certificaciones,             Text
  property :seguridad,                   Text
  property :calidad,                     Text
  property :medio_ambiente,              Text
  property :caracteristicas_ambientales, Text
  property :tipo_movilidad,              Text
  property :frecuencia,                  Text
  property :idiomas,                     Text
  property :datos_empresa_anterior,      Text

  property :ultima_actualizacion ,       Text

  has n, :jobs
  has n, :main_results
  #has n, :job_competences
  has n, :dimensions
  has n, :jd_table_data, 'JdTableData'
  has 1, :description_workflow

  belongs_to  :analista, 'Position', required: false
  property    :status, Integer, default: NUEVO
  #property    :role_status, Integer, default: ADMINISTRADOR


  def self.sel_estados exception = nil
    exception = [exception] if exception.class != Array
    ret = []
    ESTADOS.each_with_index do |i, k|
      next if exception.include?( k )
      ret << [i, k]
    end
    ret
  end


  def dicisions_value
    get_list 'dicisions'
  end
  def estudio_basico_value
    estudio_basico
  end
  def anios_estudio_value
    anios_estudio
  end

  def anios_experiencia_laboral_value
    anios_experiencia_laboral
  end
  def anios_experiencia_cargo_value
    anios_experiencia_cargo
  end

  def certificaciones_value
    certificaciones.blank? ? [] : JSON.parse( certificaciones )
  end
  def certificaciones_value= value
    update certificaciones: value.to_json
  end
  def movilidad_value
    tipo_movilidad.blank? ? [] : JSON.parse( tipo_movilidad )
  end

  def movilidad_value=value
    update tipo_movilidad: value.to_json
  end
  def frecuencia_value
    frecuencia
  end
  def habilidades_funcionales_value
    habilidades_funcionales
  end
  def seguridad_value
    seguridad
  end
  def calidad_value
    calidad
  end
  def caracteristicas_ambientales_value
    caracteristicas_ambientales
  end
  def medio_ambiente_value
    medio_ambiente
  end
  def conocimiento_organizacional_value
    conocimiento_organizacional
  end
  def conocimiento_organizacional_value=value
    update conocimiento_organizacional: value.to_json
  end

  def ultima_actualizacion_value
    ultima_actualizacion
  end


  def ultima_actualizacion_value=value
    update ultima_actualizacion: value
  end


  def dicisions_value=(value)
    value = [] if value.blank?
    set_list 'dicisions', value
    save
  end
  def recomendations_value
    get_list 'recomendations'
  end
  def recomendations_value=(value)
    value = [] if value.blank?
    set_list 'recomendations', value
    save
  end
  def knowledges_value
    get_list 'knowledge'
  end
  def knowledges_value=value
    set_list 'knowledge', value
    save
  end
  def habilidades_value
    get_list 'habilidades'
  end
  def habilidades_value=value
    set_list 'habilidades', value
    save
  end
  def experiences_value
    get_list 'experience'
  end
  def experiences_value=value
    set_list 'experience', value
    save
  end
  def internal_situations_value
    get_list 'internal_impact'
  end
  def internal_situations_value=value
    set_list 'internal_impact', value
    save
  end
  def external_situations_value
    get_list 'external_impact'
  end
  def external_situations_value=value
    set_list 'external_impact', value
    save
  end
  def internal_contacts_value
    internal_contacts.blank? ? [] : JSON.parse( internal_contacts )
  end
  def internal_contacts_value=value
    update internal_contacts: value.to_json
  end
  def external_contacts_value
    external_contacts.blank? ? [] : JSON.parse( external_contacts )
  end
  def external_contacts_value=value
    update external_contacts: value.to_json
  end

  def aditional_responsability_value
    aditional_responsability
  end
  def aditional_responsability_value=value
    update aditional_responsability: value.to_json

  end
  def idiomas_value

    idiomas.blank?||idiomas=='null' ? [] : JSON.parse( idiomas )
  end
  def idiomas_value=value
    update idiomas: value.to_json

  end

  def competencias_value
    conductas.blank? ? [] : JSON.parse( conductas )
  end
  def competencias_value=value
    update conductas: value.to_json

  end

  def datos_empresa_anterior_value
    datos_empresa_anterior.blank? ? [] : JSON.parse( datos_empresa_anterior )
  end
  def datos_empresa_anterior_value=value
    update datos_empresa_anterior: value.to_json
  end
  def certificaciones_value
      certificaciones.blank? ? [] : JSON.parse( certificaciones )
  end
  def certificaciones_value=value
     update certificaciones: value.to_json
  end


  def dimensions_financieras
    financieras = []


    magnitudes = Magnitude.all(kind: Magnitude::FINANCIERAS).map(&:id)

    Dimension.all( job_description_id: id ).each do |i|
        financieras.push i if magnitudes.include?(i.magnitude_id)
    end

    financieras
  end

  def dimensions_no_financieras

    no_financieras = []

    magnitudes = Magnitude.all(kind: Magnitude::NO_FINANCIERAS).map(&:id)

    Dimension.all( job_description_id: id ).each do |i|
        no_financieras.push i if magnitudes.include?(i.magnitude_id)
    end

    no_financieras
  end

  def main_results
    MainResult.all job_description_id: id
  end

end
