# -*- encoding : utf-8 -*-
class Conduct < Historiable
  include DataMapper::Resource
  include DataMapper::Timestamp

  property :id,           Serial
  property :number,       Integer
  property :description,  Text, lazy: false
  property :visible,      Boolean, default: true

  timestamps :at

  belongs_to :competence
  #belongs_to :performance_competence
  has n, :performance_competence

  has n, :job_competences
end
