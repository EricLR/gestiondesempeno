# -*- encoding : utf-8 -*-
class Historial
	include DataMapper::Resource
	include DataMapper::Timestamps

	storage_names[:default] = 'historial'

  CREACION      = 'Creación'
  ACTUALIZACION = 'Actualización'
  ELIMINACION   = 'Eliminación'

	property :id,				Serial
	property :estado_original,	Text
	property :estado_actual,	Text
	property :fecha_original,	DateTime
	property :fecha_actual, 	DateTime
	property :comentario,		Text

	property :historiable_type, String
	property :historiable_id,	Integer

	timestamps :at

	belongs_to :user, :required => false

	def self.do comentario, item, params = {}
		h = Historial.new :historiable => item, :comentario => comentario
		if params[:auto] == true
			old = Historial.last :historiable_id => item.id, :historiable_type => item.class.to_s
			if old
				h.fecha_original = old.fecha_actual
				h.estado_original = old.estado_actual
			end
		else
			h.fecha_original 	= params[:fecha_original] 	if params.has_key? :fecha_original
			h.estado_original 	= params[:estado_original]	if params.has_key? :estado_original
		end
		h.estado_actual		= params[:estado_actual]	if params.has_key? :estado_actual
		h.usuar_id		= params[:usuario].id		if params.has_key?(:usuario) && params[:usuario]
		h.save
	end

	def historiable=(item, guardar_estado = true)
		throw 'No puede ser nulo' if item.nil?

		self.historiable_id 	= item.id
		self.historiable_type	= item.class.to_s

		self.fecha_actual 		= item.created_at if item.saved?
		self.estado_actual		= item.to_json if guardar_estado
	end

	def historiable
		cls = Kernel.const_get historiable_type
		cls.get historiable_id
	end

end
