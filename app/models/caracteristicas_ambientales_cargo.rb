# -*- encoding : utf-8 -*-
class CaracteristicasAmbientalesCargo

  include DataMapper::Resource

  property :id, Serial
  property :name, String

end
