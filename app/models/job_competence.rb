# -*- encoding : utf-8 -*-
class JobCompetence < Historiable
  include DataMapper::Resource
  include DataMapper::Timestamp

  property :id,       Serial

  timestamps :at

  belongs_to :competence, require: false
  belongs_to :conduct, require: false
  belongs_to :job, required: false
  belongs_to :job_description, required: false
end
