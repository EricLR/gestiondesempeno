# -*- encoding : utf-8 -*-
class KeyPerformanceIndicator < Historiable
  include DataMapper::Resource
  include DataMapper::Timestamp

  CODES_ARRAY = ['', 'Incremento', 'Ahorro', 'Evaluación', 'Proyecto', 'Función']
  
  INCREMENTO = 1
  AHORRO     = 2
  
  CODES = [
    [CODES_ARRAY[1], 1],
    [CODES_ARRAY[2], 2],
    [CODES_ARRAY[3], 3],
    [CODES_ARRAY[4], 4],
    [CODES_ARRAY[5], 5]
  ]

  
  property :id,             Serial
  property :description,    Text, lazy: false
  property :start_date,     Date
  property :end_date,       Date
  property :code,           Integer #TODO: Incremento = 1, Ahorro = 2, Proyecto = 3, esto es una formula
  property :last_date_gold, String
  property :increase_gold,  Integer
  property :gold,           String
  property :unit,           String #Unidad de medida
  property :ponderation,    Integer # Debe dar como suma = 100%
  property :editable,       Boolean, :default => true
  property :detail,         Text, lazy: false
  
  timestamps :at
  
  has n, :planifications
  # has n, :insert_planifications

  def increase_gold=val
    self[:increase_gold] = val unless val.blank?
  end

end
