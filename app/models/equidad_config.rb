# -*- encoding : utf-8 -*-
class EquidadConfig
  include DataMapper::Resource
  include DataMapper::Timestamp

  property :id,               Serial
  property :name,             String, length: 255
  property :value,            Text

  timestamps :at

  def self.set name, value
    ec = EquidadConfig.first name: name


    if ec.blank?
      return EquidadConfig.create( name: name, value: value ) 
    end
      
    if ec.value != value
      ec.value = value
      return ec.save
    end

    
  end

  def self.get name
    ec = EquidadConfig.first name: name
    
    ec.blank? ? nil : ec.value
  end

  def self.incomes
    ec = EquidadConfig.all
    ec.map(&:income)
  end
  def self.data
    EquidadConfig.all.map{|x| {income: x.income, ecuacion: x.value.to_i}}
  end
  def self.ecuacion_with_income_id income_id
    sel = EquidadConfig.data.select{|x| x[:income].id == income_id}.first
    sel.blank? ? nil : sel[:ecuacion]
  end

  def income_id
    name.gsub("income_structure_id:", "").to_i
  end
  def income
    IncomeStructure.get(income_id)
  end


  
end
