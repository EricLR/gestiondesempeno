# -*- encoding : utf-8 -*-
class MarketData
    include DataMapper::Resource
    include DataMapper::Timestamp

    property :id,               Serial
    property :nivel,            Integer
    property :banda_inferior,   Integer, default: 20
    property :banda_superior,   Integer, default: 20

    belongs_to :income_structure
    has n, :market_percentile_data, MarketPercentileData
    
    timestamps :at

    def self.find_with_points points, income_structure
        #MarketData.first :x_min.lte => points, :x_max.gte => points, income_structure: income_structure
    end

    def self.calculate_market points, income_structure
        # md = MarketData.find_with_points points, income_structure
        # return nil if md.nil?
        # (points * md.pendiente) + md.intercepto
    end
end
