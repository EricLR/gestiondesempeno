# -*- encoding : utf-8 -*-
class Compensation
    include DataMapper::Resource
    include DataMapper::Timestamp

    property :id,       Serial
    property :update_name,  String, length: 250
    property :deleted_at, ParanoidDateTime

    timestamps :at


    belongs_to :user
    belongs_to :position

    has n, :compensation_details

    def resumen income_structure
        total = 0

        self.compensation_details.each do |i|
            
            val = income_structure.income_structure_detail.map {|i| i.id}

            total += i.value if val.include?(i.income_structure_detail_id)
        end

        total
    end

end
