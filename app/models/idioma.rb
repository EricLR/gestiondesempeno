# -*- encoding : utf-8 -*-
class Idioma

  include DataMapper::Resource

  property :id, Serial
  property :name, String

end
