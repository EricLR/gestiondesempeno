#encoding: utf-8
class CacheUtilsOrgPuestos < FirebaseUser
  include SuckerPunch::Job

  def perform extra
    puts "LOG PERFORM #{extra.inspect}"
    # fb_set "equidad-interna/last-url", ""
    equidad_informes# extra
  end


  def equidad_informes
    base_url = "organigrama/puestos"

    puestos = Position.all(fields: [:id]).map(&:id)

    rutas = [base_url, base_url]

    puestos.each do |p_id|
      rutas << base_url+"/#{p_id}."
    end

    hydra = Typhoeus::Hydra.new max_concurrency: 2


    rutas.each do |ruta|
      call_cache ruta, hydra
    end

    hydra.run

  end

  def call_cache _url, hydra
    #puts "Generando cache de: #{Rails.configuration.url}/#{_url}".red

    request = Typhoeus::Request.new( "#{ENV['DATOS_URL']}/#{_url}", userpwd: '1-9:1234' )
    request.on_complete do |response|
      logger.info "ON COMPLETE: #{_url}"
      # fb_push "masa-salarial-cache/done", {url: "#{Rails.configuration.url}/#{_url}"}
    end
    hydra.queue request
  end


end
