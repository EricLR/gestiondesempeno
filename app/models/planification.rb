# -*- encoding : utf-8 -*-
class Planification # < Historiable
  	include DataMapper::Resource
  	include DataMapper::Timestamp

  	property :id,                 Serial
    property :closed,             Boolean, :default => false
    property :evaluator_closed,   Boolean, :default => false
    property :evaluated_closed,   Boolean, :default => false

  	belongs_to :evaluator, 'User'
  	belongs_to :evaluated, 'User'
  	#TODO: Revisar position_id que está de más
  	belongs_to :key_performance_indicator
    belongs_to :period

 	  has n, :tracing_kpi


  	def self.for_select(evaluated_id, period_id = nil)
      period_id = Period.last[:id] if period_id == nil

	    # id, descripción, meta, actual
	    #TODO: Realizar búsqueda por evaluado en self.all( XXX )
	    self.all(:evaluated_id => evaluated_id, period_id: period_id).map do |i|
        last_value = i.tracing_kpi.last.nil? ? i.key_performance_indicator.last_date_gold : i.tracing_kpi.last.increase
        p "SELECT I: #{i.inspect}"
        detail = i.key_performance_indicator.detail.blank? ? '' : i.key_performance_indicator.detail.gsub("\r", "<br />")
        [i.id, i.key_performance_indicator.description, i.key_performance_indicator.gold, last_value, i.key_performance_indicator.code, detail]
      end
	 end

    def open
      self.update( :evaluator_closed => false, :evaluated_closed => false, :closed => false )
    end

    def close
      self.update( :evaluator_closed => true, :evaluated_closed => true, :closed => true )
    end
end
