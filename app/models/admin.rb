# -*- encoding : utf-8 -*-

class Admin # :nodoc:
  include DataMapper::Resource
  include DataMapper::Timestamp

  def self.repository(name = nil, &block)
    super(:rvelasco, &block)
  end
  
  def self.default_repository_name
    :rvelasco
  end

  property :id, Serial
  property :rut, String, unique: true
  property :first_name, String
  property :last_name, String
  property :status, Boolean, default: true

  timestamps :at
end
