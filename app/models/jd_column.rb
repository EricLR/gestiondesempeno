# -*- encoding : utf-8 -*-
class JdColumn
    include DataMapper::Resource
    include DataMapper::Timestamp
    
    property :id,               Serial
    property :name,             String, :length => 255
    property :position, 		Integer, default: 1
    
    has n, :jd_table_data, 'JdTableData'
    belongs_to :jd_table
end
