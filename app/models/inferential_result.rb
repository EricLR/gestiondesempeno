# -*- encoding : utf-8 -*-
class InferentialResult < Historiable
  include DataMapper::Resource
  include DataMapper::Timestamp
  
  property :id,               Serial
  property :name,             String
end
