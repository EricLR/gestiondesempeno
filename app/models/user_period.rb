# -*- encoding : utf-8 -*-
class UserPeriod < Historiable
	include DataMapper::Resource
	include DataMapper::Timestamp

	property :id,       			Serial

	property :planification_open, 	Boolean, :default => false
	property :evaluation_open,		Boolean, :default => false

	timestamps :at
	property :deleted_at, ParanoidDateTime

	belongs_to :position
	belongs_to :period


end
