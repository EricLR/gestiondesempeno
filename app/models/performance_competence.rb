# -*- encoding : utf-8 -*-
class PerformanceCompetence < Historiable
  include DataMapper::Resource
  include DataMapper::Timestamp
  
  property :id,             Serial
  property :description,    Text
  property :end_date,       Date
  property :ponderation,    Integer # Debe dar como suma = 100%
  property :parent_type,    String # Planification or Evaluation
  property :parent_id,      Integer
  
  timestamps :at
  
  #has n, :conducts
  belongs_to :conduct
end
