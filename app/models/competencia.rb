# -*- encoding : utf-8 -*-
class Competencia < Historiable


  include DataMapper::Resource

  property :id, Serial
  property :name, String

  belongs_to :macro_competencia
  has n, :conductas
end
