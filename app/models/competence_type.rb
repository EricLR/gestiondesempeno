# -*- encoding : utf-8 -*-
class CompetenceType < Historiable
	include DataMapper::Resource
	include DataMapper::Timestamp


	property :id,           Serial
	property :name,         String
	property :evaluable,	Boolean, default: true


	if Rails.configuration.competencias == :saville
		belongs_to :area 
	end

	has n, :competence
end
