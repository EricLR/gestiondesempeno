# -*- encoding : utf-8 -*-

require 'dm-is-tree'
class Job < StringList # :nodoc:
  include DataMapper::Resource
  include DataMapper::Timestamp

  def self.repository(_name = nil, &block)
    super(:rvelasco, &block)
  end

  def self.default_repository_name
    :rvelasco
  end

  DEPENDENCY_TYPE_ARRAY = ['', 'Linea', 'Staff'].freeze
  ESTAMENTO_ARRAY = ['', 'Gerentes', 'Mandos Medios', 'Profesionales y Técnicos', 'Administrativos', 'Agentes CC', 'Operativos'].freeze
  JORNADA_ARRAY = ['', 'Jornada completa', 'Media jornada'].freeze
  CENTRO_COSTOS_ARRAY = [''].freeze

  PERIODOS = %w[Diario Semanal Mensual Trimestral Semestral Anual Periódica].freeze
  ENABLED = 1
  DISABLED = 0

  property :id, Serial
  property :name, String
  property :code, String
  property :level, Integer
  property :job_description_id, Integer
  property :critical, Boolean, default: DISABLED
  property :status, Boolean, default: ENABLED
  property :puntos, Integer
  property :status, Integer, default: ENABLED

  timestamps :at

  belongs_to :boss, 'Job', required: false
  is :tree, child_key: :boss_id, required: false

  belongs_to :vicepresidencia, required: false # vicepresidencia
  belongs_to :branch_office,   required: false # sucursal
  belongs_to :cost_center,     required: false # centro de costos
  belongs_to :expense_center,  required: false # centro de gastos
  belongs_to :management_type, required: false # tipo de gestion ej: estratégico, táctico, etc.
  belongs_to :dependency_type, required: false # tipo de dependencia ej: linea, staff, etc.
  belongs_to :job_class,       required: false # estamento ej: ejecutivos, supervisor, mandos medios, etc.
  belongs_to :profile,         required: false # perfil
  # belongs_to :working_day,     :required => false #jornada

  has n, :positions
  timestamps :at
  belongs_to :boss, 'Job', required: false
  is :tree, child_key: :boss_id, required: false, order: %i[boss_id name]

  has n, :functional_bosses
  has n, :main_results
  has n, :job_competences
  has n, :dimensions
  has n, :job_description
  has 1, :description_workflow
  belongs_to :profile, required: false
  belongs_to :job_description, required: false
  # De evaluación de cargos
  # property :nivel,  Integer
  # property :puntos, Integer
  # belongs_to :empresa, :required => false
  # belongs_to :company, required: false
  # belongs_to :job_type, required: false
  # belongs_to :role, required: false
  # belongs_to :functional_boss, required: false

  def self.todos
    DataMapper.repository(:rvelasco) do
      Job.all(order: :name)
    end
  end

  def self.activos
    DataMapper.repository(:rvelasco) do
      Job.all(status: ENABLED, order: :name)
    end
  end

  def active
    status.eql? ENABLED
  end

  def self.with_permission(permission)
    roles = Role.with_permission permission
    roles.jobs unless roles.blank?
  end

  def self.lista_estamentos
    Job.all(fields: [:estamento]).map(&:estamento) # , unique: true).map(&:estamento) #arregla error con mysql 5.7
  end

  def self.lista_job_class
    Job.all(fields: [:job_class_id]).map(&:job_class_id)
  end

  def self.lista_nivel
    Job.all(fields: [:level], unique: true, order: [:level]).map(&:level)
  end

  def self.lista_jornadas
    Job.all(fields: [:jornada], unique: true).map(&:jornada)
  end

  def self.lista_dependency_types
    Job.all(fields: [:dependency_type], unique: true).map(&:dependency_type)
  end

  def job_description
    DataMapper.repository(:default) do
      jd = JobDescription.get job_description_id
      if jd.blank?
        jd = JobDescription.create name: name
        # update job_description_id: jd.id
        self.job_description_id = jd.id
        save
      end
      return jd
    end
  end

  def aditional_responsability
    job_description.aditional_responsability
  end

  def others
    job_description.others
  end

  def habilidades
    job_description.habilidades
  end

  def dicisions_value
    job_description.dicisions_value
  end

  def dicisions_value=(value)
    job_description.dicisions_value = value
  end

  def recomendations_value
    job_description.recomendations_value
  end

  def recomendations_value=(value)
    job_description.recomendations_value = value
  end

  def knowledges_value
    job_description.knowledges_value
  end

  def knowledges_value=(value)
    job_description.knowledges_value = value
  end

  def habilidades_value
    job_description.habilidades_value
  end

  def habilidades_value=(value)
    job_description.habilidades_value = value
  end

  def experiences_value
    job_description.experiences_value
  end

  def experiences_value=(value)
    job_description.experiences_value = value
  end

  def internal_situations_value
    job_description.internal_situations_value
  end

  def internal_situations_value=(value)
    job_description.internal_situations_value = value
  end

  def external_situations_value
    job_description.external_situations_value
  end

  def external_situations_value=(value)
    job_description.external_situations_value = value
  end

  def internal_contacts_value
    job_description.internal_contacts_value
  end

  def internal_contacts_value=(value)
    job_description.internal_contacts_value = value
  end

  def external_contacts_value
    job_description.external_contacts_value
  end

  def external_contacts_value=(value)
    job_description.external_contacts_value = value
  end

  def idiomas_value
    job_description.idiomas_value
  end

  def idiomas_value=(value)
    job_description.idiomas_value = value
  end

  def competencias_value
    job_description.competencias_value
  end

  def competencias_value=(value)
    job_description.competencias_value = value
  end

  def estudio_basico_value
    job_description.estudio_basico_value
  end

  def anios_estudio_value
    job_description.anios_estudio_value
  end

  # def certificaciones_value
  #  job_description.certificaciones_value
  # end

  def datos_empresa_anterior_value
    job_description.datos_empresa_anterior_value
  end

  def datos_empresa_anterior_value=(value)
    job_description.datos_empresa_anterior_value = value
  end

  def anios_experiencia_laboral_value
    job_description.anios_experiencia_laboral_value
  end

  def anios_experiencia_cargo_value
    job_description.anios_experiencia_cargo_value
  end

  def certificaciones_value
    job_description.certificaciones_value
  end

  def certificaciones_value=(value)
    job_description.certificaciones_value = value
  end

  def movilidad_value
    job_description.movilidad_value
  end

  def frecuencia_value
    job_description.frecuencia_value
  end

  def habilidades_funcionales_value
    job_description.habilidades_funcionales_value
  end

  def seguridad_value
    job_description.seguridad_value
  end

  def calidad_value
    job_description.calidad_value
  end

  def caracteristicas_ambientales_value
    job_description.caracteristicas_ambientales_value
  end

  def medio_ambiente_value
    job_description.medio_ambiente_value
  end

  def conocimiento_organizacional_value
    job_description.conocimiento_organizacional_value
  end

  def dimensions_financieras
    financieras = []
    magnitudes = Magnitude.all(kind: Magnitude::FINANCIERAS).map(&:id)

    Dimension.all(job_description_id: job_description_id).each do |i|
      financieras.push i if magnitudes.include?(i.magnitude_id)
    end

    financieras
  end

  def dimensions_no_financieras
    no_financieras = []
    magnitudes = Magnitude.all(kind: Magnitude::NO_FINANCIERAS).map(&:id)

    Dimension.all(job_description_id: job_description_id).each do |i|
      no_financieras.push i if magnitudes.include?(i.magnitude_id)
    end

    no_financieras
  end

  def main_results
    MainResult.all job_description_id: job_description_id
  end

  def jerarquia
    generar_jerarquia(self).uniq
  end

  def self.linea(job_id)
    job_id = job_id.to_i
    item = Job.get job_id
    management_types = ManagementType.all
    item.boss_id = nil
    items = [item] + item.children
    @items = []

    items.each do |i|
      jt = management_types.select { |x| x[:id] == i.management_type_id }

      @items << {
        id: i.id,
        parent: i.boss_id,
        title: i.name,
        desc1: i.name,
        desc2: "Gestión: #{jt.blank? ? '' : jt.first[:name]}"
      }
    end

    @items
  end

  private

  def generar_jerarquia(item)
    data = [item]
    tmp = item.children
    # tmp.each do |i|
    #   data << i
    # end
    # tmp_2 = []
    # tmp.each do |i|
    #  tmp_2 << i if i.status != 0
    # end
    # Rails.logger.info "meeeeee : #{tmp.inspect}".red
    return data if tmp.blank?
    tmp.each do |i|
      next if i.status.zero?
      # if i.status != 0 # Rails.logger.info "AAAAAAAAAAAAAA".red if i.status = 123123
      data << i
      generar_jerarquia(i).each do |x|
        data << x
      end
      # end
    end

    data
  end
end
