# -*- encoding : utf-8 -*-
class InsertPlanification < Historiable
	include DataMapper::Resource
	include DataMapper::Timestamp

	ESCALA_EVAL = [
        {value: 0, name: 'No Satisfactorio'},
        {value: 1, name: 'En Desarrollo'},
        {value: 2, name: 'Competente'},
        {value: 3, name: 'Alto Desempeño'}
    ]
    MATRIZ_EVAL = [
    	['A0', 'A1', 'A2', 'A3', 'A4'],
    	['B0', 'Diamante en Bruto', 'Estrella Emergente', 'Futuro Líder', 'A4'],
    	['B1', 'Dilema', 'Futuro Prometedor', 'Estrella Emergente', 'B4'],
    	['B2', 'Bajo Desarrollo', 'Dilema', 'Profesional Experimentado', 'C4']

    ]
	CUADRANTES = [[9, 7, 5], [8, 6, 3], [4, 2, 1]]
	CUADRANTES_COLORES = [
		['rgb(230, 46, 37)', 'rgb(235, 100, 37)', 'rgb(253, 250, 41)'],
		['rgb(235, 104, 37)', 'rgb(253, 250, 41)', 'rgb(188, 230, 32)'],
		['rgb(253, 250, 41)', 'rgb(180, 226, 37)', 'rgb(130, 202, 63)']
	]
	CUADRANTES_NOMBRES = [
		['Cuad. 9', 'Cuad. 7', 'Cuad. 5'],
		['Cuad. 8', 'Cuad. 6', 'Cuad. 3'],
		['Cuad. 4', 'Cuad. 2', 'Cuad. 1']
	]

	property :id,       		Serial
	property :description,		String
	property :jobs, 			JSON
	property :mod_dates,		JSON
	property :competences_ids,	JSON
	property :kpi_ids,			JSON


	timestamps :at
	property   :deleted_at, 				ParanoidDateTime

	#belongs_to :key_performance_indicator, 	required: false
	#belongs_to :competence, 				required: false

	belongs_to :user, 						required: false

	def realizar_carga
		#TODO: Pendiente desarrollar
		puts  "Insert masivo"

		period = Period.current

		status = false
		Job.transaction do

			Job.all(id: jobs.map{|j| j['id'].to_i}).positions.each do |position|
				next if position.boss_position.blank?

				kpi_ids.each do |kpi|
					puts "KPI: #{kpi.inspect}".red
					kpi.delete 'code_name'
					kpi.delete 'start_date' if kpi['start_date'].blank?
					kpi.delete 'end_date' if kpi['end_date'].blank?
					kpi.delete 'ponderation' if kpi['ponderation'].blank?

					k = KeyPerformanceIndicator.create kpi
					puts "Errorrs: #{k.errors.inspect}".yellow

					data = {
						period: period,
						key_performance_indicator: k,
						evaluator_closed: true,
						evaluated_closed: true,
						closed: true
					}



					Planification.create data.merge(
			                                          	evaluator: position.boss_position,
														evaluated: position,
													)
					Planification.create data.merge(
			                                          	evaluator: position,
														evaluated: position,
													)
					# Matricial
					job_position = position.job
					if job_position and !job_position.functional_boss_id.blank?
						fb_position = Position.first(job_id: job_position.functional_boss_id)

						Planification.create data.merge(
			                                          	evaluator: fb_position,
														evaluated: position,
													)
					end

				end

				competences_ids.each do |comp|
					puts "Competences #{comp}"
					competence = Competence.get comp['competence']
					conducts   = Conduct.all id: comp['conducts']

					puts "Competence: #{competence.inspect}".yellow
					puts "Conducts: #{conducts.inspect}".yellow

					data = {
						start_date: comp['start_date'],
						end_date: comp['end_date'],
						ponderation: comp['ponderation'].to_i,
						conduct: comp['conducts'],
						competence: competence,
						period: period,
						evaluator_closed: true,
						evaluated_closed: true,
						closed: true,
						detail: comp['detail']
					}
					PlanificationCompetence.create data.merge(
					                                          	evaluator: position.boss_position,
																evaluated: position,
															)

					PlanificationCompetence.create data.merge(
					                                          	evaluator: position,
																evaluated: position,
															)
					# Matricial
					job_position = position.job
					if job_position and !job_position.functional_boss_id.blank?
						fb_position = Position.first(job_id: job_position.functional_boss_id)

						PlanificationCompetence.create data.merge(
			                                          	evaluator: fb_position,
														evaluated: position,
													)
					end

				end

			end


			if mod_dates.blank?
				md = [{date: Time.now.strftime( "%d/%m/%Y %H:%M" )}]
				update mod_dates: md
			else
				mod_dates<<{date: Time.now.strftime( "%d/%m/%Y %H:%M" )}
				save!
			end

			status = true
		end


		return status
	end


end
