# -*- encoding : utf-8 -*-
class FunctionalBoss < Historiable
  include DataMapper::Resource
  include DataMapper::Timestamp
  
  property :id,       Serial
  
  timestamps :at
  
  belongs_to :job
  belongs_to :boss, 'Job'
  
end
