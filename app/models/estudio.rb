# -*- encoding : utf-8 -*-
class Estudio

  include DataMapper::Resource

  property :id, Serial
  property :name, String

end
