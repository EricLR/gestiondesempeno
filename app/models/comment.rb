# -*- encoding : utf-8 -*-
class Comment < Historiable
	include DataMapper::Resource
	include DataMapper::Timestamp

	property :id, Serial
    property :detalle, Text, lazy: false
    property :extra, String
    property :commentable_id, Integer
    property :commentable_type, String

	belongs_to :author, 'User'
	belongs_to :description_workflow, required: false

	timestamps :at

	def self.job_description jd_id
		all commentable_id: jd_id, commentable_type: JobDescription.to_s
	end

	def author_date
		%Q(#{author.full_name} - #{created_at.strftime('%d/%m/%Y')})
	end

	def panel_author user
		author == user ? 'panel-success' : 'panel-info'
	end
end
