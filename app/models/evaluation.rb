# -*- encoding : utf-8 -*-
class Evaluation < Historiable
    include DataMapper::Resource
    include DataMapper::Timestamp

    property :id,                       Serial
    property :kpi_result,               Float, :default => 0
    property :kpi_ponderation,          Float, :default => 0
    property :kpi_final_result,         Text, :default => 0
    property :competence_result,        Float, :default => 0
    property :competence_ponderation,   Float, :default => 0
    property :competence_final_result,  Text, :default => 0
    property :kpi_competence_result,    Float, :default => 0
    property :boss_ponderation,         Float, :default => 0
    property :boss_result,              Float, :default => 0
    property :final_result,             Text, :default => 0
    property :evaluator_closed,         Boolean, :default => false
    property :evaluated_closed,         Boolean, :default => false
    property :kpi_aprobe,               Integer,:default => -1
    property :competence_aprobe,        Integer,:default => -1

    belongs_to :evaluator, 'User', :required => false
    belongs_to :evaluated, 'User', :required => false
    belongs_to :period


    timestamps :at
end
