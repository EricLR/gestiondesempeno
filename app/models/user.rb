# -*- encoding : utf-8 -*-
class User < Historiable
  include DataMapper::Resource
  include DataMapper::Timestamp

  def self.repository(name = nil, &block)
    super(:rvelasco, &block)
  end

  ENABLED  = true
  DISABLED = false

  MASCULINO = 0
  FEMENINO  = 1

  SEXOS = [:MASCULINO, :FEMENINO]

  property :id,                    Serial
  property :rut,                   String, :unique => true
  property :email,                 String
  property :first_name,            String
  property :last_name,             String
  property :phone_mobile,          String
  property :phone_office,          String
  #property :password,              BCryptHash, default: %|12345|
  property :birthdate,             Date
  property :contract_date,         Date
  property :gender,                Integer
  property :status,                Boolean, default: ENABLED
  property :has_compensation_data, Boolean, default: DISABLED
  #property :first_time,            Boolean, default: ENABLED

  timestamps :at

  # belongs_to :user_group, required: false, default:1
  # belongs_to :role, required: false
  belongs_to :working_day, required: false
  belongs_to :syndicate, required: false
  has 1, :position
  has 1, :account
  has n, :mailing_user
  has n, :compensation
  has n, :description_workflows, 'DescriptionWorkflow', parent_key: [:id], child_key: [:analista_id]
  has n, :comments

  ##########GESTION DESEMPEÑO
  has n, :evaluator, 'Planification', :child_key => [ :evaluator_id ]
  has n, :evaluated, 'Planification', :child_key => [ :evaluated_id ]
  has n, :evaluator, 'Agreement', :child_key => [ :evaluator_id ]
  has n, :evaluated, 'Agreement', :child_key => [ :evaluated_id ]
  has n, :evaluator, 'TracingCompetence', :child_key => [ :evaluator_id ]
  has n, :evaluated, 'TracingCompetence', :child_key => [ :evaluated_id ]
  has n, :evaluator, 'TracingKpi', :child_key => [ :evaluator_id ]
  has n, :evaluated, 'TracingKpi', :child_key => [ :evaluated_id ]
  has n, :evaluator, 'Evaluation', :child_key => [ :evaluator_id ]
  has n, :evaluated, 'Evaluation', :child_key => [ :evaluated_id ]
  has n, :user_period
  ##########################
  def self.activos
    DataMapper.repository(:rvelasco) do
      User.all(order: :last_name)
    end
  end

  def self.list_with_jobs opts = {}
    n = opts.size
    where_st = "WHERE users.has_compensation_data = #{opts[:has_compensation_data]}"
    opts.delete(:has_compensation_data)
    if n > 1
      opts.each do |x, opt|
        where_st += x.eql?(:sex) ? " and users.gender = #{opt}" : " and jobs.#{x} = '#{opt}'"
      end
    end

    by_sql(Job, Position) { |u, j, p|
      %{
        SELECT
        #{u.*}, #{j.name} AS job_name, #{j.level} AS job_level, #{p.id} AS position_id, #{j.job_class_id} AS estamento,  #{j.dependency_type_id} AS dependency_type
        FROM #{u} INNER JOIN #{p} ON #{p.user_id} = #{u.id} INNER JOIN #{j} ON #{j.id} = #{p.job_id} #{where_st}
      }
    }
  end
  def self.list_with_jobs_compe opts = {}
    users = User.all opts
    tmp = {}
    users.each do |u|
        position = u.position
        job = position.job
        tmp = {id: u.id, user: u, position_id: position.id, job_level: job.level, job_name: job.name, estamento: job.job_class_id, dependency_type: job.dependency_type_id }
    end

    tmp
  end

  def job_name
      @job_name
  end

  def job_level
      @job_level
  end

  def position_id
      @position_id.to_i
  end

  def estamento
      @estamento
  end

  def jornada
      @jornada
  end

  def dependency_type
      @dependency_type
  end

  def rut_name
      "#{rut} #{full_name}"
  end

  def first_time?
    first_time
  end

  def full_name
    %Q|#{first_name} #{last_name}|
  end

  def name_surname
    f_name = first_name.split
    fName = first_name.split
    %Q|#{f_name[0]} #{first_name[0]}|
  end

  def self.login(rut, password)
    user = User.first :rut => rut
    (user && (user[:password].nil? || user[:password] == password )) ? user : false
  end

  def self.uniq_attr atr, extra = {}
      all({fields: [atr], unique: true}.merge(extra)).map{|x| x[atr]}
  end

  def config name
      UserConfig.config name.to_s, id
  end

  def set_config name, value = nil
      UserConfig.set_config name, value, id
      value
  end

  def global_config name
      UserConfig.config name
  end

  def set_global_config name, value = nil
      UserConfig.set_config name, value
      value
  end

end
