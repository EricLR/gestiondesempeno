# -*- encoding : utf-8 -*-
class Period < Historiable
	include DataMapper::Resource
	include DataMapper::Timestamp

	property :id,       				Serial

	property :name,						String
	property :start_date,				Date
	property :end_date,					Date

	property :planification_closed, 	Boolean, :default => false
	property :evaluation_closed,		Boolean, :default => false
	property :deleted_at, ParanoidDateTime

	timestamps :at

	has n, :agreements
	has n, :planifications
	has n, :user_period
	has n, :tracing_kpi
	has n, :tracing_competence
	has n, :evaluations

	def self.current
		Period.first :start_date.lte => DateTime.now, :end_date.gte => DateTime.now
	end
end
