# -*- encoding : utf-8 -*-
class Agreement < Historiable
	include DataMapper::Resource
	include DataMapper::Timestamp

	property :id,           		Serial

	property :meta_o_conducta, 		Text
	property :acuerdo_o_comentario, String
	property :fecha,				Date
	property :detalle,				Text, lazy: false

	belongs_to :evaluator, 'User'
	belongs_to :evaluated, 'User'
  belongs_to :period, :required => false

	timestamps :at
end
