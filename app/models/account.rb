# -*- encoding : utf-8 -*-
class Account

  include DataMapper::Resource
  include DataMapper::Validate
  include DataMapper::Timestamp

  attr_accessor :password, :password_confirmation

  def self.repository(name = nil, &block)
    super(:rvelasco, &block)
  end

  def self.default_repository_name
    :rvelasco
  end

  ENABLED  = true
  DISABLED = false

  # Properties
  property :id, Serial
  property :username, String
  property :password, BCryptHash, default: %(12345)
  property :first_time, Boolean, default: ENABLED
  property :status, Boolean, default: true
  property :user_id, Integer

  belongs_to :role, required: true, default: 2

  # Validations
  validates_presence_of :username, :password
  validates_length_of :password, :min => 5, :max => 255
  validates_length_of :username, :min => 8, :max => 20
  validates_uniqueness_of :username, :case_sensitive => false
  ##
  # This method is for authentication purpose
  #
  # def self.authenticate(email, password)
  #  account = first(conditions: { email: email }) if email.present?
  #  account && account.password?(password) ? account : nil
  # end
  ##
  # This method is used by AuthenticationHelper
  #
  def self.find_by_id(id)
    get(id)
  rescue StandardError
    nil
  end

  def password?(password)
    ::BCrypt::Password.new(crypted_password) == password
  end

  def user
    admin? ? Admin.get(user_id) : User.get(user_id)
  end

  def image
    admin? ? '/images/user.png' : user.image.url(:sidebar)
  end

  def email
    admin? ? 'no email' : user.email
  end

  def admin?
    role.name.casecmp('administrador').zero? ? true : false
  end

  def self.authenticate(username, password)
    account = first(:conditions => { :username => username }) if username.present?
    #(account && ( account[:password].nil? || password == account[:password] )) ? account : false
    account.blank? ? false : account
  end

  ##
  # This method is used by AuthenticationHelper
  #
  def self.find_by_id(id)
    get(id) rescue nil
  end

  def has_password?(password)
    ::BCrypt::Password.new(crypted_password) == password
  end

  def config name
    UserConfig.config name.to_s, id
  end

  def set_config name, value = nil
    UserConfig.set_config name, value, id
    value
  end

  def global_config name
    UserConfig.config name
  end

  def set_global_config name, value = nil
    UserConfig.set_config name, value
    value
  end

  def self.crear_cuentas
    User.all.each do |user|
      Account.first_or_create({rut: user.rut, first_name: user.first_name, last_name: user.last_name, email: user.email, user_id: user.id, role_id: 2, first_time: false})
    end
  end

  private
  def password_required
    crypted_password.blank? || password.present?
  end

  def encrypt_password
    self.crypted_password = ::BCrypt::Password.create(password) if password.present?
  end

end
