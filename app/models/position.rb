# -*- encoding : utf-8 -*-
require 'dm-is-tree'

class Position < Historiable
  include DataMapper::Resource
  include DataMapper::Timestamp

  def self.default_repository_name
    :rvelasco
  end
  def self.repository(name = nil, &block)
    super(:rvelasco, &block)
  end

  property :id,       Serial

  timestamps :at

  belongs_to :job
  belongs_to :user
  belongs_to :boss_position, 'Position', child_key: [:boss_position_id], required: false


  is :tree, :child_key => [:parent_id], :required => false

  def children_jobs
    job.children
  end

  def children_positions include_functional = false, auto_evaluacion = false
    jobs = children_jobs
    jobs += Job.all(functional_boss_id: job.id) if include_functional
    jobs += [job] if auto_evaluacion

    arr = []

    jobs.each do |i|
      arr += Position.all :job => i
    end

    arr
  end

  def children_users
    positions = children_positions

    tmp = []

    users = User.all id: positions.map(&:user_id)
    jobs  = Job.all id: positions.map(&:job_id)

    positions.each do |pos|
      user = users.select{|x| x.id == pos.user_id}
      job  = jobs.select{|x| x.id == pos.job_id}

      if !user.blank? && !job.blank?
        user = user.first
        job = job.first

        tmp << {id: user.id, name: user.name, job_name: job.name}
      end
    end

    tmp
  end



  has 1, :job_evaluation
  has n, :compensation
  has n, :income_structure_data, IncomeStructureData
end
