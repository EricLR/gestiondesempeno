# -*- encoding : utf-8 -*-
class JobEvaluation < Historiable
    include DataMapper::Resource
    include DataMapper::Timestamp

    property :id, Serial

    belongs_to :position

    belongs_to :kh_tecnico, 'JobEvaluationData', :required => false
    belongs_to :kh_gerencia, 'JobEvaluationData', :required => false
    belongs_to :kh_relaciones_humanas, 'JobEvaluationData', :required => false
    property   :kh_puntos, Integer
    property   :kh_nivel, Integer
    belongs_to :sp_ambito, 'JobEvaluationData', :required => false
    belongs_to :sp_desafio, 'JobEvaluationData', :required => false
    property   :sp_porcentaje, Integer
    property   :sp_puntos, Integer
    belongs_to :ac_libertad, 'JobEvaluationData', :required => false
    belongs_to :ac_magnitud, 'JobEvaluationData', :required => false
    belongs_to :ac_impacto, 'JobEvaluationData', :required => false
    property   :ac_puntos, Integer
    property   :total, Integer
    property   :perfil, String
    property   :nivel, Integer
    property   :errores, Text, default: "", required: false
    property   :comentario, Text, required: false


    def errores_value
        errores.blank? ? [] : JSON.parse( errores )
    end

    timestamps :at
end
