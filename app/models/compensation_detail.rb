# -*- encoding : utf-8 -*-
class CompensationDetail

    include DataMapper::Resource
    include DataMapper::Timestamp

    property :id,       Serial
    property :value,    Integer
    property :deleted_at, ParanoidDateTime
    timestamps :at

    belongs_to :compensation

    belongs_to :income_structure_detail

end
