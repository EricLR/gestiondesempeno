# -*- encoding : utf-8 -*-
class ActionVerb < Historiable
  include DataMapper::Resource
  include DataMapper::Timestamp

  property :id,               Serial
  property :name,             String

  #belongs_to :job_type
  belongs_to :management_type
end
