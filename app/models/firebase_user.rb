#encoding: utf-8
class FirebaseUser
  def firebase
    Firebase::Client.new "https://#{ENV['FIREBASE_ID']}.firebaseio.com", ENV['FIREBASE_TOKEN']
  end

  def fb_get object

    begin
      firebase.get(object).body
    rescue Exception => e
      logger.info "Error en fb_get object=#{object.inspect}".red
    end
  end

  def fb_set object, value

    begin
      firebase.set object, value
    rescue Exception => e
      logger.info "Error en fb_set object=#{object.inspect} value=#{value.inspect}".red
    end
  end

  def fb_push object, values
    logger.info "FB-PUSH: #{object} -> #{value}".red
    begin
      firebase.push object, values
    rescue Exception => e
      logger.info "Error en fb_push object=#{object.inspect}".red
    end
  end
  def fb_del object, query = {}

    begin
      firebase.delete object, values
    rescue Exception => e
      logger.info "Error en fb_del object=#{object.inspect}".red
    end
  end
end
