# -*- encoding : utf-8 -*-
class ExperienciaLaboral

  include DataMapper::Resource

  property :id, Serial
  property :name, String

end
