# -*- encoding : utf-8 -*-
class IncomeStructure
    # def self.default_repository_name
    #   :rvelasco
    # end
    include DataMapper::Resource
    include DataMapper::Timestamp

    property :id,               Serial
    property :name,             String
    property :acronym,          String
    property :position,         Integer
    property :banda_inferior,   Integer, default: 20
    property :banda_superior,   Integer, default: 20
    # property :in_masa_salarial, Boolean, default: true
    # property :deleted_at, ParanoidDateTime

    # has n, :income_structure_detail
    # has n, :market_data, 'MarketData'
    has n, :income_structure_data, IncomeStructureData
    has n, :equidad_puntos
    has n, :income_structure_detail

    timestamps :at

    def full_name
        name.include?("(#{acronym})") ? name : "#{name} (#{acronym})"
    end
end
