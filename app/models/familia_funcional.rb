# -*- encoding : utf-8 -*-
class FamiliaFuncional
  include DataMapper::Resource

  # property <name>, <type>
  property :id, Serial
  property :name, String

  #has n, :job
end
