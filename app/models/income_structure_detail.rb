# -*- encoding : utf-8 -*-
class IncomeStructureDetail
    include DataMapper::Resource
    include DataMapper::Timestamp

    property :id,       Serial
    property :name,     String
    property :deleted_at, ParanoidDateTime

    belongs_to :income_structure
    has n, :compensation_detail

    timestamps :at

end
