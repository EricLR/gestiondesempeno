# -*- encoding : utf-8 -*-
class Syndicate
	include DataMapper::Resource
  	include DataMapper::Validate

    def self.repository(name = nil, &block)
      super(:rvelasco, &block)
    end

  	property :id,           Serial
  	property :name,         String, unique: true

  	timestamps :at

  	has n, :users
end
