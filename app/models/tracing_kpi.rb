# -*- encoding : utf-8 -*-
class TracingKpi < Historiable
	include DataMapper::Resource
	include DataMapper::Timestamp

	SEGUIMIENTO = 'seguimiento'
	EVALUACION  = 'evaluacion'

	property :id,           Serial
	property :trace_date,	Date
	property :increase,		Integer
	property :increment,	Integer
	property :kind,			String

	property :aprobe , Integer , :default => -1
	property :closed,		Boolean, :default => false
	property :apelacion, String
	property :comment, String

	timestamps :at

	belongs_to :evaluator, 'User', :required => false
	belongs_to :evaluated, 'User', :required => false
	belongs_to :planification
	belongs_to :period

	def self.id_swap
      tc = TracingKpi.all
			tc.each do |xswap|
					ps = Position.first(id: xswap.evaluated_id)
					next if ps.blank?
					puts "--------------------------------------------------------------------------------------------------------------".green
					puts "IDS ANTES DE CONVERTIR : evaluated_id #{xswap.evaluated_id} , evaluator_id #{xswap.evaluator_id}".red.bold
					xswap.evaluated_id = ps.user_id
					xswap.evaluator_id = ps.boss_position.user_id unless ps.boss_position.blank?
					xswap.save
					puts "IDS Despues DE CONVERTIR : evaluated_id #{xswap.evaluated_id} , evaluator_id #{xswap.evaluator_id}".blue.bold
					puts "--------------------------------------------------------------------------------------------------------------".green
			end
  end
end
