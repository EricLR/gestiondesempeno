# -*- encoding : utf-8 -*-
class Magnitude < Historiable
  include DataMapper::Resource
  include DataMapper::Timestamp

  FINANCIERAS     = 0
  NO_FINANCIERAS  = 1

  TIPOS = [:FINANCIERAS, :NO_FINANCIERAS]

  property :id,      Serial
  property :name,    String
  property :kind,		Integer

  has n, :dimensions

  timestamps :at
end
