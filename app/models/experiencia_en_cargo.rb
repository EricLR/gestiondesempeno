# -*- encoding : utf-8 -*-
class ExperienciaEnCargo

  include DataMapper::Resource

  property :id, Serial
  property :name, String

end
