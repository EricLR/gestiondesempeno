# -*- encoding : utf-8 -*-
class JobdescriptionProcess
	NO_ENCONTRADO = 0
	ENCONTRADO    = 1
    FINALIZADO    = 2

	def self.process upload, job_id = nil
		snow      = Time.now.strftime("%Y-%m-%d-%H%M%S").to_s
    file_name = "#{snow}_#{upload.original_filename}"
    file_path = "#{Rails.root}/public/convert/uploads/#{file_name.gsub(' ', '')}"

    begin
      File.open(file_path, "wb") {|f| f.write upload.read}
      tmp = convert_to_html file_path, "#{snow}-descripcion"
      @descripcion              = ""
      @desc_funcional           = []
      @decisions_recomendations = {decisions: [], recomendations: []}
			@situaciones							= {internas: [], externas: []}
      @relaciones               = {internas: [], externas: []}
      @perfil = {
				rubro:[],
				conocimiento: [],
				anios_estudio: "",
				estudio_basico: "",
				certificacion: [],
				experience: "",
				anios_experiencia_cargo: "",
				anios_experiencia_laboral: "",
				idiomas: [],
				movilidad: []#,
				#frecuencia: ""
				#continuar aqui
			}
			@contexto                 = []
      @dimensiones              = {magnitudes: [], recursos: []}
      @titulo_cargo             = nil
      @habilidades              = []
			@otras 										= ""
      dinamicas_array 		  = []
      hay_descripcion           = NO_ENCONTRADO
		  hay_titulo                = NO_ENCONTRADO
      trs = Nokogiri::HTML(tmp).css('table tr')

     	trs.each do |tr|
				tr_text  = tr.content
				tr_textd = tr_text.downcase

       	###### DESCRIPCIONES #######
       	if hay_descripcion == ENCONTRADO # Entonces proceso
         	@descripcion    = limpia_string tr_text
         	hay_descripcion = FINALIZADO
       	end

       	if hay_descripcion == NO_ENCONTRADO
         	hay_descripcion = ENCONTRADO if tr_textd.include?("propósito")&&tr_textd.include?("cargo")
       	end
       	######!DESCRIPCIONES #######
     	end
			count = 0
     	hay_desc_funcional             = NO_ENCONTRADO
     	hay_desc_funcional_counter     = 0
     	hay_desc_funcional_tmp         = []
     	hay_desc_funcional_tmp_counter = 0

     	hay_decisiones          = NO_ENCONTRADO
     	hay_decisiones_counter  = 0
     	decisiones_tmp          = []
     	recomendaciones_tmp     = []

			hay_dimensiones          = NO_ENCONTRADO
     	hay_dimensiones_counter  = 0
			hay_dimensiones_tmp_counter = 0
     	dimensiones_tmp          = []

			hay_situaciones_internas          = NO_ENCONTRADO
			hay_situaciones_internas_counter  = 0
			hay_situaciones_internas_tmp_counter = 0
			situaciones_internas_tmp          = ""

			hay_situaciones_externas          = NO_ENCONTRADO
			hay_situaciones_externas_counter  = 0
			hay_situaciones_externas_tmp_counter = 0
			situaciones_externas_tmp          = ""

     	hay_rel_int             = NO_ENCONTRADO
     	hay_rel_int_go          = false
     	hay_rel_int_counter     = 0
     	hay_rel_ext             = NO_ENCONTRADO
     	hay_rel_ext_go          = false
     	hay_rel_ext_counter     = 0
     	relaciones_internas_tmp = {quien: "" , que: "", periodo: ""}
     	relaciones_externas_tmp = {quien: "" , que: "", periodo: ""}

			hay_otras_res  					= NO_ENCONTRADO
			hay_otras_res_count 		= 0
			responsabilidad_tmp     = ""

			hay_rubro		  					= NO_ENCONTRADO
			hay_rubro_counter 			= 0
			rubro_tmp    					  = {tipo_empresa:"" ,sector:"",tamano_empresa:"",experiencia_sector:"" }
			rubro_tmp_counter       = 0

			hay_estudios						= NO_ENCONTRADO
			hay_estudios_counter    = 0
			estudios_tmp 						= ""
			estudios_tmp_counter    = 0

			hay_conocimientos				    = NO_ENCONTRADO
			hay_conocimientos_counter   = 0
			conocimientos_tmp 					= ""
			certificacion_tmp           = {certificacion:""}
			conocimientos_tmp_counter   = 0

			hay_experiencias         = NO_ENCONTRADO
			hay_experiencias_counter = 0
			experiencias_tmp				 = {anios_experiencia_laboral: "" , anios_experiencia_cargo: "", experience: ""}
			experiencias_tmp_counter = 0

			hay_idiomas  						 = NO_ENCONTRADO
			hay_idiomas_counter      = 0
			idiomas_tmp						   =  {idioma:"", lectura:"",oral:"", escrito:""}
			idiomas_tmp_counter      = 0

			hay_movilidad            = NO_ENCONTRADO
			hay_movilidad_counter    = 0
			movilidad_tmp            = {movilidad: "", frecuencia: ""}
			movilidad_tmp_counter    = 0

			hay_habilidades          = NO_ENCONTRADO
			hay_habilidades_counter  = 0
			habilidades_tmp          = []


     	Nokogiri::HTML(tmp).css('td').each do |td|
         	td_text  = td.content
         	td_textd = td_text.downcase
         	td_title = td.css('b').blank? ? "" : td.css('b').first.content.downcase
					if td_text.include?('Elija') && td_text.include?('un') && td_text.include?('elemento')
						 td_text = ""
					end
					Rails.logger.info "td_title #{td_title}".yellow
					Rails.logger.info "td_text #{td_text}".blue
					#### TITULO ####
					if hay_titulo == ENCONTRADO
						@titulo = limpia_string(td_text)
						hay_titulo = FINALIZADO
					end
					hay_titulo = ENCONTRADO if td_textd.include?("Nombre") && td_textd.include?("Cargo")

         	if hay_desc_funcional == ENCONTRADO
             	hay_desc_funcional_counter += 1

             	if hay_desc_funcional_counter >= 6 # Supero 5 TD para encontrar las que corresponden
                 	hay_desc_funcional_tmp_counter += 1
									if hay_desc_funcional_tmp_counter != 1
                 		hay_desc_funcional_tmp << limpia_string( td_text ) # Guardo texto
									end
                 	if hay_desc_funcional_tmp_counter == 4 # Voy guardando de 3 columnas
											count +=1
											hay_desc_funcional_tmp << count
                     	hay_desc_funcional_tmp_counter = 0
                     	@desc_funcional << hay_desc_funcional_tmp
                     	hay_desc_funcional_tmp = []
                 	end
             	end
         	end
         	if hay_decisiones == ENCONTRADO
             	hay_decisiones_counter += 1

             	if hay_decisiones_counter == 3
                 	#@decisions_recomendations[:decisions] = td.css('ul li').map{|li| limpia_string(li.content)}
									td.children.each do |li|
										li.children.each do |li2|
											next if limpia_string(li2.text).blank?
											@decisions_recomendations[:decisions] << limpia_string(li2.text)
										end
									end
             	end
             	if hay_decisiones_counter == 4
                 	#@decisions_recomendations[:recomendations] = td.css('ul li').map{|li| limpia_string(li.content)}
									td.children.each do |li|
										li.children.each do |li2|
											next if limpia_string(li2.text).blank?
											#Rails.logger.info "  #{limpia_string(li2.text)}".red.bold
											@decisions_recomendations[:recomendations] << limpia_string(li2.text)
										end
									end
             	end
             	if hay_decisiones_counter >= 5
                 	hay_decisiones = FINALIZADO
             	end
         	end

					if hay_dimensiones == ENCONTRADO
             	hay_dimensiones_counter += 1

             	if hay_dimensiones_counter >= 4 # Supero 5 TD para encontrar las que corresponden

                 	hay_dimensiones_tmp_counter  += 1
                 	dimensiones_tmp << limpia_string( td_text ) # Guardo texto
                 	if hay_dimensiones_tmp_counter == 2 # Voy guardando de 3 columnas
                     	@dimensiones[:magnitudes] << dimensiones_tmp
                     	dimensiones_tmp = []
                 	end
									if hay_dimensiones_tmp_counter == 4
											@dimensiones[:recursos] << dimensiones_tmp
											dimensiones_tmp = []
											hay_dimensiones_tmp_counter = 0
									end
             	end
         	end

					if hay_situaciones_internas == ENCONTRADO
             	hay_situaciones_internas_counter += 1
             	if hay_situaciones_internas_counter >= 2
									unless limpia_string(td_textd).include?('situaciones')&&limpia_string(td_textd).include?('externas')
	                   	hay_situaciones_internas_tmp_counter  += 1
	                   	situaciones_internas_tmp += limpia_string( td_text )# Guardo texto
											if situaciones_internas_tmp.length > 0
													@situaciones[:internas] << situaciones_internas_tmp
											end
											situaciones_internas_tmp = ""
									end
             	end
         	end
					if hay_situaciones_externas == ENCONTRADO
             	hay_situaciones_externas_counter += 1
             	if hay_situaciones_externas_counter >= 2
									unless limpia_string(td_textd).include?('relaciones')&&limpia_string(td_textd).include?('internas')
	                   	hay_situaciones_externas_tmp_counter  += 1
	                   	situaciones_externas_tmp += limpia_string( td_text )# Guardo texto
											if situaciones_externas_tmp.length > 0
													@situaciones[:externas] << situaciones_externas_tmp
											end
											situaciones_externas_tmp = ""
									end

             	end
         	end

         	if hay_rel_int == ENCONTRADO

             	if hay_rel_int_go
                 	hay_rel_int_counter +=1
									if hay_rel_int_counter == 1
                 			relaciones_internas_tmp[:quien] = limpia_string( td_text )
									end
									if hay_rel_int_counter == 2
                 			relaciones_internas_tmp[:que] = limpia_string( td_text )
									end
									if hay_rel_int_counter == 3
                 			relaciones_internas_tmp[:periodo] = limpia_string( td_text )
									end

                 	if hay_rel_int_counter == 3
                     	hay_rel_int_counter = 0
                     	@relaciones[:internas] << relaciones_internas_tmp
                     	relaciones_internas_tmp = {quien: "" , que: "", periodo: ""}
                 	end
             	end

             	hay_rel_int_go = true if td_text.downcase.include?("período") && !td_text.downcase.include?("períodos")
         	end

         	if hay_rel_ext == ENCONTRADO
             	if hay_rel_ext_go
                 	hay_rel_ext_counter +=1
									if hay_rel_ext_counter == 1
											relaciones_externas_tmp[:quien] = limpia_string( td_text )
									end
									if hay_rel_ext_counter == 2
											relaciones_externas_tmp[:que] = limpia_string( td_text )
									end
									if hay_rel_ext_counter == 3
											relaciones_externas_tmp[:periodo] = limpia_string( td_text )
									end

                 	if hay_rel_ext_counter == 3
                     	hay_rel_ext_counter = 0
                     	@relaciones[:externas] << relaciones_externas_tmp
                     	relaciones_externas_tmp = {quien: "" , que: "", periodo: ""}
                 	end
             	end

             	hay_rel_ext_go = true if td_text.downcase.include?("período") && !td_text.downcase.include?("períodos")
         	end

					if hay_otras_res == ENCONTRADO
							hay_otras_res_count +=1
							responsabilidad_tmp = limpia_string( td_text )
							if hay_otras_res_count == 1
								@otras = responsabilidad_tmp
								responsabilidad_tmp = ""
							end
					end


         	if hay_rubro == ENCONTRADO
							hay_rubro_counter += 1
             	if hay_rubro_counter >= 5
										rubro_tmp_counter +=1
										if rubro_tmp_counter == 1
												rubro_tmp[:tipo_empresa] = limpia_string( td_text )
										end
										if rubro_tmp_counter == 2
												rubro_tmp[:sector] = limpia_string( td_text )
										end
										if rubro_tmp_counter == 3
												rubro_tmp[:tamano_empresa] = limpia_string( td_text )
										end
										if rubro_tmp_counter == 4
												rubro_tmp[:experiencia_sector] = limpia_string( td_text )
										end
										if rubro_tmp_counter == 4
												@perfil[:rubro] << rubro_tmp
												rubro_tmp_counter = 0
												rubro_tmp = {tipo_empresa:"" ,sector:"",tamano_empresa:"",experiencia_sector:"" }
										end
             	end
         	end

					if hay_estudios == ENCONTRADO
						hay_estudios_counter +=1
						if hay_estudios_counter >=3
							estudios_tmp_counter +=1
							if estudios_tmp_counter == 1
								@perfil[:estudio_basico] = limpia_string( td_text )
							end
							if estudios_tmp_counter == 2
								@perfil[:anios_estudio] = limpia_string( td_text )


							end
							if estudios_tmp_counter == 3
								@perfil[:conocimiento] << limpia_string( td_text )
								conocimientos_tmp = ""
								estudios_tmp_counter = 0
								hay_estudios = FINALIZADO
							end
						end
					end


					if hay_experiencias == ENCONTRADO
						hay_experiencias_counter +=1
						if hay_experiencias_counter >= 2
							experiencias_tmp_counter +=1
							if experiencias_tmp_counter == 1
									experiencias_tmp[:anios_experiencia_laboral] = limpia_string( td_text )
									@perfil[:anios_experiencia_laboral] = experiencias_tmp[:anios_experiencia_laboral]
							end
							if experiencias_tmp_counter == 2
									experiencias_tmp[:anios_experiencia_cargo] = limpia_string( td_text )
									@perfil[:anios_experiencia_cargo] = experiencias_tmp[:anios_experiencia_cargo]
							end
							if experiencias_tmp_counter >=5
									conocimientos_tmp_counter +=1
									if conocimientos_tmp_counter == 1 && !td_text.include?("Idiomas")
										certificacion_tmp[:certificacion] << limpia_string( td_text )
										unless certificacion_tmp.length > 0
												@perfil[:certificacion] << certificacion_tmp
										end
										certificacion_tmp = {certificacion: ""}

									end
									if conocimientos_tmp_counter == 2
										experiencias_tmp[:experience] = limpia_string( td_text )
										@perfil[:experience] << experiencias_tmp[:experience]
										experiencias_tmp = {experience: ""}
										conocimientos_tmp_counter = 0
									end
							end

						end

					end
					#if hay_conocimientos == ENCONTRADO
					#		hay_conocimientos_counter += 1
					#		if hay_conocimientos_counter >= 2
					#				conocimientos_tmp_counter  += 1
					#				if conocimientos_tmp_counter == 2
					#					certificacion_tmp[:certificacion] << limpia_string( td_text )
					#					@perfil[:certificacion] << certificacion_tmp
					#					certificacion_tmp = {certificacion: ""}
					#					conocimientos_tmp_counter = 0
					#				end

					#		end

					#end

					if hay_idiomas == ENCONTRADO
							hay_idiomas_counter +=1
							if hay_idiomas_counter >=2
									idiomas_tmp_counter+=1
									if idiomas_tmp_counter == 1
											idiomas_tmp[:idioma] = limpia_string( td_text )

									end
									if idiomas_tmp_counter == 2
											idiomas_tmp[:lectura] = limpia_string( td_text )
											idiomas_tmp[:oral] = limpia_string( td_text )
											idiomas_tmp[:escrito] = limpia_string( td_text )
											@perfil[:idiomas] << idiomas_tmp
											idiomas_tmp= {idioma:"", lectura:"",oral:"", escrito:""}
									end

									if 	idiomas_tmp_counter ==  3
											movilidad_tmp[:movilidad] = limpia_string( td_text )
											Rails.logger.info "Movilidad #{td_text}".blue.bold
											#@perfil[:movilidad] = limpia_string( td_text )
									end
									if idiomas_tmp_counter == 4
											movilidad_tmp[:frecuencia] = limpia_string( td_text )
											Rails.logger.info "frecuencia #{td_text}".blue.bold
											#@perfil[:frecuencia] = limpia_string( td_text )
											@perfil[:movilidad] << movilidad_tmp
											movilidad_tmp = {movilidad: "", frecuencia:""}
											idiomas_tmp_counter = 0
											#hay_movilidad = FINALIZADO
									end

									#if hay_movilidad == FINALIZADO && idiomas_tmp_counter == 4
									#	idiomas_tmp_counter = 0
									#end
							end
					end


					if hay_habilidades == ENCONTRADO
							hay_habilidades_counter +=1
							Rails.logger.info "hay_habilidades_counter #{hay_habilidades_counter} td_text #{td_text}".red.bold
							if hay_habilidades_counter >= 2
									@habilidades << limpia_string(td_text)
									Rails.logger.info "hay_habilidades_counter #{hay_habilidades_counter} td_text #{td_text}".blue.bold
							end
					end

					#if hay_movilidad == ENCONTRADO
					#		hay_movilidad_counter +=1
					#		if hay_movilidad_counter >=3
					#				movilidad_tmp_counter+=1
					#				if movilidad_tmp_counter == 1

					#						@perfil[:movilidad] = limpia_string( td_text )
					#				end
					#				if movilidad_tmp_counter == 2
					#					  @perfil[:frecuencia] = limpia_string( td_text )
					#				end

					#		end
					#end


         	###### PRIMERO BUSCO TITULO Y EN EL CODIGO ANTERIOR LO PROCESO

         	if hay_desc_funcional == NO_ENCONTRADO
             	if td_textd.include?("descripción")&&td_textd.include?("funcional")
                 	hay_desc_funcional = ENCONTRADO
             	end
         	end
         	if td_title.include?("dimensiones")
             	hay_desc_funcional = FINALIZADO
							hay_dimensiones = ENCONTRADO
         	end

					if td_title.include?("organigrama")
							hay_dimensiones = FINALIZADO
					end

         	if td_title.include?("decisiones")
             	hay_decisiones = ENCONTRADO
							hay_dimensiones = FINALIZADO
         	end
					if td_title.include?("situaciones")&&td_title.include?("internas")
             	hay_decisiones = FINALIZADO
							hay_situaciones_internas = ENCONTRADO
         	end
					if td_title.include?("situaciones")&&td_title.include?("externas")
             	hay_situaciones_internas = FINALIZADO
							hay_situaciones_externas = ENCONTRADO
         	end
					if td_title.include? "relaciones"
				  		hay_situaciones_externas = FINALIZADO
					end
					if td_title.include? "relaciones"
             	if td_title.include? "internas"
                 	hay_rel_int = ENCONTRADO
             	end
             	if td_title.include? "externas"
                 	hay_rel_int = FINALIZADO
                 	hay_rel_ext = ENCONTRADO
             	end
         	end
					if td_title.include? "perfil"
							#hay_otras_res = ENCONTRADO
							hay_rel_ext = FINALIZADO
					end
         	if td_title.include? "rubro"
             	hay_rubro  = ENCONTRADO
         	end

         	if td_title.include?("estudios")
             	hay_rubro = FINALIZADO
							hay_estudios = ENCONTRADO
         	end
					#if td_title.include?("conocimientos")
          #   	hay_estudios = FINALIZADO
					#		hay_conocimientos = ENCONTRADO
         	#end
					if td_title.include?("experiencia")
							#hay_conocimientos = FINALIZADO
							hay_experiencias = ENCONTRADO
					end

					if td_title.include?("idiomas")
							hay_experiencias = FINALIZADO
							hay_idiomas = ENCONTRADO
							hay_movilidad = ENCONTRADO
					end
					if td_title.include?("habilidades")
							hay_idiomas = FINALIZADO
							hay_movilidad = FINALIZADO
							hay_habilidades = ENCONTRADO
					end
					#if td_title.include?("actualizacion") || td_title.include?("actualización")
					#	 hay_movilidad = FINALIZADO
					#end
         	#if hay_desc_funcional == 1
         	#logger.info "TD: #{td.content}".blue

     	end
      ##### FIN PLANILLA RVELASCO
      puts "_______ JOB_ID: #{job_id}".yellow.bold
      job = nil

      DataMapper.repository(:rvelasco) do
        if job_id.blank?
        	# Busco por titulo de cargo
        	job = Job.first(:name.like => "%#{@titulo}%")
					puts "@titulo #{@titulo}"
        	puts "JOB ENCONTRADO CON LIKE: #{job.inspect}".red.bold

        	return {status: false, info: "'#{@titulo}' no existe el registro. Debe existir el cargo para guardar la descripción"} if job.blank?
        else
        	job = Job.get job_id
        end
        @titulo = job.name
    	end

      jobdesc = job.job_description
			jobdesc = JobDescription.create if jobdesc.blank?
    	jobdesc.mission = @descripcion

      # job.main_result
      MainResult.all(job_id: job.id).destroy

      @desc_funcional.each do |i|
  			MainResult.create({
					job_id: job.id,
      		action_function: i[0],
          result: i[1],
          periodicidad: limpia_string(i[2]),
					number: i[3],
          job_description_id: jobdesc.id
				})
      end

      Dimension.all(job_id: job.id).destroy

      [:magnitudes, :recursos].each do |tipo|
      	@dimensiones[tipo].each do |magnitud|
	        magnitud[0] = magnitud[0][0..-2] if magnitud[0][-1] == ":"
	        mag = Magnitude.first_or_create({name: magnitud[0], kind: tipo.eql?(:magnitudes) ? Magnitude::FINANCIERAS : Magnitude::NO_FINANCIERAS})

          Dimension.create({
						magnitude_id: mag.id,
			      value: magnitud[1],
			      job_id: job.id,
          	job_description_id: jobdesc.id
					})
        end
      end

      jobdesc.dicisions_value               = @decisions_recomendations[:decisions]
      jobdesc.recomendations_value          = @decisions_recomendations[:recomendations]
			#jobdesc.aditional_responsability_value= @otras
      jobdesc.internal_contacts_value       = @relaciones[:internas]
      jobdesc.external_contacts_value       = @relaciones[:externas]
			jobdesc.external_situations_value     = @situaciones[:externas]
			jobdesc.internal_situations_value	    = @situaciones[:internas]
			#jobdesc.datos_empresa_anterior_value  = @perfil[:rubro]
			jobdesc.estudio_basico  						  = @perfil[:estudio_basico]
			jobdesc.anios_estudio    							= @perfil[:anios_estudio]
			jobdesc.certificaciones 							= @perfil[:certificacion].to_json
			jobdesc.knowledges_value 							= @perfil[:conocimiento]
			jobdesc.anios_experiencia_cargo				= @perfil[:anios_experiencia_cargo]
			jobdesc.anios_experiencia_laboral		  = @perfil[:anios_experiencia_laboral]
			jobdesc.experience          					= @perfil[:experience]
			jobdesc.idiomas       								= @perfil[:idiomas].to_json
			#jobdesc.frecuencia    								= @perfil[:frecuencia]
      jobdesc.save
			jobdesc.ultima_actualizacion_value = Time.now.strftime("%Y/%m/%d %T")
			jobdesc.movilidad_value     						= @perfil[:movilidad]
			jobdesc.competencias_value           	  = @habilidades

			Rails.logger.info "jobdesc -------> #{jobdesc.inspect}".red
			job.update job_description_id: jobdesc.id

			last_d = nil

    	dinamicas_array.each do |d|

    		unless d[:jd_table_id].blank?
      		value_opt  = {value: d.delete(:value)}
      		search_opt = d.merge({job_description_id: jobdesc.id})

      		unless search_opt[:jd_row_id].blank?
        		search_opt[:jd_row_id] = search_opt[:jd_row_id][:row_id]

        		if search_opt[:jd_column_id].blank?
        			search_opt[:jd_column_id] = last_d[:jd_column_id]
        		end

        		next if search_opt[:jd_column_id].blank?

        		search_opt[:jd_column_id] = search_opt[:jd_column_id][:col_id]
        		Rails.logger.info "DINAMICAS DATA SEARCH: #{search_opt} VALUE: #{value_opt}".magenta
        		JdTableData.update_or_create( search_opt, value_opt )
        		last_d = d
        	end

      	end

    	end

      return {
      	status: job.save,
      	descripcion: @descripcion,
      	desc_funcional: @desc_funcional.blank? ? [] : @desc_funcional,
      	decisions_recomendations: @decisions_recomendations,
      	relaciones: @relaciones,
        perfil: @perfil,
        dimensiones: @dimensiones,
				situaciones: @situaciones,
        titulo_cargo: @titulo,
        info: "'#{@titulo}' actualizado correctamente",
        job_id: job.id,
        tables: @tables,
        dinamicas: dinamicas_array
	    }
		rescue
			puts $!.inspect, $@
			return {status: false, info: "'#{@titulo_cargo}' no pudo ser actualizado, el formato de la descripción es incorrecto"}
		end
	end

	def self.convert_to_html input_file, output_file = 'tmp'
	  output_file_path = "#{Rails.root}/public/convert/converts/#{output_file}.html"
	  puts "convert_to_html(input_file: #{input_file}, output_file: #{output_file_path}".red.bold
	  command = "java -jar #{Rails.root}/lib/jodconverter-2.2.2/lib/jodconverter-cli-2.2.2.jar '#{input_file}' '#{output_file_path}'"
	  puts `#{command}`
	  IO.read output_file_path
	end

  def self.limpia_string str
      str.gsub("\n", "").squish
  end

  def self.is_title? str
  	!(limpia_string(str) =~ /\d+\./).blank?
  end

end
