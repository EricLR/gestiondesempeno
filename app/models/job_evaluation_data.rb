# -*- encoding : utf-8 -*-
class JobEvaluationData < Historiable
    include DataMapper::Resource
    include DataMapper::Timestamp

    KH_TECNICO              = 1
    KH_GERENCIA             = 2
    KH_RELACIONES_HUMANAS   = 3
    SP_AMBITO               = 4
    SP_DESAFIO              = 5
    AC_LIBERTAD             = 6
    AC_MAGNITUD             = 7
    AC_IMPACTO              = 8

    property :id,       Serial
    property :kind,     Integer
    property :name,     String

    belongs_to :job_evaluation_data_description, required: false
    belongs_to :management_type, required: false

    timestamps :at


    def indicators_list management_id
        indicators = JobEvaluationData.all(management_type_id: management_id)
        texts=""
        indicators.each do |ind|
            texts << "#{ind.name} , "
        end
        texts
    end
end
