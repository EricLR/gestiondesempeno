# -*- encoding : utf-8 -*-
class DotacionDato
	include DataMapper::Resource
    include DataMapper::Timestamp
    
    MESES 			 = [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", 
	                	 "Julio", "Agosto", "Septiembre", "Octubre", 
	                	 "Noviembre", "Diciembre"]
	# MESES            = ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic']
    MESES_CORTO      = ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic']
    MESES_DIFERENCIA = ["ene_plan", "ene_real", "ene_diff", "feb_plan", "feb_real", "feb_diff", "mar_plan", "mar_real", "mar_diff", "abr_plan", "abr_real", "abr_diff", "may_plan", "may_real", "may_diff", "jun_plan", "jun_real", "jun_diff", "jul_plan", "jul_real", "jul_diff", "ago_plan", "ago_real", "ago_diff", "sep_plan", "sep_real", "sep_diff", "oct_plan", "oct_real", "oct_diff", "nov_plan", "nov_real", "nov_diff", "dic_plan", "dic_real", "dic_diff"]
    
    PLAN     = 0
    PLANILLA = 1
    
    TIPOS = [:PLAN, :PLANILLA]
    
    property :id,               Serial
    property :posicion,			Integer
    property :datos,			Text, lazy: false
    property :tipo,				Integer, default: PLAN
    
    timestamps :at
    
    def self.plan
    	DotacionDato.all(fields: [:datos, :updated_at], order: :posicion.asc, tipo: PLAN)
    end
    
    def self.planilla
    	DotacionDato.all(fields: [:datos, :updated_at], order: :posicion.asc, tipo: PLANILLA)
    end
    
    def self.calcular_dotacion_real
    	@data = []
		
		@items     = Job.all(:status => Job::ENABLED)
		@positions = Position.all job_id: @items.map(&:id)
		#meses     = {ene: 0, feb: 0, mar: 0, abr: 0, may: 0, jun: 0, jul: 0, ago: 0, sep: 0, oct: 0, nov: 0, dic: 0, total: 0}
		meses      = [:ene, :feb, :mar, :abr, :may, :jun, :jul, :ago, :sep, :oct, :nov, :dic]
		
		@items.each do |job|
			item = {
				job: {
					id: job.id, 
					name: "#{"&nbsp;&nbsp;&nbsp;"*job.ancestors.count}#{job.name}",
					jornada: job.jornada
				}
			}
			position_count = Position.select{|x| x.job_id == job.id}.count
			total          = 0
			meses.each do |m|
				item[m] = position_count
				total += position_count
			end
			item[:total] = total
			
			@data << item
		end
		
		@data
    end
    
    def self.meses_plan
    	MESES_DIFERENCIA.select{|x| x.include?('_plan')}
    end
    
    def self.meses_real
    	MESES_DIFERENCIA.select{|x| x.include?('_real')}
    end
    
    def self.meses_diff
    	MESES_DIFERENCIA.select{|x| x.include?('_diff')}
    end
end
