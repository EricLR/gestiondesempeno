# -*- encoding : utf-8 -*-
class Pais
	include DataMapper::Resource
  	include DataMapper::Validate

  	property :id,           Serial
  	property :name,         String, unique: true

  	timestamps :at

  	#has n, :users
	#has n, :jobs
end
