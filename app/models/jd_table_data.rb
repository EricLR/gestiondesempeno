# -*- encoding : utf-8 -*-
class JdTableData
    include DataMapper::Resource
    include DataMapper::Timestamp
    
    def self.repository(name = nil, &block); super(:default, &block); end
	def self.default_repository_name; :default; end
    
    storage_names[:default] = 'jd_table_data'
    
    property :id,               Serial
    property :value,			Text
    
    belongs_to :jd_table
    belongs_to :jd_row
    belongs_to :jd_column
    belongs_to :job_description
    
    def self.all_data job_description_id, table_data = nil
    	table_data = JdTable::all_tables if table_data.nil?
    	data       = {}  
    	t_data     = all job_description_id: job_description_id
    	
    	t_data.each do |td|
    		i_0, i_1, i_2 = td.jd_table_id, td.jd_column_id, td.jd_row_id
    		
    		data[i_0]           = {} if data[i_0].blank?
    		data[i_0][i_1]      = {} if data[i_0][i_1].blank?
            data[i_0][i_1][i_2] = {} if data[i_0][i_1][i_2].blank?
    		
    		data[i_0][i_1][i_2] = td.value
    		
    		
    	end
    	
    	data
    end
    
end
