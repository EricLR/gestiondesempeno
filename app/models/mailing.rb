# -*- encoding : utf-8 -*-
class Mailing
    include DataMapper::Resource
    include DataMapper::Timestamp

    property :id,           Serial
    property :subject,      String
    property :body,         Text

    property :user_id,      Integer

    timestamps :at

    has n, :mailing_user
    has n, :mailing_sendgrid_category
    
    def category_name
        "#{id}-#{created_at.strftime("%m%d%Y")}"
    end
end
