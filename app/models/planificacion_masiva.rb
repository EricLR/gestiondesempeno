# -*- encoding : utf-8 -*-
class PlanificacionMasiva
	include DataMapper::Resource
    include DataMapper::Timestamp
    
    property :id,                   Serial
    
    timestamps :at
end
