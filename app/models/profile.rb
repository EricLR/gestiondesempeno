# -*- encoding : utf-8 -*-
class Profile < Historiable
  include DataMapper::Resource
  include DataMapper::Timestamp
  def self.repository(name = nil, &block)
    super(:rvelasco, &block)
  end
  def self.default_repository_name
    :rvelasco
  end
  property :id,       Serial
  property :name,     String
  property :acronim,  String
  timestamps :at

  has 1, :job
end
