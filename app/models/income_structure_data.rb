# -*- encoding : utf-8 -*-
class IncomeStructureData
    include DataMapper::Resource
    include DataMapper::Timestamp

    MENSUAL    = 1
    SEMESTRAL  = 2
    TRIMESTRAL = 4
    ANUAL      = 12

    PERIDIOCIDAD = {
        MENSUAL    => MENSUAL,
        SEMESTRAL  => SEMESTRAL,
        TRIMESTRAL => TRIMESTRAL,
        ANUAL      => ANUAL
    }
    PERIODOS = {
        MENSUAL    => :MENSUAL,
        SEMESTRAL  => :SEMESTRAL,
        TRIMESTRAL => :TRIMESTRAL,
        ANUAL      => :ANUAL
    }
    # def self.default_repository_name
    #   :rvelasco
    # end
    DETAIL_EXCEPTION = [" (Resultado)", " (Original)", '(hsem_val)', '(normalizado)']

    property :id,                   Serial
    property :value,                String
    property :detail,               Json
    property :periodicity,          Integer
    property :periodicity_detail,   Json

    belongs_to :income_structure
    belongs_to :position

    timestamps :at

    def insertar
        asd = IncomeStructureData.create(position_id: self.position_id)
        asd.value = self.value
        asd.detail = self.detail
        asd.income_structure_id = self.income_structure_id
        asd.periodicity_detail = self.periodicity_detail
        asd.save!

        if asd.save
        else
          asd.errors.each do |e|
              puts "=====================================".yellow.bold
              puts e.inspect.red.bold
              puts "=====================================".yellow.bold
          end
        end
        puts asd.inspect.red.bold
    end

    def self.ids_income_structure
        ids = IncomeStructureData.all(fields: [:income_structure_id]).map(&:income_structure_id)#, unique: true).map(&:income_structure_id)
    end

    def self.exception? k
        DETAIL_EXCEPTION.each do |de|
            return false if k.include?(de)
        end
        true
    end
end
