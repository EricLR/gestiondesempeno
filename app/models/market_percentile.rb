# -*- encoding : utf-8 -*-
class MarketPercentile
  include DataMapper::Resource
  include DataMapper::Timestamp
  
  property :id,       Serial
  property :name,     String
  property :position, Integer, default: 1
  
  timestamps :at

  has n, :market_percentile_data, MarketPercentileData


end
