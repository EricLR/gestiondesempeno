# -*- encoding : utf-8 -*-
class StringList < Historiable

  def get_list(column)
    self[column].blank? ? [] : CSV.parse_line(self[column], quote_char: "'").map{|x| URI.unescape(x)}

  end

  def set_list(column, values)
    if values == nil
      self[column] = nil 
    else
      self[column] = StringList::parse_list(values)
    end
  end
  
  def self.parse_list values
    str = ""
    values.each_with_index do |v, k|
      str += "," unless k == 0
      str += "'#{ERB::Util.url_encode v}'"
    end
    str
  end
  def parse_list values
    str = ""
    values.each_with_index do |v, k|
      str += "," unless k == 0
      str += "'#{ERB::Util.url_encode v}'"
    end
    str
  end
end
