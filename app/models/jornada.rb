# -*- encoding : utf-8 -*-
class Jornada
  include DataMapper::Resource

  # property <name>, <type>
  property :id, Serial
  property :name, String

  #has n, :jobs
end
