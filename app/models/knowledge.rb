# -*- encoding : utf-8 -*-
class Knowledge
  include DataMapper::Resource
  include DataMapper::Timestamp

  property :id,         Serial
  property :name,       String 
  property :enabled,    Boolean, default: true 
  timestamps :at
end
