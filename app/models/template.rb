# -*- encoding : utf-8 -*-
class Template
  include DataMapper::Resource
  include DataMapper::Timestamp
  
  property :id,       Serial
  property :name,     String
  property :data,     Text
  
  timestamps :at
end
