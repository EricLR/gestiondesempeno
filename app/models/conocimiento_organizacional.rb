# -*- encoding : utf-8 -*-
class ConocimientoOrganizacional

  include DataMapper::Resource

  property :id, Serial
  property :name, String

end
