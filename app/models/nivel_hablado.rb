# -*- encoding : utf-8 -*-
class NivelHablado

  include DataMapper::Resource

  property :id, Serial
  property :name, String

end
