# -*- encoding : utf-8 -*-
class Area < Historiable

  include DataMapper::Resource

  property :id, Serial
  property :name, Text

	if Rails.configuration.competencias == :saville
    has n, :competence_type
  end

end
