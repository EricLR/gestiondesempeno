# -*- encoding : utf-8 -*-
class UserGroup
	include DataMapper::Resource
 	include DataMapper::Timestamp

 	property :id,		Serial
 	property :name,		String
 	timestamps :at

 	#has n, :users

end
