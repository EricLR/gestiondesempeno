# -*- encoding : utf-8 -*-
class Estamento
  include DataMapper::Resource
  include DataMapper::Validate

  property :id,               Serial
  property :name,             String

  #has n, :jobs
end
