# -*- encoding : utf-8 -*-
class ManagementType
  include DataMapper::Resource
  def self.repository(name = nil, &block)
    super(:rvelasco, &block)
  end
  def self.default_repository_name
    :rvelasco
  end

  def self.repository(name = nil, &block)
      super(:rvelasco, &block)
  end

  property :id, Serial
  property :name, String

  timestamps :at

  has n, :jobs
  has n, :action_verbs
  has n, :result_verbs
end
