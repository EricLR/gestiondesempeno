# -*- encoding : utf-8 -*-
class CentroCosto
	include DataMapper::Resource
  	include DataMapper::Validate

  	property :id,           Serial
  	property :name,         String, unique: true

  	#has n, :jobs
end
