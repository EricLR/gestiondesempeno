# -*- encoding : utf-8 -*-
class EquidadPunto
    include DataMapper::Resource
    include DataMapper::Timestamp

    PROMEDIO           = 0
    MEDIANA            = 1
    LINEAL             = 2
    LOGARITMICA        = 3
    POLINOMIAL         = 4
    ECUACIONES         = [:PROMEDIO, :MEDIANA, :LINEAL, :LOGARITMICA, :POLINOMIAL, :EXPONENTIAL]
    ECUACIONES_PARAMS  = [:PROMEDIO, :MEDIANA, :LINEAR, :LOGARITHMIC, :POLYNOMIAL, :EXPONENTIAL]
    ECUACIONES_NOMBRES = ['Promedio', 'Mediana', 'Lineal', 'Logarítmica', 'Polinomial', 'Exponencial']

    DATOS          = 0
    DATOS_INFERIOR = 1
    DATOS_SUPERIOR = 2
    DATOS_LINEA    = 3
    TIPOS_DATOS    = [:DATOS, :DATOS_INFERIOR, :DATOS_SUPERIOR, :DATOS_LINEA]

    property :id,               Serial
    property :ecuacion,         Integer
    property :x,                Float, default: 0.0
    property :y,                Float, default: 0.0
    property :tipo,             Integer, default: DATOS



    belongs_to :income_structure

    timestamps :at

    def self.get_ecuacion name
        name = name.upcase.to_sym
        ECUACIONES_PARAMS.each_with_index do |e, i|
            return i if e.eql?(name)
        end

        return nil
    end
    def self.ecuacion_param ecuacion
        ecuacion.blank? ? nil : ECUACIONES_PARAMS[ecuacion].to_s.downcase
    end
    def self.param_to_name
        tmp = {}
        ECUACIONES_PARAMS.each_with_index do |i, x|
            tmp[i.to_s.downcase] = ECUACIONES_NOMBRES[x]
        end
        tmp
    end

    def self.calcula_resumen income_structure_id, ecuacion, data = nil, all_data = nil
        base_opt = {income_structure_id: income_structure_id, ecuacion: ecuacion, fields: [:x, :y, :income_structure_id, :tipo, :ecuacion]}#, unique: true}
        if all_data.blank?
            datos    = data.blank?||!data.has_key?(:datos) ? (EquidadPunto.all base_opt.merge({tipo: DATOS})) : data[:datos]
            # Cache u optimizar carga de DATOS_INFERIOR y DATOS_SUPERIOR1
            inferior = data.blank?||!data.has_key?(:inferior) ? (EquidadPunto.all base_opt.merge({tipo: DATOS_INFERIOR})) : data[:inferior]
            superior = data.blank?||!data.has_key?(:superior) ? (EquidadPunto.all base_opt.merge({tipo: DATOS_SUPERIOR})) : data[:superior]
            linea    = data.blank?||!data.has_key?(:linea) ? (EquidadPunto.all base_opt.merge({tipo: DATOS_LINEA})) : data[:linea]
        else # all_data tiene contenido
            # Completed 200 OK in 179645ms (Views: 0.6ms | Models: 851.145ms)
            # Completed 200 OK in 120635ms (Views: 0.6ms | Models: 2302.543ms)
            # Completed 200 OK in 107084ms (Views: 2.5ms | Models: 694.724ms)
            busqueda = lambda{|tipo|
                all_data[tipo].select{ |y|
                    y[:ecuacion] == base_opt[:ecuacion]
                }
            }
            datos = data.blank?||!data.has_key?(:datos) ? busqueda.(DATOS) : data[:datos]
            inferior = data.blank?||!data.has_key?(:inferior) ? (busqueda.(DATOS_INFERIOR)) : data[:inferior]
            superior = data.blank?||!data.has_key?(:superior) ? (busqueda.(DATOS_SUPERIOR)) : data[:superior]
            linea    = data.blank?||!data.has_key?(:linea) ? (busqueda.(DATOS_LINEA)) : data[:linea]
        end

        items    = {rango: 0, sobrepagados: 0, subpagados: 0}
        niveles  = datos.map{|x| x.x.to_i}.uniq.sort

        items[:subpagados]   += datos.select{|d| inferior.select{|i| i[:x] == d[:x] && d[:y] < i[:y] }.length > 0}.length
        items[:sobrepagados] += datos.select{|d| superior.select{|i| i[:x] == d[:x] && d[:y] > i[:y] }.length > 0}.length
        items[:rango]        += (datos.select do |d|
                                    (inferior.select do |i|
                                        r_sup = superior.select{|s| s[:x] == d[:x] && s[:y] >= d[:y]}

                                        r_sup.blank? ? false : (i[:x] == d[:x] && d[:y] >= i[:y])
                                    end).length > 0
                                end).length

        # niveles.times do |i|
        #     items[:subpagados] += datos.select{|x| }
        # end

        items[:total]          = items.map(&:second).sum
        if items[:total] > 0
            items[:subpagados_p]   = ((items[:subpagados]*100.0)/items[:total]).round
            items[:sobrepagados_p] = ((items[:sobrepagados]*100.0)/items[:total]).round
            items[:rango_p]        = ((items[:rango]*100.0)/items[:total]).round
        end

        items
    end

end
