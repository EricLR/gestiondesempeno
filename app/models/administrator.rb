class Administrator
  include DataMapper::Resource
  include DataMapper::Timestamp

  def self.default_repository_name
    :rvelasco
  end

  #Properties
  property :id, Serial, accessor: :private
  property :rut, String
  property :first_name, String
  property :last_name, String
  property :email, String
  property :password, String
  property :first_time, Boolean
  property :role, String

  timestamps :at

  #Accesors

  def rut=(new_rut)
    super new_rut.gsub(".", "")
  end

  def password=(new_pass)
    super md5Hash(new_pass)
  end

  #Public methods

  def self.repository(name = nil, &block)
    super(:rvelasco, &block)
  end

  def self.login rut, password
    user = first(rut: rut)
    (user && user[:password] == Digest::MD5.hexdigest(password)) ? user : false
  end

  def name_last
    nombre = first_name.split
    apellido = last_name.split
    "#{nombre[0] }"+" "+"#{apellido[0]}"
  end

  #Private methods
  private

  def md5Hash value
    Digest::MD5.hexdigest(value)
  end
end
