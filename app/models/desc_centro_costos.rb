# -*- encoding : utf-8 -*-
class DescCentroCostos
	include DataMapper::Resource
  	include DataMapper::Validate

  	property :id,           Serial
  	property :name,         String, unique: true

  	#has n, :jobs
end
