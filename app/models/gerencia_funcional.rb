# -*- encoding : utf-8 -*-
class GerenciaFuncional
	include DataMapper::Resource
  	include DataMapper::Validate

  	property :id,           Serial
  	property :name,         String, unique: true

  	timestamps :at


	#has n, :jobs

end
