# -*- encoding : utf-8 -*-
class JobType < Historiable
  include DataMapper::Resource
  include DataMapper::Timestamp

  property :id,       Serial
  property :name,     String

  timestamps :at

  #has 1, :job
  #has n, :action_verb
  #has n, :result_verb
end
