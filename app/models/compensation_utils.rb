# -*- encoding : utf-8 -*-
class CompensationUtils
    def self.masa_salarial users_opts = {}
        #job_opts = {:nivel.gte => 0}
        job_opts = {}
        unless users_opts[:params].blank?
            params = users_opts.delete(:params)
            #puts "MASA SALARIAL PARAMS: #{params}".red
            # user_opts = params.reject { |k, x| ['filtro', 'action', 'controller'].include?(k)}

            users_opts[:gender]     = params[:gender] unless params[:gender].blank?
            users_opts[:ceco]    = params[:ceco] unless params[:ceco].blank?
            users_opts[:subceco] = params[:subceco] unless params[:subceco].blank?
            users_opts[:grado]   = params[:grado] unless params[:grado].blank?

            job_opts[:job_class]       = params[:job_class] unless params[:job_class].blank?
            job_opts[:level]         = params[:level] unless params[:level].blank?
            job_opts[:dependency_type] = params[:dependency_type] unless params[:dependency_type].blank?
            job_opts[:name]            = params[:job] unless params[:job].blank?
        end

        @incomes_cols                      = []
        @incomes                           = IncomeStructure.all order: :position.asc
        @data                              = IncomeStructureData.all income_structure_id: @incomes.map(&:id), fields: [:id, :detail, :periodicity_detail, :income_structure_id, :periodicity, :value, :position_id]
        @positions                         = Position.all id: @data.map(&:position_id)
        #puts @positions.inspect.red.bold
        #puts job_opts.merge(id: @positions.map(&:job_id)).inspect.green.bold
        users_opts[:has_compensation_data] = true
        users_opts.delete(:params)

        @users         = User.all( users_opts )
        @jobs          = Job.all job_opts.merge(id: @positions.map(&:job_id))
        @incomes_show  = []
        @masa_salarial = []




        @users.each do |user|
            position = @positions.select{|x| x.user_id == user.id}.first
            job      = @jobs.select{|x| x.id == position.job_id}.first
            next if job.blank?||!job.status.eql?(Job::ENABLED)
            # next unless job.nivel > 0
            # next if !job_opts[:name].blank?&&job.name.eql?(job_opts[:name])
            data     = @data.select{|x| x.position_id == position.id}
            counter  = 0.0
            tmp      = {user: user, job: job, incomes: []}

            @incomes.each_with_index do |income, ik|
                detail_names =[]
                detail = []
                i_d = data.select{|x| x.income_structure_id == income.id}
                # logger.info "DATA I_D: #{i_d.inspect} SHOW?: #{!@incomes_show.include?(income.id)} VALUE: #{!i_d.select{|x| x.value.to_i <= 0}.blank?} IDS: #{i_d.inspect}".bold.yellow
                i_d.each do |idd|
                    #puts idd.inspect.green.bold
                    detail_names << idd.detail.map{|k,v| k}
                    detail << idd.periodicity_detail
                end
                if !@incomes_show.include?(income.id) && !i_d.select{|x| x.value.to_f >= 0.0}.blank?
                    @incomes_show << income.id
                    #puts income.id
                    #puts detail_names.inspect.red.bold
                    #puts detail.inspect.green.bold
                    @incomes_cols << {id: income.id, income: income, detail_names: detail_names, detail: detail}
                end
                #puts "============================ #{@incomes_cols.inspect}".green.bold
                #counter += i_d.first.value.to_f unless i_d.blank?
                i_d.each do |idd|
                    counter += idd.value.to_f
                    puts idd.inspect.red.bold
                end

                puts counter.inspect.blue.bold
                tmp[:incomes][income.id] = {} if tmp[:incomes][income.id].blank?

                tmp[:incomes][income.id][:detalle]      = i_d
                tmp[:incomes][income.id][:counter]      = counter
                tmp[:incomes][income.id][:sumatoria]    = i_d.blank? ? 0.0 : i_d.first.value.to_f
                tmp[:incomes][income.id][:income_id]    = income.id
                _detalle_tmp = {}
                _ftm = @incomes_cols.find{|x| x[:id] == income.id}
                _ftm[:detail_names].each{|x| _detalle_tmp[x] = 0} unless _ftm.blank?

                tmp[:incomes][income.id][:detalle_cols] = i_d.blank? ? [] : _detalle_tmp.merge(i_d.first.detail)#.select{|k, v| k.include?(detail_names)}
            end

            @masa_salarial << tmp
        end

        return @incomes, @incomes_show, @masa_salarial, @users, @data, @incomes_cols
    end
end
