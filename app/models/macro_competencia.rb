class MacroCompetencia < Historiable

  include DataMapper::Resource

  property :id, Serial
  property :name, String

  has n, :competencias
  belongs_to :area

end
