# -*- encoding : utf-8 -*-
class MainResult < Historiable
  include DataMapper::Resource
  include DataMapper::Timestamp
  
  PERIDIOCIDADES = ['Diario', 'Semanal', 'Mensual', 'Trimestral', 'Semestral', 'Anual', 'Periódica']
  
  if Rails.configuration.cliente == :disal
  	PERIDIOCIDADES << 'Plan Anual'
  	PERIDIOCIDADES << 'De acuerdo a Plan estratégico anual, y carta Gantt'
  end
  
  property :id,               Serial
  property :action_function,  Text
  property :result,           Text
  property :periodicidad,     Text
  property :number, 		  Integer, default: 99
  
  belongs_to :job, required: false
  belongs_to :job_description, required: false
end
