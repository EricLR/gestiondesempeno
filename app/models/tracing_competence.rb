# -*- encoding : utf-8 -*-
class TracingCompetence < Historiable
	include DataMapper::Resource
	include DataMapper::Timestamp

    SEGUIMIENTO = 'seguimiento'
    EVALUACION  = 'evaluacion'

    NO_CUMPLE   = 0
    CUMPLE      = 1
    EXCEDE      = 2

    INCREASE_VALUES = [0, 100, 150]

	property :id,           Serial

	property :trace_date,	Date
	property :increase,		Integer
	property :kind,			String
  property :conduct_id,   Integer
	property :aprobe , Integer , :default => -1
	property :closed,		Boolean, :default => false

	timestamps :at

	belongs_to :evaluator, 'User', :required => false
	belongs_to :evaluated, 'User', :required => false
	belongs_to :planification_competence
	belongs_to :period
end
