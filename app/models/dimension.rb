# -*- encoding : utf-8 -*-
class Dimension < Historiable
  include DataMapper::Resource
  include DataMapper::Timestamp

  property :id,           Serial
  property :value,        String

  belongs_to :job, required: false
  belongs_to :job_description, required: false
  belongs_to :magnitude

  timestamps :at
end
