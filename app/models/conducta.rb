# -*- encoding : utf-8 -*-
class Conducta < Historiable




  include DataMapper::Resource

  property :id, Serial
  property :name, String

  belongs_to :competencia
end
