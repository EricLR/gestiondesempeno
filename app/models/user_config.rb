# -*- encoding : utf-8 -*-
class UserConfig
    include DataMapper::Resource

    def self.default_repository_name
        :rvelasco 
    end
    

    property :id,       Serial
    property :name,     String, length: 255
    property :value,    Text

    belongs_to :user, required: false

    def self.config name, user_id = nil

        DataMapper.repository(:rvelasco) do
            uc = UserConfig.first name: name.to_s, user_id: user_id
            return uc.blank? ? nil : uc.value
        end
        
        return nil
    end

    def self.set_config name, value, user_id = nil
        DataMapper.repository(:rvelasco) do
            cfg = UserConfig.first_or_create name: name.to_s, user_id: user_id
            cfg.value = value
            cfg.save
        end
        
        return value
    end
    
    def self.clean_config name
        DataMapper.repository(:rvelasco) do
            UserConfig.all(:name.like => name).each{|x| x.destroy}
        end
        
    end
end
