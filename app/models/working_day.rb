# -*- encoding : utf-8 -*-
class WorkingDay
  include DataMapper::Resource

  property :id,   Serial
  property :name, String

  timestamps :at

  has n, :users
end
