# -*- encoding : utf-8 -*-
class JobEvaluationDataDescription < Historiable
    include DataMapper::Resource
    include DataMapper::Timestamp
    
    property :id,       Serial
    property :name,     Text

    has n, :job_evaluation_datas

    timestamps :at
end
