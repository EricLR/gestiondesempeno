# -*- encoding : utf-8 -*-
class DescriptionWorkflow
  include DataMapper::Resource
  include DataMapper::Timestamp

  NUEVO      = 0
  ASIGNADO   = 1
  REVISION   = 2
  CORRECCION = 3
  APROBACION = 4

  ESTADOS = ['Nuevo', 'Asignado', 'Revisión', 'Corrección', 'Aprobado']

  ADMINISTRADOR = 0
  ANALISTA      = 1
  JEFATURA      = 2

  ROLES = ['Administrador', 'Analista', 'Jefatura']

  property :id, Serial
  property :status, Integer
  timestamps :at

  belongs_to :user, required: false
  #belongs_to :user, 'User', parent_key: [ :id ], child_key: [ :analista_id ], required: true
  belongs_to :analista, model: 'User', foreign_key: 'analista_id', required: true
  belongs_to :jefe, model: 'User', foreign_key: 'jefe_id', required: false
  belongs_to :job_description, required: false
  belongs_to :job, required: false
  has n, :comments

  def self.nombre_analista job
    User.first(id: job.job_description.description_workflow.analista_id).full_name
  end

  def self.exist_status(id)
    if dw = self.first(job_id: id)
      return dw.status == REVISION
    else
      return false
    end
  end

  def self.status_action(id, rol)
    action = ''
    if dw = self.first(job_id: id)
      case dw.status
      when 1
        action = %{<a id='informar' onclick='informar_jefes(this, 2);'>Informar Jefes</a>} if rol.eql?('Analista')
      when 2
        action = %{<a id='informar' onclick='informar_jefes(this, 3);'>Corrección</a>} if rol.eql?('Analista')
      when 3
         action = rol.eql?('Analista') ? %{Esperando aprobación} : %{<a id='informar' onclick='informar_jefes(this, 4);'>Aprobar</a>}
     when 4
        action = %{Aprobada}
      end
    end
    action.html_safe
  end
end
