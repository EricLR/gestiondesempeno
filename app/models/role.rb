# -*- encoding : utf-8 -*-
class Role < StringList
    include DataMapper::Resource
    include DataMapper::Timestamp

    def self.repository(name = nil, &block)
        super(:rvelasco, &block)
    end

    PERMISOS = {
        default:                       'default',
        # organizacion:  {
        #     org_cargos:       'Organigrama de Cargos',
        #     org_puestos:      'Organigrama de Puestos',
        #     org_mantenedores: 'Menú: Mantenedores',
        #     org_admin:        'Menú: Administrador'
        #     #org_mant_cargos_edicion: 'Edición de Cargos',
        #     #org_mant_puestos_edicion: 'Edición de Puestos',
        #     #org_carga_datos: 'Administrador: Carga de datos',
        #     #org_roles: 'Menú: Administrador > Roles',
        #     #org_permisos: 'Menú: Administrador > Permisos (Asignar rol a usuario)',
        #     #org_limpieza: 'Administrador > Limpiar base de datos'
        # },
        descripcion_cargos: {
            dc_menu_lista_descripciones: 'Menú: Lista de Descripciones',
            dc_menu_carga_masiva:        'Menú: Carga Masiva',
            dc_menu_mantenedores:        'Menú: Mantenedores',
            dc_lista_editar:             'Lista: Editar',
            dc_lista_cargar:             'Lista: Cargar Word',
            dc_ver_org_propio:           'Sólo ver Cargo propio',
            dc_editar_org_propio:        'Editar Cargo propio',
            dc_editar_org_sub:           'Editar Cargos Subordinados',
            dc_administrador:            'Administrar descripciones',
            dc_analista:                 'Analista de descripciones'
        },
        compensacion: {
            cp_captura_xls:             'Carga de datos y generación de estudios',
            cp_masa_salarial:           'Masa Salarial',
            cp_equidad_interna:         'Equidad Interna',
            cp_equidad_interna_guardar: 'Equidad Interna: Guardar',
            cp_competitividad:          'Competitividad',
            cp_calculo_personal:        'Calculo Personal',
            cp_mantenedores:            'Mantenedores',
            cp_mantenedores_est_comp:   'Mantenedores > Estructuras Compensación',
            cp_mantenedores_perc_merc:  'Mantenedores > Percentiles de Mercado',
            cp_mantenedores_datos_mcdo: 'Mantenedores > Datos de Mercado',
        },
        dotacion: {
            do_dotacion_real:     'Dotación Real',
            do_plan_dotacion:     'Planificación de la dotación',
            do_carga_dotacion:    'Carga de dotación',
            do_plan_dotacion_dif: 'Plan de dotación: Diferencias'
        },
        gestion_desempeno: {
            gd_informes: 'Informes'
        },
        mantenedores: {
            m_menu_gestion_desemeno: 'Menú: Gestión del Desempeño',
            m_menu_sistema_es:       'Menú: Sistema'
        }
    }

    property :id,                   Serial
    property :name,                 String
    property :organizacion,         Boolean, :default => false
    property :gestion_desempeno,    Boolean, :default => false
    property :descripcion_cargos,   Boolean, :default => false
    property :evaluacion_cargos,    Boolean, :default => false
    property :adm_compensacion,     Boolean, :default => false
    property :plan_dotacion,        Boolean, :default => false
    property :adm_presupuesto,      Boolean, :default => false
    property :mantenedores,         Boolean, :default => false
    property :permissions,          String, length: 1000

    timestamps :at
    # has n, :jobs
    # has n, :users
    has n, :accounts

    def can? permission = :default
        get_list( :permissions ).include? permission.to_s
    end

    def can= permission
        set_list :permissions, ( get_list( :permissions ) << permission.to_s).uniq
    end

    def self.parse_permissions value
        parse_list value.map{|k, v| k if v == "1"}.compact
    end

    def self.with_permission permission
        all :permissions.like => "%'#{permission.to_s}'%"
    end

end
