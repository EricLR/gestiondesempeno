# -*- encoding : utf-8 -*-
class Company
	include DataMapper::Resource
	include DataMapper::Validate

  def self.repository(name = nil, &block)
    super(:rvelasco, &block)
  end
  def self.default_repository_name
    :rvelasco
  end

	property :id,        Serial
	property :name,      String
	property :color,     String
  property :logo,     String
  property :banner,     String

	#mount_uploader :logo, ImageUploader
	#mount_uploader :banner, ImageUploader

	timestamps :at

	has n, :branch_offices
	belongs_to :country
end
