# -*- encoding : utf-8 -*-
class MailingUser
    include DataMapper::Resource
    include DataMapper::Timestamp

    property :id,           Serial
    property :sent,         Boolean, default: false
    property :readed,       Boolean, default: false
    property :clicked,      Boolean, default: false
    property :web_view,     Integer, default: 0


    timestamps :at

    belongs_to :mailing
    belongs_to :user
end
