$.fn.scrollTo = function( target, options, callback ){
  if(typeof options == 'function' && arguments.length == 2){ callback = options; options = target; }
  var settings = $.extend({
    scrollTarget  : target,
    offsetTop     : 50,
    duration      : 500,
    easing        : 'swing'
  }, options);
  return this.each(function(){
    var scrollPane = $(this);
    var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
    var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
    scrollPane.animate({scrollTop : scrollY }, parseInt(settings.duration), settings.easing, function(){
      if (typeof callback == 'function') { callback.call(this); }
    });
  });
}
var Qurl
  ;

(function () {
  'use strict';

  var proto
    ;

  Qurl = function () {
    if ( !(this instanceof Qurl) ) {
      return new Qurl();
    }
  }

  proto = Qurl.prototype;

  proto.query = function (key,value) {
    if ( !key ) {
      return getSearch();
    }

    if ( key && typeof(value) === 'undefined' ) {
      return getSearch()[key];
    }

    if ( key && typeof(value) !== 'undefined' ) {
      return setSearch(key,value);
    }
  };

  function getSearch() {
    var string = window.location.search
      , pairs = []
      , obj = {}
      ;

    if ( !string ) {
      return obj;
    }
    string = string.replace('?','');

    pairs = string.split('&');
    pairs.forEach(function (p) {
      var pair = decodeURIComponent(p).split('=')
        , key = pair[0]
        , val = pair[1]
        ;

      obj[key] = val;
    });

    return obj;
  }

  function setSearch(key,value) {
    var search = getSearch()
      , string = window.location.search
      ;

    if ( value === false ) {
      delete search[key];
    }
    else {
      search[key] = value;
    }

    setSearchString(getSearchString(search));

    return search;
  }

  function setSearchString(string) {
    if ( history.pushState ) {
      history.pushState({}, document.title, string);
    }
  }

  function getSearchString(query) {
    var pairs = []
      ;

    Object.keys(query).forEach(function (key) {
      if ( typeof query[key] === 'undefined' ) {
        pairs.push(key);
      } else {
        pairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(query[key] || ''));
      }
    });

    if ( pairs.length === 0 ) {
      return '?';
    }
    else {
      return '?' + pairs.join('&');
    }
  }

  Qurl.create = Qurl;
}());
function omitirAcentos(text) {
    var acentos = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóóöôùúüûÑñÇç";
    var original = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc";
    for (var i=0; i<acentos.length; i++) {
        text = text.replace(acentos.charAt(i), original.charAt(i));
    }
    return text;
}
String.prototype.titleize = function() {
  var words = this.split(' ')
  var array = []
  for (var i=0; i<words.length; ++i) {
    array.push(words[i].charAt(0).toUpperCase() + words[i].toLowerCase().slice(1))
  }
  return array.join(' ')
}
$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
function postForm(url)
{
  str = '<form action="'+url+'" method="POST"></form>'
  return $(str);
}

function array_select(arr, closure){
    for(var n = 0; n < arr.length; n++) {
        if(closure(arr[n])){
            return arr[n];
        }
    }

    return null;
};
function equidad_puntos_nivel(nivel, datos_nivel)
{

  return $.map(
                 $.grep(
                        datos_nivel,
                        function(g){return g[0] == nivel}
                  ),
                  function(m){ return m[1] }
          );
}
function equidad_compara_array(datos_originales, datos_compara, operacion)
{
  if(operacion == undefined) operacion = "superior";

  var counter = 0;

  for(var i in datos_originales)
  {
    counter += $.grep(datos_compara, function(g){

      return operacion == "superior" ? (datos_originales[i] < g) : (datos_originales[i] > g);
    }).length;
  }

  return counter;
}
function calcular_resumen_equidad(income_id, tipo, data)
{
  if(tipo == 'total') return data['datos'].length;

  var counter        = 0;
  var datos          = data['datos'];
  var datos_inferior = data['inferior'];
  var datos_superior = data['superior'];
  var tmp_linea      = null;
  var tmp_dato       = null;
  var k              = null;
  var j              = null;
  var niveles        = $.unique($.map(datos_inferior, function(x){return x[0]}));

  if( tipo == 'inferior' )
  {
    for( kn in niveles )
    {
      tmp_dato  = equidad_puntos_nivel(niveles[kn], datos);
      tmp_linea = $.unique(equidad_puntos_nivel(niveles[kn], datos_inferior));
      counter  += equidad_compara_array(tmp_dato, tmp_linea, tipo)
    }
    // for( k in datos_inferior )
    // {
    //   tmp_linea = datos_inferior[k];

    //   counter += $.grep(datos, function(n){console.log("N:", n, "tmp_linea:", tmp_linea, "coincide:", n[0] == tmp_linea[0] && n[1] < tmp_linea[1], "counter: ", counter); return n[0] == tmp_linea[0] && n[1] < tmp_linea[1]}).length;

    //   // for( var j in datos )
    //   // {
    //   //   tmp_dato = datos[j];
    //   //   console.log("PTO INFERIOR: ", tmp_linea, "PTO DATO: ", tmp_dato, " ES: ", tmp_linea[0] == tmp_dato[0] && tmp_linea[1] > tmp_dato[1], "Counter:", counter);
    //   //   if(tmp_linea[0] == tmp_dato[0] && tmp_linea[1] > tmp_dato[1])
    //   //     counter += 1;
    //   // }
    // }
  }
  if( tipo == 'superior' )
  {
    for( kn in niveles )
    {
      tmp_dato  = equidad_puntos_nivel(niveles[kn], datos);
      tmp_linea = $.unique(equidad_puntos_nivel(niveles[kn], datos_superior));
      counter  += equidad_compara_array(tmp_dato, tmp_linea, tipo)
    }
    // for( k in datos_superior )
    // {
    //   tmp_linea = datos_superior[k];
    //   counter += $.grep(datos, function(n){return n[0] == tmp_linea[0] && n[1] > tmp_linea[1]}).length;
    // }
  }
  if( tipo == 'rango' )
  {

    for( j in datos )
    {
      tmp_dato   = datos[j];
      encontrado = false;

      for( k in datos_inferior )
      {
        tmp_linea = datos_inferior[k];

          if(tmp_linea[0] == tmp_dato[0] && tmp_linea[1] > tmp_dato[1])
            encontrado = true;

      }

      for( k in datos_superior )
      {
        tmp_linea = datos_superior[k];
        if(tmp_linea[0] == tmp_dato[0] && tmp_linea[1] >= tmp_dato[1] && encontrado)
          counter += 1;
      }
    }


  }

  // for( di in (tdata = data['datos']) )
  // {

  //   dtmp = tdata[di];

  //   if(tipo == 'inferior')
  //   {
  //     // console.log("Inferior datos: ", data['inferior'], "DTMP: ", dtmp);
  //     for(dii in (tmp = data['inferior'])) // Recorro puntos de la linea inferior
  //     {

  //       if(tmp[dii][0] == dtmp[0] && tmp[dii][1] > dtmp[1])// Busco punto en el mismo nivel que la persona// && tmp[dii][1] > dtmp[1]
  //       {
  //         counter += 1;
  //       }
  //     }

  //   }
  //   if(tipo == 'superior')
  //   {
  //     for(dii in (tmp = data['superior']))
  //     {
  //       if(tmp[dii][0] == dtmp[0] && tmp[dii][1] < dtmp[1])
  //       {
  //         counter += 1;
  //       }
  //     }
  //   }
  //   if(tipo == 'rango')
  //   {
  //     en_rango = 0;

  //     for(dii in (tmp = data['inferior']))
  //     {
  //       if(tmp[dii][0] == dtmp[0] && tmp[dii][1] >= dtmp[1])
  //         en_rango += 1
  //     }

  //     for(dii in (tmp = data['superior']))
  //     {
  //       if(tmp[dii][0] == dtmp[0] && tmp[dii][1] >= dtmp[1])
  //         en_rango += 1
  //     }

  //     if(en_rango == 2) counter += 1;
  //   }
  // }

  return counter;
}
function tableSorterInit(jq_obj)
{
  jq_obj.tablesorter({
        textExtraction: function(node){
          val = $(node).text();
          if( $(node).is("[normalizado]") )
          {
            val = $(node).attr('normalizado');
            val = parseFloat(val);
          }

          if( $(node).is("[valor]") )
          {
            val = $(node).attr('valor');
            val = parseFloat(val);
          }
          if( $(node).is("[dif-a-min]") )
          {
            val = $(node).attr('dif-a-min');
            val = parseFloat(val);
          }
          if( $(node).is("[ratio-a-min]") )
          {
            val = $(node).attr('ratio-a-min');
            val = parseFloat(val);
          }
          if( $(node).is("[final]") )
          {
            val = $(node).attr('final');
            val = parseFloat(val);
          }
          return val;
        },
        widthFixed: true,
        cssAsc:     "sorting_asc",
        cssDesc:    "sorting_desc",
        theme: 'bootstrap',
        headerTemplate: '{content} {icon}'/*,
        widgets: ['stickyHeaders'],
        widgetOptions: {
            stickyHeaders_offset : 50,
            stickyHeaders_addCaption: true
        }*/

    });
}
function init()
{

  $('.kinetic').kinetic();
  $('select:not(.flexbox,.not)').chosen({allow_single_deselect: true});
  $("table.fixhead").floatThead({top: 50});
  // $("table.fixhead").fixedHeaderTable({ footer: true, cloneHeadToFoot: false, fixedColumn: false });
  $('.spinner').TouchSpin({
    buttondown_class: "hidden",
    buttonup_class: "hidden",
    min: -9999999999,
    max: 9999999999
  });
  Ladda.bind( 'input[type=submit]' );
  $('[data-loading-text]:not(.not)').on('click', function(){
    var btn = $(this);
        btn.button('loading');
        btn.addClass('active');

        setTimeout(function(){
          btn.button('reset')
        }, 15000);
  });

	$("#loggedBar .dropdown")
		.click(function(){
			$(this).hasClass('open') ? $(this).removeClass('open') : $(this).addClass('open');
	});

    // $(".rut").Rut({
    //     on_error: function(){ $('.rut').parent().parent().removeClass('success').addClass('error') },
    //     on_success: function(){ $('.rut').parent().parent().removeClass('error').addClass('success') }
    // });

    tableSorterInit( $("table:not(.simple, .no-filter)") );
    // $("table.no-filter").tablesorter({
    //     theme : "bootstrap",

    //     widthFixed: true,

    //     headerTemplate : '{content} {icon}', // new in v2.7. Needed to add the bootstrap icon!

    //     // widget code contained in the jquery.tablesorter.widgets.js file
    //     // use the zebra stripe widget if you plan on hiding any rows (filter widget)
    //     widgets : [ "uitheme", "zebra" ],

    //     widgetOptions : {
    //       // using the default zebra striping class name, so it actually isn't included in the theme variable above
    //       // this is ONLY needed for bootstrap theming if you are using the filter widget, because rows are hidden
    //       zebra : ["even", "odd"],

    //       // reset filters button
    //       filter_reset : ".reset"

    //       // set the uitheme widget to use the bootstrap theme class names
    //       // this is no longer required, if theme is set
    //       // ,uitheme : "bootstrap"

    // }
    // });

    // $("form :input").on("keypress", function(e) {
    //     return e.keyCode != 13;
    // });

    if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');

    $('a[data-toggle="tab"]').on('shown', function(e) {
      return location.hash = $(e.target).attr('href').substr(1);
    });

    $('.tablesorter-header').removeClass('tablesorter-header');
}




$(init);
