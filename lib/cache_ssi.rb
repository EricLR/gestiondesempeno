# -*- encoding : utf-8 -*-
class CacheSsi
  
	def self.process_includes(content)
        
		content.to_s.gsub(/<!--\s?#\s?include\s+(?:virtual|file)="[^"]+"(?:\s+stub="(\w+)")?\s+-->/) do |val|
			stub = nil
			location = val.match(/"(\S+)"/)[0].gsub('"', '')
			_info "processing include directive with location=#{location}"
			status, _, body = file_fetch location
			
			if stub && (status != 200 || body.nil? || body == "")
			  blocks[stub] 
			else
			  body.force_encoding(content.encoding)
			end
      # body = file_fetch(location)
      # logger.info "BODY: #{body.inspect}"
      # body.force_encoding( content.encoding )
		end
    
  end
    
  def self.file_fetch location
    logger.info "Intentando leer: #{location}".red
    file_name = File.join(Padrino.root, 'tmp/cache', location+".html")
    
    content = nil
    if File.exists?(file_name) 
      content = IO.read(file_name)
      content = CacheSsi.valid? content
      
      unless content.nil?
        _info "Retorno archivo: #{location}".yellow
        return [200, '', content] 
      end
    end
    
    if content.nil?
      _info "Archivo desactualizado, descargo: #{location}".yellow
      return fetch(location)
    end
    
    
  end
  def self.fetch(location)
    ##{request.host}
      return _get("#{location}")
      _error "no match found for location=#{location}"
  end
  def self.valid? content
    matchData = content.match(/^\[cache:(\d+)]\n/)

    if matchData[1].blank? #no existe tag de cache
      content = nil # No existe cache, devuelvo nil
    else
      void_at = matchData[1].to_i
      
      if void_at > Time.now.to_i # Cache valido, lo retorno
        content = content.gsub(matchData[0], "")
      else # Cache inválido, vacío todo
        content = nil 
      end
      
    end
    
    content
  end
  
  private
  
  def self._get(url)
    _info "fetching #{url}"
    headers = ENV['HTTP_COOKIE'] ? {'Cookie' => ENV['HTTP_COOKIE']} : {}
    response = HTTParty.get(url, headers: headers, verify: false)
    _error "error fetching #{url}: #{response.code} response" if response.code != 200
    [response.code, response.headers, response.body]
  end
  
  def self._info(message)
    logger.info "CacheSSI #{message}" if logger
  end

  def self._error(message)
    logger.info "CacheSSI #{message}" if logger
  end
  
  
end
