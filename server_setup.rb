# -*- encoding : utf-8 -*-
require 'rubygems'
require 'thor'

class ServerSetup < Thor
  SERVER_FILE = './start.sh'


  desc "start_script", "Generate start script"
  method_option :port, default: 3000, desc: 'Puerto que funcionará en el servidor', aliases: '-p'
  method_option :dir, desc: 'Nombre de la carpeta del proyecto', aliases: '-d'
  def start_script
script = %{#!/bin/bash
while true
do
    cd /mnt/sites/#{options[:dir]}/current && bundle exec thin start -S #{options[:port]} -e production
done
}
    File.open(SERVER_FILE, 'w'){|f| f.write(script)}
    File.chmod(0744, SERVER_FILE)
    p "Script generado"
  end

  desc "config_nginx", "Genera archivo para Nginx"
  method_option :port, default: 3000, desc: 'Puerto que funcionará en el servidor', aliases: '-p'
  method_option :dir, desc: 'Nombre de la carpeta del proyecto', aliases: '-d'
  method_option :subdomain, desc: 'Subdominio asociado para el sistema', aliases: '-s'
  
  def config_nginx
str = %{server {
  listen 80;
  server_name #{options[:subdomain]}.rvelasco.cl;
  root /mnt/sites/#{options[:dir]}/current/public;

  location  ^~ /images/ {
    log_not_found     off;
    expires           30d;
    add_header Pragma public;
    add_header Cache-Control "public, must-revalidate, proxy-revalidate";
  }
  location / {
      ssi on;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header Host $http_host;
      proxy_redirect off;
      if (-f $request_filename/index.html) {
        rewrite (.*) $1/index.html break;
      }
      if (-f $request_filename.html) {
        rewrite (.*) $1.html break;
      }
      if (!-f $request_filename) {
        proxy_pass http://s_#{options[:dir]};
        break;
      }

    }
}
upstream s_#{options[:dir]} {
  server 127.0.0.1:#{options[:port]};
}
}
    File.open("/etc/nginx/sites-enabled/#{options[:port]}-#{options[:subdomain]}", 'w'){|f| f.write(str)}
  end

end

ServerSetup.start
